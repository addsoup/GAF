package com.supermap.gaf.boot.filter;

import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.*;

public class HeaderRequestWrapper extends HttpServletRequestWrapper {
    /**
     * Constructs a request object wrapping the given request.
     *
     * @param request The request to wrap
     * @throws IllegalArgumentException if the request is null
     */
    public HeaderRequestWrapper(HttpServletRequest request) {
        super(request);
    }


    private final Map<String, List<String>> customHeaders = new CaseInsensitiveMap();
    private final Map<String,Object> removedHeaders = new  CaseInsensitiveMap();
    private final Map<String, Object> customQueryParams = new HashMap<>();

    void removeHeader(String header){
        customHeaders.remove(header);
        removedHeaders.put(header,null);
    }
    /**
     * put a header with given name and value
     * note:
     * The header name is case sensitive.
     * this is not same with  "HttpServletRequest.getHeader"
     *
     * @param name
     * @param value
     */
    void putHeader(String name, String value) {
        this.removedHeaders.remove(name);
        this.customHeaders.put(name, Arrays.asList(value));
    }

    void putHeader(String name, List<String> value) {
        this.removedHeaders.remove(name);
        this.customHeaders.put(name, value);
    }



    @Override
    public String getHeader(String name) {
        if(removedHeaders.containsKey(name)){
            return null;
        }
        // check the custom headers first
        List<String> headerValue = customHeaders.get(name);
        if (!CollectionUtils.isEmpty(headerValue)) {
            return headerValue.get(0);
        }
        return super.getHeader(name);
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        if(removedHeaders.containsKey(name)){
            return null;
        }
        // check the custom headers first
        List<String> headerValue = customHeaders.get(name);
        if (!CollectionUtils.isEmpty(headerValue)) {
            return Collections.enumeration(headerValue);
        }
        return super.getHeaders(name);
    }

    /**
     * get the Header names
     */
    @Override
    public Enumeration<String> getHeaderNames() {
        Map<String,Object> set = new CaseInsensitiveMap(customHeaders);
        Enumeration<String> headerEnums = super.getHeaderNames();
        if (headerEnums != null) {
            while (headerEnums.hasMoreElements()) {
                String headerName = headerEnums.nextElement();
                if(!set.containsKey(headerName)){
                    set.put(headerName,null);
                }
            }
        }
        removedHeaders.keySet().forEach(item->set.remove(item));
        return Collections.enumeration(set.keySet());
    }

    void setQueryParam(String key,Object value){
        customQueryParams.put(key,value);
    }

    @Override
    public String getQueryString() {
        String queryString = super.getQueryString();
        UriBuilder uriBuilder = UriBuilder.fromUri("http://xx?"+(queryString==null?"":queryString));
        for(String key:customQueryParams.keySet()){
            uriBuilder.replaceQueryParam(key,customQueryParams.get(key));
        }
        return uriBuilder.build().getQuery();
    }

    public static void main(String[] args) {
        URI uriBuilder = UriBuilder.fromUri("http://xx?").queryParam("fields","xxxx").build();
        String query = uriBuilder.getQuery();
        System.out.println(query);
    }
}
