package com.supermap.gaf.boot.util;

import lombok.SneakyThrows;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

/**
 * @author kb
 */
public class BootCommonUtils {

    @SneakyThrows
    public static URI getURI(HttpServletRequest request){
        StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
        String queryString = request.getQueryString();
        if (queryString != null) {
            requestURL.append('?').append(queryString).toString();
        }
        return new URI(requestURL.toString());
    }

}
