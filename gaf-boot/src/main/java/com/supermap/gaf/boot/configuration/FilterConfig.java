package com.supermap.gaf.boot.configuration;

import com.supermap.gaf.boot.filter.*;
import com.supermap.gaf.boot.servlet.RouteProxyServlet;
import com.supermap.gaf.extend.spi.ValidateAuthentication;
import com.supermap.gaf.extend.spi.ValidateAuthority;
import com.supermap.gaf.gateway.GatewayFilterDefinition;
import com.supermap.gaf.gateway.GatewayPredicateDefinition;
import com.supermap.gaf.gateway.GatewayRouteDefinition;
import com.supermap.gaf.gateway.commontypes.properties.GatewaySecurityProperties;
import com.supermap.gaf.webgis.client.ServiceShareSettingClient;
import com.supermap.gaf.webgis.client.WebgisServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.*;
import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.GATEWAY_AUTHENTICATION_QUERY_FILTER_ORDER;

@Configuration
@EnableConfigurationProperties({GatewaySecurityProperties.class})
public class FilterConfig {
    private static final String[] PROXY_URL_PATTERNS = {"/storage/*","/service-proxy/*"};

    @Value("${GAF_STORAGE_URL:http://gaf-storage:8080/storage}")
    private String storageUrl;
    @Bean
    public FilterRegistrationBean<SecurityHeaderFilter> securityHeaderFilterRegistrationBean() {
        FilterRegistrationBean<SecurityHeaderFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new SecurityHeaderFilter());
        bean.addUrlPatterns("/*");
        bean.setOrder(Integer.MIN_VALUE);
        bean.setName("securityHeaderFilter");
        return bean;
    }

    @Bean
    public FilterRegistrationBean<XwebRedirectFilter> xWebRedirectFilterRegistrationBean(@Autowired GatewaySecurityProperties gatewaySecurityProperties) {
        FilterRegistrationBean<XwebRedirectFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new XwebRedirectFilter(gatewaySecurityProperties));
        bean.addUrlPatterns("/*");
        bean.setOrder(Integer.MIN_VALUE + 1);
        bean.setName("xWebRedirectFilter");
        return bean;
    }


    @Bean
    public FilterRegistrationBean<XgatewayAuthenticationQueryFilter> xGatewayAuthenticationQueryFilterRegistrationBean(@Autowired ValidateAuthentication validateAuthentication) {
        FilterRegistrationBean<XgatewayAuthenticationQueryFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new XgatewayAuthenticationQueryFilter(validateAuthentication));
        bean.addUrlPatterns("/*");
        bean.setOrder(100);
        bean.setName("xGatewayAuthenticationQueryFilter");
        return bean;
    }


    @Bean
    public FilterRegistrationBean<XgatewayAuthenticationValidateFilter> xGatewayAuthenticationValidateFilterRegistrationBean() {
        FilterRegistrationBean<XgatewayAuthenticationValidateFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new XgatewayAuthenticationValidateFilter());
        bean.addUrlPatterns("/*");
        bean.setOrder(150);
        bean.setName("xGatewayAuthenticationValidateFilter");
        return bean;
    }

    @Bean
    public FilterRegistrationBean<XgatewayAuthorizationValidateFilter> xGatewayAuthorizationValidateFilterRegistrationBean(@Autowired ValidateAuthority validateAuthority) {
        FilterRegistrationBean<XgatewayAuthorizationValidateFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new XgatewayAuthorizationValidateFilter(validateAuthority));
        bean.addUrlPatterns("/*");
        bean.setOrder(175);
        bean.setName("xGatewayAuthorizationValidateFilter");
        return bean;
    }


    @Bean
    public FilterRegistrationBean<XgatewayRequestTokenFilter> xGatewayRequestTokenFilterRegistrationBean() {
        FilterRegistrationBean<XgatewayRequestTokenFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new XgatewayRequestTokenFilter());
        bean.addUrlPatterns("/*");
        bean.setOrder(200);
        bean.setName("xGatewayRequestTokenFilter");
        return bean;
    }


    @Bean
    public FilterRegistrationBean<XgatewayIserverFilter> xgatewayIserverFilterRegistrationBean(@Autowired WebgisServiceClient webgisServiceClient, @Autowired ServiceShareSettingClient serviceShareSettingClient) {
        FilterRegistrationBean<XgatewayIserverFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new XgatewayIserverFilter(webgisServiceClient, serviceShareSettingClient));
        bean.addUrlPatterns("/*");
        bean.setOrder(300);
        bean.setName("XgatewayIserverFilter");
        return bean;
    }

    @Bean
    public FilterRegistrationBean<ProxyRouteFilter> proxyRouteFilterRegistrationBean() {
        GatewayRouteDefinition routeDefinition = new GatewayRouteDefinition();
        routeDefinition.setUri(storageUrl);
        GatewayFilterDefinition filterDefinition = new GatewayFilterDefinition();
        filterDefinition.setName("StripPrefix");
        filterDefinition.setArgs("0");
        GatewayPredicateDefinition predicateDefinition = new GatewayPredicateDefinition();
        predicateDefinition.setName("Path");
        predicateDefinition.setArgs("/storage/**");
        routeDefinition.setFilters(Arrays.asList(filterDefinition));
        routeDefinition.setPredicates(Arrays.asList(predicateDefinition));
        FilterRegistrationBean<ProxyRouteFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new ProxyRouteFilter(Arrays.asList(routeDefinition)));
        bean.addUrlPatterns(PROXY_URL_PATTERNS);
        bean.setOrder(GATEWAY_AUTHENTICATION_QUERY_FILTER_ORDER-1);
        bean.setName("ProxyRouteFilter");
        return bean;
    }

    @Bean
    public FilterRegistrationBean<XgatewayCacheRequestBodyFilter> xgatewayCacheRequestBodyFilterRegistrationBean() {
        FilterRegistrationBean<XgatewayCacheRequestBodyFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new XgatewayCacheRequestBodyFilter());
        bean.addUrlPatterns("/*");
        bean.setOrder(160);
        bean.setName("XgatewayCacheRequestBodyFilter");
        return bean;
    }

    @Bean
    public ServletRegistrationBean storageProxyServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean(new RouteProxyServlet(), PROXY_URL_PATTERNS);
        bean.addInitParameter(RouteProxyServlet.P_LOG, "true");
        bean.addInitParameter(RouteProxyServlet.P_HANDLEREDIRECTS, "false");
        bean.setLoadOnStartup(1);
        return bean;
    }



}
