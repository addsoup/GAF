package com.supermap.gaf.extend.spi;

import com.supermap.gaf.extend.commontypes.AuthorizationParam;

/**
 * 校验当前用户是否有uri权限
 */
public interface ValidateAuthority {
    /**
     * 校验当前用户是否有uri权限
     * @param authorizationParam 鉴权所需的参数 包括 username(用户名) uri  method(GET PUT POST DELETE)
     * @return true,表示有权限,false,表示没有权限
     */
    Boolean hasPermission(AuthorizationParam authorizationParam);
}
