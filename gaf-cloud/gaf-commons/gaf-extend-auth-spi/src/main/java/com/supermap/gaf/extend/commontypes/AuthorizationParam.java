/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.extend.commontypes;

import java.io.Serializable;

/**
 * @author : duke
 * @date:2021/3/25
 * @since 2020/11/23 11:01 AM
 */
public class AuthorizationParam implements Serializable {
    private static final long serialVersionUID = -1L;

    /**
     * 用户名
     */
    private String username;
    /**
     * uri路径
     */
    private String uri;
    /**
     * GET POST PUT DELETE
     * @see org.springframework.http.HttpMethod
     */
    private String method;


    public AuthorizationParam() {
    }

    public AuthorizationParam(String username, String uri, String method) {
        this.username = username;
        this.uri = uri;
        this.method = method;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
