package com.supermap.gaf.security;

import com.supermap.gaf.extend.commontypes.User;
import com.supermap.gaf.extend.spi.UserService;
import com.supermap.gaf.security.spring.ApplicationContextHolder;

/**
 *
 *
 * @author wxl
 * @since 2022/4/11
 */
public class SecurityUtilsExt {

    private static UserService userService;

    static {
        userService = ApplicationContextHolder.getBean(UserService.class);
    }

    private SecurityUtilsExt() {
    }

    public static String getUserId() {
        return userService.getUserId();
    }

    public static  String getUserName() {
        return userService.getUserName();
    }

    public static User getUser() {
        return userService.getUser();
    }

    public static String getTenantId() {
        return userService.getTenantId();
    }

    public static String getToken() {
        return userService.getToken();
    }

}
