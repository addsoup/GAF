package com.supermap.gaf.sqlhelper;

import io.github.heykb.sqlhelper.handler.InjectColumnInfoHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

@Component
public class CreatedTimeInjectColumnHandler implements InjectColumnInfoHandler {
    private static final AntPathMatcher antPathMatcher = new AntPathMatcher(".");
    @Override
    public String getColumnName() {
        return "created_time";
    }

    @Override
    public String getValue() {
        return "now()";
    }

    @Override
    public int getInjectTypes() {
        return INSERT;
    }

    // 排除tmp表
    @Override
    public boolean checkTableName(String tableName) {
        return !"tmp".equals(tableName);
    }

    @Override
    public boolean checkMapperId(String mapperId) {
        return !antPathMatcher.match("com.supermap.gaf.api.scanner.dao.SwaggerApiDocMapper.*",mapperId)
                && antPathMatcher.match("com.supermap.gaf.**",mapperId);
    }
}
