package com.supermap.gaf.security.config;

import com.supermap.gaf.security.registry.UserServiceSpiRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author wxl
 * @since 2022/4/9
 */
@Configuration
public class UserServiceSpiConfigration {

    @Bean
    public UserServiceSpiRegistry userServiceSpiRegistry() {
        return new UserServiceSpiRegistry();
    }
}
