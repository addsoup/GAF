package com.supermap.gaf.authority.service.impl;

import com.supermap.gaf.authority.commontype.IauthUserName;
import com.supermap.gaf.security.RequestUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Service;

/**
 * @author wxl
 * @since 2022/3/18
 */
@Service
public class AuthUserNameImpl implements IauthUserName {
    @Override
    public String getAuthUserName(Object o) {
        return RequestUtil.getUserName();
    }
}
