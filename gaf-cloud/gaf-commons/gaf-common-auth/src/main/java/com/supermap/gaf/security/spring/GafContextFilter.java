package com.supermap.gaf.security.spring;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author : duke
 * @since 2022/3/23 6:18 PM
 */
@Component
public class GafContextFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        try {
            filterChain.doFilter(servletRequest,servletResponse);
        }finally {
            GafContextHolder.remove();
        }
    }

    @Override
    public void destroy() {

    }
}
