package com.supermap.gaf.sqlhelper;

import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import io.github.heykb.sqlhelper.handler.InjectColumnInfoHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

@Component
public class UpdateByInjectColumnHandler implements InjectColumnInfoHandler {
    private static final Logger log = LoggerFactory.getLogger(UpdateByInjectColumnHandler.class);

    private static final AntPathMatcher antPathMatcher = new AntPathMatcher(".");
    @Override
    public String getColumnName() {
        return "updated_by";
    }

    @Override
    public String getValue() {
        String username = SecurityUtilsExt.getUserName();
        return "'"+username+"'";
    }

    @Override
    public int getInjectTypes() {
        return UPDATE|INSERT;
    }

    // 排除user_info表
    @Override
    public boolean checkTableName(String tableName) {
        boolean re = !"auth_user".equals(tableName);
        if(re){
            String username = SecurityUtilsExt.getUserName();
            if(username == null){
                log.error("sqlhelper 无法获取用户名,跳过拦截");
                return false;
            }
        }
        return re;
    }

    @Override
    public boolean checkMapperId(String mapperId) {
        return !antPathMatcher.match("com.supermap.gaf.data.access.dao.*.*",mapperId)
                && !antPathMatcher.match("com.supermap.gaf.data.mgt.mapper.CacheBuildHistoryMapper.*",mapperId)
                && !antPathMatcher.match("com.supermap.gaf.api.scanner.dao.*.*",mapperId)
                && !antPathMatcher.match("com.supermap.gaf.portal.dao.*.*",mapperId)
                && !antPathMatcher.match("com.supermap.gaf.platform.dao.TenantResourceMapper.insert",mapperId)
                && !antPathMatcher.match("com.supermap.gaf.platform.dao.TenantMapper.updateSelective",mapperId)
                && !antPathMatcher.match("com.supermap.gaf.platform.dao.TenantUserMapper.*",mapperId)
                && !antPathMatcher.match("com.supermap.gaf.portal.dao.UserMessageMapper.insert",mapperId)
                && antPathMatcher.match("com.supermap.gaf.**",mapperId);
    }
}
