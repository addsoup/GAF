package com.supermap.gaf.security.registry;

import com.supermap.gaf.extend.spi.UserAuthoritiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

/**
 *
 * @author wxl
 * @since 2022/4/9
 */
public class UserAuthoritiesServiceSpiRegistry implements BeanDefinitionRegistryPostProcessor, EnvironmentAware {

    private static final Logger log = LoggerFactory.getLogger(UserAuthoritiesServiceSpiRegistry.class);

    String userAuthoritiesServiceProviderUrl;
    String userAuthoritiesServiceProviderClassName;

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        serviceLoadAndRegistry(registry, UserAuthoritiesService.class, userAuthoritiesServiceProviderClassName, userAuthoritiesServiceProviderUrl);
    }

    private <S> void  serviceLoadAndRegistry(BeanDefinitionRegistry registry,Class<S> spiInterface,String providerClassName,String providerUrl) {
        if (StringUtils.isEmpty(providerClassName )) throw new IllegalArgumentException("providerClassName 不能为空");
        ServiceLoader<S> serviceLoader;
        if (!StringUtils.isEmpty(providerUrl)) {
            URL[] pluginUrl;
            try {
                pluginUrl = new URL[]{new URL(providerUrl)};
            } catch (MalformedURLException e) {
                log.error("providerUrl格式异常",e);
                throw new IllegalArgumentException("providerUrl格式异常",e);
            }
            URLClassLoader urlClassLoader = new URLClassLoader(pluginUrl, Thread.currentThread().getContextClassLoader());
            serviceLoader = ServiceLoader.load(spiInterface,urlClassLoader);
        } else {
            serviceLoader = ServiceLoader.load(spiInterface);
        }
        Map<String,Object> map = new HashMap<>();
        for (S s : serviceLoader) {
            map.putIfAbsent(s.getClass().getName(),s);
        }
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(map.get(providerClassName).getClass());
        registry.registerBeanDefinition(providerClassName,beanDefinitionBuilder.getBeanDefinition());
        log.info("BeanDefinition:{}的注册成功",providerClassName);
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }

    @Override
    public void setEnvironment(Environment environment) {
        this.userAuthoritiesServiceProviderUrl = environment.getProperty("spi-config.user-authorities-service.provider-url");
        String authenticationProviderClassName = environment.getProperty("spi-config.user-authorities-service.provider-class-name");
        this.userAuthoritiesServiceProviderClassName = !StringUtils.isEmpty(authenticationProviderClassName) ? authenticationProviderClassName: "com.supermap.gaf.extend.spi.provider.DefaultUserAuthoritiesServiceProvider";
    }
}
