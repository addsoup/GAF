package com.supermap.gaf.extend.commontypes;

/**
 * 角色
 * 字段注释,必要表示必须提供,可选表示可以不提供
 * @author wxl
 * @since 2022/4/11
 */
public class Role {

    /**
     * 角色id
     * 必要
     */
    private String roleId;

    /**
     * 角色英文名
     * 必要
     * 例如：admin
     */
    private String roleNameEn;

    /**
     * 角色别名
     * 必要
     * 例如：管理员
     */
    private String roleAlias;

    /**
     * 用于反射调用
     */
    protected Role() {
    }

    public Role(String roleId, String roleNameEn, String roleAlias) {
        this.roleId = roleId;
        this.roleNameEn = roleNameEn;
        this.roleAlias = roleAlias;
    }


    public String getRoleId() {
        return roleId;
    }
    /**
     * 设置角色id
     * 必须提供
     * @return
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }


    public String getRoleAlias() {
        return roleAlias;
    }

    /**
     * 设置角色别名
     * 必须提供
     * @return
     */
    public void setRoleAlias(String roleAlias) {
        this.roleAlias = roleAlias;
    }


    public String getRoleNameEn() {
        return roleNameEn;
    }

    /**
     * 设置角色英文名
     * 必须提供
     * @return
     */
    public void setRoleNameEn(String roleNameEn) {
        this.roleNameEn = roleNameEn;
    }
}
