package com.supermap.gaf.utils;

import com.supermap.data.*;
import com.supermap.gaf.data.mgt.entity.DatasetChangeChain;
import com.supermap.gaf.data.mgt.entity.FieldMapping;
import com.supermap.gaf.data.mgt.entity.changeop.dataset.ChangeName;
import com.supermap.gaf.data.mgt.entity.changeop.dataset.CreateFromTemplate;
import com.supermap.gaf.data.mgt.entity.changeop.dataset.Delete;
import com.supermap.gaf.data.mgt.entity.changeop.dataset.ImportFromOtherDataset;
import com.supermap.gaf.data.mgt.entity.iserver.BaseResponse;
import com.supermap.gaf.data.mgt.entity.iserver.PublishParam;
import com.supermap.gaf.data.mgt.entity.iserver.PublishResult;
import com.supermap.gaf.data.mgt.entity.iserver.PublishTileParam;
import com.supermap.gaf.data.mgt.util.IobjectUtils;
import com.supermap.gaf.data.mgt.util.IserverUtils;
import com.supermap.services.rest.management.ServiceType;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class IobjectUtilsTests {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    @Test
    public void test() {
        DatasourceConnectionInfo connectionInfo = new DatasourceConnectionInfo();
        connectionInfo.setServer("192.168.192.232:32023");
        connectionInfo.setAlias("chongqing22");
        connectionInfo.setDatabase("TDLYXZ_140121_2018");
        connectionInfo.setUser("admin");
        connectionInfo.setPassword("123456");
        connectionInfo.setEngineType(EngineType.POSTGRESQL);

        DatasourceConnectionInfo targetConn = new DatasourceConnectionInfo();
        targetConn.setServer("192.168.11.118:32224");
        targetConn.setAlias("target");
        targetConn.setDatabase("space555");
        targetConn.setUser("admin");
        targetConn.setPassword("123456");
        targetConn.setEngineType(EngineType.POSTGRESQL);

        String destDatasetName = "NewRegion";
        boolean re = (boolean) IobjectUtils.datasourceProcessor(connectionInfo, datasource -> {
            DomainManager domainManager = datasource.getDomainManager();
            return IobjectUtils.datasetProcessor(datasource,destDatasetName,dataset -> {
                DatasetVector datasetVector = (DatasetVector) dataset;
                CodeDomain domain = new CodeDomain(1,"FDFDF","FDFDF",FieldType.TEXT);
                CodeInfo codeInfo = new CodeInfo(FieldType.TEXT,"56","56");
                domain.add(codeInfo);
//                domain.append(new CodeInfo[]{codeInfo});

//                RangeDomain domain = new RangeDomain(1,"xx","xx",FieldType.CHAR);
//                RangeInfo rangeInfo = new RangeInfo(FieldType.CHAR,"15","56",RangeType.CLOSE_CLOSE);
////                RangeInfo rangeInfo2 = new RangeInfo(FieldType.CHAR,1,10,RangeType.CLOSE_CLOSE);
//                domain.append(new RangeInfo[]{rangeInfo});

//                domain.check(15);
                Domain domain1 = domainManager.getDomain(datasetVector,"test");

                return true;
//                boolean fg = domainManager.setDomain(datasetVector,"test",domain);
//                return domainManager.setDomain(datasetVector,"test2",domain);
            });
        });

        Assert.assertTrue(re);

    }

    @Test
    public void test2(){
        RangeDomain domain = new RangeDomain(1,"xx","xx",FieldType.INT64);
        RangeInfo rangeInfo = new RangeInfo(FieldType.INT64,15,56,RangeType.CLOSE_CLOSE);
        RangeInfo rangeInfo2 = new RangeInfo(FieldType.INT64,1,10,RangeType.CLOSE_CLOSE);
        domain.append(new RangeInfo[]{rangeInfo,rangeInfo2});

        Assert.assertTrue(domain.check(60));
    }

}
