package com.supermap.gaf.commontypes;

/**
 * @author : duke
 * @since 2021/3/12 1:50 PM
 */
public class GafCommonConstant {
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    public static final String SPRING_APPLICATION_NAME = "${spring.application.name}";


    public static final String ANONYMOUS_ACCESS = "anonymous_access";

    public static final String SYSTEM_TENANT = "system";
    public static final String TENANT_HEADER = "tenant";

    public static final String MICRO_FRONTEND_CONTAINER = "#frame";
}
