package com.supermap.gaf.authority.vo;

import com.supermap.gaf.authority.commontype.AuthUserSupplement;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 用户信息补充表 条件查询实体
 * @author zrc
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户信息补充表 条件查询实体")
public class AuthUserSupplementSelectVo {
    private static final long serialVersionUID = -1L;
    @QueryParam("searchFieldName")
    @ApiModelProperty("查询字段名")
    @StringRange(entityClass = AuthUserSupplement.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = AuthUserSupplement.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod;
    @QueryParam("userId")
    @ApiModelProperty("主键")
    private String userId;
    @QueryParam("userName")
    @ApiModelProperty("用户名")
    private String userName;
    @QueryParam("departmentId")
    @ApiModelProperty("部门id")
    private String departmentId;
    @QueryParam("sortSn")
    @ApiModelProperty("排序字段")
    private Integer sortSn;
    @QueryParam("postId")
    @ApiModelProperty("岗位id")
    private String postId;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("更新人")
    private String updatedBy;
}
