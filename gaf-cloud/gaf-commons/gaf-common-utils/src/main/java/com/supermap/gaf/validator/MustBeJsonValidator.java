package com.supermap.gaf.validator;

import com.supermap.gaf.utils.GlobalJacksonObjectMapper;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.IOException;

public class MustBeJsonValidator implements ConstraintValidator<MustBeJson,String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(value!=null){
            try {
                GlobalJacksonObjectMapper.instance().readTree(value);
                return true;
            } catch (IOException e) {
                context.unwrap(HibernateConstraintValidatorContext.class)
                        .addMessageParameter("com.supermap.gaf.validator.MustBeJson.message","必须是json格式字符串");
                return false;
            }
        }
        return true;
    }
}
