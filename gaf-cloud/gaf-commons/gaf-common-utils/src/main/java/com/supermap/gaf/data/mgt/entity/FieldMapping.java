package com.supermap.gaf.data.mgt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@Data
@NoArgsConstructor
public  class FieldMapping{
    private String srcName;
    private String destName;
}
