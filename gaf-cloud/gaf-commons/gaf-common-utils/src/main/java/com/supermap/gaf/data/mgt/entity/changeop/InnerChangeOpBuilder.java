package com.supermap.gaf.data.mgt.entity.changeop;

/**
 * The type Inner change op builder.
 *
 * @param <T>     the type parameter
 * @param <Outer> the type parameter
 */
public abstract class InnerChangeOpBuilder<T extends ChangeOp,Outer extends ChangeOpChain>{
    /**
     * The Parent chain.
     */
    protected Outer parentChain;

    /**
     * Parent chain inner change op builder.
     *
     * @param parentChain the parent chain
     * @return the inner change op builder
     */
    protected InnerChangeOpBuilder parentChain(Outer parentChain) {
        this.parentChain = parentChain;
        return this;
    }

    /**
     * Build t.
     *
     * @return the t
     */
    public abstract  T build();

}
