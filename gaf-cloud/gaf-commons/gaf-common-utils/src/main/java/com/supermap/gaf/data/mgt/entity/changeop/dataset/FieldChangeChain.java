package com.supermap.gaf.data.mgt.entity.changeop.dataset;

import com.supermap.data.Dataset;
import com.supermap.data.Datasource;
import com.supermap.gaf.data.mgt.entity.DatasetChangeChain;
import com.supermap.gaf.data.mgt.entity.changeop.ChangeOp;
import com.supermap.gaf.data.mgt.entity.changeop.ChangeOpChain;
import com.supermap.gaf.data.mgt.entity.changeop.InnerChangeOpBuilder;
import com.supermap.gaf.data.mgt.util.IobjectUtils;
import org.apache.http.util.Asserts;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Field change chain.
 */
public class FieldChangeChain extends DatasetChangeChain.DatasetChangeOpChain {

    private String fieldName;

    private FieldChangeChain(DatasetChangeChain outer, String fieldName){
        outer.super();
        this.fieldName = fieldName;
    }

    /**
     * The type Field change op.
     */
    public abstract class FieldChangeOp implements ChangeOp {
        /**
         * Get field name string.
         *
         * @return the string
         */
        protected String getFieldName(){
            return fieldName;
        }

        /**
         * Set field name.
         *
         * @param newName the new name
         */
        protected void setFieldName(String newName){
            fieldName = newName;
        }

        /**
         * Update.
         *
         * @param dataset the dataset
         */
        public abstract void update(Dataset dataset);

        @Override
        public void update(Datasource datasource) {
            Dataset dataset = datasource.getDatasets().get(getDatasetName());
            try{
                dataset.open();
                update(datasource.getDatasets().get(getDatasetName()));

            }finally {
                if(dataset.isOpen()){
                    dataset.close();
                }
            }
        }
    }


    /**
     * Builder builder.
     *
     * @return the builder
     */
    public static Builder builder(){
        return new Builder();
    }

    /**
     * The type Builder.
     */
    public static class Builder extends InnerChangeOpBuilder<FieldChangeChain,DatasetChangeChain> {
        private String fieldName;
        private List<Object> ops = new ArrayList<>();

        /**
         * Field name builder.
         *
         * @param fieldName the field name
         * @return the builder
         */
        public Builder fieldName(String fieldName){
            this.fieldName = fieldName;
            return this;
        }

        /**
         * Add op builder.
         *
         * @param op the op
         * @return the builder
         */
        public Builder addOp(ChangeOp op){
            ops.add(op);
            return this;
        }

        /**
         * Add op builder.
         *
         * @param builder the builder
         * @return the builder
         */
        public Builder addOp(InnerChangeOpBuilder builder){
            ops.add(builder);
            return this;
        }

        @Override
        public FieldChangeChain build(){
            Asserts.notNull(fieldName,"Builder.fieldName(xx) required");
            FieldChangeChain re = new FieldChangeChain(parentChain, fieldName);
            for(Object item:ops){
                if(item instanceof ChangeOp){
                    re.addOp((ChangeOp) item);
                }
                if(item instanceof InnerChangeOpBuilder){
                    re.addOp((InnerChangeOpBuilder) item);
                }
            }
            return re;
        }
    }
}
