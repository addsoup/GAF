package com.supermap.gaf.data.mgt.entity.iserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddProviderResult{
    private String name;
    
}
