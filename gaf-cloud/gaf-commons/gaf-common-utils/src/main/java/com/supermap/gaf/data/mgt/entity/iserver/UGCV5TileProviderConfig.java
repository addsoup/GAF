package com.supermap.gaf.data.mgt.entity.iserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.annotation.Nullable;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class UGCV5TileProviderConfig {
    public String configFile;
    private boolean cacheEnabled = false;
    @Nullable
    private String name;
}
