package com.supermap.gaf.data.mgt.entity.iserver;

import lombok.Data;

import javax.annotation.Nullable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class MongoDBMVTTileProviderConfig {
    @NotEmpty
    public String[] serverAdresses;
    public String mapName;
    public String[] tilesetNames;
    @NotNull
    public String database;
    @NotNull
    public String username;
    @NotNull
    public String password;
    private boolean cacheEnabled = false;
    private String name;
}
