package com.supermap.gaf.authority.commontype;

/**
 * @author : duke
 * @since 2021/5/19 9:09 AM
 */
public interface IauthUserName {
    String getAuthUserName(Object o);
}