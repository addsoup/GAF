package com.supermap.gaf.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 该注解用于校验字段名是否在指定范围内
 * range和entityClass至少一个不为空
 * 都不为空则范围求并集
 *
 * @author wxl, zrc
 * @since 2021 /6/18
 */
@Documented
@Constraint(validatedBy = {StringRangeValidator.class})
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
public @interface StringRange {
    /**
     * Message string.
     *
     * @return the string
     */
    String message() default "{com.supermap.gaf.validator.StringRange.message}";

    /**
     * Groups class [ ].
     *
     * @return the class [ ]
     */
    Class<?>[] groups() default {};

    /**
     * Payload class [ ].
     *
     * @return the class [ ]
     */
    Class<? extends Payload>[] payload() default {};

    /**
     * 指定集合
     *
     * @return string [ ]
     */
    String[] value() default {};

    /**
     * 允许的值是实体类所有字段名的集合.关联isUnderscoreToCamelCase属性
     *
     * @return class
     */
    Class<?> entityClass() default void.class;

    /**
     * 驼峰转下换线
     * @return
     */
    boolean isCamelCaseToUnderscore() default true;

    /**
     * 允许的值是enum每项运行指定getter或者name()【默认】的集合。关联enumGetter属性
     *
     * @return class
     */
    Class<? extends Enum> fromEnum() default NoThing.class;


    /**
     * 为fromEnum指定属性名 然后会使用属性的getter方法作为校验值。不指定使用name
     *
     * @return the string
     */
    String enumGetter() default "name";

    /**
     * Ignore case boolean.
     *
     * @return the boolean
     */
    boolean ignoreCase() default false;

    /**
     * The enum No thing.
     */
    enum NoThing{

    }



}
