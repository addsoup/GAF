package com.supermap.gaf.data.mgt.entity.iserver;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.annotation.Nullable;
import java.util.Map;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ComponentParam {
    private String interfaceNames;
    private String type;
    private String providers;
    @Nullable
    private Map<String,Object> config;
    @Nullable
    private String name;

}
