package com.supermap.gaf.data.mgt.entity.changeop.dataset;

import com.supermap.data.Datasource;
import com.supermap.gaf.data.mgt.entity.DatasetChangeChain;
import com.supermap.gaf.data.mgt.entity.changeop.InnerChangeOpBuilder;
import org.apache.http.util.Asserts;

/**
 * The type Change name.
 */
public class ChangeName extends DatasetChangeChain.DatasetChangeOp {

    private String newName;

    private ChangeName(DatasetChangeChain outer, String newName){
        outer.super();
        this.newName = newName;
    }
    @Override
    public void update(Datasource datasource) {
        String datasetName = getDatasetName();
        if(datasetName.equals(newName)){
            return;
        }
        datasource.getDatasets().rename(datasetName,newName);
        setDatasetName(newName);
    }

    /**
     * Builder builder.
     *
     * @return the builder
     */
    public static Builder builder(){
        return new Builder();
    }

    /**
     * The type Builder.
     */
    public static class Builder extends InnerChangeOpBuilder<ChangeName,DatasetChangeChain> {
        private String newName;

        /**
         * New name builder.
         *
         * @param newName the new name
         * @return the builder
         */
        public Builder newName(String newName){
            this.newName = newName;
            return this;
        }

        @Override
        public ChangeName build(){
            Asserts.notNull(newName,"Builder.newName(xx) required");
            return new ChangeName(parentChain, newName);
        }
    }
}
