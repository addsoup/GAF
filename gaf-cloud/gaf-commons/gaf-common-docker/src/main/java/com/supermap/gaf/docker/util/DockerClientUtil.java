package com.supermap.gaf.docker.util;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.SaveImageCmd;
import com.github.dockerjava.api.model.AuthConfig;
import com.github.dockerjava.api.model.BuildResponseItem;
import com.github.dockerjava.api.model.PushResponseItem;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.command.BuildImageResultCallback;
import com.github.dockerjava.core.command.PushImageResultCallback;
import com.supermap.gaf.exception.GafException;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author dqc
 * @date 2022/4/28
 */
public class DockerClientUtil {
    /**
     * docker地址
     */
    private String dockerAddress = "tcp://gaf-docker:2375/";
    /**
     * docker连接
     */
    private DockerClient dockerClient;

    private static class DockerClientUtilHolder{
        private static DockerClientUtil instance = new DockerClientUtil();
    }

    public DockerClientUtil() {
        this.dockerClient = connectDocker();
    }

    /**
     * 暂不使用单例模式
     * @return
     */
    public static DockerClientUtil getInstance() {
        return DockerClientUtilHolder.instance;
    }

    /**
     * 构建镜像并保存镜像文件
     * @return
     */
    public String buildAndSave(String dockerFilePath, String tagFullName){
        File dockerFile = new File(dockerFilePath);
        String imageId = buildImage(dockerFile, tagFullName);
        if (StringUtils.isEmpty(imageId)){
            throw new GafException("镜像构建失败");
        }
        String[] parts = tagFullName.split(":");
        String imageFullName = parts[0];
        String image = imageFullName.substring(imageFullName.lastIndexOf("/") + 1);
        String tag = parts[1];
        SaveImageCmd saveImage = dockerClient.saveImageCmd(imageFullName).withTag(tag);
        File targetFile = new File(dockerFile.getParentFile(), image + ".tar");
        try (InputStream input = saveImage.exec()){
            write(targetFile, input);
        }catch (Exception e){
            e.printStackTrace();
            throw new GafException("镜像写入失败");
        }
        return targetFile.getPath();
    }
    /**
     * 构建镜像并推送镜像
     * @return
     */
    public boolean buildAndPush(String dockerFilePath, String tagFullName, String registryAddress, String registryUsername, String registryPassword){

        String imageId = buildImage(new File(dockerFilePath), tagFullName);
        if (StringUtils.isEmpty(imageId)){
            throw new GafException("镜像构建失败");
        }
        AuthConfig authConfig = new AuthConfig()
                .withUsername(registryUsername)
                .withPassword(registryPassword)
                .withRegistryAddress(registryAddress);
        dockerClient.pushImageCmd(tagFullName).withAuthConfig(authConfig).exec(push).awaitSuccess();
        return true;
    }

    /**
     * 构建镜像
     * @return
     */
    private String buildImage(File dockerFile, String tagName){
        return dockerClient.buildImageCmd(dockerFile).withNoCache(true).withBaseDirectory(dockerFile.getParentFile()).withTag(tagName).exec(callback).awaitImageId();
    }

    /**
     * 构建镜像callback
     */
    private BuildImageResultCallback callback = new BuildImageResultCallback() {
        @Override
        public void onNext(BuildResponseItem item) {
            System.out.println("" + item);
            super.onNext(item);
        }
    };
    /**
     * 推送镜像callbck
     */
    private static PushImageResultCallback push = new PushImageResultCallback() {
        @Override
        public void onNext(PushResponseItem item) {
            System.out.println("id:" + item.getId()  +" status: "+item.getStatus());
            super.onNext(item);
        }
        @Override
        public void onComplete() {
            System.out.println("Image pushed completed!");
            super.onComplete();
        }
    };


    /**
     * 链接客户端
     * @return
     */
    public DockerClient connectDocker(){
        return DockerClientBuilder.getInstance(dockerAddress).build();
    }

    /**
     * 写入文件
     */
    private void write(File destinationFile, InputStream input)  {
        try (FileOutputStream downloadFile = new FileOutputStream(destinationFile)){
            int index;
            byte[] bytes = new byte[1024];
            while ((index = input.read(bytes)) != -1) {
                downloadFile.write(bytes, 0, index);
                downloadFile.flush();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
