package com.supermap.gaf.extend.spi.provider;

import com.supermap.gaf.authority.client.AuthorizationClient;
import com.supermap.gaf.extend.commontypes.AuthorizationParam;
import com.supermap.gaf.extend.spi.ValidateAuthority;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author wxl
 * @since 2022/4/7
 */
public class DefaultValidateAuthorityProvider implements ValidateAuthority {

    @Autowired
    private AuthorizationClient authorizationClient;

    @Override
    public Boolean hasPermission(AuthorizationParam authorizationParam) {
        com.supermap.gaf.authority.entity.AuthorizationParam param = new com.supermap.gaf.authority.entity.AuthorizationParam();
        BeanUtils.copyProperties(authorizationParam,param);
        return authorizationClient.authorization(param).checkAndGetData();
    }
}
