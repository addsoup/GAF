package com.supermap.gaf.extend.spi.provider;

import com.supermap.gaf.extend.commontypes.User;
import com.supermap.gaf.extend.spi.UserService;

/**
 * @author wxl
 * @since 2022/4/7
 */
public class SimpleUserServiceProvider implements UserService {


    @Override
    public String getUserName() {
        System.out.println("call SimpleUserAuthoritiesServiceProvider.getUserName");
        return "sys_admin";
    }

    @Override
    public String getTenantId() {
        System.out.println("call SimpleUserAuthoritiesServiceProvider.getTenantId");
        return "system";
    }

    @Override
    public String getUserId() {
        System.out.println("call SimpleUserAuthoritiesServiceProvider.getUserId");
        return "user_000000";
    }

    @Override
    public User getUser() {
        System.out.println("call SimpleUserAuthoritiesServiceProvider.getUser");
        return new User(getUserId(),getUserName(),getUserName());
    }


    @Override
    public String getToken() {
        System.out.println("call SimpleUserAuthoritiesServiceProvider.getToken");
        return "user_000000 sys_admin 系统管理员 system";
    }
}
