package com.supermap.gaf.extend.spi.provider;

import com.supermap.gaf.extend.commontypes.Role;
import com.supermap.gaf.extend.spi.UserAuthoritiesService;

import java.util.Collections;
import java.util.List;


/**
 * @author wxl
 * @since 2022/4/7
 */
public class SimpleUserAuthoritiesServiceProvider implements UserAuthoritiesService {

    @Override
    public List<Role> listRoles() {
        Role role = new Role("xxx","platform_admin","平台管理员");
        return Collections.singletonList(role);
    }

}
