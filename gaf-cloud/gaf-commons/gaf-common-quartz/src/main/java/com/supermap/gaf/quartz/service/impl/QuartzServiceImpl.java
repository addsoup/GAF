package com.supermap.gaf.quartz.service.impl;

import com.supermap.gaf.quartz.entity.QuartzJobVo;
import com.supermap.gaf.quartz.service.QuartzService;
import com.supermap.gaf.quartz.util.CronUtils;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author : duke
 * @since 2021/11/8 3:44 PM
 */
@Slf4j
@Service
public class QuartzServiceImpl implements QuartzService {
    @Autowired
    private Scheduler scheduler;

    @Override
    public String scheduleCronJob(Class<? extends Job> jobBeanClass, String jobName, String groupName, String cron, JobDataMap data) {
        //创建任务属性
        JobDetail jobDetail = JobBuilder.newJob(jobBeanClass)
                .withIdentity(jobName, groupName)
                .usingJobData(data)
                .build();
        //创建任务触发器
        CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                .withIdentity(jobName, groupName)
                .withSchedule(CronScheduleBuilder.cronSchedule(cron).withMisfireHandlingInstructionDoNothing())
                .build();
        //使用调度器进行任务调度
        try {
            scheduler.scheduleJob(jobDetail,cronTrigger);
        }catch (SchedulerException e){
            e.printStackTrace();
            log.error("创建定时任务失败！");
        }
        return jobName;
    }

    @Override
    public String scheduleDateJob(Class<? extends Job> jobBeanClass, String jobName, String groupName, Date startTime, JobDataMap data) {
        return scheduleCronJob(jobBeanClass, jobName, groupName, CronUtils.getCron(startTime), data);
    }

    @Override
    public  Date scheduleJob(JobDetail jobDetail, Trigger trigger) throws SchedulerException{
        return scheduler.scheduleJob(jobDetail,trigger);
    }


    @Override
    public Boolean deleteScheduleJob(String jobName, String groupName) {
        boolean result = false;
        try {
            TriggerKey triggerKey = new TriggerKey(jobName, groupName);
            JobKey jobKey = new JobKey(jobName,groupName);
            scheduler.pauseTrigger(triggerKey);
            scheduler.unscheduleJob(triggerKey);
            scheduler.deleteJob(jobKey);
            result = true;
        }catch (SchedulerException e){
            e.printStackTrace();
            log.error("取消定时任务失败！");
        }
        return result;
    }


    @Override
    public Boolean pauseScheduleJob(String jobName, String groupName) {
        JobKey jobKey = new JobKey(jobName,groupName);
        boolean result = false;
        try {
            scheduler.pauseJob(jobKey);
            result = true;
        }catch (SchedulerException e){
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public Boolean resumeScheduleJob(String jobName, String groupName) {
        JobKey jobKey = new JobKey(jobName,groupName);
        boolean result = false;
        try {
            scheduler.resumeJob(jobKey);
            result = true;
        }catch (SchedulerException e){
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<QuartzJobVo> getJobs(String groupName) {
        List<QuartzJobVo> result = new ArrayList<>();
        try {
            GroupMatcher groupMatcher = GroupMatcher.groupEquals(groupName);
            Set<TriggerKey> triggerKeySet = scheduler.getTriggerKeys(groupMatcher);
            for (TriggerKey triggerKey : triggerKeySet) {
                CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
                JobKey jobKey = trigger.getJobKey();
                JobDetailImpl jobDetail = (JobDetailImpl) scheduler.getJobDetail(jobKey);
                QuartzJobVo quartzJobVo = new QuartzJobVo();
                quartzJobVo.setGroupName(Scheduler.DEFAULT_GROUP);
                quartzJobVo.setJobDetailName(jobDetail.getName());
                quartzJobVo.setJobCronExpression(trigger.getCronExpression());
                quartzJobVo.setTimeZone(trigger.getTimeZone().getID());
                result.add(quartzJobVo);
            }
        }catch (Exception e){
            log.info("获取定时任务列表失败");
        }
        return result;
    }

}
