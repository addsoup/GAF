package com.supermap.gaf.quartz.entity;

import lombok.Data;

/**
 * @author : duke
 * @since 2021/11/9 9:06 AM
 */
@Data
public class QuartzJobVo {
    /**
     * 任务名
     */
    private String jobDetailName;
    /**
     * 任务定时执行的cron表达式
     */
    private String jobCronExpression;
    /**
     * 时区
     */
    private String timeZone;
    /**
     * 分组名
     */
    private String groupName;
}
