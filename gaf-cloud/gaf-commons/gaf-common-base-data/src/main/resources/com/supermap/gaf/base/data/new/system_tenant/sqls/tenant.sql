-- liquibase formatted sql logicalFilePath:system_tenant/tenant
-- changeset SYS:20220303-0
CREATE TABLE "tenant" (
	tenant_id varchar(36) NOT NULL,
	tenant_name varchar(255) NOT NULL,
	tenant_resource_id varchar(36) NULL,
	tenant_type varchar(1000) NULL,
	description varchar(2000) NULL,
    imgr_username varchar(255) NULL,
    imgr_password varchar(255) NULL,
    imgr_appset_id varchar(255) NULL,
	created_time timestamp NOT NULL DEFAULT now(),
	created_by varchar(255) NULL,
	updated_time timestamp NULL,
	updated_by varchar(255) NULL,
	status int2 NOT NULL DEFAULT 0,
	CONSTRAINT tenant_pkey PRIMARY KEY (tenant_id)
);


COMMENT ON COLUMN "tenant"."tenant_id" IS '主键';

COMMENT ON COLUMN "tenant"."tenant_name" IS '租户名称';

COMMENT ON COLUMN "tenant"."tenant_resource_id" IS '租户初始化数据库资源id';

COMMENT ON COLUMN "tenant"."status" IS '租户状态。0未初始化 1 初始化中 2 使用中 3已冻结';

COMMENT ON COLUMN "tenant"."tenant_type" IS '租户类型';

COMMENT ON COLUMN "tenant"."description" IS '描述';
COMMENT ON COLUMN "tenant"."imgr_username" IS 'imgr用户名';
COMMENT ON COLUMN "tenant"."imgr_password" IS 'imgr用户密码';
COMMENT ON COLUMN "tenant"."imgr_appset_id" IS 'imgr站点id';

COMMENT ON COLUMN "tenant"."created_time" IS '创建时间';

COMMENT ON COLUMN "tenant"."created_by" IS '创建人';

COMMENT ON COLUMN "tenant"."updated_time" IS '更新时间';

COMMENT ON COLUMN "tenant"."updated_by" IS '更新人';

COMMENT ON TABLE "tenant" IS '租户表';

INSERT INTO "tenant"("tenant_id", "tenant_name", "tenant_resource_id", "status", "tenant_type", "description", "imgr_username", "imgr_password", "imgr_appset_id", "created_time", "created_by", "updated_time", "updated_by") VALUES ('system', '系统租户', 'f1d794ce-bd47-44ab-90a5-de85a9f2bc22', 0 , NULL, NULL, NULL, NULL, NULL, '2022-03-02 17:05:08.131505', NULL, NULL, NULL);