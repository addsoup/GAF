-- liquibase formatted sql logicalFilePath:base_tenant/service_share_setting
-- changeset SYS:20220303-0
CREATE TABLE "service_share_setting" (
  "gis_service_id" varchar(36) NOT NULL,
  "user_id" varchar(36) NOT NULL,
  "service_share_setting_id" varchar(36) NOT NULL,
  "fields_setting" varchar(500) ,
  "spatial_setting" text ,
  "created_time" timestamp(6),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  "username" varchar(50) ,
  CONSTRAINT "service_share_setting_pkey" PRIMARY KEY ("service_share_setting_id"),
  CONSTRAINT "service_share_setting_gis_service_id_user_id_key" UNIQUE ("gis_service_id", "user_id")
)
;



COMMENT ON COLUMN "service_share_setting"."gis_service_id" IS 'GIS服务id。主键,uuid';

COMMENT ON COLUMN "service_share_setting"."user_id" IS '用户id。主键,uuid';

COMMENT ON COLUMN "service_share_setting"."service_share_setting_id" IS '服务共享设置id。主键,uuid';

COMMENT ON COLUMN "service_share_setting"."fields_setting" IS '字段设置';

COMMENT ON COLUMN "service_share_setting"."spatial_setting" IS '空间设置';

COMMENT ON COLUMN "service_share_setting"."created_time" IS '创建时间。生成时间不可变更';

COMMENT ON COLUMN "service_share_setting"."created_by" IS '创建人。创建人user_id';

COMMENT ON COLUMN "service_share_setting"."updated_time" IS '修改时间。修改时更新';

COMMENT ON COLUMN "service_share_setting"."updated_by" IS '修改人。修改人user_id';

COMMENT ON COLUMN "service_share_setting"."username" IS '用户名';

COMMENT ON TABLE "service_share_setting" IS '服务共享设置表';