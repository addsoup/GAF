-- liquibase formatted sql logicalFilePath:base_tenant/user_message
-- changeset SYS:20220303-0
CREATE TABLE "user_message" (
  "user_message_id" varchar(36) NOT NULL,
  "user_name" varchar(1000) NOT NULL,
  "type" varchar(1000) NOT NULL,
  "type_link" varchar(1000) NOT NULL,
  "user_message_content" text NOT NULL,
  "user_message_status" bool NOT NULL DEFAULT false,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "user_message_pkey" PRIMARY KEY ("user_message_id")
)
;



COMMENT ON COLUMN "user_message"."user_message_id" IS '主键';

COMMENT ON COLUMN "user_message"."user_name" IS '用户名';

COMMENT ON COLUMN "user_message"."type" IS '类型';

COMMENT ON COLUMN "user_message"."type_link" IS '类型链接';

COMMENT ON COLUMN "user_message"."user_message_content" IS '消息';

COMMENT ON COLUMN "user_message"."user_message_status" IS '未读已读状态';

COMMENT ON COLUMN "user_message"."created_time" IS '创建时间';

COMMENT ON COLUMN "user_message"."created_by" IS '创建人';

COMMENT ON COLUMN "user_message"."updated_time" IS '更新时间';

COMMENT ON COLUMN "user_message"."updated_by" IS '更新人';

COMMENT ON TABLE "user_message" IS '用户消息通知表';