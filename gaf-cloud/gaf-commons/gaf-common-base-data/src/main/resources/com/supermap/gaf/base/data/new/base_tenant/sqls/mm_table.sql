-- liquibase formatted sql logicalFilePath:base_tenant/mm_table
-- changeset SYS:20220303-0
CREATE TABLE "mm_table" (
  "table_id" varchar(36) NOT NULL,
  "table_name" varchar(255) NOT NULL,
  "model_id" varchar(36) NOT NULL,
  "sdx_info" text ,
  "sort_sn" int2 NOT NULL DEFAULT 1,
  "description" text ,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "mm_table_pkey" PRIMARY KEY ("table_id")
)
;



COMMENT ON COLUMN "mm_table"."table_id" IS '主键';

COMMENT ON COLUMN "mm_table"."table_name" IS '逻辑表名称';

COMMENT ON COLUMN "mm_table"."model_id" IS '模型id';

COMMENT ON COLUMN "mm_table"."sdx_info" IS '空间数据源sdx数据集信息';

COMMENT ON COLUMN "mm_table"."sort_sn" IS '排序';

COMMENT ON COLUMN "mm_table"."description" IS '描述';

COMMENT ON COLUMN "mm_table"."created_time" IS '创建时间';

COMMENT ON COLUMN "mm_table"."created_by" IS '创建人';

COMMENT ON COLUMN "mm_table"."updated_time" IS '更新时间';

COMMENT ON COLUMN "mm_table"."updated_by" IS '更新人';

COMMENT ON TABLE "mm_table" IS '数据模型管理-逻辑表';