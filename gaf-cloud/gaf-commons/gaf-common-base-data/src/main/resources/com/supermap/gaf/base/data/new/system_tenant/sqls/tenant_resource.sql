-- liquibase formatted sql logicalFilePath:system_tenant/tenant_resource
-- changeset SYS:20220303-3
CREATE TABLE "tenant_resource" (
  "tenant_resource_id" varchar(36) NOT NULL,
  "tenant_resource_name" varchar(255) NOT NULL,
  "tenant_resource_type" int2 NOT NULL DEFAULT 0,
  "database_address" varchar(2000) NOT NULL,
  "database_name" varchar(1000),
  "database_user" varchar(1000),
  "database_password" varchar(1000),
  "description" varchar(2000),
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255),
  "updated_time" timestamp(6),
  "updated_by" varchar(255),
  CONSTRAINT "tenant_resource_pkey" PRIMARY KEY ("tenant_resource_id"),
	CONSTRAINT tenant_resource_un UNIQUE (tenant_resource_name)
)
;


COMMENT ON COLUMN "tenant_resource"."tenant_resource_id" IS '主键';

COMMENT ON COLUMN "tenant_resource"."tenant_resource_name" IS '租户资源名称';

COMMENT ON COLUMN "tenant_resource"."tenant_resource_type" IS '租户资源类型,0业务数据源';

COMMENT ON COLUMN "tenant_resource"."database_address" IS '租户资源数据库地址';

COMMENT ON COLUMN "tenant_resource"."database_name" IS '租户资源数据库名称';

COMMENT ON COLUMN "tenant_resource"."database_user" IS '租户资源数据库用户';

COMMENT ON COLUMN "tenant_resource"."database_password" IS '租户资源数据库密码';

COMMENT ON COLUMN "tenant_resource"."description" IS '租户资源描述';

COMMENT ON COLUMN "tenant_resource"."created_time" IS '创建时间';

COMMENT ON COLUMN "tenant_resource"."created_by" IS '创建人';

COMMENT ON COLUMN "tenant_resource"."updated_time" IS '更新时间';

COMMENT ON COLUMN "tenant_resource"."updated_by" IS '更新人';

COMMENT ON TABLE "tenant_resource" IS '页面元素';

INSERT INTO "tenant_resource" ("tenant_resource_id", "tenant_resource_name", "tenant_resource_type", "database_address", "database_name", "database_user", "database_password", "description", "created_time", "created_by", "updated_time", "updated_by") VALUES ('f1d794ce-bd47-44ab-90a5-de85a9f2bc22', 'system_datasource', 0, 'gaf-postgres:5432', 'gaf', 'admin', '123456', NULL, '2022-03-03 09:00:26.005609', 'sys_admin', '2022-03-15 14:32:36.614435', 'sys_admin');