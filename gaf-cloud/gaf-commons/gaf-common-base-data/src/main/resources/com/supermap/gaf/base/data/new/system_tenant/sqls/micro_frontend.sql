-- liquibase formatted sql logicalFilePath:system_tenant/micro_frontend
-- changeset SYS:20220322-0
create table micro_frontend
(
    micro_frontend_id varchar(36)             not null constraint micro_frontend_pkey primary key,
    name              varchar(255)            not null,
    entry             varchar(255)            not null,
    container         varchar(255)            not null,
    active_rule       varchar(255)            not null constraint micro_frontend_activerule_key unique,
    created_time      timestamp default now() not null,
    created_by        varchar(255),
    updated_time      timestamp,
    updated_by        varchar(255)
);

comment on table micro_frontend is '微前端应用';
comment on column micro_frontend.micro_frontend_id is '主键';
comment on column micro_frontend.name is '微应用名称';
comment on column micro_frontend.entry is '微应用入口';
comment on column micro_frontend.container is '微应用挂载节点';
comment on column micro_frontend.active_rule is '微应用触发的路由规则';
comment on column micro_frontend.created_time is '创建时间';
comment on column micro_frontend.created_by is '创建人';
comment on column micro_frontend.updated_time is '更新时间';
comment on column micro_frontend.updated_by is '更新人';

INSERT INTO micro_frontend (micro_frontend_id, name, entry, container, active_rule, created_time, created_by, updated_time, updated_by) VALUES ('app1', 'apps-log', '/apps-log/', '#frame', '/log', '2022-03-19 09:45:29.000404', null, null, null);
INSERT INTO micro_frontend (micro_frontend_id, name, entry, container, active_rule, created_time, created_by, updated_time, updated_by) VALUES ('app2', 'apps-config', '/apps-config/', '#frame', '/config', '2022-03-19 09:45:29.009746', null, null, null);
INSERT INTO micro_frontend (micro_frontend_id, name, entry, container, active_rule, created_time, created_by, updated_time, updated_by) VALUES ('app6', 'apps-project', '/apps-project/', '#frame', '/project', '2022-03-19 09:45:29.023030', null, null, null);
INSERT INTO micro_frontend (micro_frontend_id, name, entry, container, active_rule, created_time, created_by, updated_time, updated_by) VALUES ('app7', 'apps-map', '/apps-map/', '#frame', '/map', '2022-03-19 09:45:29.026078', null, null, null);
INSERT INTO micro_frontend (micro_frontend_id, name, entry, container, active_rule, created_time, created_by, updated_time, updated_by) VALUES ('app8', 'apps-data-mgt', '/apps-data-mgt/', '#frame', '/dataMgt', '2022-03-19 09:45:29.029343', null, null, null);
INSERT INTO micro_frontend (micro_frontend_id, name, entry, container, active_rule, created_time, created_by, updated_time, updated_by) VALUES ('app9', 'apps-sys-mgt', '/apps-sys-mgt/', '#frame', '/sysMgt', '2022-03-19 09:45:29.032779', null, null, null);
INSERT INTO micro_frontend (micro_frontend_id, name, entry, container, active_rule, created_time, created_by, updated_time, updated_by) VALUES ('app10', 'apps-authority', '/apps-authority/', '#frame', '/authority', '2022-03-19 09:45:29.036952', null, null, null);
INSERT INTO micro_frontend (micro_frontend_id, name, entry, container, active_rule, created_time, created_by, updated_time, updated_by) VALUES ('app12', 'apps-governance', '/apps-governance/', '#frame', '/governance', '2022-03-19 09:45:29.043115', null, null, null);
INSERT INTO micro_frontend (micro_frontend_id, name, entry, container, active_rule, created_time, created_by, updated_time, updated_by) VALUES ('app13', 'apps-monitor', '/apps-monitor/', '#frame', '/monitor', '2022-03-19 09:45:29.046961', null, null, null);
INSERT INTO micro_frontend (micro_frontend_id, name, entry, container, active_rule, created_time, created_by, updated_time, updated_by) VALUES ('app11', 'apps-platform', '/apps-platform/', '#frame', '/platform', '2022-03-19 09:45:29.040067', null, null, null);