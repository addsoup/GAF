-- liquibase formatted sql logicalFilePath:base_tenant/data_dataset
-- changeset SYS:20220303-0
CREATE TABLE "data_dataset" (
  "dataset_id" varchar(36) NOT NULL,
  "dataset_name" varchar(100) NOT NULL,
  "datasource_id" varchar(36) NOT NULL,
  "sort_sn" int4,
  "category_factor_code" varchar(100) ,
  "is_sdx" bool,
  "map_services" text ,
  "description" varchar(500) ,
  "status" bool,
  "created_time" timestamp(6),
  "created_by" varchar(36) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(36) ,
  CONSTRAINT "data_dataset_pkey" PRIMARY KEY ("dataset_id")
)
;



COMMENT ON COLUMN "data_dataset"."dataset_id" IS '数据集id。主键,uuid';

COMMENT ON COLUMN "data_dataset"."dataset_name" IS '数据集名称。';

COMMENT ON COLUMN "data_dataset"."datasource_id" IS '所属数据源。所属数据源id';

COMMENT ON COLUMN "data_dataset"."sort_sn" IS '排序序号。';

COMMENT ON COLUMN "data_dataset"."category_factor_code" IS '分类要素。选自数据字典:自然资源数据体系第四级的编码，
存各级code斜级分隔(level1code/level2code...)：自定义，必须隶属于所属数据源的体系分类中';

COMMENT ON COLUMN "data_dataset"."is_sdx" IS '是否空间数据。true:是，false:否';

COMMENT ON COLUMN "data_dataset"."map_services" IS '关联地图。json数组格式，地图服务名+服务地址，[{name:现状1，url:http://xxx1},{name:现状2，url:http://xxx2}]';

COMMENT ON COLUMN "data_dataset"."description" IS '描述。';

COMMENT ON COLUMN "data_dataset"."status" IS '状态。true:有效，false:无效';

COMMENT ON COLUMN "data_dataset"."created_time" IS '创建时间。生成时间不可变更';

COMMENT ON COLUMN "data_dataset"."created_by" IS '创建人。创建人user_id';

COMMENT ON COLUMN "data_dataset"."updated_time" IS '修改时间。修改时更新';

COMMENT ON COLUMN "data_dataset"."updated_by" IS '修改人。修改人user_id';

COMMENT ON TABLE "data_dataset" IS '数据集表';