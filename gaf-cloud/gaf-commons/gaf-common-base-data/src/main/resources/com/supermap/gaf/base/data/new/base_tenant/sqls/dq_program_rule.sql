-- liquibase formatted sql logicalFilePath:base_tenant/dq_program_rule
-- changeset SYS:20220303-0
CREATE TABLE "dq_program_rule" (
  "dq_program_rule_id" varchar(36) NOT NULL,
  "data_quality_program_id" varchar(36) NOT NULL,
  "data_quality_rule_id" varchar(36) NOT NULL,
  "sort_sn" int2 DEFAULT 1,
  "description" text ,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "dq_program_rule_pkey" PRIMARY KEY ("dq_program_rule_id")
)
;



COMMENT ON COLUMN "dq_program_rule"."dq_program_rule_id" IS '主键';

COMMENT ON COLUMN "dq_program_rule"."data_quality_program_id" IS '方案id';

COMMENT ON COLUMN "dq_program_rule"."data_quality_rule_id" IS '规则id';

COMMENT ON COLUMN "dq_program_rule"."sort_sn" IS '排序';

COMMENT ON COLUMN "dq_program_rule"."description" IS '描述';

COMMENT ON COLUMN "dq_program_rule"."created_time" IS '创建时间';

COMMENT ON COLUMN "dq_program_rule"."created_by" IS '创建人';

COMMENT ON COLUMN "dq_program_rule"."updated_time" IS '更新时间';

COMMENT ON COLUMN "dq_program_rule"."updated_by" IS '更新人';

COMMENT ON TABLE "dq_program_rule" IS '数据质量管理-方案-规则联系表';