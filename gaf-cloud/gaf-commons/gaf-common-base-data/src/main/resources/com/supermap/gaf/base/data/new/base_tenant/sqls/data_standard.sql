-- liquibase formatted sql logicalFilePath:base_tenant/data_standard
-- changeset SYS:20220303-0
CREATE TABLE "data_standard" (
  "chinese_name" varchar(255) NOT NULL,
  "code" varchar(255) NOT NULL,
  "en_name" varchar(255) NOT NULL,
  "description" text ,
  "data_standard_id" varchar(36) NOT NULL,
  "catalog_id" varchar(36) NOT NULL,
  "ref_code" varchar(36) ,
  "domain_type" char(1) NOT NULL,
  "field_type" varchar(50) NOT NULL DEFAULT false,
  "field_length" int2,
  "field_precision" int2,
  "field_default" varchar(2000) ,
  "field_not_null" bool NOT NULL DEFAULT false,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "data_standard_pkey" PRIMARY KEY ("data_standard_id")
)
;



COMMENT ON COLUMN "data_standard"."chinese_name" IS '中文名。
';

COMMENT ON COLUMN "data_standard"."code" IS '编码。';

COMMENT ON COLUMN "data_standard"."en_name" IS '英文名。';

COMMENT ON COLUMN "data_standard"."description" IS '描述。';

COMMENT ON COLUMN "data_standard"."data_standard_id" IS '主键。';

COMMENT ON COLUMN "data_standard"."catalog_id" IS '目录id。';

COMMENT ON COLUMN "data_standard"."ref_code" IS '引用代码id。（当值域是枚举型的时候）';

COMMENT ON COLUMN "data_standard"."domain_type" IS '值域类型。（E:枚举型，R:范围型）';

COMMENT ON COLUMN "data_standard"."field_type" IS '字段类型';

COMMENT ON COLUMN "data_standard"."field_length" IS '字段长度';

COMMENT ON COLUMN "data_standard"."field_precision" IS '字段精度';

COMMENT ON COLUMN "data_standard"."field_default" IS '字段默认值';

COMMENT ON COLUMN "data_standard"."field_not_null" IS '字段是否非空';

COMMENT ON COLUMN "data_standard"."created_time" IS '创建时间';

COMMENT ON COLUMN "data_standard"."created_by" IS '创建人';

COMMENT ON COLUMN "data_standard"."updated_time" IS '更新时间';

COMMENT ON COLUMN "data_standard"."updated_by" IS '更新人';

COMMENT ON TABLE "data_standard" IS '数据标准';