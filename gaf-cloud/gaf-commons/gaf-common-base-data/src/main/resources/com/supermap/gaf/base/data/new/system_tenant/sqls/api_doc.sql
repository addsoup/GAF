-- liquibase formatted sql logicalFilePath:system_tenant/api_doc

-- changeset SYS:20220302-0
CREATE TABLE "api_doc" (
  "id" varchar(36) NOT NULL,
  "name" varchar(255) ,
  "data" text ,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "updated_time" timestamp(6),
  "created_by" varchar(255) ,
  "updated_by" varchar(255)
)
;
COMMENT ON TABLE "api_doc" IS 'api doc文档';

ALTER TABLE "api_doc" ADD CONSTRAINT "api_doc_pk" PRIMARY KEY ("id");

INSERT INTO "api_doc" VALUES ('aed59230-b120-4596-b15e-60ddd483232a', 'knife-ui', '{ "deepLinking":true, "displayOperationId":false, "defaultModelsExpandDepth":1, "defaultModelExpandDepth":1, "defaultModelRendering":"example", "displayRequestDuration":false, "docExpansion":"none", "filter":false, "operationsSorter":"alpha", "showExtensions":false, "tagsSorter":"alpha", "validatorUrl":"", "apisSorter":"alpha", "jsonEditor":false, "showRequestHeaders":false, "supportedSubmitMethods":[ "get", "put", "post", "delete", "options", "head", "patch", "trace" ] }', '2022-03-02 20:19:11.569442', NULL, 'SYS', NULL);
INSERT INTO "api_doc" VALUES ('cxd59230-b120-4596-b15e-60ddd483232a', 'knife-config', '[]', '2022-03-02 20:19:11.569442', NULL, 'SYS', NULL);
