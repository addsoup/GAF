-- liquibase formatted sql logicalFilePath:system_tenant/tenant_user
-- changeset SYS:20220303-0
create table tenant_user
(
    tenant_user_id varchar(36)             not null constraint tenant_user_pkey primary key,
    tenant_id      varchar(36)             not null,
    user_id        varchar(36)             not null,
    status         boolean   default true  not null,
    tenant_admin   boolean   default false not null,
    created_time   timestamp default now() not null,
    created_by     varchar(255),
    updated_time   timestamp,
    updated_by     varchar(255)
);

comment on table tenant_user is '页面元素';

comment on column tenant_user.tenant_user_id is '主键';

comment on column tenant_user.tenant_id is '租户id';

comment on column tenant_user.user_id is '用户id';

comment on column tenant_user.status is '租户用户状态';

comment on column tenant_user.tenant_admin is '是否是租户管理员';

comment on column tenant_user.created_time is '创建时间';

comment on column tenant_user.created_by is '创建人';

comment on column tenant_user.updated_time is '更新时间';

comment on column tenant_user.updated_by is '更新人';

INSERT INTO "tenant_user"("tenant_user_id", "tenant_id", "user_id", "status", "tenant_admin", "created_time", "created_by", "updated_time", "updated_by") VALUES ('system_tenant_user', 'system', 'user_000000', 't', 't', '2022-03-02 17:06:28.982135', NULL, NULL, NULL);
