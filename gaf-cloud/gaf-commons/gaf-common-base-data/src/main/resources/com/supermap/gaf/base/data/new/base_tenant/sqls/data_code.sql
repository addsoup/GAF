-- liquibase formatted sql logicalFilePath:base_tenant/data_code
-- changeset SYS:20220303-0
CREATE TABLE "data_code" (
  "name" varchar(255) NOT NULL,
  "code" varchar(255) NOT NULL,
  "en_name" varchar(255) NOT NULL,
  "description" text ,
  "catalog_id" varchar(36) NOT NULL,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  "data_code_id" varchar(36) NOT NULL,
  CONSTRAINT "data_code_pkey" PRIMARY KEY ("data_code_id")
)
;



COMMENT ON COLUMN "data_code"."name" IS '名称。';

COMMENT ON COLUMN "data_code"."code" IS '代码编号';

COMMENT ON COLUMN "data_code"."en_name" IS '英文名。';

COMMENT ON COLUMN "data_code"."description" IS '描述。';

COMMENT ON COLUMN "data_code"."catalog_id" IS '目录id';

COMMENT ON COLUMN "data_code"."created_time" IS '创建时间';

COMMENT ON COLUMN "data_code"."created_by" IS '创建人';

COMMENT ON COLUMN "data_code"."updated_time" IS '更新时间';

COMMENT ON COLUMN "data_code"."updated_by" IS '更新人';

COMMENT ON COLUMN "data_code"."data_code_id" IS '主键。';

COMMENT ON TABLE "data_code" IS '数据代码';