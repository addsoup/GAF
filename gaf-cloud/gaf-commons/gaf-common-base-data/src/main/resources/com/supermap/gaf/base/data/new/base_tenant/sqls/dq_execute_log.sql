-- liquibase formatted sql logicalFilePath:base_tenant/dq_execute_log
-- changeset SYS:20220303-0
CREATE TABLE "dq_execute_log" (
  "dq_execute_log_id" varchar(36) NOT NULL,
  "content" text NOT NULL,
  "sort_sn" int2 DEFAULT 1,
  "description" text ,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  "total_amount" int8 NOT NULL,
  "failed_amount" int8 NOT NULL,
  "cost_time" int8 NOT NULL,
  "data_quality_rule_id" varchar(36) NOT NULL,
  "dq_execute_result_id" varchar(64) NOT NULL,
  "data_quality_program_id" varchar(36) NOT NULL,
  "result_status" int2 NOT NULL DEFAULT 0,
  "data_quality_rule_name" varchar(255) ,
  CONSTRAINT "dq_execute_log_pkey" PRIMARY KEY ("dq_execute_log_id")
)
;



COMMENT ON COLUMN "dq_execute_log"."dq_execute_log_id" IS '主键';

COMMENT ON COLUMN "dq_execute_log"."content" IS '执行日志记录';

COMMENT ON COLUMN "dq_execute_log"."sort_sn" IS '排序';

COMMENT ON COLUMN "dq_execute_log"."description" IS '描述';

COMMENT ON COLUMN "dq_execute_log"."created_time" IS '创建时间';

COMMENT ON COLUMN "dq_execute_log"."created_by" IS '创建人';

COMMENT ON COLUMN "dq_execute_log"."updated_time" IS '更新时间';

COMMENT ON COLUMN "dq_execute_log"."updated_by" IS '更新人';

COMMENT ON COLUMN "dq_execute_log"."total_amount" IS '数据总量';

COMMENT ON COLUMN "dq_execute_log"."failed_amount" IS '失败数据总量';

COMMENT ON COLUMN "dq_execute_log"."cost_time" IS '耗费时间-秒';

COMMENT ON COLUMN "dq_execute_log"."data_quality_rule_id" IS '规则id';

COMMENT ON COLUMN "dq_execute_log"."dq_execute_result_id" IS '执行结果id。同一批执行规则';

COMMENT ON COLUMN "dq_execute_log"."data_quality_program_id" IS '方案id。';

COMMENT ON COLUMN "dq_execute_log"."result_status" IS '执行状态。0代表成功，1代表失败。';

COMMENT ON COLUMN "dq_execute_log"."data_quality_rule_name" IS '规则名称';

COMMENT ON TABLE "dq_execute_log" IS '数据质量管理-执行日志表';