-- liquibase formatted sql logicalFilePath:others/storage/storage_s3_server

-- changeset SYS:20210728-0
CREATE TABLE "storage_s3_server" (
  "access_key" varchar(255) NOT NULL,
  "secret_key" varchar(255) NOT NULL,
  "service_endpoint" varchar(255) NOT NULL,
  "id" varchar(64) NOT NULL DEFAULT NULL::character varying,
  CONSTRAINT "s3_server_pkey" PRIMARY KEY ("id")
)
;
