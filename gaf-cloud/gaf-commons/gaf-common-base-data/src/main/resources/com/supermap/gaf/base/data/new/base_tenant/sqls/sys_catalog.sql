-- liquibase formatted sql logicalFilePath:base_tenant/sys_catalog
-- changeset SYS:20220411-0
CREATE TABLE "sys_catalog" (
  "catalog_id" varchar(36) NOT NULL,
  "parent_id" varchar(36),
  "sort_sn" int4,
  "name" varchar(255) NOT NULL,
  "type" varchar(3),
  "code" varchar(100),
  "icon_url" varchar(500),
  "description" varchar(500),
  "created_time" timestamp(6),
  "created_by" varchar(255),
  "updated_time" timestamp(6),
  "updated_by" varchar(255),
  "biz_type_code" varchar(3),
  CONSTRAINT "sys_catalog_pkey" PRIMARY KEY ("catalog_id")
)
;
COMMENT ON COLUMN "sys_catalog"."catalog_id" IS '目录id。主键,uuid';

COMMENT ON COLUMN "sys_catalog"."parent_id" IS '上级目录id。(创建目录类别时，生成该类别根记录)';

COMMENT ON COLUMN "sys_catalog"."sort_sn" IS '排序序号。同级中的序号';

COMMENT ON COLUMN "sys_catalog"."name" IS '目录名称。中文名称';

COMMENT ON COLUMN "sys_catalog"."type" IS '2：API分组，3：角色分组，4：菜单分组，5:webgis服务分组，6：资源目录,7:字典目录';

COMMENT ON COLUMN "sys_catalog"."code" IS '编码。每级4位数字，同级递增';

COMMENT ON COLUMN "sys_catalog"."icon_url" IS '图标地址。';

COMMENT ON COLUMN "sys_catalog"."description" IS '描述。';

COMMENT ON COLUMN "sys_catalog"."created_time" IS '创建时间。生成时间不可变更';

COMMENT ON COLUMN "sys_catalog"."created_by" IS '创建人。创建人user_id';

COMMENT ON COLUMN "sys_catalog"."updated_time" IS '修改时间。修改时更新';

COMMENT ON COLUMN "sys_catalog"."updated_by" IS '修改人。修改人user_id';

COMMENT ON COLUMN "sys_catalog"."biz_type_code" IS '业务类别。业务类型字典码，资源目录等业务数据目录使用';

COMMENT ON TABLE "sys_catalog" IS '分类目录表';
-- INSERT INTO "sys_catalog"("catalog_id", "parent_id", "sort_sn", "name", "type", "code", "icon_url", "description", "created_time", "created_by", "updated_time", "updated_by", "biz_type_code") VALUES ('6025f83b-9e07-47f9-a3af-8116fc659e16', '0', 11, '演示账户API', '2', NULL, NULL, '1默认根目录', '2021-11-30 06:28:24.006', NULL, '2021-11-30 06:28:24.006', NULL, NULL);
INSERT INTO "sys_catalog"("catalog_id", "parent_id", "sort_sn", "name", "type", "code", "icon_url", "description", "created_time", "created_by", "updated_time", "updated_by", "biz_type_code") VALUES ('059b362d-251c-4a17-bb1a-3a5ed2fb105d', '0', 2, '字典根目录', '7', NULL, NULL, NULL, '2021-04-22 09:34:57.678619', NULL, '2021-04-22 09:34:57.678619', NULL, NULL);
INSERT INTO "sys_catalog"("catalog_id", "parent_id", "sort_sn", "name", "type", "code", "icon_url", "description", "created_time", "created_by", "updated_time", "updated_by", "biz_type_code") VALUES ('abd24097-c251-4419-a9a2-a46848d1fa6d', '059b362d-251c-4a17-bb1a-3a5ed2fb105d', 2, '地图应用数据', '7', NULL, NULL, NULL, '2021-02-03 03:04:15.545059', NULL, '2021-02-03 03:04:15.545059', NULL, NULL);
INSERT INTO "sys_catalog"("catalog_id", "parent_id", "sort_sn", "name", "type", "code", "icon_url", "description", "created_time", "created_by", "updated_time", "updated_by", "biz_type_code") VALUES ('64b6f34d-7f8e-42d8-b50c-590b653b2107', '059b362d-251c-4a17-bb1a-3a5ed2fb105d', 1, '基础数据', '7', NULL, NULL, NULL, '2021-01-28 15:05:01.239997', NULL, '2021-01-28 15:05:01.239997', NULL, NULL);
INSERT INTO "sys_catalog"("catalog_id", "parent_id", "sort_sn", "name", "type", "code", "icon_url", "description", "created_time", "created_by", "updated_time", "updated_by", "biz_type_code") VALUES ('6d8fd1ce-67c7-4005-98a6-06966aa6b123', '0', 1, '元数据标签', '8', NULL, NULL, '元数据标签分组根目录', '2021-09-06 06:09:19.874', 'SYS', '2021-09-06 06:09:19.874', 'SYS', NULL);
INSERT INTO "sys_catalog"("catalog_id", "parent_id", "sort_sn", "name", "type", "code", "icon_url", "description", "created_time", "created_by", "updated_time", "updated_by", "biz_type_code") VALUES ('57bcfa59-feb8-4905-99a0-bd4463857a68', '0', 1, '内置角色组', '3', NULL, NULL, NULL, '2022-08-05 01:31:47.025728', 'sys_admin', '2022-08-05 01:31:47.025728', 'sys_admin', NULL);
