-- liquibase formatted sql logicalFilePath:base_tenant/publish_history
-- changeset SYS:20220608-1
create table publish_history
(
    id                 varchar(36) not null constraint publish_history_pkey primary key,
    source_type        varchar(50),
    source_name        varchar(200),
    source_info        text,
    service_type       varchar(100),
    additional_setting text,
    state              integer,
    result             text,
    description        text,
    created_time       timestamp,
    created_by         varchar(50),
    updated_time       timestamp,
    updated_by         varchar(50)
);

comment on table publish_history is '发布历史.当未进行服务发布时，可以充当连接信息类';

comment on column publish_history.id is '发布历史id。主键，uuid';

comment on column publish_history.source_type is '来源类型，包括FILE_WORKSPACE_FOR_STORAGE_TILE(用于存储瓦片地址的文件型工作空间)、MONGODB(MongoDB)、MANAGED_WORKSPACE(管理的工作空间)、MANAGED_TILE(管理的瓦片,包括ugcv5 和 mongodb瓦片)';

comment on column publish_history.source_name is '来源名称';

comment on column publish_history.source_info is '来源信息.json格式,根据来源类型 约定有不同的格式和内容';

comment on column publish_history.service_type is '发布服务类型.多个使用,分割,可选值,RESTDATA,RESTMAP,RESTREALSPACE,RESTSPATIALANALYST,ARCGISMAP,WMS,WMTS,WPS,WCS,WPS,GRID_DEM,TIN_DEM,MAPWORLD,YINGXIANG,BAIDUREST,GOOGLEREST,OTHER';

comment on column publish_history.additional_setting is '发布设置,. json格式,不同的来源类型 可以有额外的特有的发布设置 根据来源类型 约定有不同的格式和内容,通过代码对格式和内容进行控制';

comment on column publish_history.state is '状态. 0 新建   4 已发布 5 发布失败';

comment on column publish_history.result is '发布结果.若state是发布成功,则发布结果为 json格式 例如["serviceType": "RESTDATA","serviceAddress": "http://xxx.xxx.xxx/xxx/xx"].若失败则为失败消息';

comment on column publish_history.description is '描述';

comment on column publish_history.created_time is '创建时间';

comment on column publish_history.created_by is '创建人';

comment on column publish_history.updated_time is '修改时间';

comment on column publish_history.updated_by is '修改人';