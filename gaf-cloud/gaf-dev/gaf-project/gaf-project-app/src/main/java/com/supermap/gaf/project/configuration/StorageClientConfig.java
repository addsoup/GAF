package com.supermap.gaf.project.configuration;

import com.supermap.gaf.common.storage.client.StorageClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class StorageClientConfig {

    @Value("${GAF_PROJECT_STORAGE_CONFIG_NAME:default}")
    private String configName;

    @Value("${GAF_PROJECT_STORAGE_PRE_URL:http://gaf-storage/storage/api/tenant-created-first/}")
    private String storagePreUrl;

    @Bean("storageRestTemplate")
    @LoadBalanced
    public RestTemplate storageRestTemplate() {
        return new RestTemplate();
    }


    @Bean
    public StorageClient storageClient() {
        return new StorageClient(storagePreUrl, configName, storageRestTemplate());
    }
}
