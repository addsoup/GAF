package com.supermap.gaf.project.commontype.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 代码模板表
 * 
 * @author wxl
 * @email wxl@gmail.com
 * @date 2020-09-07 10:53:44
 */
@Data
@TableName("proj_code_template")
@ApiModel("工程模板信息")
public class ProjCodeTemplateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 代码模板id。主键,uuid
	 */
	@TableId
	@ApiModelProperty("工程模板id")
	private String projCodeTemplateId;
	/**
	 * 模板名称。
	 */
	@ApiModelProperty("模板名称")
	private String templateName;
	/**
	 * 模板中文名称。
	 */
	@ApiModelProperty("模板中文名称")
	private String templateNameCn;
	/**
	 * 模板类型。11:工程-前端，12：工程-后端，13：工程-前后端，21-表操作-前端，22：表操作-后端，23：表操作-前后端
	 */
	@ApiModelProperty("模板类型。11:工程-前端，12：工程-后端，13：工程-前后端，21-表操作-前端，22：表操作-后端，23：表操作-前后端")
	private Integer templateType;
	/**
	 * 描述。
	 */
	@ApiModelProperty("描述")
	private String description;
	/**
	 * 版本。
	 */
	@ApiModelProperty("版本")
	private String version;
	/**
	 * 模板地址。
	 */
	@ApiModelProperty("模板地址")
	private String templatePath;
	/**
	 * 创建时间。
	 */
	@ApiModelProperty("创建时间")
	private Date createdTime;
	/**
	 * 创建人。
	 */
	@ApiModelProperty("创建人")
	private String createdBy;
	/**
	 * 修改时间。
	 */
	@ApiModelProperty("修改时间")
	private Date updatedTime;
	/**
	 * 修改人。
	 */
	@ApiModelProperty("修改人")
	private String updatedBy;

}
