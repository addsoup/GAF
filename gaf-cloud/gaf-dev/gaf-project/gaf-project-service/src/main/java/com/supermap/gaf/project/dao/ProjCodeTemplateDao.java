package com.supermap.gaf.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 代码模板表
 * 
 * @author wxl
 * @email wxl@gmail.com
 * @date 2020-09-07 10:53:44
 */
@Mapper
public interface ProjCodeTemplateDao extends BaseMapper<ProjCodeTemplateEntity> {
	
}
