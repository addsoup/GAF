package com.supermap.gaf.project.commontype.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 *
 * @author wangxiaolong
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("工程表视图返回Vo")
public class ProjInfoVo {
    @ApiModelProperty("工程id")
    private String projId;
    @ApiModelProperty("工程名称")
    private String projName;
    @ApiModelProperty("工程描述")
    private String description;
    @ApiModelProperty("工程模板id")
    private String projTemplateId;
    @ApiModelProperty("工程设置")
    private String settingParams;
    @ApiModelProperty("权限")
    private String visibility;
    @ApiModelProperty("所属组织")
    private String orgId;
    @ApiModelProperty("工程组")
    private String gitPrjGroup;
    @ApiModelProperty("代码地址")
    private String gitUrl;
    @ApiModelProperty("状态")
    private Integer status;
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("修改时间")
    private Date updatedTime;
    @ApiModelProperty("修改人")
    private String updatedBy;
    @ApiModelProperty("template")
    private String templateName;
    @ApiModelProperty("gitlab工程id")
    private String gitProjId;
    @ApiModelProperty("git")
    private String gitGroupName;
    @ApiModelProperty("代码地址")
    private String webUrl;
    @ApiModelProperty("工程组id。开发者仅可选取他所属的工程组")
    private String projGroupId;
    @ApiModelProperty("工程组名称")
    private String projGroupName;
    @ApiModelProperty("工程组名缩写")
    private String briefName;
}
