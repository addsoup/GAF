package com.supermap.gaf.project.commontype.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * 
 * @author wxl
 * @email wxl@gmail.com
 * @date 2020-09-04 14:46:24
 */
@Data
@TableName("proj_code_template_param")
@ApiModel("工程模板参数信息")
public class ProjCodeTemplateParamEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 模板参数id。主键,uuid
	 */
	@TableId
	@ApiModelProperty("模板参数id")
	private String projCodeTemplateParamId;
	/**
	 * 模板id。该值为000000，则为参数样板
	 */
	@ApiModelProperty("模板id")
	private String codeTemplateId;
	/**
	 * 参数名称。
	 */
	@ApiModelProperty("参数名称")
	private String paramName;
	/**
	 * 参数中文名称。
	 */
	@ApiModelProperty("参数中文名称")
	private String paramNameCn;
	/**
	 * 描述。
	 */
	@ApiModelProperty("描述")
	private String description;
	/**
	 * 默认值。
	 */
	@ApiModelProperty("默认值")
	private String defaultValue;
	/**
	 * 创建时间。
	 */
	@ApiModelProperty("创建时间")
	private Date createdTime;
	/**
	 * 创建人。
	 */
	@ApiModelProperty("创建人")
	private String createdBy;
	/**
	 * 修改时间。
	 */
	@ApiModelProperty("修改时间")
	private Date updatedTime;
	/**
	 * 修改人。
	 */
	@ApiModelProperty("修改人")
	private String updatedBy;
	/**
	 * 配置参数分组id。
	 */
	@ApiModelProperty("配置参数分组id")
	private String paramGroupId;

}
