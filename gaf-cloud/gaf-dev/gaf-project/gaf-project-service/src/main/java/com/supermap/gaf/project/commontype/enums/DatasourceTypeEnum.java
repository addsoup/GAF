package com.supermap.gaf.project.commontype.enums;
/**
 * @author:yw
 * @Date 2021/3/17
 **/
public enum DatasourceTypeEnum {
    /**
     * MYSQL数据库
     */
    MYSQL,
    /**
     * POSTGRESQL
     */
    POSTGRESQL,
    /**
     * SQLSERVER
     */
    SQLSERVER,
    /**
     * ORACLE
     */
    ORACLE
}
