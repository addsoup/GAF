package com.supermap.gaf.project.service.impl;

import com.alibaba.fastjson.JSON;
import com.supermap.gaf.project.commontype.VmMetadata;
import com.supermap.gaf.project.commontype.VmProperty;
import com.supermap.gaf.project.service.VmMetadataService;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Service("xmlVmMetadataService")
public class XmlVmMetadataServiceImpl implements VmMetadataService {
    private String medataXmlFileName = "metadata.xml";

    public XmlVmMetadataServiceImpl() {}
    public XmlVmMetadataServiceImpl(String medataXmlFileName) {
        this.medataXmlFileName = medataXmlFileName;
    }

    public String getMedataXmlFileName() {
        return medataXmlFileName;
    }

    public void setMedataXmlFileName(String medataXmlFileName) {
        this.medataXmlFileName = medataXmlFileName;
    }

    @Override
    public VmMetadata select(String vmName, String vmRootPath) {
        return select(vmName,vmRootPath,true);
    }

    @Override
    public List<VmMetadata> selectList(String vmRootPath, String captionName) {
        Path vmRoot = FileSystems.getDefault().getPath(vmRootPath);
        List<VmMetadata> vmNames= new ArrayList<>();
        try (Stream<Path> fileStream = Files.list(vmRoot)){
            vmNames = fileStream
                    .filter(path-> Files.isDirectory(path))
                    .map(path->select(path.getFileName().toString(),vmRootPath,false))
                    .filter(vmMetadata ->
                            StringUtils.isEmpty(captionName) || (!StringUtils.isEmpty(vmMetadata.getCaption()) && vmMetadata.getCaption().contains(captionName))
                    )
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vmNames;
    }

    public VmMetadata select(String vmName,String vmRootPath,boolean detail){
        VmMetadata vmMetadata = new VmMetadata();
        try {
            Path vmRoot = FileSystems.getDefault().getPath(vmRootPath + "/" + vmName);
            if(!Files.exists(vmRoot)){
                throw new IllegalArgumentException(vmName+"模板不存在");
            }
            Path vmMetadataPath = vmRoot.resolve(medataXmlFileName);
            if(!Files.exists(vmMetadataPath)){
                return null;
            }
            SAXReader reader = new SAXReader();
            Document document = reader.read(vmMetadataPath.toFile());
            Element root = document.getRootElement();

            Element caption = root.element("caption");
            Element description = root.element("description");
            vmMetadata.setName(vmName);
            if (caption != null) vmMetadata.setCaption(caption.getText());
            if (description != null) vmMetadata.setDescription(description.getText());
            if(detail){
                Element requiredProperties = root.element("requiredProperties");
                Element fileMapping = root.element("fileMappings");
                if (requiredProperties != null) vmMetadata.setRequiredProperties(parseVmProperty(requiredProperties));
                if (fileMapping != null) vmMetadata.setFileMappings(parseVmFileMapping(fileMapping));
            }
        }catch (DocumentException e) {
            throw new IllegalArgumentException("模板元数据配置文件解析失败");
        }
        return vmMetadata;
    }
    private List<VmProperty> parseVmProperty(Element root){
        List<VmProperty> vmProperties = new ArrayList<>();
        List<Element> properties = root.elements("property");
        boolean list = false;
        int i =1;
        do{
            for (Element property:properties) {
                String key = property.attribute("key").getValue();
                VmProperty vmPropertie = VmProperty.builder().key(key).list(list).build();
                Element caption = property.element("caption");
                Element defaultValue = property.element("defaultValue");
                Attribute required = property.attribute("required");
                Element elmentMetadata = property.element("elementMetadata");

                if(caption!=null) vmPropertie.setCaption(caption.getText());
                if(required!=null) vmPropertie.setRequired(Boolean.parseBoolean(required.getText()));
                if(defaultValue!=null) vmPropertie.setDefaultValue(defaultValue.getText());
                if(elmentMetadata!=null) {
                    List<VmProperty> elmentMetadataProperty = parseVmProperty(elmentMetadata);
                    vmPropertie.setElementMetadata(elmentMetadataProperty);
                }
                vmProperties.add(vmPropertie);
            }
            properties = root.elements("properties");
            list = true;
        }while(i-->0);
        return vmProperties;
    }
    private Map<String,String> parseVmFileMapping(Element root){
        List<Element> nodes = root.elements("fileMapping");
        Map<String,String> fileMappings = new HashMap<>(16);
        for(Element node:nodes){
            Element filePath = node.element("filePath");
            Element pattern = node.element("pattern");
            if(filePath == null || pattern == null){
                continue;
            }
            fileMappings.put(filePath.getText(),pattern.getText());
        }
        return fileMappings;
    }


    public static void main(String[] args) {
        System.out.println(JSON.toJSONString(new XmlVmMetadataServiceImpl().selectList("D:\\template\\vms","模板4")));
    }
}
