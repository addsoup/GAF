package com.supermap.gaf.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateParamGrpEntity;
import com.supermap.gaf.project.commontype.utils.PageUtils;
import com.supermap.gaf.project.commontype.vo.GroupWithParamsVo;

import java.util.List;
import java.util.Map;

/**
 * 代码模板参数分组表
 *
 * @author wxl
 * @email wxl@gmail.com
 * @date 2020-09-07 10:53:45
 */
public interface ProjCodeTemplateParamGrpService extends IService<ProjCodeTemplateParamGrpEntity> {
    /**
     * 分页条件查询代码模板参数分组
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 更新代码模板参数分组表
     * @param projCodeTemplateParamGrpEntity
     */
    void updateProjCodeTemplateParamGrp(ProjCodeTemplateParamGrpEntity projCodeTemplateParamGrpEntity);

    /**
     * 单字段条件 分页查询
     * @param searchFieldName 字段名
     * @param searchFieldValue 字段值
     * @param orderFieldName 排序字段名
     * @param orderMethod 排序字段值
     * @param pageNum 当前页
     * @param pageSize 页大小
     * @return 分页信息和查询到的数据
     */
    PageUtils queryPageCondition(String searchFieldName, String searchFieldValue, String orderFieldName, String orderMethod, Long pageNum, Long pageSize);

    /**
     * 根据模板id 查询代码模板参数分组和模板参数
     * @param projCodeTemplateId
     * @return
     */
    List<GroupWithParamsVo> getParamGroupAndParams(String projCodeTemplateId);

    /**
     * 新增代码模板参数分组
     * @param projCodeTemplateParamGrpEntity
     */
    void insertGroup(ProjCodeTemplateParamGrpEntity projCodeTemplateParamGrpEntity);

    /**
     * 根据模板Id查询参数分组信息
     * @param projCodeTemplateId 模板Id
     * @return 参数分组信息
     */
    List<ProjCodeTemplateParamGrpEntity> listGroupsByTemplateId(String projCodeTemplateId);

    /**
     * 级联删除分组及关联的参数
     * @param codeTemplateParamGrpId 参数分组id
     */
    void removeGrpAndParams(String codeTemplateParamGrpId);
}

