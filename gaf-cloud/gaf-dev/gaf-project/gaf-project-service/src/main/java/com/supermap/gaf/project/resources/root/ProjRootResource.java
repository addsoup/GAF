package com.supermap.gaf.project.resources.root;

import com.supermap.gaf.project.resources.*;
import com.supermap.gaf.rest.jersey.JaxrsStaticViewResource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Component
@Path("/")
@Api(tags = "代码模板组件")
public class ProjRootResource {
    @Path("/hello")
    @GET
    @Produces(APPLICATION_JSON)
    public String hello(){
        return "hello i am project";
    }

    @Path("/table-code")
    public Class<TableCodeResource> tableCodeResource() {
        return TableCodeResource.class;
    }

    @Path("/view")
    @ApiOperation(value = "界面", hidden = true)
    public Class<JaxrsStaticViewResource> staticViewResoruce() {
        return JaxrsStaticViewResource.class;
    }

    @Path("/projcodetemplate")
    public Class<ProjCodeTemplateResource> projCodeTemplateResource() {
        return ProjCodeTemplateResource.class;
    }

    @Path("/projcodetemplateparam")
    public Class<ProjCodeTemplateParamResource> projCodeTemplateParamResource() {
        return ProjCodeTemplateParamResource.class;
    }

    @Path("/paramgroup")
    public Class<ProjCodeTemplateParamGrpResource> projCodeTemplateParamGrpResource() {
        return ProjCodeTemplateParamGrpResource.class;
    }

}
