package com.supermap.gaf.project.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.supermap.gaf.project.commontype.entity.BatchHandleParamsEntity;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateParamEntity;
import com.supermap.gaf.project.commontype.utils.Constant;
import com.supermap.gaf.project.commontype.utils.PageUtils;
import com.supermap.gaf.project.commontype.utils.Query;
import com.supermap.gaf.project.dao.ProjCodeTemplateParamDao;
import com.supermap.gaf.project.service.ProjCodeTemplateParamService;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.utils.LogUtil;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Service("projCodeTemplateParamService")
public class ProjCodeTemplateParamServiceImpl extends ServiceImpl<ProjCodeTemplateParamDao, ProjCodeTemplateParamEntity> implements ProjCodeTemplateParamService {
    /**
     * 数据库模板参数表中的字段名，该字段名表示参数分组的Id
     */
    public static final String PARAM_GROUP_ID = "param_group_id";
    private static Logger logger = LogUtil.getLocLogger(ProjCodeTemplateParamServiceImpl.class);

    @Override
    public PageUtils queryPageCondition(String searchFieldName, String searchFieldValue, String orderFieldName, String orderMethod, Long pageNum, Long pageSize) {
        QueryWrapper<ProjCodeTemplateParamEntity> queryWrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(searchFieldName)) {
            queryWrapper.like(searchFieldName,searchFieldValue);
        }
        if (!StringUtils.isEmpty(orderFieldName)) {
            if (Constant.ASC.equalsIgnoreCase(orderMethod)) {
                queryWrapper.orderByAsc(orderFieldName);
            }else if (Constant.DESC.equalsIgnoreCase(orderMethod)) {
                queryWrapper.orderByDesc(orderFieldName);
            }
        }
        IPage<ProjCodeTemplateParamEntity> page = this.page(
                new Query<ProjCodeTemplateParamEntity>().getPage(orderFieldName, orderMethod, pageNum, pageSize),
                queryWrapper
        );
        return new PageUtils(page);
    }

    @Override
    public List<ProjCodeTemplateParamEntity> getByGroupId(String codeTemplateParamGrpId) {
        QueryWrapper<ProjCodeTemplateParamEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(PARAM_GROUP_ID,codeTemplateParamGrpId);
        return this.list(wrapper);
    }

    @Override
    public void insertBatch(List<ProjCodeTemplateParamEntity> projCodeTemplateParamEntites) {
        if (projCodeTemplateParamEntites!=null&& projCodeTemplateParamEntites.size()>0) {
            String userName = SecurityUtilsExt.getUserName();
            projCodeTemplateParamEntites.stream().forEach(param -> {
                Date date = new Date();
                param.setProjCodeTemplateParamId(null);
                param.setCreatedTime(date);
                param.setCreatedBy(userName);
                param.setUpdatedBy(userName);
                param.setUpdatedTime(date);
            });
            this.saveBatch(projCodeTemplateParamEntites);
        }
    }

    @Override
    public void insertOrUpdateBatch(List<ProjCodeTemplateParamEntity> projCodeTemplateParamEntites) {
        if (projCodeTemplateParamEntites.isEmpty()) return;
        if (projCodeTemplateParamEntites!=null&& projCodeTemplateParamEntites.size()>0) {
            String userName = SecurityUtilsExt.getUserName();
            projCodeTemplateParamEntites.stream().forEach(param -> {
                if (param.getProjCodeTemplateParamId() == null){
                    Date date = new Date();
                    param.setProjCodeTemplateParamId(null);
                    param.setCreatedTime(date);
                    param.setCreatedBy(userName);
                    param.setUpdatedBy(userName);
                    param.setUpdatedTime(date);
                }else {
                    Date date = new Date();
                    param.setUpdatedBy(userName);
                    param.setUpdatedTime(date);
                }
            });
            this.saveOrUpdateBatch(projCodeTemplateParamEntites);
        }
    }

    @Override
    public Boolean batchHandleParams(BatchHandleParamsEntity batchHandleParamsEntity) {
        if (batchHandleParamsEntity ==null) return false;
        List<String> projCodeTemplateParamIds = batchHandleParamsEntity.getDeleteIdsLst();
        List<ProjCodeTemplateParamEntity> projCodeTemplateParamEntites = batchHandleParamsEntity.getTemplateParamsLst();
        if (!projCodeTemplateParamEntites.isEmpty()){
            try{insertOrUpdateBatch(projCodeTemplateParamEntites);}
            catch (Exception e){
                logger.error("批量新增模板或修改参数操作失败");
                logger.error(e.getMessage());
                return false;
            }
            logger.info("批量新增模板或修改参数操作成功");
        }
        if (!projCodeTemplateParamIds.isEmpty()){
            try{removeByIds(projCodeTemplateParamIds);}
            catch (Exception e){
                logger.error("批量删除模板参数操作失败");
                logger.error(e.getMessage());
                return false;
            }
            logger.info("批量删除模板参数操作成功");
        }
        return true;
    }

    @Override
    public void deleteByGroupIds(List<String> collect) {
        QueryWrapper<ProjCodeTemplateParamEntity> wrapper = new QueryWrapper<ProjCodeTemplateParamEntity>().in(PARAM_GROUP_ID, collect);
        this.remove(wrapper);
    }

    @Override
    public List<ProjCodeTemplateParamEntity> getByparamGroupId(String paramGroupId) {
        QueryWrapper<ProjCodeTemplateParamEntity> wrapper = new QueryWrapper<ProjCodeTemplateParamEntity>().eq(PARAM_GROUP_ID, paramGroupId);
        return this.list(wrapper);
    }

}