package com.supermap.gaf.project.resources;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateParamGrpEntity;
import com.supermap.gaf.project.commontype.utils.PageUtils;
import com.supermap.gaf.project.commontype.vo.GroupWithParamsVo;
import com.supermap.gaf.project.service.ProjCodeTemplateParamGrpService;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Component
@Api(value = "模板参数分组 rest接口")
public class ProjCodeTemplateParamGrpResource{
    @Autowired
    private ProjCodeTemplateParamGrpService projCodeTemplateParamGrpService;


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "删除模板参数分组", notes = "删除模板参数分组")
	@Path("/{codeTemplateParamGrpId}")
    public MessageResult<Void> deleteProjCodeTemplateParamGrp(@NotEmpty  @PathParam("codeTemplateParamGrpId")String codeTemplateParamGrpId){
        projCodeTemplateParamGrpService.removeGrpAndParams(codeTemplateParamGrpId);
        return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }


    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "更新模板参数分组", notes = "更新模板参数分组")
	@Path("/{codeTemplateParamGrpId}")
    public MessageResult<Void> updateProjCodeTemplateParamGrp(ProjCodeTemplateParamGrpEntity projCodeTemplateParamGrpEntity,@NotNull @PathParam("codeTemplateParamGrpId")String codeTemplateParamGrpId){
        projCodeTemplateParamGrpEntity.setCodeTemplateParamGrpId(codeTemplateParamGrpId);
        projCodeTemplateParamGrpService.updateProjCodeTemplateParamGrp(projCodeTemplateParamGrpEntity);
        return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }

 
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "id查询模板参数分组", notes = "id查询模板参数分组")
	@Path("/{codeTemplateParamGrpId}")
    public MessageResult<ProjCodeTemplateParamGrpEntity> selectById(@PathParam("codeTemplateParamGrpId")String codeTemplateParamGrpId){
        ProjCodeTemplateParamGrpEntity projCodeTemplateParamGrpEntity = projCodeTemplateParamGrpService.getById(codeTemplateParamGrpId);
        return MessageResult.successe(ProjCodeTemplateParamGrpEntity.class).data(projCodeTemplateParamGrpEntity).status(200).message("查询成功").build();
    }


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除模板参数分组", notes = "批量删除模板参数分组")
    public  MessageResult<Void> batchDelete(List<String> codeTemplateParamGrpIds){
        projCodeTemplateParamGrpService.removeByIds(codeTemplateParamGrpIds);
        return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增模板参数分组", notes = "新增模板参数分组")
    public MessageResult<Void> insertProjCodeTemplateParamGrp(ProjCodeTemplateParamGrpEntity projCodeTemplateParamGrpEntity){
        projCodeTemplateParamGrpService.insertGroup(projCodeTemplateParamGrpEntity);
        return MessageResult.successe(Void.class).status(200).message("新增模板参数分组操作成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "条件查询模板参数分组", notes = "条件查询模板参数分组")
    public MessageResult<PageUtils> pageList(@StringRange(entityClass = ProjCodeTemplateParamGrpEntity.class) @QueryParam("searchFieldName")String searchFieldName,
                                       @QueryParam("searchFieldValue")String searchFieldValue,
                                       @StringRange(entityClass = ProjCodeTemplateParamGrpEntity.class) @QueryParam("orderFieldName")String orderFieldName,
                                       @StringRange(value = {"asc", "desc"},ignoreCase = true) @QueryParam("orderMethod")String orderMethod,
                                       @QueryParam("pageNum")Long pageNum,
                                       @DefaultValue("50")@QueryParam("pageSize")Long pageSize){
        PageUtils pageUtils = projCodeTemplateParamGrpService.queryPageCondition(searchFieldName, searchFieldValue, orderFieldName, orderMethod, pageNum, pageSize);
        return MessageResult.successe(PageUtils.class).data(pageUtils).status(200).message("查询成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    @ApiOperation(value = "批量新增模板参数分组", notes = "批量新增模板参数分组")
    public MessageResult<Void> batchInsert(List<ProjCodeTemplateParamGrpEntity> projCodeTemplateParamGrpEntites){
        projCodeTemplateParamGrpService.saveBatch(projCodeTemplateParamGrpEntites);
        return MessageResult.successe(Void.class).status(200).message("批量新增模板参数分组操作成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/groupandparam/{projCodeTemplateId}")
    @ApiOperation(value = "根据模板id分页查询其所属的参数分组及参数信息")
    public MessageResult<List> getParamGroupAndParams(@NotNull @PathParam("projCodeTemplateId") String projCodeTemplateId){
        List<GroupWithParamsVo> groupWithParamsVos = projCodeTemplateParamGrpService.getParamGroupAndParams(projCodeTemplateId);
        return MessageResult.successe(List.class).data(groupWithParamsVos).status(200).message("查询参数分组及参数信息成功").build();

    }
}
