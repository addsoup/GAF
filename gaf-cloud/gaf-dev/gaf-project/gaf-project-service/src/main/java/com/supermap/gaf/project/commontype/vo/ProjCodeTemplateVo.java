package com.supermap.gaf.project.commontype.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Data
@ApiModel("带参数的工程模板信息")
public class ProjCodeTemplateVo {
    /**
     * 代码模板id。主键,uuid
     */
    @ApiModelProperty(value = "代码模板id",required = true)
    private String projCodeTemplateId;
    /**
     * 模板名称。
     */
    @ApiModelProperty(value = "模板名称",required = true)
    private String templateName;
    /**
     * 模板中文名称。
     */
    @ApiModelProperty(value = "模板中文名称",required = true)
    private String templateNameCn;
}
