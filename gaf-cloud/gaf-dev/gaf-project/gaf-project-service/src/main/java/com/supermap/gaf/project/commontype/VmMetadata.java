package com.supermap.gaf.project.commontype;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel("模板信息数据对象")
public class VmMetadata {
    @ApiModelProperty("模板名称")
    private String name;
    @ApiModelProperty("别名")
    private String caption;
    @ApiModelProperty("描述")
    private String description;
    @ApiModelProperty("多个属性配置对象")
    private List<VmProperty> requiredProperties;
    @ApiModelProperty("文件名映射配置")
    private Map<String,String> fileMappings;
}
