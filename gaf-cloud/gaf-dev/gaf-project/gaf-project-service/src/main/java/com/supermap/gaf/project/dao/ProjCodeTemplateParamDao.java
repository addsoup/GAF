package com.supermap.gaf.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateParamEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * 
 * @author wxl
 * @email wxl@gmail.com
 * @date 2020-09-04 14:46:24
 */
@Mapper
public interface ProjCodeTemplateParamDao extends BaseMapper<ProjCodeTemplateParamEntity> {
	
}
