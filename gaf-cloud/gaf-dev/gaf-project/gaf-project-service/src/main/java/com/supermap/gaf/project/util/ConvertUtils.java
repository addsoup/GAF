package com.supermap.gaf.project.util;

import com.supermap.gaf.project.commontype.DatasourceConnParam;
import com.supermap.gaf.project.commontype.JdbcConnectionInfo;
import org.springframework.util.StringUtils;

/**
* @author:yw
* @Date 2021/3/17
**/
public class ConvertUtils {
    public static JdbcConnectionInfo convert2JdbcConnectionInfo(DatasourceConnParam datasourceConnParam){
        String url = "";
        switch (datasourceConnParam.getDatasourceType()){
            case POSTGRESQL:{
                url = "jdbc:postgresql://"+datasourceConnParam.getHost()+":"+datasourceConnParam.getPort()+"/"+datasourceConnParam.getDatabaseOrServiceName();
                if(!StringUtils.isEmpty(datasourceConnParam.getSchema())){
                    url+="?"+"searchpath="+datasourceConnParam.getSchema()+"&"+"currentSchema="+datasourceConnParam.getSchema();
                }
                break;
            }
            case MYSQL:{
                url ="jdbc:mysql://"+datasourceConnParam.getHost()+":"+datasourceConnParam.getPort()+"/"+datasourceConnParam.getDatabaseOrServiceName()+"?serverTimezone=UTC";

                break;
            }
            case ORACLE:{
               url ="jdbc:oracle:thin:@//"+datasourceConnParam.getHost()+":"+datasourceConnParam.getPort()+"/"+datasourceConnParam.getDatabaseOrServiceName();
                break;
            }
            case SQLSERVER:{
                url = "jdbc:sqlserver://"+datasourceConnParam.getHost()+":"+datasourceConnParam.getPort()+";databaseName="+datasourceConnParam.getDatabaseOrServiceName();
                break;
            }
            default:{
                throw new IllegalArgumentException("不允许的数据源类型:"+datasourceConnParam.getDatasourceType().name());
            }
        }
        return JdbcConnectionInfo.builder()
                .url(url)
                .username(datasourceConnParam.getUsername())
                .password(datasourceConnParam.getPassword())
                .build();
    }
}
