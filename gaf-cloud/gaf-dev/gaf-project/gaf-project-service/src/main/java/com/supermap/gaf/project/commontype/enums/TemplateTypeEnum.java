package com.supermap.gaf.project.commontype.enums;

/**
 * 模板类型枚举
 * @author wangxiaolong
 */
public enum TemplateTypeEnum {
    /**
     * 工程模板-前端
     */
    PROJECT_FRONT(11, "工程模板-前端"),
    /**
     * 工程模板-后端
     */
    PROJECT_BACK(12, "工程模板-后端"),
    /**
     * 工程模板-前后端
     */
    PROJECT_FRONT_BACK(13, "工程模板-前后端"),
    /**
     * 代码模板-前端
     */
    CODE_FRONT(21, "代码模板-前端"),
    /**
     * 代码模板-后端
     */
    CODE_BACK(22, "代码模板-后端"),
    /**
     * 代码模板-前后端
     */
    CODE_FRONT_BACK(23, "代码模板-前后端");
    /**
     * 模板类型编码
     */
    private int type;
    /**
     * 模板类型说明
     */
    private String description;

    TemplateTypeEnum(int type, String description) {
        this.type = type;
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public static String typeNumToDescription(int typeNum) {
        if (PROJECT_FRONT.getType() == typeNum) {
            return PROJECT_FRONT.getDescription();
        }
        if (PROJECT_BACK.getType() == typeNum) {
            return PROJECT_BACK.getDescription();
        }
        if (PROJECT_FRONT_BACK.getType() == typeNum) {
            return PROJECT_FRONT_BACK.getDescription();
        }
        if (CODE_FRONT.getType() == typeNum) {
            return CODE_FRONT.getDescription();
        }
        if (CODE_BACK.getType() == typeNum) {
            return CODE_BACK.getDescription();
        }
        if (CODE_FRONT_BACK.getType() == typeNum) {
            return CODE_FRONT_BACK.getDescription();
        }
        throw new IllegalArgumentException("未查找到该模板类型");
    }

    public static int descriptionToTypeNum(String description) {
        if (PROJECT_FRONT.getDescription().equalsIgnoreCase(description)) {
            return PROJECT_FRONT.getType();
        }
        if (PROJECT_BACK.getDescription().equalsIgnoreCase(description)) {
            return PROJECT_BACK.getType();
        }
        if (PROJECT_FRONT_BACK.getDescription().equalsIgnoreCase(description)) {
            return PROJECT_FRONT_BACK.getType();
        }
        if (CODE_FRONT.getDescription().equalsIgnoreCase(description)) {
            return CODE_FRONT.getType();
        }
        if (CODE_BACK.getDescription().equalsIgnoreCase(description)) {
            return CODE_BACK.getType();
        }
        if (CODE_FRONT_BACK.getDescription().equalsIgnoreCase(description)) {
            return CODE_FRONT_BACK.getType();
        }
        throw new IllegalArgumentException("未查找到该模板类型");
    }
}
