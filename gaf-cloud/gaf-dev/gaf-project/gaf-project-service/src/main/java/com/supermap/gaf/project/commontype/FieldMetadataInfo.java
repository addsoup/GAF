package com.supermap.gaf.project.commontype;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
* @author:yw
* @Date 2021/3/17
**/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel("字段元数据信息")
public class FieldMetadataInfo {
    @ApiModelProperty(value = "字段名")
    private String fieldName;
    @ApiModelProperty(value = "小写驼峰字段名")
    private String fieldLowCamelName;
    @ApiModelProperty(value = "大写驼峰字段名")
    private String fieldUpperCamelName;
    @ApiModelProperty(value = "sql类型")
    private String sqlType;
    @ApiModelProperty(value = "jdbc类型")
    private String jdbcDataType;
    @ApiModelProperty(value = "java类型")
    private String javaType;
    @JSONField(name="isNullable")
    @ApiModelProperty(value = "是否不能为空")
    private Boolean isNullable = false;
    @ApiModelProperty(value = "注释")
    private String remarks;
    @ApiModelProperty(value = "默认值")
    private String defaultValue;
    @ApiModelProperty(value = "字段长度")
    private Integer columnSize;
    @JSONField(name="isPrimaryKey")
    @ApiModelProperty(value = "是否外键")
    private Boolean isPrimaryKey = false;
    @JSONField(name="isAutoincrement")
    @ApiModelProperty(value = "是否自增")
    private Boolean isAutoincrement = false;
}
