package com.supermap.gaf.portal.entity.vo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;
import javax.validation.constraints.*;
import com.alibaba.fastjson.annotation.JSONField;

import javax.ws.rs.QueryParam;

/**
 * 用户消息通知表 条件查询实体
 * @author zrc
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户消息通知表条件查询实体")
public class UserMessageSelectVo {
    @QueryParam("searchFieldName")
    @ApiModelProperty("模糊查询字段名")
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("模糊查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    private String orderMethod;
    @QueryParam("userMessageId")
    @ApiModelProperty("主键")
    private String userMessageId;
    @QueryParam("userName")
    @ApiModelProperty("用户名")
    private String userName;
    @QueryParam("type")
    @ApiModelProperty("类型")
    private String type;
    @QueryParam("typeLink")
    @ApiModelProperty("类型链接")
    private String typeLink;
    @QueryParam("userMessageContent")
    @ApiModelProperty("消息")
    private String userMessageContent;
    @QueryParam("userMessageStatus")
    @ApiModelProperty("未读已读状态")
    private Boolean userMessageStatus;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("更新人")
    private String updatedBy;
}