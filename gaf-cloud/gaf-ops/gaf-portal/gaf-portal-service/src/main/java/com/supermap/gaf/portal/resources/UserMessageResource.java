package com.supermap.gaf.portal.resources;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.portal.entity.po.UserMessage;
import com.supermap.gaf.portal.entity.vo.UserMessageSelectVo;
import com.supermap.gaf.portal.service.UserMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 用户消息通知表接口
 * @author zrc 
 * @date yyyy-mm-dd
 * /portal/user/message
 */
@Component
@Api(value = "用户消息通知表接口")
public class UserMessageResource{
    @Autowired
    private UserMessageService userMessageService;

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询用户消息通知表", notes = "通过id查询用户消息通知表")
	@Path("/{userMessageId}")
    public MessageResult<UserMessage> getById(@PathParam("userMessageId")String userMessageId){
        UserMessage userMessage = userMessageService.getById(userMessageId);
		return MessageResult.data(userMessage).message("查询成功").build();
    }
	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "查询用户消息通知", notes = "查询用户消息通知")
    public MessageResult<List<UserMessage>> pageList(@QueryParam("userName")String userName){
        UserMessageSelectVo selectVo = UserMessageSelectVo.builder().userName(userName).userMessageStatus(false).build();
        List<UserMessage> result = userMessageService.listByCondition(selectVo);
		return MessageResult.data(result).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增用户消息通知表", notes = "新增用户消息通知表")
    public MessageResult<Void> insertUserMessage(UserMessage userMessage){
        userMessageService.insertUserMessage(userMessage);
		return MessageResult.successe(Void.class).status(200).message("新增操作成功").build();
    }
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    @ApiOperation(value = "批量新增用户消息通知表", notes = "批量新增用户消息通知表")
    public MessageResult<Void> batchInsert(List<UserMessage> UserMessages){
        userMessageService.batchInsert(UserMessages);
		return MessageResult.successe(Void.class).status(200).message("批量新增操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除用户消息通知表", notes = "根据id删除用户消息通知表")
	@Path("/{userMessageId}")
    public MessageResult<Void> deleteUserMessage(@PathParam("userMessageId")String userMessageId){
        userMessageService.deleteUserMessage(userMessageId);
		return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除用户消息通知表", notes = "批量删除用户消息通知表")
    public MessageResult<Void> batchDelete(List<String> userMessageIds){
        userMessageService.batchDelete(userMessageIds);
		return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新用户消息通知表", notes = "根据id更新用户消息通知表")
	@Path("/{userMessageId}")
    public MessageResult<Void> updateUserMessage(UserMessage userMessage,@PathParam("userMessageId")String userMessageId){
        userMessage.setUserMessageId(userMessageId);
        userMessageService.updateUserMessage(userMessage);
		return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }

	
	


}
