package com.supermap.gaf.portal.service;

import com.supermap.gaf.portal.entity.po.UserMessage;
import com.supermap.gaf.portal.entity.vo.UserMessageSelectVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : duke
 * @since 2021/12/21 4:16 PM
 */
@Slf4j
@Service
public class StompService {
    private static final String BELL_MSG_DEST = "/bellMsg";
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    /**
     * 向目的地/user/{username}/BELL_MSG_DEST 发送即时消息
     * @param username
     * @param userMessageList
     * @return
     */
    public void sendBellMsg(String username, List<UserMessage> userMessageList) {
        try {
            simpMessagingTemplate.convertAndSendToUser(username, BELL_MSG_DEST, userMessageList);
        }catch (Exception e){
            log.error("sendBellMsg failed with username:" + username);
        }
    }

}
