package com.supermap.gaf.authority.service.impl;

import com.supermap.gaf.authority.commontype.AuthResourceApi;
import com.supermap.gaf.authority.commontype.AuthRole;
import com.supermap.gaf.authority.commontype.AuthUserInfoDetails;
import com.supermap.gaf.authority.commontype.IauthUserInfoDetails;
import com.supermap.gaf.authority.entity.AuthorizationParam;
import com.supermap.gaf.authority.enums.ResourceApiMethodEnum;
import com.supermap.gaf.authority.service.AuthorizationService;
import com.supermap.gaf.authority.util.AuthUtil;
import com.supermap.gaf.authority.vo.AuthUserSupplementVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

import java.util.List;
import java.util.Locale;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {

    @Autowired
    private IauthUserInfoDetails iauthUserInfoDetails;

    @Override
    public Boolean authorization(AuthorizationParam authorizationParam) {
        AuthUserInfoDetails authUserInfoDetails = iauthUserInfoDetails.getAuthUserInfoDetails(authorizationParam.getUsername());
        AuthUserSupplementVo authUser = authUserInfoDetails.getAuthUser();
        Boolean state = authUser.getState();
        if(state != null &&  !state) {
            return false;
        }

        if (AuthUtil.isAdmin(authUserInfoDetails.getAuthRoleList())) {
            return true;
        }

        List<AuthResourceApi> authResourceApiList = authUserInfoDetails.getAuthResourceApiList();
        return accessValid(authorizationParam, authResourceApiList);
    }

    /**
     * 匹配uri和权限列表
     *
     * @return
     */
    private boolean accessValid(AuthorizationParam authorizationParam, List<AuthResourceApi> authResourceApis) {
        AntPathMatcher matcher = new AntPathMatcher();
        String uri = authorizationParam.getUri();
        if (uri != null && uri.startsWith("/api")) {
            uri = uri.replaceFirst("/api","");
        }
        String method = authorizationParam.getMethod();
        ResourceApiMethodEnum resourceApiMethodEnum = ResourceApiMethodEnum.valueOf(method.toUpperCase(Locale.ROOT));
        String methodValue = resourceApiMethodEnum.getValue();
        for (AuthResourceApi authResourceApi : authResourceApis) {
            String path = authResourceApi.getRouteUrl();
            if (matcher.match(path, uri) && methodValue.equalsIgnoreCase(authResourceApi.getMethod())) {
                return true;
            }
        }
        return false;
    }

}
