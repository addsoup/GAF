package com.supermap.gaf.authority.resources;

import com.supermap.gaf.authority.client.AuthorizationClient;
import com.supermap.gaf.authority.entity.AuthorizationParam;
import com.supermap.gaf.authority.service.AuthorizationService;
import com.supermap.gaf.commontypes.MessageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Component
@Api(value = "鉴权接口")
public class AuthorizationResource implements AuthorizationClient {

    @Autowired
    private AuthorizationService authorizationService;

    @ApiOperation(value = "api鉴权", notes = "对用户是否拥有api资源访问权限进行鉴权")
    @POST
    @Produces(APPLICATION_JSON)
    @Override
    public MessageResult<Boolean> authorization(AuthorizationParam authorizationParam) {
        Boolean re = authorizationService.authorization(authorizationParam);
        return MessageResult.data(re).build();
    }

}
