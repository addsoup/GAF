package com.supermap.gaf.authority.service;

import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.authority.commontype.AuthUserSupplement;
import com.supermap.gaf.authority.vo.AuthUserSupplementSelectVo;
import com.supermap.gaf.authority.vo.AuthUserSupplementVo;
import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.platform.entity.vo.AuthUserSelectVo;

import java.util.List;

/**
 * 用户信息补充表服务类
 * @author zrc
 * @date yyyy-mm-dd
 */
public interface AuthUserSupplementService {

	/**
	 * 查询某租户下的部门用户树
	 *
	 * @return 查询部门用户树 返回的节点已组织为树形结构
	 */
	List<TreeNode> getUserTree();


	/**
	 * 查询某岗位下的用户
	 *
	 * @param postId 岗位id
	 * @return 用户集合
	 */
	List<AuthUserSupplementVo> listUserByPost(String postId);



	/**
	 * 根据部门id查询用户 并携带部门名和岗位名
	 *
	 * @param departmentId 部门id
	 * @return 用户集合
	 */
	List<AuthUserSupplementVo> listUserByDepartmentWithName(String departmentId);


	/**
	 * 可能没没用了
	 * 获取当前用户的信息
	 *
	 * @return 用户
	 */
	AuthUserSupplementVo getUserInfo();

	/**
    * id查询用户信息补充表
    * @return
    */
	AuthUserSupplement getById(String userId);

	/**
     * 分页条件查询
     * @param authUserSupplementSelectVo 查询条件
     * @param pageNum 当前页数
     * @param pageSize 页面大小
     * @return 分页对象
     */
	Page<AuthUserSupplement> listByPageCondition(AuthUserSupplementSelectVo authUserSupplementSelectVo, int pageNum, int pageSize);

	/**
	 * 多条件查询
	 * @param authUserSupplementSelectVo 查询条件
	 * @return 若未查询到则返回空集合
	 */
	List<AuthUserSupplement> selectList(AuthUserSupplementSelectVo authUserSupplementSelectVo);


		/**
         * 新增用户信息补充表
         * @return
         */
    void insertAuthUserSupplement(AuthUserSupplement authUserSupplement);

    /**
     * 删除用户信息补充表
     *
     */
    int deleteAuthUserSupplement(String userId);

    /**
     * 批量删除
     *
	 */
    int batchDelete(List<String> userIds);

    /**
     * 更新用户信息补充表
     * @return
     */
    int updateAuthUserSupplement(AuthUserSupplement authUserSupplement);

	/**
	 * 根据用户id获取用户及其补充信息
	 * @param userId
	 * @return
	 */
	AuthUserSupplementVo getAuthUserSupplementVoByUserId(String userId);



	/**
	 * AuthUserSupplement的数据通过切换数据源查询用户信息表补充完整
	 * @param authUserSupplement
	 * @return
	 */
	AuthUserSupplementVo getAuthUserSupplementVoBySupplement(AuthUserSupplement authUserSupplement);

	/**
	 * AuthUser的数据通过切换数据源查询用户补充信息表补充完整
	 * @param authUser
	 * @return
	 */
	AuthUserSupplementVo getAuthUserSupplementVoByAuthUser(AuthUser authUser);

	/**
	 * AuthUserSupplement的数据通过切换数据源查询用户信息表补充完整
	 * @param authUserSupplementList
	 * @return
	 */
	List<AuthUserSupplementVo> listAuthUserSupplementVoBySupplement(List<AuthUserSupplement> authUserSupplementList);

	/**
	 * AuthUser的数据通过切换数据源查询用户补充信息表补充完整
	 * @param authUserList
	 * @return
	 */
	List<AuthUserSupplementVo> listAuthUserSupplementVoByAuthUser(List<AuthUser> authUserList);



	/**
	 *
	 * @param userId
	 * @return
	 */
	AuthUserSupplementVo getMemberInfo(String userId);

	/**
	 * 分页条件查询用户及其补充信息
	 * @param authUserSearchVo
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
    Page<AuthUserSupplementVo> page(AuthUserSelectVo authUserSearchVo, Integer pageNum, Integer pageSize);

	/**
	 * 新增用户及其补充信息
	 * @param userSupplementVo
	 */
	void saveUser(AuthUserSupplementVo userSupplementVo);

	/**
	 * 更新用户及其补充信息
	 * @param userSupplementVo
	 */
	void updateUser(AuthUserSupplementVo userSupplementVo);


	/**
	 * 删除用户及其补充信息
	 * @param userId
	 */
	void reomveUser(String userId);

	/**
	 *
	 * @param userIds
	 */
    void reomveUsers(List<String> userIds);
}
