package com.supermap.gaf.authority.dao;

import com.supermap.gaf.authority.commontype.AuthUserSupplement;
import com.supermap.gaf.authority.vo.AuthUserSupplementSelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

/**
 * 用户信息补充表数据访问类
 * @author zrc
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface AuthUserSupplementMapper{
	/**
     * 根据主键 userId 查询
     *
	 */
    AuthUserSupplement select(@Param("userId") String userId);

	/**
	 * 根据主键 userIdList 查询
	 *
	 */
	List<AuthUserSupplement> batchSelect(@Param("userIdList") List<String> userIdList);

	/**
     * 多条件查询
     * @param authUserSupplementSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<AuthUserSupplement> selectList(AuthUserSupplementSelectVo authUserSupplementSelectVo);

    /**
     * 新增
     *
	 */
    void insert(AuthUserSupplement authUserSupplement);

	/**
     * 批量插入
     *
	 */
	void batchInsert(@Param("list") Collection<AuthUserSupplement> authUserSupplements);

	/**
     * 批量删除
     *
	 */
    int batchDelete(@Param("list") Collection<String> userIds);

	/**
     * 刪除
     *
	 */
    int delete(@Param("userId") String userId);

    /**
    * 更新
    *
    **/
    int update(AuthUserSupplement authUserSupplement);
	/**
	 * 选择性更新
	 *
	 **/
	int updateSelective(AuthUserSupplement authUserSupplement);

}
