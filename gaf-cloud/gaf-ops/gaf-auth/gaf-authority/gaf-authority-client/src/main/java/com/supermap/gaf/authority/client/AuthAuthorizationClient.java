/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authority.client;

import com.supermap.gaf.authority.commontype.AuthResourceMenu;
import com.supermap.gaf.commontypes.MessageResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;


/**
 * 用户的权限资源信息查询
 *
 * @author dqc
 */
@FeignClient(name = "GAF-AUTHORITY")
public interface AuthAuthorizationClient {
    /**
     * 查询当前用户的菜单
     *
     * @return
     */
    @GetMapping("/authority/auth-authorizations/menus")
    MessageResult<List<AuthResourceMenu>> listAuthResourceMenus();

}
