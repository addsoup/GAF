/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authentication.entity.entity.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


/**
 * @author : duke
 * @date:2021/3/25
 * @since 2020/11/16 10:57 AM
 */
@ConfigurationProperties(prefix = "login")
@Component
@RefreshScope
public class LoginProperties {
    public static final int THIRDPARTY_PROPERTY_LEVEL = 2;
    private static final Logger logger = LoggerFactory.getLogger(LoginProperties.class);

    private Boolean enableThirdParty = false;

    private String thirdPartyLoginModel = "mutiple";

    /**
     * 当使用single登录模式时使用
     * eg: oidc:keycloak
     */
    private String thirdPartyName = "oidc:keycloak";

    public Boolean getEnableThirdParty() {
        return enableThirdParty;
    }

    public void setEnableThirdParty(Boolean enableThirdParty) {
        this.enableThirdParty = enableThirdParty;
    }

    public String getThirdPartyLoginModel() {
        return thirdPartyLoginModel;
    }

    public void setThirdPartyLoginModel(String thirdPartyLoginModel) {
        this.thirdPartyLoginModel = thirdPartyLoginModel;
    }

    public String getThirdPartyName() {
        return thirdPartyName;
    }

    public void setThirdPartyName(String thirdPartyName) {
        this.thirdPartyName = thirdPartyName;
    }


    @PostConstruct
    public void validate() {
        if (!enableThirdParty) {
            return;
        }
        if (!"single".equalsIgnoreCase(thirdPartyLoginModel) && !"mutiple".equalsIgnoreCase(thirdPartyLoginModel) ) {
            logger.error("value of thirdPartyLoginModel must be 'single' or 'mutiple' ");
            return;
        }
        if ("single".equalsIgnoreCase(thirdPartyLoginModel) ) {
            try {
                String[] enableThirdParts = thirdPartyName.split(":");
                if (enableThirdParts.length != THIRDPARTY_PROPERTY_LEVEL) {
                    logger.error("thirdPartyName format error ," + "thirdPartyName must be like 'oidc:keyclaok',param after split having length is " + THIRDPARTY_PROPERTY_LEVEL);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(),e);
            }

        }

    }

}
