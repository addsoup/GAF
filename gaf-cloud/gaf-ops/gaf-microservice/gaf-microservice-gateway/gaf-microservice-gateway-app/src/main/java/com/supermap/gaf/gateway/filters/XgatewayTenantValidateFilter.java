package com.supermap.gaf.gateway.filters;

import com.supermap.gaf.commontypes.GafCommonConstant;
import com.supermap.gaf.commontypes.GafCommonUriConstant;
import com.supermap.gaf.extend.commontypes.AuthenticationResult;
import com.supermap.gaf.gateway.commontypes.ExchangeAuthenticationAttribute;
import com.supermap.gaf.gateway.util.GafFluxUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

import static com.supermap.gaf.commontypes.GafCommonConstant.SYSTEM_TENANT;
import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.*;

/**
 * 此拦截器的主要功能是：
 * 获取请求头中的租户信息
 * 如果是无法匿名访问的资源，则需要校验用户是否含有此租户
 * @author : duke
 * @since 2022/3/2 10:16 AM
 */
@Component
public class XgatewayTenantValidateFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ExchangeAuthenticationAttribute attribute = exchange.getAttribute(EXCHANGE_AUTHENTICATION_ATTRIBUTE_NAME);
        AuthenticationResult authenticationResult = attribute.getAuthenticationResult();
        String tenant = getTenantIdByRequest(exchange);
        //如果没带tenant租户请求头，默认为系统租户
        if (StringUtils.isBlank(tenant)){
            tenant = SYSTEM_TENANT;
        }
        exchange.getAttributes().put(TENANT_ATTRIBUTE_NAME,tenant);
        if (attribute.getIsPublicUrl()){
            return chain.filter(exchange);
        }else {
            //如果不是可匿名访问资源，需要校验一下用户是否在此租户
            List<String> tenantList = authenticationResult.getTenantList();
            if (CollectionUtils.contains(tenantList.iterator(), tenant)){
                return chain.filter(exchange);
            }else if (attribute.getUri().endsWith(GafCommonUriConstant.URI_TENANT_LIST)){
                //获取用户列表接口不拦截
                return chain.filter(exchange);
            }
            else {
                return GafFluxUtils.unAuth(exchange,"该用户不在此租户下： " + tenant);
            }
        }
    }

    /**
     * 请求头获取tenant
     * @return
     */
    private String getTenantIdByRequest(ServerWebExchange swe){
        ServerHttpRequest request = swe.getRequest();
        HttpCookie cookie = request.getCookies().getFirst(GafCommonConstant.TENANT_HEADER);
        return cookie == null ? null : cookie.getValue();
    }


    @Override
    public int getOrder() {
        return GATEWAY_TENANT_VALIDATE_FILTER_ORDER;
    }
}
