package com.supermap.gaf.gateway.filters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.factory.rewrite.ModifyRequestBodyGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.GATEWAY_REQUEST_CACHE_FILTER_ORDER;


@Component
public class XgatewayCacheRequestBodyFilter implements GlobalFilter, Ordered {
    public static final String MY_CACHED_REQUEST_DATA = "myCachedRequestData";
    @Autowired
    private ModifyRequestBodyGatewayFilterFactory modifyRequestBodyGatewayFilterFactory;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        if(XgatewayIserverFilter.needRequestBody(exchange)){
            GatewayFilter delegate = modifyRequestBodyGatewayFilterFactory.apply(new ModifyRequestBodyGatewayFilterFactory.Config()
                    .setRewriteFunction(byte[].class, byte[].class, (serverWebExchange, body)->{
                        serverWebExchange.getAttributes().put(MY_CACHED_REQUEST_DATA,body);
                        return Mono.just(body);
                    }));
            return delegate.filter(exchange,chain);
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return GATEWAY_REQUEST_CACHE_FILTER_ORDER;
    }
}
