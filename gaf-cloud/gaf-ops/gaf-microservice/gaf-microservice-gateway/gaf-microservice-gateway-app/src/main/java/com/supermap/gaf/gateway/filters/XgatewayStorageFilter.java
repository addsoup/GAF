package com.supermap.gaf.gateway.filters;

import com.supermap.gaf.authority.client.AuthUserSupplementClient;
import com.supermap.gaf.extend.commontypes.AuthenticationResult;
import com.supermap.gaf.gateway.commontypes.ExchangeAuthenticationAttribute;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Map;

import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.*;

/**
 * @author wxl
 * @since 2022/4/10
 */
@Component
public class XgatewayStorageFilter implements GlobalFilter, Ordered {

    private static final String STORAGE_FILTER_URL_REGIX = "^/api/storage/api/tenant[^/]*/.*";
    private static final String STORAGE_PERMISSION_HEADER = "PERMISSION";
    private static final String STORAGE_TENANTID_HEADER = "TENANT_ID";

    @Autowired
    private AuthUserSupplementClient authUserSupplementClient;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ExchangeAuthenticationAttribute attribute = exchange.getAttribute(EXCHANGE_AUTHENTICATION_ATTRIBUTE_NAME);
        if (attribute.getIsPublicUrl() || attribute.getIsProfileUrl()){
            return chain.filter(exchange);
        }

        AuthenticationResult authenticationResult = attribute.getAuthenticationResult();
        String uri = exchange.getRequest().getURI().getPath();
        if (uri.startsWith("/api/storage/")) {
            Map<String,Object> infos = authUserSupplementClient.someInfo(authenticationResult.getUsername()).getData();
            String roleIds = (String) infos.get(AuthUserSupplementClient.SOME_INFO_ROLE_IDS_KEY);
            String tenantId = exchange.getAttribute(TENANT_ATTRIBUTE_NAME);
            ServerHttpRequest.Builder reqBuilder = exchange.getRequest().mutate();
            reqBuilder.header(STORAGE_TENANTID_HEADER, tenantId);
            // 文件操作api跳过GAF api校验，走storage权限校验
            if(uri.matches(STORAGE_FILTER_URL_REGIX)){
                if(!StringUtils.isEmpty(roleIds)){
                    reqBuilder.header(STORAGE_PERMISSION_HEADER,roleIds);
                }
            }
            return chain.filter(exchange.mutate().request(reqBuilder.build()).build());
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return GATEWAY_STORAGE_FILTER_ORDER;
    }
}
