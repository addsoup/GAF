package com.supermap.gaf.gateway.configuration;

import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.extend.spi.ValidateAuthentication;
import com.supermap.gaf.extend.spi.ValidateAuthority;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * @author wxl
 * @since 2022/4/9
 */
@Configuration
@Slf4j
public class ValidateAuthSpiConfigration implements BeanDefinitionRegistryPostProcessor, EnvironmentAware {


    //@Value("#{spi-config.validate-authentication.provider-url}")
    String validateAuthenticationProviderUrl;
    //@Value("#{spi-config.validate-authentication.provider-class-name}")
    String validateAuthenticationProviderClassName;

    //@Value("#{spi-config.validate-authority.provider-url}")
    String validateAuthorityProviderUrl;
    //@Value("#{spi-config.validate-authority.provider-class-name}")
    String validateAuthorityProviderClassName;


    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        serviceLoadAndRegistry(registry, ValidateAuthentication.class, validateAuthenticationProviderClassName, validateAuthenticationProviderUrl);
        serviceLoadAndRegistry(registry, ValidateAuthority.class, validateAuthorityProviderClassName, validateAuthorityProviderUrl);
    }

    private <S> void  serviceLoadAndRegistry(BeanDefinitionRegistry registry,Class<S> spiInterface,String providerClassName,String providerUrl) {
        if (StringUtils.isEmpty(providerClassName )) throw new IllegalArgumentException("providerClassName 不能为空");
        ServiceLoader<S> authentications;
        if (!StringUtils.isEmpty(providerUrl)) {
            URL[] pluginUrl;
            try {
                pluginUrl = new URL[]{new URL(providerUrl)};
            } catch (MalformedURLException e) {
                log.error("providerUrl格式异常",e);
                throw new GafException("providerUrl格式异常",e);
            }
            URLClassLoader urlClassLoader = new URLClassLoader(pluginUrl, Thread.currentThread().getContextClassLoader());
            authentications = ServiceLoader.load(spiInterface,urlClassLoader);
        } else {
            authentications = ServiceLoader.load(spiInterface);
        }
        Map<String,Object> map = new HashMap<>();
        for (S s : authentications) {
            map.putIfAbsent(s.getClass().getName(),s);
        }
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(map.get(providerClassName).getClass());
        registry.registerBeanDefinition(providerClassName,beanDefinitionBuilder.getBeanDefinition());
        log.info("{}的BeanDefinition注册成功",providerClassName);
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }

    @Override
    public void setEnvironment(Environment environment) {
        this.validateAuthenticationProviderUrl = environment.getProperty("spi-config.validate-authentication.provider-url");
        String authenticationProviderClassName = environment.getProperty("spi-config.validate-authentication.provider-class-name");
        this.validateAuthenticationProviderClassName = !StringUtils.isEmpty(authenticationProviderClassName) ? authenticationProviderClassName: "com.supermap.gaf.extend.spi.provider.DefaultValidateAuthenticationProvider";

        this.validateAuthorityProviderUrl = environment.getProperty("spi-config.validate-authority.provider-url");
        String authorityProviderClassName = environment.getProperty("spi-config.validate-authority.provider-class-name");
        this.validateAuthorityProviderClassName = !StringUtils.isEmpty(authorityProviderClassName) ? authorityProviderClassName: "com.supermap.gaf.extend.spi.provider.DefaultValidateAuthorityProvider";

    }
}
