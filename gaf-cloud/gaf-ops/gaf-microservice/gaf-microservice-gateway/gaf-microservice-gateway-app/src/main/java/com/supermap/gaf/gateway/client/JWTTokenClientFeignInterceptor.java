/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.gateway.client;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;



/**
 *
 * @author:yj
 * @date:2021/3/25
 */
public class JWTTokenClientFeignInterceptor implements RequestInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(JWTTokenClientFeignInterceptor.class);


    private ServerHttpRequest serverHttpRequest;

    public JWTTokenClientFeignInterceptor(ServerHttpRequest serverHttpRequest) {
        this.serverHttpRequest = serverHttpRequest;
    }

    @Override
    public void apply(RequestTemplate template) {
        template.header(HttpHeaders.COOKIE,serverHttpRequest.getHeaders().getFirst(HttpHeaders.COOKIE));
        template.header(HttpHeaders.AUTHORIZATION,serverHttpRequest.getHeaders().getFirst(HttpHeaders.AUTHORIZATION));
    }
}
