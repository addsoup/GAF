/**
 * Created by huanl on 2019/7/15
 */
import Vue from 'vue'
import AntUI from 'ant-design-vue/lib'
// import MonacoWebpackPlugin from 'monaco-editor-webpack-plugin'
export default () => {
  Vue.use(AntUI)
  // Vue.use(MonacoWebpackPlugin)
}
