/**
 * Created by huanl on 2019/7/23
 */
import { axios } from 'gaf-ui'

import gafUI from './gaf-ui'
import antUI from './ant-ui'
gafUI()
antUI()

export default axios
