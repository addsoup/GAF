package com.supermap.gaf.storage.resources;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.storage.entity.ConfigName;
import com.supermap.gaf.storage.entity.Space;
import com.supermap.gaf.storage.entity.SpaceConfig;
import com.supermap.gaf.storage.entity.vo.SpaceConfigSelectVo;
import com.supermap.gaf.storage.service.SpaceConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.List;

/**
 * 接口
 *
 * @author zrc
 * @date yyyy-mm-dd
 */
@Api(value = "存储配置接口")
public class SpaceConfigResource {
   private String configType;

    private SpaceConfigService spaceConfigService;

    public SpaceConfigResource(String configType, SpaceConfigService spaceConfigService) {
        this.configType = configType;
        this.spaceConfigService = spaceConfigService;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @Path("/{id}")
    public MessageResult<SpaceConfig> getById(@PathParam("id") String id) {
        return MessageResult.data(spaceConfigService.getById(configType, id)).status(200).message("查询成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询", notes = "分页条件查询")
    public MessageResult<Page> pageList(@BeanParam SpaceConfigSelectVo spaceConfigSelectVo,
                                        @DefaultValue("1") @QueryParam("pageNum") Integer pageNum,
                                        @DefaultValue("10") @QueryParam("pageSize") Integer pageSize) {
        Page<SpaceConfig> page = spaceConfigService.listByPageCondition(configType, spaceConfigSelectVo, pageNum, pageSize);
        return MessageResult.successe(Page.class).data(page).status(200).message("查询成功").build();
    }
    @Path("/list")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "条件查询", notes = "条件查询")
    public MessageResult<List<SpaceConfig>> selectList(@BeanParam SpaceConfigSelectVo spaceConfigSelectVo,
                                        @DefaultValue("1") @QueryParam("pageNum") Integer pageNum,
                                        @DefaultValue("10") @QueryParam("pageSize") Integer pageSize) {
        List<SpaceConfig> re = spaceConfigService.selectList(configType, spaceConfigSelectVo);
        return MessageResult.data(re).status(200).message("查询成功").build();
    }


    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增", notes = "新增")
    public MessageResult<Void> insert(SpaceConfig spaceConfig) {
        spaceConfigService.insert(configType, spaceConfig);
        return MessageResult.successe(Void.class).status(200).message("新增操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @Path("/{id}")
    public MessageResult<Void> delete(@PathParam("id") String id) {
        spaceConfigService.delete(configType, id);
        return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除", notes = "批量删除")
    public MessageResult<Void> batchDelete(List<String> ids) {
        spaceConfigService.batchDelete(configType, ids);
        return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "所有可用存储名称", notes = "所有可用存储名称")
    @Path("/all-names")
    public MessageResult<Collection<ConfigName>> getAllNames(@QueryParam("ower") String ower) {
        Collection<ConfigName> re = spaceConfigService.getAllNames(configType, ower);
        return MessageResult.data(re).status(200).message("查询成功").build();
    }
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @Path("/{id}")
    public MessageResult<Void> update(SpaceConfig spaceConfig, @PathParam("id") String id) {
        spaceConfig.setId(id);
        spaceConfigService.update(configType, spaceConfig);
        return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{id}/allocate")
    @ApiOperation(value = "分配存储", notes = "分配存储")
    public MessageResult<Void> allocate(@PathParam("id") String id, Space space) {
        space.setParentSpaceId(id);
        spaceConfigService.allocate(configType,space);
        return MessageResult.successe(Void.class).status(200).message("查询成功").build();
    }

}
