package com.supermap.gaf.storage.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.storage.entity.ConfigName;
import com.supermap.gaf.storage.entity.Space;
import com.supermap.gaf.storage.entity.SpaceConfig;
import com.supermap.gaf.storage.entity.vo.SpaceConfigSelectVo;

import java.util.Collection;
import java.util.List;

/**
 * 服务类
 *
 * @author zrc
 * @date yyyy-mm-dd
 */
public interface SpaceConfigService {

    /**
     * 根据id查询
     *
     * @return
     */
    SpaceConfig getById(String configType, String id);


    List<SpaceConfig> selectList(String configType,SpaceConfigSelectVo spaceConfigSelectVo);
    /**
     * 分页条件查询
     *
     * @param pageNum  当前页码
     * @param pageSize 每页数量
     * @return 分页对象
     */
    Page<SpaceConfig> listByPageCondition(String configType, SpaceConfigSelectVo spaceConfigSelectVo, int pageNum, int pageSize);

    Collection<ConfigName> getAllNames(String configType,String ower);
    /**
     * 新增
     *
     * @return 新增的globalServerConfig
     */
    void insert(String configType, SpaceConfig spaceConfig);

    /**
     * 删除
     */
    void delete(String configType, String id);

    /**
     * 批量删除
     **/
    void batchDelete(String configType, List<String> ids);

    /**
     * 更新
     *
     * @return 更新后的globalServerConfig
     */
    void update(String configType, SpaceConfig spaceConfig);

    void allocate(String configType, Space space);
}
