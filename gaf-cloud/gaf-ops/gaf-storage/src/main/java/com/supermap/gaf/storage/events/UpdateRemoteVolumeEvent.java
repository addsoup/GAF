package com.supermap.gaf.storage.events;

import org.springframework.context.ApplicationEvent;

public class UpdateRemoteVolumeEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public UpdateRemoteVolumeEvent(Object source) {
        super(source);
    }
}
