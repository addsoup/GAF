package com.supermap.gaf.platform.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wxl
 * @since 2022/3/16
 */
@Configuration
public class SeataConfig {

    @ConditionalOnProperty(prefix = "seata",name = "enabled",havingValue = "true",matchIfMissing = true)
    @Bean
    public FilterRegistrationBean<SeataFilter> seataFilter() {
        FilterRegistrationBean<SeataFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new SeataFilter());
        bean.addUrlPatterns("/*");
        bean.setOrder(7);
        bean.setName("seataFilter");
        return bean;
    }
}
