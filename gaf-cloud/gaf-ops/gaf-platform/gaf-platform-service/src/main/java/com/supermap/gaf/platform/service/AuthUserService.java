/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.platform.service;

import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.authority.vo.EmailChangeVo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.platform.entity.vo.AuthUserSelectVo;

import java.util.List;

/**
 * 用户服务类
 *
 * @author dqc
 * @date:2021/3/25
 */
public interface AuthUserService {

    /**
     * 根据id查询用户
     *
     * @param userId 用户id
     * @return 用户 若未查询到则返回null
     */
    AuthUser getByUserId(String userId);

    /**
     * 根据id查询用户
     *
     * @param userIds 用户id
     * @return 用户 若未查询到则返回空集合
     */
    List<AuthUser> listByUserIds(List<String> userIds);


    /**
     * 新增用户
     *
     * @param authUser 用户
     * @return 用户
     */
    AuthUser save(AuthUser authUser);

    /**
     * 批量插入用户
     *
     * @param authUsers 用户集合
     */
    List<AuthUser> saveBatch(List<AuthUser> authUsers);

    /**
     * 禁用用户即删除用户，给用户的分配的角色、岗位、挂职都会被清空
     *
     * @param userId 用户id
     * @return 用户
     */
    AuthUser deleteAuthUser(String userId);

    /**
     * 批量删除用户
     * 根据id批量删除用户。注意：给用户的分配的角色、岗位、挂职都会被清空
     *
     * @param userIds 用户id集合
     */
    void batchDelete(List<String> userIds);

    /**
     * 更新用户
     *
     * @param authUser 用户
     * @return 用户
     */
    AuthUser updateAuthUser(AuthUser authUser);

    /**
     * 重置密码
     *
     * @param userId 用户id
     * @return 用户
     */
    AuthUser resetPassword(String userId);

    /**
     * 启用用户
     *
     * @param userId 用户id
     * @return 返回启用后的信息
     */
    AuthUser active(String userId);

    /**
     * 禁用用户
     * @param userId
     * @return
     */
    AuthUser inActive(String userId);



    /**
     * 变更用户密码
     *
     * @param userId    用户id
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     */
    void changePassword(String  userId, String oldPassword, String newPassword);

    /**
     * 当前用户变更邮箱
     *
     * @param emailChangeVo 变更邮箱参数
     */
    String changeEmail(EmailChangeVo emailChangeVo);

    /**
     * 发送校验码
     *
     * @param email 邮箱 若邮箱为空则默认使用当前用户邮箱
     * @return 若为空则表示成功，否则表示失败原因
     */
    void sendCheckCode(String email);

    /**
     * 分页条件查询用户
     * @param authUserSelectVo
     * @param pageNum
     * @param pageSize
     * @return
     */
    Page<AuthUser> page(AuthUserSelectVo authUserSelectVo, Integer pageNum, Integer pageSize);

    AuthUser getByUsername(String username);

    AuthUser getByEmail(String email);


}
