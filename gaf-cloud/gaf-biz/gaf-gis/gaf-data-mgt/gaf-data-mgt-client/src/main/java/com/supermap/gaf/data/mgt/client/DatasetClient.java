package com.supermap.gaf.data.mgt.client;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.data.mgt.entity.DataDataset;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.constraints.NotEmpty;
import javax.ws.rs.PathParam;
import java.util.List;

/**
 * ClassName:DatasetClient
 * Package:com.supermap.gaf.data.mgt.client
 *
 * @date:2021/5/18 16:52
 * @author:Yw
 */
@FeignClient(name = "GAF-DATA-MGT", contextId = "datasetClient")
public interface DatasetClient {
    /**
     * 根据数据集ID查询单个数据集信息
     * @param datasetId
     * @return
     */
    @GetMapping("/data-mgt/data-datasets/{datasetId}")
    MessageResult<DataDataset> getById(@NotEmpty @PathVariable("datasetId") String datasetId);
    @GetMapping("/data-mgt/data-datasets/datasource/{datasourceId}")
    MessageResult<List<DataDataset>> getByDatasourceId(@NotEmpty @PathVariable("datasourceId") String datasourceId);
}
