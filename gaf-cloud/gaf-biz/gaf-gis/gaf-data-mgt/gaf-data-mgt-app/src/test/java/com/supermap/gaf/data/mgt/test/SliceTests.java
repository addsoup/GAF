package com.supermap.gaf.data.mgt.test;

import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.data.EngineType;
import com.supermap.gaf.data.mgt.service.DatasetTypeCacheService;
import com.supermap.gaf.data.mgt.service.impl.ModelCacheServiceImpl;
import com.supermap.gaf.data.mgt.util.IobjectUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wxl
 * @since 2022/6/16
 */
public class SliceTests {


    public static void main(String[] args) {

        DatasourceConnectionInfo connetionInfo = new DatasourceConnectionInfo();

        connetionInfo.setEngineType(EngineType.POSTGRESQL);
        connetionInfo.setDatabase("video_map");
        connetionInfo.setServer("192.168.192.232:32023");
        connetionInfo.setAlias("lmap_video_map");
        connetionInfo.setUser("admin");
        connetionInfo.setPassword("123456");
        IobjectUtils.datasetProcessor(connetionInfo,"set_model", dataset ->  {

            Map<String, Object> otherParams = new HashMap<>();
            otherParams.put("isSimp",true);
            otherParams.put("simplifyingRate",0.1);
            DatasetTypeCacheService datasetTypeCacheService = new ModelCacheServiceImpl();
            boolean isSuccessed = datasetTypeCacheService.executeS3MBuilder(dataset, "C:\\Users\\wxl\\Desktop\\slice\\my_tmp", "set_model_x", otherParams, percent -> System.out.println("percent = " + percent));
            return null;
        });



    }
}
