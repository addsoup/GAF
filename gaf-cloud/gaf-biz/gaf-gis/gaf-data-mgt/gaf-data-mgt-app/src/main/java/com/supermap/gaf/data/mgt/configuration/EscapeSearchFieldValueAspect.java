package com.supermap.gaf.data.mgt.configuration;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

/**
 * @author wxl
 * @since 2021/11/25
 */
@Aspect
@Component
public class EscapeSearchFieldValueAspect {

    @Pointcut("execution(* com.supermap.gaf.data.mgt.resource.*Resource.pageList(..))")
    public void escapeSearchFieldValuePointcut() {}

    @Around("escapeSearchFieldValuePointcut()")
    public Object escapeSearchFieldValue(ProceedingJoinPoint pjp) throws Throwable {
        //获取传入的参数
        Object[] args = pjp.getArgs();
        for (Object obj : args) {
            if (obj != null && obj.getClass().getName().endsWith("SelectVo")) {
                Field searchFieldValueField = null;
                try {
                    searchFieldValueField = obj.getClass().getDeclaredField("searchFieldValue");
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                }
                if (searchFieldValueField != null) {
                    searchFieldValueField.setAccessible(true);
                    String searchFieldValue = (String)searchFieldValueField.get(obj);
                    searchFieldValueField.set(obj,escape(searchFieldValue));
                }
                break;
            }
        }
        return pjp.proceed(args);
    }

    public static String escape(String value) {
        if (value == null || value.isEmpty()) return value;
        return value.replaceAll("\\\\", "\\\\\\\\")
                .replaceAll("_", "\\\\_")
                .replaceAll("%", "\\\\%");
    }

}
