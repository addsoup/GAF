package com.supermap.gaf.data.mgt.configuration;

import com.alibaba.ttl.threadpool.TtlExecutors;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author : duke
 * @since 2021/11/16 2:57 PM
 */
@EnableAsync
@Configuration
public class AsyncConfiguration {

    /**
     * 线程池配置
     */
    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(10);
        //executor.setTaskDecorator(new ContextTaskDecorator());
        executor.setRejectedExecutionHandler(new CustomRejectedExecutionHandler());
        executor.initialize();
        return TtlExecutors.getTtlExecutor(executor);
    }

    /**
     * 自定义淘汰策略（队列满了阻塞）
     */
    class CustomRejectedExecutionHandler implements RejectedExecutionHandler {
        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            try {
                System.out.println("task executor block...");
                executor.getQueue().put(r);
                System.out.println("task executor block end");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
