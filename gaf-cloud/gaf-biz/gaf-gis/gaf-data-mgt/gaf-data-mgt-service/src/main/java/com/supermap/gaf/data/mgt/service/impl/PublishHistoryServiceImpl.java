package com.supermap.gaf.data.mgt.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.common.IServerManager;
import com.supermap.gaf.data.mgt.entity.CacheBuildHistory;
import com.supermap.gaf.data.mgt.entity.PublishHistory;
import com.supermap.gaf.data.mgt.entity.ServerInfo;
import com.supermap.gaf.data.mgt.entity.iserver.PublishResult;
import com.supermap.gaf.data.mgt.entity.vo.PublishHistoryVo;
import com.supermap.gaf.data.mgt.enums.PublishHistoryStateEnum;
import com.supermap.gaf.data.mgt.mapper.PublishHistoryMapper;
import com.supermap.gaf.data.mgt.service.CacheBuildHistoryService;
import com.supermap.gaf.data.mgt.service.PublishHistoryService;
import com.supermap.gaf.data.mgt.util.IserverUtils2;
import com.supermap.gaf.data.mgt.util.MongoUtils;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.tilestorage.TileStorageManager;
import com.supermap.tilestorage.TileStorageMiniInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * @description
 * @date:2022/1/26 17:30
 * @author:yw
 */
@Service
public class PublishHistoryServiceImpl implements PublishHistoryService {
    private static Logger logger = LoggerFactory.getLogger(PublishHistoryServiceImpl.class);

    @Autowired
    private PublishHistoryMapper publishHistoryMapper;

    @Autowired
    private CacheBuildHistoryService cacheBuildHistoryService;

    @Autowired
    private IServerManager iServerManager;


    public void mongoUpdateService(String serviceName,String[] datasetNames) {
        try {
            MessageResult<String> result = iServerManager.applyToken();
            if (!result.IsSuccessed()) {
                throw new GafException("获取iserverToken失败");
            }
            String token = result.getData();
            String hostServerUrl = iServerManager.getAvailableIServerSetting().getHostServerUrl();
            new IserverUtils2().updateMongoSerice(hostServerUrl,serviceName,datasetNames,token);
        }catch (Exception e) {
            throw new GafException("更新服务实例失败"+e.getMessage());
        }
    }

    @Override
    public boolean checkMongoConn(ServerInfo serverInfo) {
        String host = serverInfo.getHost();
        String database = serverInfo.getDatabase();
        String password = serverInfo.getPassword();
        Integer port = serverInfo.getPort();
        String username = serverInfo.getUsername();
        try {
            new MongoUtils().checkMongoConn(host,port,database,username,password);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public  List<String> getMongoCollection(ServerInfo serverInfo) {
        String host = serverInfo.getHost();
        String databaseName = serverInfo.getDatabase();
        String password = serverInfo.getPassword();
        Integer port = serverInfo.getPort();
        String username = serverInfo.getUsername();

        TileStorageMiniInfo[] tileStorageMiniInfos = TileStorageManager.getMongoTileStorageMiniInfo(host+":"+port, databaseName, username, password);
        return Arrays.stream(tileStorageMiniInfos).map(TileStorageMiniInfo::getName).collect(Collectors.toList());
    }

    @Override
    public Page<PublishHistory> pageList(int pageNum, int pageSize, PublishHistoryVo publishHistoryVo) {
        PageInfo<PublishHistory> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            publishHistoryMapper.pageList(publishHistoryVo);
        });
        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
    }

    @Override
    public PublishHistory selectById(String id) {
        return publishHistoryMapper.selectById(id);
    }

    @Override
    public void update(PublishHistory publishHistory) {
        publishHistory.setUpdatedBy(SecurityUtilsExt.getUserName());
        publishHistoryMapper.update(publishHistory);
    }


    @Override
    public void insert(PublishHistory publishHistory) {
        String userName = SecurityUtilsExt.getUserName();
        publishHistory.setCreatedBy(userName);
        publishHistory.setUpdatedBy(userName);
        publishHistory.setId(UUID.randomUUID().toString());
        publishHistoryMapper.insert(publishHistory);
    }

    @Override
    public void delete(String id) {
        publishHistoryMapper.delete(id);
    }

    @Override
    public void deleteWithResource(String id, Boolean isDeleteServices, Boolean isDeleteCacheTiles) {
        PublishHistory publishHistory = selectById(id);
        if (publishHistory == null) return;
        List<CacheBuildHistory> cacheBuildHistories = cacheBuildHistoryService.listByPublishHistoryId(id);
        List<String> ids = cacheBuildHistories.stream().map(CacheBuildHistory::getId).collect(Collectors.toList());
        cacheBuildHistoryService.batchDeleteWithResource(ids,isDeleteCacheTiles);
        Integer state = publishHistory.getState();
        if (PublishHistoryStateEnum.PUBLISHED.getCode() == state) {
            String result = publishHistory.getResult();
            if (!StringUtils.isEmpty(result)) {
                CompletableFuture.runAsync(() -> {
                    List<PublishResult> publishResults = JSONObject.parseArray(result, PublishResult.class);
                    for (PublishResult publishResult : publishResults) {
                        String serviceAddress = publishResult.getServiceAddress();
                        String serviceName = parseName(serviceAddress);
                        //删除服务
                        iServerManager.removeInstance(serviceName);
                    }
                });
            }
        }
        delete(id);
    }

    private String parseName(String serviceAddress) {
        URL url = null;
        try {
            url = new URL(serviceAddress);
        } catch (MalformedURLException e) {
            logger.error("通过服务地址解析服务名失败",e);
            throw new RuntimeException(e);
        }
        String path = url.getPath();
        String[] splits = path.split("/");
        String serviceName = splits[3];
        return serviceName;
    }


}
