package com.supermap.gaf.data.mgt.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;
import javax.validation.constraints.*;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 数据同步任务表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据同步任务表")
public class DataSyncTask implements Serializable{
    @ApiModelProperty("主键")
    private String dataSyncTaskId;
    @NotNull
    @ApiModelProperty("同步任务名称")
    private String dataSyncTaskName;
    @ApiModelProperty("同步任务状态")
    private Integer dataSyncStatus;
    @NotNull
    @ApiModelProperty("空间数据源id")
    private String spatialDatasourceId;
    @NotNull
    @ApiModelProperty("标准数据源id")
    private String standardDatasourceId;
    @ApiModelProperty("映射信息 格式如：[{srcName: A表 ,destName: B表 ,fieldsMapping:[{srcName: X1字段 ,destName: X1字段 },{srcName: X2字段 ,destName: X2字段 }]},{}]")
    private String mappingInfo;
    @Email
    @ApiModelProperty("通知邮箱")
    private String alertEmail;
    @ApiModelProperty("通知方式 多选 1开始时通知、2结束时通知、3报错时通知；多选时用,作为分隔符")
    private String alertType = "";
    @ApiModelProperty("定时任务cron")
    private String dataSyncCron;
    @ApiModelProperty("描述")
    private String description;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;

    public enum  DataSyncStatus{
        NONE(1),RUNNING(2),RECENT_SUCCESS(3),RECENT_FAIL(4);
        private int code;

        DataSyncStatus(int code) {
            this.code = code;
        }
        public int getCode() {
            return code;
        }
    }
}