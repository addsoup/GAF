package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.DataSyncTask;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 数据同步任务表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据同步任务视图")
public class DataSyncTaskVo extends DataSyncTask {
    @ApiModelProperty("空间数据源名称")
    private String spatialDatasourceName;
    @ApiModelProperty("标准数据源名称")
    private String standardDatasourceName;
    @ApiModelProperty("空间数据源类型")
    private String spatialDatasourceTypeCode;
    @ApiModelProperty("标准数据源类型")
    private String standardDatasourceTypeCode;
}