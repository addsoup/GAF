package com.supermap.gaf.data.mgt.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 工作空间下场景简略信息
 * @author wxl
 * @since 2021/9/14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SceneSimpleInfo {
    /**
     * 场景名
     */
    @ApiModelProperty("场景名")
    private String name;
    /**
     * 场景类型
     */
    @ApiModelProperty(value = "场景类型",allowableValues="球面场景,平面场景")
    private String sceneType;
}
