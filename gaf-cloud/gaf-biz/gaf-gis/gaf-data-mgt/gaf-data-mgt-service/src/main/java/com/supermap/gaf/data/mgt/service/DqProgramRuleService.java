package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DqProgramRule;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleVo;

import java.util.List;

/**
 * 数据质量管理-方案-规则联系表服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface DqProgramRuleService {
	
	/**
    * 根据id查询数据质量管理-方案-规则联系表
    * @return
    */
    DqProgramRule getById(String dqProgramRuleId);
	
	/**
     * 分页条件查询
     * @param dqProgramRuleSelectVo 查询条件
     * @param pageNum 当前页码
     * @param pageSize 每页数量
     * @return 分页对象
     */
	Page<DqProgramRuleVo> listByPageCondition(DqProgramRuleSelectVo dqProgramRuleSelectVo, int pageNum, int pageSize);
	
	
    /**
    * 新增数据质量管理-方案-规则联系表
    * @return 新增的dqProgramRule
    */
    DqProgramRule insertDqProgramRule(DqProgramRule dqProgramRule);
	
	/**
    * 批量插入
    * 
    **/
    void batchInsert(List<DqProgramRule> dqProgramRules);

    /**
    * 删除数据质量管理-方案-规则联系表
    * 
    */
    void deleteDqProgramRule(String dqProgramRuleId);

    /**
    * 批量删除
    * 
    **/
    void batchDelete(List<String> dqProgramRuleIds);
    /**
    * 更新数据质量管理-方案-规则联系表
    * @return 更新后的dqProgramRule
    */
    DqProgramRule updateDqProgramRule(DqProgramRule dqProgramRule);
    
}
