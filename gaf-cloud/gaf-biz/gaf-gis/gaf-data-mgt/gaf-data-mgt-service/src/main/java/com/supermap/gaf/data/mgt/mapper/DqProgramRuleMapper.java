package com.supermap.gaf.data.mgt.mapper;

import com.supermap.gaf.data.mgt.entity.DqProgramRule;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 数据质量管理-方案-规则联系表数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface DqProgramRuleMapper{
	/**
     * 根据主键 dqProgramRuleId查询
     * 
	 */
    DqProgramRule select(@Param("dqProgramRuleId") String dqProgramRuleId);

	List<DqProgramRuleVo> selectVoList(DqProgramRuleSelectVo dqProgramRuleSelectVo);

	/**
     * 多条件查询
     * @param dqProgramRuleSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<DqProgramRule> selectList(DqProgramRuleSelectVo dqProgramRuleSelectVo);

    /**
	 * 新增
	 * 
	 */
    int insert(DqProgramRule dqProgramRule);
	
	/**
     * 批量插入
     * 
	 */
    int batchInsert(List<DqProgramRule> dqProgramRules);
	
	/**
     * 批量删除
     *
	 */
    int batchDelete(List<String> dqProgramRuleIds);

	/**
     * 刪除
     * 
	 */
    int delete(@Param("dqProgramRuleId") String dqProgramRuleId);

	/**
	 * 通过方案id删除
	 * @param dataQualityProgramIds
	 * @return
	 */
	int  deleteByProgramId(List<String> dataQualityProgramIds);
    /**
     * 更新
     *
	 */
    int update(DqProgramRule dqProgramRule);
}
