package com.supermap.gaf.data.mgt.mapper;
import com.supermap.gaf.data.mgt.entity.DataCode;
import com.supermap.gaf.data.mgt.entity.vo.DataCodeSelectVo;
import java.util.*;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * 数据代码数据访问类
 *
 * @author zrc
 * @date yyyy -mm-dd
 */
@Mapper
@Component
public interface DataCodeMapper{
	/**
	 * 根据主键 dataCodeId 查询
	 *
	 * @param dataCodeId the data code id
	 * @return the data code
	 */
	DataCode select(@Param("dataCodeId")String dataCodeId);

	/**
	 * 多条件查询
	 *
	 * @param dataCodeSelectVo 查询条件
	 * @return 若未查询到则返回空集合 list
	 */
	List<DataCode> selectList(DataCodeSelectVo dataCodeSelectVo);

	/**
	 * 新增
	 *
	 * @param dataCode the data code
	 */
	void insert(DataCode dataCode);

	/**
	 * 批量插入
	 *
	 * @param dataCodes the data codes
	 */
	void batchInsert(@Param("list") Collection<DataCode> dataCodes);

	/**
	 * 批量删除
	 *
	 * @param dataCodeIds the data code ids
	 * @return the int
	 */
	int batchDelete(@Param("list") Collection<String> dataCodeIds);

	/**
	 * 刪除
	 *
	 * @param dataCodeId the data code id
	 * @return the int
	 */
	int delete(@Param("dataCodeId")String dataCodeId);

	/**
	 * 更新
	 *
	 * @param dataCode the data code
	 * @return the int
	 */
	int update(DataCode dataCode);

	/**
	 * 选择性更新
	 *
	 * @param dataCode the data code
	 * @return the int
	 */
	int updateSelective(DataCode dataCode);
}
