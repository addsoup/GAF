package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.CacheBuildResult;
import lombok.Data;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author wxl
 * @since 2022/5/25
 */
@Data
public class PublishCacheBuildHistorTaskVo {


    private PublishCacheBuildHistorVo publishCacheBuildHistorVo;

    private List<CompletableFuture<CacheBuildResult>> allFutures;

    private List<CacheBuildResult> cacheBuildResults;

}
