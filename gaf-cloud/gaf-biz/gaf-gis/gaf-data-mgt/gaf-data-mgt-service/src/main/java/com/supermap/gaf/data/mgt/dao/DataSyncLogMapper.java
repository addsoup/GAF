package com.supermap.gaf.data.mgt.dao;
import com.supermap.gaf.data.mgt.entity.DataSyncLog;
import java.util.*;

import com.supermap.gaf.data.mgt.entity.vo.DataSyncLogSelectVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * 数据同步日志表数据访问类
 *
 * @author zrc
 * @date yyyy -mm-dd
 */
@Mapper
@Component
public interface DataSyncLogMapper{
	/**
	 * 根据主键 dataSyncLogId查询
	 *
	 * @param dataSyncLogId the data sync log id
	 * @return the data sync log
	 */
	DataSyncLog select(@Param("dataSyncLogId") String dataSyncLogId);

	/**
	 * 多条件查询
	 *
	 * @param dataSyncLogSelectVo 查询条件
	 * @return 若未查询到则返回空集合 list
	 */
	List<DataSyncLog> selectList(DataSyncLogSelectVo dataSyncLogSelectVo);

	/**
	 * 新增
	 *
	 * @param dataSyncLog the data sync log
	 * @return the int
	 */
	int insert(DataSyncLog dataSyncLog);

	/**
	 * 批量插入
	 *
	 * @param dataSyncLogs the data sync logs
	 * @return the int
	 */
	int batchInsert(List<DataSyncLog> dataSyncLogs);

	/**
	 * 批量删除
	 *
	 * @param dataSyncLogIds the data sync log ids
	 * @return the int
	 */
	int batchDelete(List<String> dataSyncLogIds);

	/**
	 * 刪除
	 *
	 * @param dataSyncLogId the data sync log id
	 * @return the int
	 */
	int delete(@Param("dataSyncLogId") String dataSyncLogId);

	/**
	 * 更新
	 *
	 * @param dataSyncLog the data sync log
	 * @return the int
	 */
	int update(DataSyncLog dataSyncLog);

	/**
	 * Batch delete by task ids.
	 *
	 * @param dataSyncTaskIds the data sync task ids
	 */
	void batchDeleteByTaskIds(List<String> dataSyncTaskIds);
}
