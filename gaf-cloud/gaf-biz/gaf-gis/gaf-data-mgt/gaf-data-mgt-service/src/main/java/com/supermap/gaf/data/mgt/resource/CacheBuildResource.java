package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.data.mgt.entity.CacheBuildHistory;
import com.supermap.gaf.data.mgt.entity.CacheBuildParam;
import com.supermap.gaf.data.mgt.entity.CacheBuildStatus;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorTaskVo;
import com.supermap.gaf.data.mgt.service.CacheBuildService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 缓存构建接口
 * 数据切片
 *
 * @author wsw
 * @date yyyy-mm-dd
 */
@Component
@Api("缓存构建接口")
public class CacheBuildResource {

    @Autowired
    private CacheBuildService cacheBuildService;


    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据缓存构建参数集合执行切片", notes = "根据缓存构建参数集合执行切片")
    @Path("/s3m/new")
    public MessageResult<List<CacheBuildHistory>> CacheBuilderNew(CacheBuildParam CacheBuildParam) {
        PublishCacheBuildHistorTaskVo vo = cacheBuildService.cacheBuild(CacheBuildParam);
        List<CacheBuildHistory> cacheBuildHistorys = vo.getPublishCacheBuildHistorVo().getCacheBuildHistorys();
        return MessageResult.data(cacheBuildHistorys).build();
    }


    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据缓存构建历史id集合查询切片状态", notes = "根据缓存构建历史id集合查询切片状态")
    @ApiImplicitParam(name = "cacheBuildHistoryIds", value = "缓存构建历史id集合", paramType = "body", dataTypeClass = List.class)
    @Path("/cache-status/")
    public MessageResult<List<CacheBuildStatus>> getCacheSatus(List<String> cacheBuildHistoryIds){
        List<CacheBuildStatus> results = cacheBuildService.getCacheSatus(cacheBuildHistoryIds);
        return MessageResult.data(results).status(200).message("查询成功").build();
    }

}
