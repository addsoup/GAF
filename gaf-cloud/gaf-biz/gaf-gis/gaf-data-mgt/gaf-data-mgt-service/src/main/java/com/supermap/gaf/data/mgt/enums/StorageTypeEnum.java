package com.supermap.gaf.data.mgt.enums;

/**
 * 三维瓦片存储类型
 *
 *
 */
public enum StorageTypeEnum {

    // FILE_WORKSPACE
    WORKSPACE("WORKSPACE","工作空间"),
    // MONGODB_TILE
    MONGODB("MONGODB","MongoDB");

    /**
     * 值
     */
    private final String value;

    /**
     * 描述
     */
    private final String description;

    StorageTypeEnum(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String value() {
        return this.value;
    }

    public String getDescription() {
        return description;
    }

}
