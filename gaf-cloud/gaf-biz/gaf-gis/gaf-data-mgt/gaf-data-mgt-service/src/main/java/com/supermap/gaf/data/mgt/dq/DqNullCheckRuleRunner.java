package com.supermap.gaf.data.mgt.dq;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.data.Feature;
import com.supermap.data.Recordset;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleVo;
import com.supermap.gaf.data.mgt.log.SimpleLogger;
import com.supermap.gaf.data.mgt.mapper.DqExecuteLogMapper;
import com.supermap.gaf.data.mgt.util.IobjectUtils;

import java.util.Map;


public class DqNullCheckRuleRunner extends DqRuleRunner {


    public DqNullCheckRuleRunner(DqExecuteLogMapper dqExecuteLogMapper) {
        super(dqExecuteLogMapper);
    }

    @Override
    public DqResult run(DatasourceConnectionInfo datasourceConnectionInfo, DqProgramRuleVo rule, SimpleLogger logger) {
        DqResult re = new DqResult();
        try{
            JSONObject paramsObj = JSON.parseObject(rule.getParams());
            logger.info("开始执行【空值检查】规则。");
            logger.info("参数【{}】", rule.getParams());
            String datasetName = paramsObj.getString("datasetName");
            JSONArray fields = paramsObj.getJSONArray("fieldNames");
            IobjectUtils.datasetProcessor(datasourceConnectionInfo,datasetName,dataset -> {
                long totalCount = IobjectUtils.getRecordAmount(dataset);
                re.setTotalAmount(totalCount);
                if(totalCount<=0){
                    logger.info("空表，检查结束");
                    return null;
                }
                StringBuffer where = new StringBuffer();
                for(int i = 0;i<fields.size();++i){
                    if(i>0){
                        where.append(" or ");
                    }
                    where.append(fields.getString(i)).append(" is null ");
                }
                Recordset recordset = IobjectUtils.query(dataset,where.toString(),new String[]{"smid"},false);
                Map<Integer, Feature> featureMap = recordset.getAllFeatures();
                re.setFailedAmount(featureMap.size());
                if(featureMap.size()==0){
                    logger.info("检查完成，无错误");
                }else{
                    int[] errorIds = new int[featureMap.size()];
                    int i = 0;
                    for(Feature feature:featureMap.values()){
                        errorIds[i++] = feature.getInt32(0);
                    }
                    logger.error("检查失败，错误记录smids：{}", JSON.toJSONString(errorIds));
                }
                return null;
            });
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return re;
    }
}
