package com.supermap.gaf.data.mgt.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;
import javax.validation.constraints.*;
import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import java.io.Serializable;

/**
 * 数据代码编码
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据代码编码")
public class DataCodeContent implements Serializable{
    private static final long serialVersionUID = -1L;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("名称。")
    private String name;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("英文名。")
    private String enName;
    @ApiModelProperty("描述。")
    private String description;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("数据代码id")
    private String dataCodeId;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;
    @ApiModelProperty("主键。")
    private String dataCodeContentId;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("编码取值")
    private String value;
}