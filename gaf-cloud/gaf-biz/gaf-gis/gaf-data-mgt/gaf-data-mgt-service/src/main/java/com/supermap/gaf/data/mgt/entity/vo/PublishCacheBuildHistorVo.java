package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.CacheBuildHistory;
import com.supermap.gaf.data.mgt.entity.PublishHistory;
import lombok.Data;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author wxl
 * @since 2022/5/25
 */
@Data
public class PublishCacheBuildHistorVo {

    private PublishHistory publishHistory;

    private List<CacheBuildHistory> cacheBuildHistorys;

    // 用于测试使用
    private CompletableFuture<Void> allOf;
}
