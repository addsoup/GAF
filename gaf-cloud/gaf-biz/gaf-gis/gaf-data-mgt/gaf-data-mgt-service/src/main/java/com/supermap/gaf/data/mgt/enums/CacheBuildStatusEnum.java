package com.supermap.gaf.data.mgt.enums;

/**
 * 切片任务状态枚举
 */
public enum CacheBuildStatusEnum {
    NEW(0,"就绪"),
    RUNNING(1,"切片中"),
    FAILED(2,"失败"),
    SUCCESSED(3,"成功");


    private int code;

    private String description;

    CacheBuildStatusEnum(int code,String description) {
        this.code = code;
        this.description = description;

    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public  static String getDescriptionByCode(int code) {
        CacheBuildStatusEnum[] values = values();
        for (CacheBuildStatusEnum value : values) {
            if (value.getCode() == code) {
                return value.getDescription();
            }
        }
        throw new IllegalArgumentException("不支持的缓存构建状态码:"+code);
    }

}
