package com.supermap.gaf.data.mgt.dq.plugin;

import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleVo;
import lombok.Data;

@Data
public class RequestParam {

    private ConnectionInfo connectionInfo;
    private DqProgramRuleVo rule;

    @Data
    public static class ConnectionInfo{
        private String server;
        private String alias;
        private String engineType;
        private String user;
        private String password;
        private String database;

    }



}
