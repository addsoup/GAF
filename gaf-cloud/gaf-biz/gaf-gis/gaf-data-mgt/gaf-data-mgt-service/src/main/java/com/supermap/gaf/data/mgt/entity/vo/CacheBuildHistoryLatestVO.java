package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.CacheBuildHistory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("最新缓存切片历史")
public class CacheBuildHistoryLatestVO extends CacheBuildHistory {
    @ApiModelProperty("数据源名")
    public String datasourceName;

    @ApiModelProperty(value = "切片类型" , allowableValues = "increment(增量),fullVolume(全量)")
    public String cacheTypeName;

    @ApiModelProperty("状态名")
    private String statusName;

}
