package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.PublishHistory;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 发布历史VO
 *
 */
@Data
@ApiModel("发布历史VO")
public class PublishHistoryVo {

    @QueryParam("searchFieldName")
    @ApiModelProperty("查询字段名")
    @StringRange(entityClass = PublishHistory.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = PublishHistory.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = PublishHistory.class)
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"}, message = "不在指定的范围[asc,desc]内")
    private String orderMethod;

    @QueryParam("id")
    @ApiModelProperty("发布历史id。主键，uuid")
    public String id;

    // 服务的来源信息 ------------------------------------------
    // 对应 SourceTypeEnum
    @QueryParam("sourceType")
    @ApiModelProperty("来源类型，包括FILE_WORKSPACE(文件型工作空间)、MONGODB_TILE(MongoDB)、MANAGED_WORKSPACE(管理的工作空间)、MANAGED_TILE(管理的瓦片,包括ugcv5 和 mongodb瓦片)")
    public String sourceType; // 原  type 类型（工作空间或者mongo连接信息）

    // 可为null
    @QueryParam("sourceName")
    @ApiModelProperty("来源名称")
    public String sourceName; //原 name

    /**
     * 服务来源信息，根据来源类型 约定有不同的格式和内容,通过代码对格式和内容进行控制
     */
    @QueryParam("sourceInfo")
    @ApiModelProperty("来源信息.json格式,根据来源类型 约定有不同的格式和内容")
    public String sourceInfo; // 原 工作空间或瓦片连接信息 serverInfo

    // ------------------------------------------
    // 发布服务设置信息，包括 发布服务的类型 ...
    // 根据来源类型和具体的数据集类型 来判断 可选择的服务类型
    // @see com.supermap.services.rest.management.ServiceType
    @QueryParam("serviceType")
    @ApiModelProperty("发布服务类型")
    public String serviceType;

    // json格式不同的来源类型 可以有额外的特有的发布设置 根据来源类型 约定有不同的格式和内容,通过代码对格式和内容进行控制
    // 例如
    // 由数据集切片得到缓存存储在的文件型工作空间或者是mongodb 来源 是否发布缓存切片关联的数据服务  切片完成是否立即发布服务
    @QueryParam("additionalSetting")
    @ApiModelProperty("发布设置. json格式,不同的来源类型 可以有额外的特有的发布设置 根据来源类型 约定有不同的格式和内容,通过代码对格式和内容进行控制")
    public String additionalSetting;

    @QueryParam("state")
    @ApiModelProperty("状态. 0 新建 1切片中 2 已切片  3 已发布 4 切片失败 5 发布失败")
    private Integer state;

    @QueryParam("result")
    @ApiModelProperty("发布结果.若state是发布成功,则发布结果为 json格式 例如[\"serviceType\": \"RESTDATA\",\"serviceAddress\": \"http://xxx.xxx.xxx/xxx/xx\"].若失败则为失败消息")
    private String result;

    @QueryParam("serviceUrl")
    @ApiModelProperty("发布后服务地址.多个使用,分割")
    public String serviceUrl;

    @QueryParam("description")
    @ApiModelProperty("描述")
    public String description;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("修改时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("修改人")
    private String updatedBy;

}
