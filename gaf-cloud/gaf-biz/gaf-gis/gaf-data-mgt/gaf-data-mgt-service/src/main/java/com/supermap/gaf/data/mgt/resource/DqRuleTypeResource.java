package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.data.mgt.dq.DqRuleType;
import com.supermap.gaf.data.mgt.service.DqRuleTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 数据质量管理-模型表接口
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "数据质量管理-规则类型接口")
public class DqRuleTypeResource {

    @Autowired
    private DqRuleTypeService dqRuleTypeService;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "所有规则类型", notes = "所有规则类型")
    public MessageResult<List<DqRuleType>> pageList(){
        return MessageResult.data(dqRuleTypeService.selectAll()).status(200).message("查询成功").build();
    }
}
