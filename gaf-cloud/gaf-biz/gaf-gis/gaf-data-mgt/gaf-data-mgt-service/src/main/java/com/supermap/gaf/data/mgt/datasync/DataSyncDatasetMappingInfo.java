package com.supermap.gaf.data.mgt.datasync;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.supermap.gaf.data.mgt.entity.FieldMapping;
import com.supermap.gaf.utils.GlobalJacksonObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataSyncDatasetMappingInfo {
    private String srcName;
    private String destName;
    private FieldMapping[] fieldMappings;

    public static void main(String[] args) throws JsonProcessingException {
        DataSyncDatasetMappingInfo info = new DataSyncDatasetMappingInfo();
        info.setSrcName("CJDCQ");
        info.setDestName("MyCJDCQ");
        FieldMapping[] fieldMappings = {
                new FieldMapping("BSM","BSM"),
                new FieldMapping("YSDM","YSDM"),
                new FieldMapping("XZQDM","XZQDM"),
                new FieldMapping("XZQMC","XZQMC"),
                new FieldMapping("JSMJ","JSMJ"),
                new FieldMapping("MSSM","MSSM"),
                new FieldMapping("BZ","BZ"),
                new FieldMapping("DCMJ","DCMJ"),
                new FieldMapping("ZLDWMC","ZLDWMC"),
                new FieldMapping("test","test"),
                new FieldMapping("ZLDWDM","ZLDWDM"),
                new FieldMapping("HDMC","HDMC"),
                new FieldMapping("testfields","testfields1"),
                new FieldMapping("name","name")
        };
        info.setFieldMappings(fieldMappings);
        ObjectMapper writer = GlobalJacksonObjectMapper.instance();
        String sjon = writer.writeValueAsString(Arrays.asList(info));
        System.out.println(sjon);
//        "[{\"srcName\":\"CJDCQ\",\"destName\":\"MyCJDCQ\",\"fieldMappings\":[{\"srcName\":\"BSM\",\"destName\":\"BSM\"},{\"srcName\":\"ysdm\",\"destName\":\"ysdm\"},{\"srcName\":\"xzqdm\",\"destName\":\"xzqdm\"},{\"srcName\":\"xzqmc\",\"destName\":\"xzqmc\"},{\"srcName\":\"jsmj\",\"destName\":\"jsmj\"},{\"srcName\":\"mssm\",\"destName\":\"mssm\"},{\"srcName\":\"bz\",\"destName\":\"bz\"},{\"srcName\":\"dcmj\",\"destName\":\"dcmj\"},{\"srcName\":\"hdmc\",\"destName\":\"hdmc\"},{\"srcName\":\"zldwmc\",\"destName\":\"zldwmc\"},{\"srcName\":\"test\",\"destName\":\"test\"},{\"srcName\":\"zldwdm\",\"destName\":\"zldwdm\"},{\"srcName\":\"hdmc\",\"destName\":\"hdmc\"},{\"srcName\":\"testfields\",\"destName\":\"testfields1\"},{\"srcName\":\"name\",\"destName\":\"name\"}]}]\n"
    }
}
