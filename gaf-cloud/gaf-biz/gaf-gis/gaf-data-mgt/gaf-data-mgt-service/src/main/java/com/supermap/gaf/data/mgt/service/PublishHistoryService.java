package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.PublishHistory;
import com.supermap.gaf.data.mgt.entity.ServerInfo;
import com.supermap.gaf.data.mgt.entity.vo.PublishHistoryVo;

import java.util.List;

/**
 * @description
 * @date:2022/1/26 17:30
 * @author:yw
 */
public interface PublishHistoryService {

    void mongoUpdateService(String serviceName,String[] datasetNames);

    /**
     * 校验mongo连接是否成功
     * @param serverInfo
     * @return
     */
    boolean checkMongoConn(ServerInfo serverInfo);

    /**
     * 获取mongo连接信息下所有集合信息
     * @param serverInfo
     */
    List<String> getMongoCollection(ServerInfo serverInfo);
    /**
     * 条件查询
     * @param publishHistoryVo
     * @return
     */
    Page<PublishHistory> pageList(int pageNum, int pageSize, PublishHistoryVo publishHistoryVo);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    PublishHistory selectById(String id);

    /**
     * 跟新
     * @param publishHistory
     */
    void update(PublishHistory publishHistory);

    /**
     * 新增
     * @param publishHistory
     */
    void insert(PublishHistory publishHistory);

    /**
     * 根据id删除
     * @param id
     * @return
     */
    void delete(String id);

    /**
     * 根据id删除发布历史及其下的缓存构建历史，包括删除发布的服务,其下的缓存切片资源
     * @param id 发布历史id
     * @param isDeleteServices 是否删除发布的服务
     * @param isDeleteCacheTiles 是否删除缓存切片资源
     */
    void deleteWithResource(String id, Boolean isDeleteServices, Boolean isDeleteCacheTiles);
}
