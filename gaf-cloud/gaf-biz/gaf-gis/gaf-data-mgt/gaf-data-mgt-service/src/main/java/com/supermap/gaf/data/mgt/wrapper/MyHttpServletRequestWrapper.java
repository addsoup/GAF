package com.supermap.gaf.data.mgt.wrapper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Enumeration;
import java.util.HashMap;

/**
 * @author wxl
 * @since 2022/6/6
 */
public class MyHttpServletRequestWrapper extends HttpServletRequestWrapper {


    //private final byte[] body;

    private final HashMap<String,String> headMap;
    private final HashMap<String,String> requestParamMap;

    private final Cookie[] cookies;


    /**
     * Constructs a request object wrapping the given request.
     *
     * @param request The request to wrap
     * @throws IllegalArgumentException if the request is null
     */
    public MyHttpServletRequestWrapper(HttpServletRequest request) {
        super(request);

        headMap = new HashMap<>();
        Enumeration<String> headNameList = request.getHeaderNames();
        while (headNameList.hasMoreElements()){
            String key = headNameList.nextElement();
            headMap.put(key.toLowerCase(),request.getHeader(key));
        }

        requestParamMap = new HashMap<>();
        Enumeration<String> parameterNameList = request.getParameterNames();
        while (parameterNameList.hasMoreElements()){
            String key = parameterNameList.nextElement();
            requestParamMap.put(key,request.getParameter(key));
        }

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            int cookiesLength = cookies.length;
            this.cookies = new Cookie[cookiesLength];
            for (int i = 0; i < cookiesLength; i++) {
                Cookie cookie = cookies[i];
                Cookie copy = new Cookie(cookie.getName(), cookie.getValue());
                copy.setMaxAge(cookie.getMaxAge());
                copy.setPath(cookie.getPath());
                copy.setComment(cookie.getComment());
                copy.setHttpOnly(cookie.isHttpOnly());
                if (cookie.getDomain() != null) {
                    copy.setDomain(cookie.getDomain());
                }
                copy.setSecure(cookie.getSecure());
                copy.setVersion(cookie.getVersion());
                this.cookies[i] = copy;
            }
        } else {
            this.cookies = null;
        }

    }

    @Override
    public String getHeader(String name) {
        return headMap.get(name.toLowerCase());
    }

    @Override
    public String getParameter(String name) {
        return requestParamMap.get(name);
    }

    @Override
    public Cookie[] getCookies() {
        return this.cookies;
    }
}
