package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.DataRange;
import com.supermap.gaf.data.mgt.entity.DataStandard;
import com.supermap.gaf.validGroup.AddGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 数据标准Vo
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "数据标准Vo",description = "将数据范围一并返回")
public class DataStandardVo extends DataStandard {

    @ApiModelProperty("值域范围设置。 ")
    @Valid
    private List<DataRange> dataRanges;
    @ApiModelProperty("标准代码名称。查询回显 ")
    private String dataCodeName;
}