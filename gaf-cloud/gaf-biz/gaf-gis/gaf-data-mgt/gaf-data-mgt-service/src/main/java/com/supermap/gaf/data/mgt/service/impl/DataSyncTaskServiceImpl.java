package com.supermap.gaf.data.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.data.Dataset;
import com.supermap.data.DatasetVector;
import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import com.supermap.gaf.data.mgt.dao.DataSyncLogMapper;
import com.supermap.gaf.data.mgt.dao.DataSyncTaskMapper;
import com.supermap.gaf.data.mgt.datasync.DataSyncDatasetMappingInfo;
import com.supermap.gaf.data.mgt.datasync.DataSyncJob;
import com.supermap.gaf.data.mgt.entity.DataSyncTask;
import com.supermap.gaf.data.mgt.entity.DatasetChangeChain;
import com.supermap.gaf.data.mgt.entity.changeop.ChangeOpChain;
import com.supermap.gaf.data.mgt.entity.changeop.dataset.*;
import com.supermap.gaf.data.mgt.entity.vo.DataSyncTaskSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DataSyncTaskVo;
import com.supermap.gaf.data.mgt.log.DataSyncLogger;
import com.supermap.gaf.data.mgt.publisher.MailSendPublisher;
import com.supermap.gaf.data.mgt.service.DataSyncTaskService;
import com.supermap.gaf.data.mgt.service.SysResourceDatasourceService;
import com.supermap.gaf.data.mgt.support.ConvertHelper;
import com.supermap.gaf.data.mgt.util.IobjectUtils;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.quartz.service.QuartzService;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.utils.GlobalJacksonObjectMapper;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.SynchronousQueue;

/**
 * 数据同步任务表服务实现类
 *
 * @author zrc
 * @date yyyy -mm-dd
 */
@Service
public class DataSyncTaskServiceImpl implements DataSyncTaskService{
    public static final String JOB_GROUP = "DATA-SYNC";
    
	private static final Logger  log = LoggerFactory.getLogger(DataSyncTaskServiceImpl.class);
	
	@Autowired
    private DataSyncTaskMapper dataSyncTaskMapper;

	@Autowired
	private SysResourceDatasourceService datasourceService;

    @Autowired
    private ConvertHelper convertHelper;

    @Autowired
    private DataSyncLogMapper dataSyncLogMapper;

    @Autowired
    private QuartzService quartzService;

    @Autowired
    private MailSendPublisher mailSendPublisher;

	@Override
    public DataSyncTask getById(String dataSyncTaskId){
        if(dataSyncTaskId == null){
            throw new IllegalArgumentException("dataSyncTaskId不能为空");
        }
        return dataSyncTaskMapper.select(dataSyncTaskId);
    }
    @Override
    public DataSyncTaskVo getVoById(String dataSyncTaskId){
        DataSyncTask syncTask = getById(dataSyncTaskId);
        SysResourceDatasource[] sysResourceDatasources = validateDatasourceId(syncTask);
        SysResourceDatasource destSysDatasource =  sysResourceDatasources[1];
        SysResourceDatasource srcSysDatasource =  sysResourceDatasources[0];
        DataSyncTaskVo re = new DataSyncTaskVo();
        BeanUtils.copyProperties(syncTask,re);
        re.setSpatialDatasourceName(srcSysDatasource.getDsName());
        re.setSpatialDatasourceTypeCode(srcSysDatasource.getTypeCode());
        re.setStandardDatasourceName(destSysDatasource.getDsName());
        re.setStandardDatasourceTypeCode(destSysDatasource.getTypeCode());
        return re;
    }
	
	@Override
    public Page<DataSyncTask> listByPageCondition(DataSyncTaskSelectVo dataSyncTaskSelectVo, int pageNum, int pageSize) {
        PageInfo<DataSyncTask> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dataSyncTaskMapper.selectList(dataSyncTaskSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }


	@Transactional(rollbackFor = Exception.class)
    @Override
    public DataSyncTask insertDataSyncTask(DataSyncTask dataSyncTask){
        // 主键非GeneratedKey，此处添加自定义主键生成策略
		dataSyncTask.setDataSyncTaskId(UUID.randomUUID().toString());
        dataSyncTask.setDataSyncStatus(DataSyncTask.DataSyncStatus.NONE.getCode());
        String userName = SecurityUtilsExt.getUserName();
        dataSyncTask.setCreatedBy(userName);
        dataSyncTask.setUpdatedBy(userName);
        if(!StringUtils.isEmpty(dataSyncTask.getMappingInfo())){
            validateMappingInfo(dataSyncTask.getMappingInfo());
        }
        validateTackName(dataSyncTask.getDataSyncTaskName(),null);
        dataSyncTaskMapper.insert(dataSyncTask);
        addJob(dataSyncTask);
        return dataSyncTask;
    }


	@Override
    public void deleteDataSyncTask(String dataSyncTaskId){
        batchDelete(Arrays.asList(dataSyncTaskId));
    }

	@Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDelete(List<String> dataSyncTaskIds){
	    if(!CollectionUtils.isEmpty(dataSyncTaskIds)){
            dataSyncTaskMapper.batchDelete(dataSyncTaskIds);
            dataSyncLogMapper.batchDeleteByTaskIds(dataSyncTaskIds);
            for(String item:dataSyncTaskIds){
                quartzService.deleteScheduleJob(item,JOB_GROUP);
            }
        }

    }

    public void validateTackName(String name,String exceptId){
        List<DataSyncTask> exists = dataSyncTaskMapper.selectList(DataSyncTaskSelectVo.builder().dataSyncTaskName(name).build());
        if(CollectionUtils.isEmpty(exists)){
            return;
        }
        if(exceptId!= null && exists.size()==1 && exceptId.equals(exists.get(0))){
            return;
        }
        throw new GafException("任务名称重复");
	}


    /**
     * Validate mapping info data sync dataset mapping info [ ].
     *
     * @param mappingInfo the mapping info
     * @return the data sync dataset mapping info [ ]
     */
    DataSyncDatasetMappingInfo[] validateMappingInfo(String mappingInfo){
        try {
            return GlobalJacksonObjectMapper.instance().readValue(mappingInfo, DataSyncDatasetMappingInfo[].class);
        } catch (IOException e) {
            throw new GafException("映射配置格式解析错误："+e.getMessage());
        }
    }

    /**
     * Validate datasource id sys resource datasource [ ].
     *
     * @param syncTask the sync task
     * @return the sys resource datasource [ ]
     */
    SysResourceDatasource[] validateDatasourceId(DataSyncTask syncTask){
        SysResourceDatasource destSysDatasource =  datasourceService.getById(syncTask.getStandardDatasourceId());
        if(destSysDatasource == null){
            throw new GafException("目标数据源不存在");
        }
        SysResourceDatasource srcSysDatasource =  datasourceService.getById(syncTask.getSpatialDatasourceId());
        if(srcSysDatasource == null){
            throw new GafException("来源数据源不存在");
        }
        return new SysResourceDatasource[]{srcSysDatasource,destSysDatasource};
    }
	@Override
    @Transactional(rollbackFor = Exception.class)
    public int updateDataSyncTask(DataSyncTask dataSyncTask){
        DataSyncTask old = getById(dataSyncTask.getDataSyncTaskId());
        if(old == null){
            return 0;
        }
        if(dataSyncTask.getDataSyncTaskName()!=null && !old.getDataSyncTaskName().equals(dataSyncTask.getDataSyncTaskName())){
            validateTackName(dataSyncTask.getDataSyncTaskName(),dataSyncTask.getDataSyncTaskId());
        }
        try {
            CronExpression.validateExpression(dataSyncTask.getDataSyncCron());
        } catch (ParseException e) {
            throw new GafException("cron表达式错误："+e.getMessage());
        }
        dataSyncTask.setUpdatedBy(SecurityUtilsExt.getUserName());
        if(!StringUtils.isEmpty(dataSyncTask.getMappingInfo())){
            validateMappingInfo(dataSyncTask.getMappingInfo());
        }

        dataSyncTaskMapper.update(dataSyncTask);
        quartzService.deleteScheduleJob(dataSyncTask.getDataSyncTaskId(),JOB_GROUP);
        addJob(dataSyncTask);
        return 1;
    }

    /**
     * Add job. 错过了不再执行
     *
     * @param dataSyncTask the data sync task
     */
    void addJob(DataSyncTask dataSyncTask){
        try{
            JobDataMap data = new JobDataMap();
            data.put("id",dataSyncTask.getDataSyncTaskId());
            String cron = dataSyncTask.getDataSyncCron();
            String jobName = dataSyncTask.getDataSyncTaskId();
            quartzService.scheduleCronJob(DataSyncJob.class,jobName,JOB_GROUP,cron,data);
        }catch (Exception e){
            e.printStackTrace();
            throw new GafException("添加定时任务失败:"+e.getMessage());
        }
    }




    /**
     *
     * @param dataSyncTaskId the data sync task id
     */
    @Override
    public void startSyncJob(String dataSyncTaskId){
        DataSyncTask syncTask = getById(dataSyncTaskId);
        if(syncTask == null){
            throw new GafException("任务不存在");
        }
        try{
            validateDatasourceId(syncTask);
            JobDataMap data = new JobDataMap();
            data.put("id",dataSyncTaskId);
            String jobName = "now_"+syncTask.getDataSyncTaskId();
            //创建任务触发器
            Trigger trigger =  TriggerBuilder.newTrigger()
                    .withIdentity(jobName, JOB_GROUP)
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withMisfireHandlingInstructionFireNow())
                    .startNow()
                    .build();
            //创建任务属性
            JobDetail jobDetail = JobBuilder.newJob(DataSyncJob.class)
                    .withIdentity(jobName, JOB_GROUP)
                    .usingJobData(data)
                    .build();
            quartzService.scheduleJob(jobDetail,trigger);
        }catch (ObjectAlreadyExistsException e){
            throw new GafException("已经存在相同的一个立即执行任务");
        }catch (Exception e){
            e.printStackTrace();
            throw new GafException("创建立即执行任务失败:"+e.getMessage());
        }
    }


    /**
     * Update status.
     *
     * @param dataSyncTaskId the data sync task id
     * @param syncStatus     the sync status
     */
    void updateStatus(String dataSyncTaskId, DataSyncTask.DataSyncStatus syncStatus){
        dataSyncTaskMapper.updateStatus(dataSyncTaskId,syncStatus);
    }



    @Override
    public void execute(String dataSyncTaskId) {
        DataSyncTask syncTask = getById(dataSyncTaskId);
        if(syncTask == null){
            quartzService.deleteScheduleJob(syncTask.getDataSyncTaskId(),JOB_GROUP);
            throw new GafException("任务不存在");
        }
        DataSyncLogger logger = new DataSyncLogger(dataSyncLogMapper, syncTask, mailSendPublisher);
	    try{
            logger.info("开始执行同步。");
            SysResourceDatasource[] sysResourceDatasources = validateDatasourceId(syncTask);
            SysResourceDatasource destSysDatasource =  sysResourceDatasources[1];
            SysResourceDatasource srcSysDatasource =  sysResourceDatasources[0];
            // 正在执行状态
            updateStatus(dataSyncTaskId, DataSyncTask.DataSyncStatus.RUNNING);
            logger.info("参数：【源：{},目标：{},映射：{}】",syncTask.getSpatialDatasourceId(),syncTask.getStandardDatasourceId(),syncTask.getMappingInfo());
            DatasourceConnectionInfo destConn = convertHelper.conver2DatasourceConnectionInfo(destSysDatasource);
            DatasourceConnectionInfo srcConn = convertHelper.conver2DatasourceConnectionInfo(srcSysDatasource);
            DataSyncDatasetMappingInfo[] mappingInfos = validateMappingInfo(syncTask.getMappingInfo());

            IobjectUtils.datasourceProcessor(srcConn, srcDatasource ->{
                IobjectUtils.datasourceProcessor(destConn,destDatasource -> {
                    for(DataSyncDatasetMappingInfo mappingInfo:mappingInfos){
                        long startTime = System.currentTimeMillis();
                        logger.info("正在将数据集{}同步到{}...",mappingInfo.getSrcName(),mappingInfo.getDestName());
                        Dataset srcDataset = srcDatasource.getDatasets().get(mappingInfo.getSrcName());
                        Dataset destDataset = destDatasource.getDatasets().get(mappingInfo.getDestName());
                        String updatingName = destDatasource.getDatasets().getAvailableDatasetName("updating_"+mappingInfo.getDestName());
                        ChangeOpChain changeChain = null;
                        Long totalAmount = null;
                        if(!ArrayUtils.isEmpty(mappingInfo.getFieldMappings())){
                            if(!(srcDataset instanceof DatasetVector)){
                                throw new GafException(String.format("数据集%s不支持字段映射",srcDataset.getName()));
                            }
                            // 矢量数据源
                            changeChain = new DatasetChangeChain(updatingName)
                                    // 创建结构一样的临时数据源
                                    .addOp(CreateFromTemplate.builder().templateDataset(destDataset))
                                    // 按照映射关系向临时数据源导入数据
                                    .addOp(ImportFromOtherDataset.builder().srcDataset((DatasetVector) srcDataset).fieldMappings(mappingInfo.getFieldMappings()));
                        }else{
                            // 非矢量数据源
                            changeChain = new DatasetChangeChain(updatingName)
                                    // 直接复制创建临时数据源
                                    .addOp(CopyFromExisted.builder().encodeType(srcDataset.getEncodeType()).fromDataset(srcDataset));
                        }
                        changeChain
                                // 将临时数据源替换成目标数据源
                                .addOp(new DatasetChangeChain(mappingInfo.getDestName())
                                        .addOp(Delete.builder())
                                )
                                .addOp(ChangeName.builder().newName(mappingInfo.getDestName()))
                                // 执行修改链
                                .update(destDatasource);
                        if(!ArrayUtils.isEmpty(mappingInfo.getFieldMappings())){
                            totalAmount = IobjectUtils.getRecordAmount(destDatasource.getDatasets().get(mappingInfo.getDestName()));
                        }
                        long castTime = System.currentTimeMillis()-startTime;
                        logger.addSyncAmount(totalAmount);
                        logger.info("数据集{}同步到{}成功。累积耗时：{}ms {}",mappingInfo.getSrcName(),mappingInfo.getDestName(),castTime,(totalAmount!=null?"，同步记录数："+totalAmount:""));
                    }
                    return null;
                });
                return null;
            });
            // 执行成功状态
            updateStatus(dataSyncTaskId, DataSyncTask.DataSyncStatus.RECENT_SUCCESS);
            logger.info("同步完成");
        }catch (Exception e){
	        e.printStackTrace();
	        logger.error("同步失败：{}",e.getMessage());
            // 执行失败状态
            updateStatus(dataSyncTaskId, DataSyncTask.DataSyncStatus.RECENT_FAIL);
	        throw new GafException(e);
        }finally {
            logger.close();
        }
    }




}
