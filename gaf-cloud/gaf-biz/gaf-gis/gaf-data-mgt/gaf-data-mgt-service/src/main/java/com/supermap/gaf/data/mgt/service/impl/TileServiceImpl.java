package com.supermap.gaf.data.mgt.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.data.processing.CacheWriter;
import com.supermap.data.processing.StorageType;
import com.supermap.gaf.common.storage.client.StorageClient;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.common.IServerManager;
import com.supermap.gaf.data.mgt.entity.HostServerSetting;
import com.supermap.gaf.data.mgt.entity.Tile;
import com.supermap.gaf.data.mgt.entity.iserver.BaseResponse;
import com.supermap.gaf.data.mgt.entity.iserver.MongoDBMVTTileProviderConfig;
import com.supermap.gaf.data.mgt.entity.iserver.PublishResult;
import com.supermap.gaf.data.mgt.entity.iserver.PublishTileParam;
import com.supermap.gaf.data.mgt.entity.vo.TileSelectVo;
import com.supermap.gaf.data.mgt.enums.TileSourceTypeEnum;
import com.supermap.gaf.data.mgt.mapper.TileMapper;
import com.supermap.gaf.data.mgt.model.MongoDBTileSourceInfo;
import com.supermap.gaf.data.mgt.model.TileDetailInfo;
import com.supermap.gaf.data.mgt.model.TileSourceInfo;
import com.supermap.gaf.data.mgt.service.TileService;
import com.supermap.gaf.data.mgt.support.ConvertHelper;
import com.supermap.gaf.data.mgt.util.IserverUtils;
import com.supermap.gaf.data.mgt.util.IserverUtils2;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.tilestorage.*;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 瓦片服务实现类
 * @author wxl
 * @date yyyy-mm-dd
 */
@Service
public class TileServiceImpl implements TileService{
    
	private static final Logger  log = LoggerFactory.getLogger(TileServiceImpl.class);
    @Autowired
    private IServerManager iServerManager;

	@Autowired
    private TileMapper tileMapper;
	
    @Autowired
    private ConvertHelper convertHelper;

    @Autowired
    @Qualifier("DatamgtStorageClient")
    private StorageClient storageClient;

	@Override
    public Tile getById(String tileId){
        if(tileId == null){
            throw new IllegalArgumentException("tileId不能为空");
        }
        return tileMapper.select(tileId);
    }

    @Override
    public List<Tile> selectList(TileSelectVo tileSelectVo) {
        return tileMapper.selectList(tileSelectVo);
    }

    @Override
    public Page<Tile> listByPageCondition(TileSelectVo tileSelectVo, int pageNum, int pageSize) {
        PageInfo<Tile> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            tileMapper.selectList(tileSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }


	@Override
    public Tile insertTile(Tile tile){
		tile.setTileId(UUID.randomUUID().toString());
        String userName = SecurityUtilsExt.getUserName();
        tile.setCreatedBy(userName);
        tile.setUpdatedBy(userName);
        tileMapper.insert(tile);
        return getById(tile.getTileId());
    }
	
	@Override
    public void batchInsert(List<Tile> tiles){
		if (tiles != null && tiles.size() > 0) {
	        tiles.forEach(tile -> {
				tile.setTileId(UUID.randomUUID().toString());
            });
            tileMapper.batchInsert(tiles);
        }
        
    }
	

    @Transactional(rollbackFor = Exception.class)
	@Override
    public Tile deleteTile(String tileId){
        Tile tile = getById(tileId);
        tileMapper.delete(tileId);
        if (TileSourceTypeEnum.UGCV5.getCode().equals(tile.getSourceType())) {
            try {
                String sourceInfo = tile.getSourceInfo();
                JSONObject sourceInfoJ = JSONObject.parseObject(sourceInfo);
                String path = sourceInfoJ.getString("path");
                int i = StringUtils.ordinalIndexOf(path, "/", 3);
                String dir = path.substring(0, i + 1);
                storageClient.delete(dir,SecurityUtilsExt.getTenantId());
            } catch (Exception e) {
                log.error("删除ugcv5瓦片文件失败",e);
            }
        }
        return tile;
    }

	@Override
    public void batchDelete(List<String> tileIds){
	    if(tileIds != null) {
            for (String tileId : tileIds) {
                deleteTile(tileId);
            }
        }
    }
	

	@Override
    public Tile updateTile(Tile tile){
        tile.setUpdatedBy(SecurityUtilsExt.getUserName());
        tileMapper.update(tile);
        return getById(tile.getTileId());
    }

    private List<String> listUgcv5TilesetNamesByRealPath(String  realPath) {
        try {
            String content = new String(Files.readAllBytes(Paths.get(realPath)));
            String replaced = content.replace("sml:", "");
            InputStream inputStream = new ByteArrayInputStream(replaced.getBytes());
            SAXReader reader = new SAXReader();
            Document document =reader.read(inputStream);
            Element root = document.getRootElement();
            Element cacheNameEl = root.element("CacheName");
            String textTrim = cacheNameEl.getTextTrim();
            return Collections.singletonList(textTrim);
        } catch (IOException | DocumentException e) {
            log.error(e.getMessage(),e);
            throw new GafException(e.getMessage(),e);
        }

    };

    @Override
    public List<String> listUgcv5TilesetNames(String path) {
        String realPath = convertHelper.resolve(path);
        return listUgcv5TilesetNamesByRealPath(realPath);
    }

    @Override
    public List<String> listMongoTilesetNames(MongoDBTileSourceInfo info) {
        TileStorageMiniInfo[] tileStorageMiniInfos = TileStorageManager.getMongoTileStorageMiniInfo(info.getServerAdresses(), info.getDatabse(), info.getUsername(), info.getPassword());
        return Arrays.stream(tileStorageMiniInfos).map(TileStorageMiniInfo::getName).collect(Collectors.toList());
    }

    TileSourceInfo convert2TileSourceInfo(Tile tile){
        TileSourceInfo tileSourceInfo = new TileSourceInfo();
        String sourceInfo = tile.getSourceInfo();
        JSONObject json = JSON.parseObject(sourceInfo);
        String path = json.getString("path");
        //String path = tileSourceInfo.getPath();
        if(path!=null){
            path = storageClient.getVolumePath(path,SecurityUtilsExt.getTenantId(),false).getPath();
            tileSourceInfo.setPath(path);
            return tileSourceInfo;
        }else{
            // 保持非文件型path为null
            tileSourceInfo.setPath(null);
        }
        tileSourceInfo.setDatabse(json.getString("dbName"));
        tileSourceInfo.setUsername(json.getString("userName"));
        tileSourceInfo.setServerAdresses(json.getString("addr"));
        tileSourceInfo.setPassword(json.getString("password"));
        return tileSourceInfo;
    }

    @Override
    public TileDetailInfo getDetail(String tileId) {
        Tile tile = getById(tileId);
        String sourceType = tile.getSourceType();
        if (TileSourceTypeEnum.UGCV5.getCode().equalsIgnoreCase(sourceType)) {
            String sourceInfo = tile.getSourceInfo();
            JSONObject sourceInfoJO = JSONObject.parseObject(sourceInfo);
            String path = sourceInfoJO.getString("path");
            String realPath = convertHelper.resolve(path);
            CacheWriter cacheWriter = new CacheWriter();
            Boolean success = cacheWriter.FromConfigFile(realPath);
            if (!success) {
                throw new GafException("获取或解析瓦片配置文件失败");
            }
            TileDetailInfo tileDetailInfo = new TileDetailInfo();
            tileDetailInfo.setStorageType(alias(cacheWriter.getStorageType().name()));
            tileDetailInfo.setImageType(cacheWriter.getTileFormat().name());
            tileDetailInfo.setBlockSize(cacheWriter.getTileSize().value());
            tileDetailInfo.setTransparent(cacheWriter.getTransparent());
            tileDetailInfo.setCoordSysName(cacheWriter.getPrjCoordSys().getName());
            double[] originLocation = {cacheWriter.getIndexBounds().getLeft(),cacheWriter.getIndexBounds().getTop()};
            tileDetailInfo.setOriginLocation(originLocation);
            HashMap<Double, String> cacheScaleCaptions = cacheWriter.getCacheScaleCaptions();
            List<Double> scales = cacheScaleCaptions.keySet().stream().sorted(Comparator.comparingDouble(value -> value)).collect(Collectors.toList());
            tileDetailInfo.setScales(scales);
            return tileDetailInfo;
        } else if (TileSourceTypeEnum.MONGODB.getCode().equalsIgnoreCase(sourceType)) {
            String sourceInfoStr = tile.getSourceInfo();
            JSONObject sourceInfoJO = JSONObject.parseObject(sourceInfoStr);

            TileStorageConnection tileStorageConnection = new TileStorageConnection();
            tileStorageConnection.setStorageType(TileStorageType.MONGO);
            tileStorageConnection.setServer(sourceInfoJO.getString("addr"));
            tileStorageConnection.setDatabase(sourceInfoJO.getString("dbName"));
            tileStorageConnection.setUser(sourceInfoJO.getString("userName"));
            tileStorageConnection.setPassword(sourceInfoJO.getString("password"));
            tileStorageConnection.setName(tile.getTileName());

            TileStorageManager tileStorageManager = new TileStorageManager();
            try {
                boolean open = tileStorageManager.open(tileStorageConnection);
                if (open) {
                    TileStorageInfo info = tileStorageManager.getInfo();
                    TileDetailInfo tileDetailInfo = new TileDetailInfo();
                    tileDetailInfo.setStorageType(alias(info.getStorageType().name()));
                    tileDetailInfo.setImageType(info.getDataType().name());
                    tileDetailInfo.setBlockSize(info.getTileStorageSize().value());
                    tileDetailInfo.setTransparent(info.getTransParent());
                    tileDetailInfo.setCoordSysName(info.getPrjCoordSys().getName());
                    double[] originLocation = {info.getOrigin().getX(),info.getOrigin().getY()};
                    tileDetailInfo.setOriginLocation(originLocation);
                    List<Double> scales = Arrays.stream(tileStorageManager.getVersions()[0].getScales()).map(operand -> 1.0D / operand).sorted().boxed().collect(Collectors.toList());
                    tileDetailInfo.setScales(scales);
                    return tileDetailInfo;
                } else {
                    throw new GafException("打开连接失败");
                }
            } finally {
                tileStorageManager.close();
                tileStorageManager.dispose();
            }
        } else {
            throw new GafException("暂不支持来源类型"+sourceType);
        }

    }

    void validateTileSourceInfo(TileSourceInfo tileSourceInfo){
        List<String> tilesetNames = null;
        if(tileSourceInfo.getPath()!=null){
            tilesetNames = listUgcv5TilesetNamesByRealPath(tileSourceInfo.getPath());
        }else{
            tilesetNames = listMongoTilesetNames(tileSourceInfo);
        }
        if(CollectionUtils.isEmpty(tilesetNames)){
            throw new GafException("没有可用的瓦片");
        }
    }

    @Override
    public List<PublishResult> publishMap(String tileId, PublishTileParam param) {
        Tile tile = getById(tileId);
        if(tile == null){
            throw new GafException("瓦片不存在");
        }
        // 使用存储在数据库中的连接信息
        TileSourceInfo tileSourceInfo = convert2TileSourceInfo(tile);
        param.setFilePath(tileSourceInfo.getPath());
        param.setUsername(tileSourceInfo.getUsername());
        param.setPassword(tileSourceInfo.getPassword());
        param.setDatabase(tileSourceInfo.getDatabse());
        if(tileSourceInfo.getServerAdresses()!=null){
            param.setServerAdresses(tileSourceInfo.getServerAdresses().split(","));
        }
        param.setName("GAF-"+tile.getTileName()+System.currentTimeMillis());
        validateTileSourceInfo(tileSourceInfo);
        BaseResponse<List<PublishResult>> baseResponse = IserverUtils.publishMapTile(param,iServerManager.getAvailableIServerSetting().getHostServerUrl(),false, HostServerSetting.TOKEN_NAME,getIserverToken());
        return getPublishResults(baseResponse);
    }


    @Override
    public List<PublishResult> publish3D(MongoDBMVTTileProviderConfig  param) {
        BaseResponse<List<PublishResult>> baseResponse = IserverUtils2.publish3DTile(param,iServerManager.getAvailableIServerSetting().getHostServerUrl(),false, HostServerSetting.TOKEN_NAME,getIserverToken());
        return getPublishResults(baseResponse);
    }


    @Override
    public List<PublishResult> publish3D(String tileId) {
        Tile tile = getById(tileId);
        if(tile == null){
            throw new GafException("瓦片不存在");
        }
        log.info("发布瓦片【{}】为三维服务",tile.getTileName());
        // 使用存储在数据库中的连接信息
        MongoDBMVTTileProviderConfig param = new MongoDBMVTTileProviderConfig();
        TileSourceInfo tileSourceInfo = convert2TileSourceInfo(tile);
        if(tileSourceInfo.getPath()!=null){
            throw new GafException("文件瓦片不支持发布三维服务");
        }
        validateTileSourceInfo(tileSourceInfo);
        param.setUsername(tileSourceInfo.getUsername());
        param.setPassword(tileSourceInfo.getPassword());
        param.setDatabase(tileSourceInfo.getDatabse());
        if(tileSourceInfo.getServerAdresses()!=null){
            param.setServerAdresses(tileSourceInfo.getServerAdresses().split(","));
        }
        param.setName("GAF-"+tile.getTileName()+System.currentTimeMillis());
        BaseResponse<List<PublishResult>> baseResponse = IserverUtils2.publish3DTile(param,iServerManager.getAvailableIServerSetting().getHostServerUrl(),false, HostServerSetting.TOKEN_NAME,getIserverToken());
        return getPublishResults(baseResponse);
    }

    private String alias(String storageTypeName) {
	    if (StorageType.Compact.name().equalsIgnoreCase(storageTypeName)) {
	        return "紧凑";
        } else if (StorageType.Original.name().equalsIgnoreCase(storageTypeName)) {
	        return "原始";
        } else if (TileStorageType.MONGO.name().equalsIgnoreCase(storageTypeName)) {
	        return "Mongo";
        } else if (TileStorageType.MONGOV2.name().equalsIgnoreCase(storageTypeName)) {
            return "MongoV2";
        } else if (TileStorageType.MVT.name().equalsIgnoreCase(storageTypeName)) {
	        return "mvt";
        } else if (TileStorageType.MONGO3D.name().equalsIgnoreCase(storageTypeName)) {
	        return "Mongo3D";
        } else if (TileStorageType.MONGOOSG.name().equals(storageTypeName)) {
	        return "MongoOSG";
        } else {
	        return storageTypeName;
        }
    }


    List<PublishResult> getPublishResults(BaseResponse<List<PublishResult>> baseResponse){
        if(!baseResponse.getSucceed()){
            throw baseResponse.toGafException();
        }
        return baseResponse.getData();
    }


    String getIserverToken(){
        MessageResult<String> tokenResult = iServerManager.applyToken();
        if (!tokenResult.isSuccessed()) {
            log.error("iServer token 申请失败:",tokenResult.getMessage());
            throw new GafException("iServer token 申请失败");
        }
        return tokenResult.getData();
    }
}
