package com.supermap.gaf.data.mgt.dao;
import com.supermap.gaf.data.mgt.entity.DataSyncTask;
import java.util.*;

import com.supermap.gaf.data.mgt.entity.vo.DataSyncTaskSelectVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * 数据同步任务表数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface DataSyncTaskMapper{
	/**
     * 根据主键 dataSyncTaskId查询
     * 
	 */
    DataSyncTask select(@Param("dataSyncTaskId") String dataSyncTaskId);
	
	/**
     * 多条件查询
     * @param dataSyncTaskSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<DataSyncTask> selectList(DataSyncTaskSelectVo dataSyncTaskSelectVo);

    /**
	 * 新增
	 * 
	 */
    int insert(DataSyncTask dataSyncTask);
	
	/**
     * 批量插入
     * 
	 */
    int batchInsert(List<DataSyncTask> dataSyncTasks);
	
	/**
     * 批量删除
     *
	 */
    int batchDelete(List<String> dataSyncTaskIds);

	/**
     * 刪除
     * 
	 */
    int delete(@Param("dataSyncTaskId") String dataSyncTaskId);
    /**
     * 更新
     *
	 */
    int update(DataSyncTask dataSyncTask);

	/**
	 * 更新
	 *
	 */
	int updateStatus(@Param("dataSyncTaskId") String dataSyncTaskId, @Param("status")DataSyncTask.DataSyncStatus status);
}
