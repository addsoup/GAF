package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.sys.mgt.enums.CatalogTypeEnum;

/**
 * The interface Common validate service.
 */
public interface CommonValidateService {
    /**
     * Validate catalog id.
     * 目录存在校验和目录类型校验
     *
     * @param catalogId       the catalog id
     * @param catalogTypeEnum the catalog type enum
     */
    void validateCatalogId(String catalogId, CatalogTypeEnum catalogTypeEnum);

    /**
     * Validate ref code.
     * 引用代码存在校验
     *
     * @param refCode the ref code
     */
    void validateRefCode(String refCode);
}
