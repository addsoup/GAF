package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.DataSyncLog;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 数据同步日志表 条件查询实体
 * @author zrc
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据同步日志表条件查询实体")
public class DataSyncLogSelectVo {
    @QueryParam("searchFieldName")
    @ApiModelProperty("模糊查询字段名")
    @StringRange(entityClass = DataSyncLog.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("模糊查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = DataSyncLog.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = DataSyncLog.class)
    private String orderFieldName = "created_time";
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod = "desc";
    @QueryParam("dataSyncLogId")
    @ApiModelProperty("主键")
    private String dataSyncLogId;
    @QueryParam("dataSyncTaskId")
    @ApiModelProperty("所属任务")
    private String dataSyncTaskId;
    @QueryParam("content")
    @ApiModelProperty("记录日志")
    private String content;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("更新人")
    private String updatedBy;
    @QueryParam("costTime")
    @ApiModelProperty("耗费时间-秒")
    private Long costTime;
    @QueryParam("totalAmount")
    @ApiModelProperty("数据总量")
    private Long totalAmount;
    @QueryParam("resultStatus")
    @ApiModelProperty("执行状态。0代表成功，1代表失败。")
    private String resultStatus;
}