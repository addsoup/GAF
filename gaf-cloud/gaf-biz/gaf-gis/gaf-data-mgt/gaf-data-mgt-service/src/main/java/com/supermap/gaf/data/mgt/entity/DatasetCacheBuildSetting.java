package com.supermap.gaf.data.mgt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wxl
 * @since 2022/5/25
 */
@Data
@ApiModel("数据集切片设置")
public class DatasetCacheBuildSetting {

    @ApiModelProperty("数据源id")
    private String datasourceId;

    @ApiModelProperty("数据集名称")
    private String datasetName;

    //@ApiModelProperty("切片存储名")
    //private String name;

    @ApiModelProperty("切片类型，增量（increment）或者全量（fullVolume）")
    private String cacheType;

    @ApiModelProperty("瓦片类型，即数据集类型 ,暂时支持GRID,IMAGE,MODEL,POINT,LINE,REGION")
    private String tileType;

    @ApiModelProperty("是否轻量化")
    private Boolean isSimp;
    @ApiModelProperty("简化率(轻量化专属)")
    private Double simplifyingRate;

    @ApiModelProperty("颜色.用于矢量数据集 点线面")
    private String color;
    @ApiModelProperty("增量切片查询条件")
    private String queryParameter;
    @ApiModelProperty("描述")
    private String description;


}
