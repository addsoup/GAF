package com.supermap.gaf.data.mgt.mapper;

import com.supermap.gaf.data.mgt.entity.DqExecuteLog;
import com.supermap.gaf.data.mgt.entity.DqExecuteResult;
import com.supermap.gaf.data.mgt.entity.vo.DqExecuteLogSelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 数据质量管理-执行日志表数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface DqExecuteLogMapper{
	/**
     * 根据主键 dqExecuteLogId查询
     * 
	 */
    DqExecuteLog select(@Param("dqExecuteLogId") String dqExecuteLogId);
	
	/**
     * 多条件查询
     * @param dqExecuteLogSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<DqExecuteLog> selectList(DqExecuteLogSelectVo dqExecuteLogSelectVo);

    /**
	 * 新增
	 * 
	 */
    int insert(DqExecuteLog dqExecuteLog);
	
	/**
     * 批量插入
     * 
	 */
    int batchInsert(List<DqExecuteLog> dqExecuteLogs);
	
	/**
     * 批量删除
     *
	 */
    int batchDelete(List<String> dqExecuteLogIds);

	/**
     * 刪除
     * 
	 */
    int delete(@Param("dqExecuteLogId") String dqExecuteLogId);


	int deleteByProgramIds(List<String> dataQualityProgramIds);

    /**
     * 更新
     *
	 */
    int update(DqExecuteLog dqExecuteLog);

	/**
	 * 统计方案执行结果
	 * @param dataQualityProgramId
	 * @return
	 */
	List<DqExecuteResult> statisticsExecuteResultByProgramId(@Param("dataQualityProgramId") String dataQualityProgramId, @Param("startTime")Date startTime, @Param("endTime")Date endTime);
}
