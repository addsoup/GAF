package com.supermap.gaf.data.mgt.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.common.storage.client.StorageClient;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import com.supermap.gaf.data.mgt.entity.CacheBuildHistory;
import com.supermap.gaf.data.mgt.entity.PublishHistory;
import com.supermap.gaf.data.mgt.entity.ServerInfo;
import com.supermap.gaf.data.mgt.entity.vo.CacheBuildHistoryLatestVO;
import com.supermap.gaf.data.mgt.entity.vo.CacheBuildHistoryVo;
import com.supermap.gaf.data.mgt.enums.CacheBuildStatusEnum;
import com.supermap.gaf.data.mgt.enums.CacheTypeEnum;
import com.supermap.gaf.data.mgt.enums.SourceTypeEnum;
import com.supermap.gaf.data.mgt.mapper.CacheBuildHistoryMapper;
import com.supermap.gaf.data.mgt.mapper.PublishHistoryMapper;
import com.supermap.gaf.data.mgt.service.CacheBuildHistoryService;
import com.supermap.gaf.data.mgt.service.SysResourceDatasourceService;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.tilestorage.TileStorageConnection;
import com.supermap.tilestorage.TileStorageManager;
import com.supermap.tilestorage.TileStorageType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * @description
 * @date:2022/2/11 9:41
 * @author:yw
 */
@Service
public class CacheBuildHistoryServiceImpl implements CacheBuildHistoryService {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(CacheBuildHistoryServiceImpl.class);

    @Autowired
    private CacheBuildHistoryMapper cacheBuildHistoryMapper;
    @Autowired
    private PublishHistoryMapper publishHistoryMapper;
    @Autowired
    @Qualifier("DatamgtStorageClient")
    private StorageClient storageClient;

    @Autowired
    private SysResourceDatasourceService sysResourceDatasourceService;


    @Override
    public void insert(CacheBuildHistory cacheBuildHistory) {
        cacheBuildHistory.setId(UUID.randomUUID().toString());
        cacheBuildHistoryMapper.insert(cacheBuildHistory);

    }

    @Override
    public CacheBuildHistory findById(String id) {
        return cacheBuildHistoryMapper.selectById(id);
    }

    @Override
    public Page<CacheBuildHistory> pageList(int pageNum, int pageSize, CacheBuildHistoryVo cacheBuildHistoryVo) {
        PageInfo<CacheBuildHistory> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            cacheBuildHistoryMapper.selectList(cacheBuildHistoryVo);
        });
        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
    }

    @Override
    public void deleteById(String id) {
        cacheBuildHistoryMapper.delete(id);
    }

    @Override
    public void update(String id, CacheBuildHistory cacheBuildHistory) {
        cacheBuildHistory.setId(id);
        cacheBuildHistoryMapper.update(cacheBuildHistory);
    }

    @Override
    public List<CacheBuildHistory> findByIds(List<String> cacheBuildHistoryIds) {
        if (cacheBuildHistoryIds == null || cacheBuildHistoryIds.isEmpty()) return Collections.emptyList();
        return cacheBuildHistoryMapper.selectByIds(cacheBuildHistoryIds);
    }

    public  List<CacheBuildHistory> listByPublishHistoryId(String publishHistoryId) {
        if (StringUtils.isEmpty(publishHistoryId)) throw new IllegalArgumentException("发布历史id不能为空");
        CacheBuildHistoryVo cacheBuildHistoryVo = new CacheBuildHistoryVo();
        cacheBuildHistoryVo.setPublishHistoryId(publishHistoryId);
        List<CacheBuildHistory> cacheBuildHistories = cacheBuildHistoryMapper.selectList(cacheBuildHistoryVo);
        return cacheBuildHistories;
    }

    @Override
    public List<CacheBuildHistory> listLastests(String publishHistoryId) {
        List<CacheBuildHistory> cacheBuildHistories = listByPublishHistoryId(publishHistoryId);
        // 根据数据源id 数据集名称 分组 并 使用选取最新的缓存构建历史
        Map<String, Optional<CacheBuildHistory>> collect = cacheBuildHistories.stream().collect(Collectors.groupingBy(cacheBuildHistory -> {
            return cacheBuildHistory.getDatasourceId() + "_" + cacheBuildHistory.getDatasetName();
        }, Collectors.maxBy(Comparator.comparing(CacheBuildHistory::getCreatedTime))));
        return collect.values().stream().filter(Optional::isPresent).map(Optional::get).sorted(Comparator.comparing(CacheBuildHistory::getCreatedTime).reversed()).collect(Collectors.toList());
    }

    @Override
    public List<CacheBuildHistoryLatestVO> listLastest(String publishHistoryId) {

        List<CacheBuildHistory> latest = listLastests(publishHistoryId);

        Set<String> datasourceIds = latest.stream().map(CacheBuildHistory::getDatasourceId).collect(Collectors.toSet());
        List<SysResourceDatasource> sysResourceDatasources = sysResourceDatasourceService.listByIds(datasourceIds);
        Map<String, String> datasourceIdNameMap = sysResourceDatasources.stream().collect(Collectors.toMap(SysResourceDatasource::getDatasourceId, SysResourceDatasource::getDsName));

        List<CacheBuildHistoryLatestVO> result = latest.stream().map(cacheBuildHistory -> {
            CacheBuildHistoryLatestVO cacheBuildHistoryLatestVO = new CacheBuildHistoryLatestVO();
            BeanUtils.copyProperties(cacheBuildHistory, cacheBuildHistoryLatestVO);
            String datasourceId = cacheBuildHistoryLatestVO.getDatasourceId();
            String datasourceName = datasourceIdNameMap.getOrDefault(datasourceId, "");
            cacheBuildHistoryLatestVO.setDatasourceName(datasourceName);
            String cacheType = cacheBuildHistoryLatestVO.getCacheType();
            String description = CacheTypeEnum.getDescriptionByCode(cacheType);
            cacheBuildHistoryLatestVO.setCacheTypeName(description);
            String statusName = CacheBuildStatusEnum.getDescriptionByCode(cacheBuildHistoryLatestVO.getStatus());
            cacheBuildHistoryLatestVO.setStatusName(statusName);
            return cacheBuildHistoryLatestVO;
        }).collect(Collectors.toList());

        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteWithResource(String id, Boolean isDeleteCacheTiles) {
        CacheBuildHistory cacheBuildHistory = cacheBuildHistoryMapper.selectById(id);
        String tenantId = SecurityUtilsExt.getTenantId();
        if (isDeleteCacheTiles) {
            CompletableFuture.runAsync(() -> {
                try {
                    String publishHistoryId = cacheBuildHistory.getPublishHistoryId();
                    PublishHistory publishHistory = publishHistoryMapper.selectById(publishHistoryId);
                    String sourceType = publishHistory.getSourceType();
                    String storageName = cacheBuildHistory.getStorageName();
                    if (SourceTypeEnum.FILE_WORKSPACE_FOR_STORAGE_TILE.value().equals(sourceType)) {
                        String path = "data/cache_to_file/" + storageName;
                        storageClient.delete(path,tenantId);
                    } else if (SourceTypeEnum.MONGODB.value().equals(sourceType)) {
                        String sourceInfo = publishHistory.getSourceInfo();
                        ServerInfo serverInfo = JSONObject.parseObject(sourceInfo, ServerInfo.class);
                        TileStorageConnection ts = new TileStorageConnection();
                        String host = serverInfo.getHost();
                        Integer port = serverInfo.getPort();
                        String database = serverInfo.getDatabase();
                        String username = serverInfo.getUsername();
                        String password = serverInfo.getPassword();
                        ts.setStorageType(TileStorageType.MONGOOSG);
                        ts.setServer(host+":"+port);
                        ts.setUser(username);
                        ts.setPassword(password);
                        ts.setDatabase(database);
                        ts.setName(storageName);
                        boolean b = TileStorageManager.deleteTileStorage(ts);
                    }
                } catch (Exception e) {
                    log.error("删除缓存构建切片失败: "+e.getMessage(),e);
                }

            });


        }
        cacheBuildHistoryMapper.delete(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchDeleteWithResource(List<String> ids, Boolean isDeleteCacheTiles) {
        if (ids == null || ids.isEmpty()) return;
        for (String id : ids) {
            deleteWithResource(id, isDeleteCacheTiles);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteWithResourceByCondition(String publishHistoryId, String datasourceId, String datasetName, Boolean isDeleteCacheTiles) {
        CacheBuildHistoryVo cacheBuildHistoryVo = new CacheBuildHistoryVo();
        cacheBuildHistoryVo.setPublishHistoryId(publishHistoryId);
        cacheBuildHistoryVo.setDatasourceId(datasourceId);
        cacheBuildHistoryVo.setDatasetName(datasetName);
        List<CacheBuildHistory> cacheBuildHistories = cacheBuildHistoryMapper.selectList(cacheBuildHistoryVo);
        List<String> ids = cacheBuildHistories.stream().map(CacheBuildHistory::getId).collect(Collectors.toList());
        batchDeleteWithResource(ids,isDeleteCacheTiles);
    }

}
