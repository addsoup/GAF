package com.supermap.gaf.data.mgt.service.impl;

import com.alibaba.fastjson.JSON;
import com.supermap.data.*;
import com.supermap.data.processing.DatasetSetting;
import com.supermap.data.processing.OSGBCacheBuilder;
import com.supermap.data.processing.OSGBVectorCacheBuilder;
import com.supermap.gaf.data.mgt.service.DatasetTypeCacheService;
import com.supermap.gaf.data.mgt.util.ScenesUtils;
import com.supermap.gaf.exception.GafException;
import com.supermap.realspace.CacheFileType;
import com.supermap.realspace.Layer3DType;
import com.supermap.tilestorage.TileStorageConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * ClassName:VectorCacheServiceImpl
 * Package:com.supermap.cim.data.service
 * @description 矢量数据集
 * @date:2022/1/24 10:51
 * @author:Yw
 */
public class VectorCacheServiceImpl implements DatasetTypeCacheService {

    private static final Logger logger = LoggerFactory.getLogger(VectorCacheServiceImpl.class);
    private String fileSuffix = ".scp";
    private static final Map<DatasetType, Function<Map, GeoStyle3D>> map = new HashMap<>();

    static {
        map.put(DatasetType.POINT, otherParam -> {
            GeoStyle3D geoStyle3D = new GeoStyle3D();
            String color = (String) otherParam.get("color");
            if (color != null && !color.equals("")) {
                Color color1 = JSON.parseObject(color, Color.class);
                geoStyle3D.setMarkerColor(color1);
            }
            return geoStyle3D;
        });
        map.put(DatasetType.LINE, otherParam -> {
            GeoStyle3D geoStyle3D = new GeoStyle3D();
            String color = (String) otherParam.get("color");
            if (color != null && !color.equals("")) {
                Color color1 = JSON.parseObject(color, Color.class);
                geoStyle3D.setLineColor(color1);
            }
            return geoStyle3D;
        });
        map.put(DatasetType.REGION, otherParam -> {
            GeoStyle3D geoStyle3D = new GeoStyle3D();
            String color = (String) otherParam.get("color");
            if (color != null && !color.equals("")) {
                Color color1 = JSON.parseObject(color, Color.class);
                geoStyle3D.setFillForeColor(color1);
            }
            return geoStyle3D;
        });
    }
    @Override
    public boolean executeS3MBuilder(Dataset dataset, String resultOutputFolder, String fileName, Map<String,Object> otherParam, Consumer<Integer> stepPercentHandler) {
        OSGBVectorCacheBuilder builder = new OSGBVectorCacheBuilder();
        boolean isBuild = false;
        SteppedListener steppedListener = steppedEvent -> {
            logger.info(String.format("切片完成百分比： %d%%", steppedEvent.getPercent()));
            try {
                stepPercentHandler.accept(steppedEvent.getPercent());
            } catch (Exception e) {
                logger.error("切片完成百分比后置处理失败",e);
            }
        };
        try {
            logger.info("当前输出地址:" + resultOutputFolder);;
            DatasetVector datasetVector = (DatasetVector) dataset;
            ArrayList<DatasetSetting> datasetSettings = new ArrayList<>();
            DatasetSetting seting = new DatasetSetting();
            seting.setDataset(datasetVector);
            seting.setUserIDField("SmID");
            seting.setFieldNames(new String[]{"MODELNAME"});
            datasetSettings.add(seting);
            builder.setDatasetSettingArray(datasetSettings);
            builder.setCacheName(fileName);
            builder.setOutputFolder(resultOutputFolder);
            builder.setFileType(CacheFileType.S3MB);
            builder.setTileBoundClip(true);
            GeoStyle3D geoStyle3D = new GeoStyle3D();
            builder.setCompressedTextureType(CompressedTextureType.DDS);
            builder.setUserDefStyle3D(geoStyle3D);
            builder.addSteppedListener(steppedListener);
            isBuild = builder.build();
            Object obj = otherParam.get("workspaceConnectionInfo");
            if (null != obj) {
                String datasetPath = new StringBuilder()
                        .append(resultOutputFolder)
                        .append("/")
                        .append(fileName)
                        .append("/")
                        .append(fileName)
                        .append(fileSuffix)
                        .toString();
                WorkspaceConnectionInfo  workspaceConnectionInfo = (WorkspaceConnectionInfo)obj;
                ScenesUtils.addLayer(fileName,workspaceConnectionInfo,datasetPath,Layer3DType.OSGB);
            }
            return isBuild;
        } catch (Exception e) {
            logger.info("切片缓存失败，原因：" + e.getMessage());
            throw new GafException(e.getMessage());
        } finally {
            if (null != steppedListener) {
                builder.removeSteppedListener(steppedListener);
            }
            builder.dispose();
        }
    }

    @Override
    public boolean writeToMongo(String configFile, TileStorageConnection ts, String datasetName) {
        String realConfigFile = new StringBuilder()
                .append(configFile)
                .append("/")
                .append(datasetName)
                .append(fileSuffix)
                .toString();
        String configMongoFile = new StringBuilder()
                .append(configFile)
                .append("/")
                .append("new_")
                .append(datasetName)
                .append(fileSuffix)
                .toString();
        return OSGBCacheBuilder.osgbFile2MongoDB(realConfigFile, ts, configMongoFile);
    }
}