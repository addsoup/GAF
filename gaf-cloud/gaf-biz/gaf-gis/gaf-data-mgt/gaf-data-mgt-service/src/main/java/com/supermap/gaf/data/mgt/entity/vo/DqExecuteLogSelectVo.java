package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.DqExecuteLog;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 数据质量管理-执行日志表 条件查询实体
 * @author zrc
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据质量管理-执行日志表条件查询实体")
public class DqExecuteLogSelectVo {
    @QueryParam("searchFieldName")
    @ApiModelProperty("模糊查询字段名")
    @StringRange(entityClass = DqExecuteLog.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("模糊查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = DqExecuteLog.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = DqExecuteLog.class)
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod;
    @QueryParam("dqExecuteLogId")
    @ApiModelProperty("主键")
    private String dqExecuteLogId;
    @QueryParam("content")
    @ApiModelProperty("执行日志记录")
    private String content;
    @QueryParam("sortSn")
    @ApiModelProperty("排序")
    private Integer sortSn;
    @QueryParam("description")
    @ApiModelProperty("描述")
    private String description;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("更新人")
    private String updatedBy;
    @QueryParam("totalAmount")
    @ApiModelProperty("数据总量")
    private Long totalAmount;
    @QueryParam("failedAmount")
    @ApiModelProperty("失败数据总量")
    private Long failedAmount;
    @QueryParam("costTime")
    @ApiModelProperty("耗费时间-秒")
    private String costTime;
    @QueryParam("dataQualityRuleId")
    @ApiModelProperty("规则id")
    private String dataQualityRuleId;
    @QueryParam("dqExecuteResultId")
    @ApiModelProperty("执行批次。以时间戳作为执行批次。同属一个方案的一批规则的执行生成一个执行批次")
    private String dqExecuteResultId;
    @QueryParam("dataQualityProgramId")
    @ApiModelProperty("方案id。")
    private String dataQualityProgramId;
    @QueryParam("resultStatus")
    @ApiModelProperty("执行状态。0代表成功，1代表失败。")
    private Integer resultStatus;
}