package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.DataRange;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.ws.rs.QueryParam;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 数据标准值域范围 条件查询实体
 * @author zrc
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据标准值域范围 条件查询实体")
public class DataRangeSelectVo {
    private static final long serialVersionUID = -1L;
    @QueryParam("searchFieldName")
    @ApiModelProperty("查询字段名")
    @StringRange(entityClass = DataRange.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = DataRange.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = DataRange.class)
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod;
    @QueryParam("dataRangeId")
    @ApiModelProperty("主键。")
    private String dataRangeId;
    @QueryParam("dataStandardId")
    @ApiModelProperty("数据标准id。")
    private String dataStandardId;
    @QueryParam("min")
    @ApiModelProperty("最小值")
    private BigDecimal min;
    @QueryParam("max")
    @ApiModelProperty("最大值")
    private BigDecimal max;
    @QueryParam("rangeType")
    @ApiModelProperty("区间类型。CC:左闭右闭，OO:左开右开...")
    private String rangeType;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间。")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人。")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("更新时间。")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("更新人。")
    private String updatedBy;
}
