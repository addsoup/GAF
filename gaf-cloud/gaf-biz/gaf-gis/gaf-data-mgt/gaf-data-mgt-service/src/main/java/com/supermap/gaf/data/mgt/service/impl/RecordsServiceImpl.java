package com.supermap.gaf.data.mgt.service.impl;

import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import com.supermap.gaf.data.mgt.entity.vo.TablePage;
import com.supermap.gaf.data.mgt.service.RecordsService;
import com.supermap.gaf.data.mgt.service.SysResourceDatasourceService;
import com.supermap.gaf.data.mgt.support.ConvertHelper;
import com.supermap.gaf.data.mgt.util.IobjectUtils;
import com.supermap.gaf.exception.GafException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecordsServiceImpl implements RecordsService {
    @Autowired
    private SysResourceDatasourceService sysResourceDatasourceService;
    @Autowired
    private ConvertHelper convertHelper;
    @Override
    public TablePage selectRecordsByPage(String datasourceId, String datasetName,Integer pageNum, Integer pageSize,boolean returnGeo) {
        SysResourceDatasource sysResourceDatasource = sysResourceDatasourceService.getById(datasourceId);
        if (sysResourceDatasource.getIsSdx()) {
            // 空间数据源
            DatasourceConnectionInfo connectionInfo = convertHelper.conver2DatasourceConnectionInfo(sysResourceDatasource);
            return (TablePage) IobjectUtils.datasetProcessor(connectionInfo,datasetName, dataset -> {
                return IobjectUtils.queryRecordsetByPage(dataset,pageNum,pageSize,returnGeo);
            });
        }else{
            throw new GafException("暂仅支持查询空间数据集记录");
        }
    }
}
