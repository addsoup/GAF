package com.supermap.gaf.data.mgt.resource;


import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.CacheBuildHistory;
import com.supermap.gaf.data.mgt.entity.vo.CacheBuildHistoryLatestVO;
import com.supermap.gaf.data.mgt.entity.vo.CacheBuildHistoryVo;
import com.supermap.gaf.data.mgt.service.CacheBuildHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @description 缓存构建历史接口
 * @date:2022/2/11 17:19
 * @author:yw
 */
@Component
@Api("缓存构建历史接口")
public class CacheBuildHistoryResource {
    @Autowired
    private CacheBuildHistoryService cacheBuildHistoryService;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据发布历史id查询其下所有最新缓存构建历史", notes = "")
    @ApiImplicitParam(name = "publishHistory", value = "发布历史id", paramType = "path", dataType = "String")
    @Path("/publish-history/{publishHistoryId}")
    public MessageResult<List<CacheBuildHistoryLatestVO>> listLastest(@PathParam("publishHistoryId")String publishHistoryId){
        List<CacheBuildHistoryLatestVO> results = cacheBuildHistoryService.listLastest(publishHistoryId);
        return MessageResult.data(results).status(200).message("查询成功").build();
    }


    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id查询切片记录", notes = "根据id查询切片记录")
    @ApiImplicitParam(name = "id", value = "切片记录id", paramType = "path", dataType = "String")
    @Path("/{id}")
    public MessageResult<CacheBuildHistory> getById(@PathParam("id")String id){
        CacheBuildHistory cacheBuildHistory = cacheBuildHistoryService.findById(id);
        return MessageResult.successe(CacheBuildHistory.class).data(cacheBuildHistory).status(200).message("查询成功").build();
    }
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询", notes = "分页条件查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cacheBuildHistoryVo", value = "切片记录查询实体",paramType = "body", dataType = "object"),
            @ApiImplicitParam(name = "pageNum", value = "页码", example = "1",defaultValue = "1", allowableValues = "range[1,infinity]",paramType = "query", dataType = "integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", example = "10", defaultValue = "10",allowableValues = "range(0,infinity]", paramType = "query", dataType = "integer")
    })
    public MessageResult<Page> pageList(@Valid @BeanParam CacheBuildHistoryVo cacheBuildHistoryVo,
                                        @DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
                                        @DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<CacheBuildHistory> cacheBuildHistoryPage = cacheBuildHistoryService.pageList(pageNum, pageSize, cacheBuildHistoryVo);
        return MessageResult.successe(Page.class).data(cacheBuildHistoryPage).status(200).message("查询成功").build();
    }
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增", notes = "新增")
    @ApiImplicitParam(name = "cacheBuildHistory", value = "切片记录实体", paramType = "body", dataType = "object")
    public MessageResult<Void> insertRegulatoryApplication(CacheBuildHistory cacheBuildHistory){
        cacheBuildHistoryService.insert(cacheBuildHistory);
        return MessageResult.successe(Void.class).status(200).message("新增操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据数据源id和数据集名批量删除缓存构建历史记录,包括缓存切片产物")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "publishHistoryId", value = "发布历史id", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "datasourceId", value = "数据源id", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "datasetName", value = "数据集名", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "isDeleteCacheTiles", value = "是否删除缓存切片产物", paramType = "query", dataType = "boolean")
    })
    @Path("/condition/with-resources")
    public MessageResult<Void> deleteWithResourceByCondition( @NotEmpty @QueryParam("publishHistoryId") String publishHistoryId ,
                                                              @NotEmpty @QueryParam("datasourceId") String datasourceId ,
                                                              @NotEmpty @QueryParam("datasetName") String datasetName ,
                                                              @DefaultValue("true") @QueryParam("isDeleteCacheTiles") Boolean isDeleteCacheTiles){
        cacheBuildHistoryService.deleteWithResourceByCondition(publishHistoryId, datasourceId, datasetName, isDeleteCacheTiles);
        return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除缓存构建历史记录,包括缓存切片产物")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "ids", paramType = "body", dataTypeClass = List.class),
            @ApiImplicitParam(name = "isDeleteCacheTiles", value = "是否删除缓存切片产物", paramType = "query", dataType = "boolean")
    })
    @Path("/batch/with-resources")
    public MessageResult<Void> batchDeleteWithResource(List<String> ids,@DefaultValue("true") @QueryParam("isDeleteCacheTiles") Boolean isDeleteCacheTiles){
        cacheBuildHistoryService.batchDeleteWithResource(ids, isDeleteCacheTiles);
        return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "删除缓存构建历史记录,包括缓存切片产物")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", paramType = "path", dataType = "string"),
            @ApiImplicitParam(name = "isDeleteCacheTiles", value = "是否删除缓存切片产物", paramType = "query", dataType = "boolean")
    })
    @Path("/{id}/with-resources")
    public MessageResult<Void> deleteWithResource(@PathParam("id")String id,@DefaultValue("true") @QueryParam("isDeleteCacheTiles") Boolean isDeleteCacheTiles){
        cacheBuildHistoryService.deleteWithResource(id,isDeleteCacheTiles);
        return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "删除历史记录", notes = "删除历史记录")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id", paramType = "path", dataType = "string"),
    })
    @Path("/{id}")
    public MessageResult<Void> deleteById(@PathParam("id")String id){
        cacheBuildHistoryService.deleteById(id);
        return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }



    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "更新历史记录", notes = "更新历史记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", paramType = "path", dataType = "string"),
            @ApiImplicitParam(name = "cacheBuildHistory", value = "历史记录实体", paramType = "body", dataType = "object")
    })
    @Path("/{id}")
    public MessageResult<Void> update(@PathParam("id")String id,CacheBuildHistory cacheBuildHistory){
        cacheBuildHistoryService.update(id,cacheBuildHistory);
        return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }
}
