package com.supermap.gaf.data.mgt.resource;
import com.supermap.gaf.commontypes.pagination.Page;
import io.swagger.annotations.*;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import com.supermap.gaf.data.mgt.service.DataCodeContentService;
import com.supermap.gaf.data.mgt.entity.DataCodeContent;
import com.supermap.gaf.data.mgt.entity.vo.DataCodeContentSelectVo;
import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.groups.ConvertGroup;
import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import com.supermap.gaf.commontypes.MessageResult;


/**
 * 数据代码编码接口
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "数据代码编码接口")
public class DataCodeContentResource{
    @Autowired
    private DataCodeContentService dataCodeContentService;
	

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id查询数据代码编码", notes = "根据id查询数据代码编码")
	@Path("/{dataCodeContentId}")
    public MessageResult<DataCodeContent> getById(@PathParam("dataCodeContentId")String dataCodeContentId){
        DataCodeContent dataCodeContent = dataCodeContentService.getById(dataCodeContentId);
		return MessageResult.data(dataCodeContent).message("查询成功").build();
    }

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询数据代码编码", notes = "分页条件查询数据代码编码")
    public MessageResult<Page<DataCodeContent>> pageList(@Valid @BeanParam DataCodeContentSelectVo dataCodeContentSelectVo,
                                                         @DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
                                                         @DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<DataCodeContent> page = dataCodeContentService.listByPageCondition(dataCodeContentSelectVo, pageNum, pageSize);
		return MessageResult.data(page).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增数据代码编码", notes = "新增数据代码编码")
    public MessageResult<Void> insertDataCodeContent(@Valid @ConvertGroup(to= AddGroup.class)DataCodeContent dataCodeContent){
        dataCodeContentService.insertDataCodeContent(dataCodeContent);
		return MessageResult.successe(Void.class).message("新增操作成功").build();
    }


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除数据代码编码", notes = "根据id删除数据代码编码")
	@Path("/{dataCodeContentId}")
    public MessageResult<Integer> deleteDataCodeContent(@PathParam("dataCodeContentId")String dataCodeContentId){
        int re = dataCodeContentService.deleteDataCodeContent(dataCodeContentId);
		return MessageResult.data(re).message("删除操作成功").build();
    }


	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除数据代码编码", notes = "批量删除数据代码编码")
    public MessageResult<Integer> batchDelete(@NotEmpty List<String> dataCodeContentIds){
        int re = dataCodeContentService.batchDelete(dataCodeContentIds);
		return MessageResult.data(re).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新数据代码编码", notes = "根据id更新数据代码编码")
	@Path("/{dataCodeContentId}")
    public MessageResult<Integer> updateDataCodeContent(@Valid @ConvertGroup(to= UpdateGroup.class)DataCodeContent dataCodeContent,@PathParam("dataCodeContentId")String dataCodeContentId){
        dataCodeContent.setDataCodeContentId(dataCodeContentId);
        int re = dataCodeContentService.updateDataCodeContent(dataCodeContent);
		return MessageResult.data(re).message("更新操作成功").build();
    }

	
	


}
