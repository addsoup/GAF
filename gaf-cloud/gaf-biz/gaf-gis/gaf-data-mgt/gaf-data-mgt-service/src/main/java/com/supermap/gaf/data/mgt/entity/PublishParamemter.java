package com.supermap.gaf.data.mgt.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("发布参数")
public class PublishParamemter {

    @ApiModelProperty("发布历史id")
    private String publishHistoryId;

    @ApiModelProperty("是否发布数据服务")
    private Boolean isPublishRestData;

}
