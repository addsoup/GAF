package com.supermap.gaf.data.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataQualityRule;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityRuleSelectVo;
import com.supermap.gaf.data.mgt.mapper.DataQualityRuleMapper;
import com.supermap.gaf.data.mgt.service.DataQualityRuleService;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * 数据质量管理-规则表服务实现类
 * @author zrc
 * @date yyyy-mm-dd
 */
@Service
public class DataQualityRuleServiceImpl implements DataQualityRuleService {
    
	private static final Logger  log = LoggerFactory.getLogger(DataQualityRuleServiceImpl.class);
	
	@Autowired
    private DataQualityRuleMapper dataQualityRuleMapper;
	

	@Override
    public DataQualityRule getById(String dataQualityRuleId){
        if(dataQualityRuleId == null){
            throw new IllegalArgumentException("dataQualityRuleId不能为空");
        }
        return dataQualityRuleMapper.select(dataQualityRuleId);
    }
	
	@Override
    public Page<DataQualityRule> listByPageCondition(DataQualityRuleSelectVo dataQualityRuleSelectVo, int pageNum, int pageSize) {
        PageInfo<DataQualityRule> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dataQualityRuleMapper.selectList(dataQualityRuleSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }


	@Override
    public DataQualityRule insertDataQualityRule(DataQualityRule dataQualityRule){
        dataQualityRule.setDataQualityRuleId(UUID.randomUUID().toString());
        String userName = SecurityUtilsExt.getUserName();
        dataQualityRule.setCreatedBy(userName);
        dataQualityRule.setUpdatedBy(userName);
        dataQualityRuleMapper.insert(dataQualityRule);
        return dataQualityRule;
    }
	
	@Override
    public void batchInsert(List<DataQualityRule> dataQualityRules){

		if (dataQualityRules != null && dataQualityRules.size() > 0) {
	        dataQualityRules.forEach(dataQualityRule -> {
                dataQualityRule.setDataQualityRuleId(UUID.randomUUID().toString());
                String userName = SecurityUtilsExt.getUserName();
                dataQualityRule.setCreatedBy(userName);
                dataQualityRule.setUpdatedBy(userName);
            });
            dataQualityRuleMapper.batchInsert(dataQualityRules);
        }
        
    }
	

	@Override
    public void deleteDataQualityRule(String dataQualityRuleId){
        dataQualityRuleMapper.delete(dataQualityRuleId);
    }

	@Override
    public void batchDelete(List<String> dataQualityRuleIds){
        dataQualityRuleMapper.batchDelete(dataQualityRuleIds);
    }
	

	@Override
    public DataQualityRule updateDataQualityRule(DataQualityRule dataQualityRule){
        dataQualityRuleMapper.update(dataQualityRule);
        return dataQualityRule;
    }
    
}
