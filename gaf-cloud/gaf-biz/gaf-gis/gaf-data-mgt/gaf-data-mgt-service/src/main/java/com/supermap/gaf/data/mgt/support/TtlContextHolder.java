package com.supermap.gaf.data.mgt.support;

import com.alibaba.ttl.TransmittableThreadLocal;
import org.springframework.web.context.request.RequestAttributes;

/**
 * @author wxl
 * @since 2022/6/5
 */
public class TtlContextHolder {

    public static  TransmittableThreadLocal<RequestAttributes> context = new TransmittableThreadLocal<>();

}
