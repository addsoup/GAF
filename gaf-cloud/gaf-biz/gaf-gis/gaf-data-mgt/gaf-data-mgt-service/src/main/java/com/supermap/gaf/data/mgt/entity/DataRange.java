package com.supermap.gaf.data.mgt.entity;
import com.supermap.data.RangeType;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.*;
import javax.validation.constraints.*;
import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 数据标准值域范围
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据标准值域范围")
public class DataRange implements Serializable{
    private static final long serialVersionUID = -1L;
    @ApiModelProperty("主键。")
    private String dataRangeId;
    @ApiModelProperty("数据标准id。")
    private String dataStandardId;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("最小值")
    private BigDecimal min;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("最大值")
    private BigDecimal max;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @StringRange(fromEnum = RangeType.class,groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("区间类型。CC:左闭右闭，OO:左开右开...")
    private String rangeType;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间。")
    private Date createdTime;
    @ApiModelProperty("创建人。")
    private String createdBy;
    @ApiModelProperty("更新时间。")
    private Date updatedTime;
    @ApiModelProperty("更新人。")
    private String updatedBy;

    public enum RangeType{
        OO,OC,CC,CO;
    }
}