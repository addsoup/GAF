package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataSyncLog;
import com.supermap.gaf.data.mgt.entity.vo.DataSyncLogSelectVo;
import com.supermap.gaf.data.mgt.service.DataSyncLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 数据同步日志表接口
 *
 * @author zrc
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "数据同步日志表接口")
public class DataSyncLogResource {
    @Autowired
    private DataSyncLogService dataSyncLogService;


    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询数据同步日志表", notes = "通过id查询数据同步日志表")
    @Path("/{dataSyncLogId}")
    @ApiImplicitParam(name = "dataSyncLogId",value =  "dataSyncLogId",paramType = "path",dataType = "string",required = true )
    public MessageResult<DataSyncLog> getById(@PathParam("dataSyncLogId") String dataSyncLogId) {
        DataSyncLog dataSyncLog = dataSyncLogService.getById(dataSyncLogId);
        return MessageResult.data(dataSyncLog).message("查询成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询数据同步日志表", notes = "分页条件查询数据同步日志表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", example = "1",defaultValue = "1", allowableValues = "range[0,infinity]",paramType = "query", dataType = "integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", example = "10", defaultValue = "10",allowableValues = "range[0,infinity]", paramType = "query", dataType = "integer")
    })
    public MessageResult<Page<DataSyncLog>> pageList(@BeanParam DataSyncLogSelectVo dataSyncLogSelectVo,
                                                     @DefaultValue("1") @QueryParam("pageNum") Integer pageNum,
                                                     @DefaultValue("10") @QueryParam("pageSize") Integer pageSize) {
        Page<DataSyncLog> page = dataSyncLogService.listByPageCondition(dataSyncLogSelectVo, pageNum, pageSize);
        return MessageResult.data(page).message("查询成功").build();
    }


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除数据同步日志表", notes = "根据id删除数据同步日志表")
    @ApiImplicitParam(name = "dataSyncLogId",value =  "dataSyncLogId",paramType = "path",dataType = "string",required = true )
    @Path("/{dataSyncLogId}")
    public MessageResult<Void> deleteDataSyncLog(@PathParam("dataSyncLogId") String dataSyncLogId) {
        dataSyncLogService.deleteDataSyncLog(dataSyncLogId);
        return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiImplicitParam(name = "dataSyncLogIds",value = "dataSyncLogIds",paramType = "body",allowMultiple = true,required = true )
    @ApiOperation(value = "批量删除数据同步日志表", notes = "批量删除数据同步日志表")
    public MessageResult<Void> batchDelete(List<String> dataSyncLogIds) {
        dataSyncLogService.batchDelete(dataSyncLogIds);
        return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

}
