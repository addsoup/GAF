package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.CacheBuildHistory;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * @description 切片缓存历史记录查询实体
 * @date:2022/2/10 17:06
 * @author:yw
 */
@Data
public class CacheBuildHistoryVo {
    @QueryParam("searchFieldName")
    @ApiModelProperty("查询字段名")
    @StringRange(entityClass = CacheBuildHistory.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = CacheBuildHistory.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = CacheBuildHistory.class)
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"}, message = "不在指定的范围[asc,desc]内")
    private String orderMethod;
    @QueryParam("id")
    @ApiModelProperty("id")
    public String id;
    @QueryParam("datasourceId")
    @ApiModelProperty("数据源id")
    public String datasourceId;
    @QueryParam("datasetName")
    @ApiModelProperty("数据集名")
    public String datasetName;
    @QueryParam("storageName")
    @ApiModelProperty("存储名")
    public String storageName;
    @QueryParam("cacheType")
    @ApiModelProperty(value = "切片类型" , allowableValues = "increment(增量),fullVolume(全量)")
    public String cacheType;
    @QueryParam("tileType")
    @ApiModelProperty("瓦片类型(数据集类型).暂时支持GRID,IMAGE,MODEL,POINT,LINE,REGION")
    public String tileType;
    @QueryParam("color")
    @ApiModelProperty("颜色。发布时的参数设置，例如{\"r\":249,\"g\":31,\"b\":31,\"a\":100}")
    public String color;
    @QueryParam("queryParameter")
    @ApiModelProperty("增量切片查询条件")
    public String queryParameter;

    @QueryParam("isSimp")
    @ApiModelProperty("是否轻量化")
    private Boolean isSimp;
    @QueryParam("simplifyingRate")
    @ApiModelProperty("简化率(轻量化专属)")
    private Double simplifyingRate;


    @QueryParam("status")
    @ApiModelProperty("状态 0 失败 1成功 2进行中")
    public Integer status;
    @QueryParam("errorMessage")
    @ApiModelProperty("错误信息")
    public String errorMessage;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    public Date createdTime;
    @QueryParam("publishHistoryId")
    @ApiModelProperty("发布历史id。当未进行服务发布时，发布历史可用作连接信息，可用于存储缓存产物")
    public String publishHistoryId;
}
