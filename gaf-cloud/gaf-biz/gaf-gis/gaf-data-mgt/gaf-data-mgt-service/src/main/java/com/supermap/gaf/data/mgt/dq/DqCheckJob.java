package com.supermap.gaf.data.mgt.dq;

import com.supermap.gaf.data.mgt.service.DataQualityProgramService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DqCheckJob extends QuartzJobBean {

    @Autowired
    private DataQualityProgramService dataQualityProgramService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobKey jobKey = jobExecutionContext.getJobDetail().getKey();
        String name = jobKey.getGroup()+":"+jobKey.getName();
        JobDataMap map = jobExecutionContext.getJobDetail().getJobDataMap();
        String programId = map.getString("dataQualityProgramId");
        log.info("执行检查方案:{}",name);
        dataQualityProgramService.executeProgram(programId);
        log.info("执行结束");
    }


}
