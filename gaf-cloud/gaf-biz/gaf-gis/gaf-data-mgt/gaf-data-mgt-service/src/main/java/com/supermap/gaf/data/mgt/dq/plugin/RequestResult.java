package com.supermap.gaf.data.mgt.dq.plugin;

import com.supermap.gaf.data.mgt.dq.DqRuleRunner;
import com.supermap.gaf.data.mgt.log.SimpleLogger;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

import java.util.ArrayList;
import java.util.List;

@Data
public class RequestResult extends DqRuleRunner.DqResult implements SimpleLogger {
    private List<Log> logs = new ArrayList<>();

    @Override
    public void info(String format, Object... arguments) {
        FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
        logs.add(new Log("info",ft.getMessage()));
    }

    @Override
    public void error(String format, Object... arguments) {
        FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
        logs.add(new Log("error",ft.getMessage()));
    }

    @Override
    public void warn(String format, Object... arguments) {
        FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
        logs.add(new Log("warn",ft.getMessage()));
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class Log{
        private String level;
        private String body;
    }

}
