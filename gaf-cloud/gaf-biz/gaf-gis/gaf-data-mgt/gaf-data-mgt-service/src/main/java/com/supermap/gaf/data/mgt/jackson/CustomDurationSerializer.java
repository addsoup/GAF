package com.supermap.gaf.data.mgt.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class CustomDurationSerializer extends JsonSerializer<Long> {


    @Override
    public void serialize(Long aLong, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String re = null;
        if(aLong!=null || aLong<0){
            re = getDurationBreakdown(aLong);
        }
        jsonGenerator.writeString(re);
    }

    static String  getDurationBreakdown(long millis){
        if(millis < 0) {
            return millis+"毫秒";
        }
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
        millis -= TimeUnit.SECONDS.toMillis(seconds);

        StringBuilder sb = new StringBuilder(64);
        if(hours>0){
            sb.append(hours);
            sb.append(" 小时 ");
        }
        if(minutes>0){
            sb.append(minutes);
            sb.append(" 分钟 ");
        }
        if(seconds>0){
            sb.append(seconds);
            sb.append(" 秒 ");
        }
        if(millis>0){
            sb.append(millis);
            sb.append(" 毫秒 ");
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(getDurationBreakdown(60030));;
    }


}
