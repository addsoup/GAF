package com.supermap.gaf.data.mgt.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 数据质量管理-方案表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据质量管理-方案表")
public class DataQualityProgram implements Serializable{
    @NotNull
    @ApiModelProperty("主键")
    private String dataQualityProgramId;
    @NotNull
    @ApiModelProperty("方案名称")
    private String dataQualityProgramName;
    @NotNull
    @ApiModelProperty("方案关联的模型id")
    private String dataQualityModelId;
    @ApiModelProperty("方案定时执行cron字符串")
    private String dataQualityCron;
    /**
    * 默认值1：1
    */
    @ApiModelProperty("排序")
    private Integer sortSn;
    @ApiModelProperty("描述")
    private String description;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;
}