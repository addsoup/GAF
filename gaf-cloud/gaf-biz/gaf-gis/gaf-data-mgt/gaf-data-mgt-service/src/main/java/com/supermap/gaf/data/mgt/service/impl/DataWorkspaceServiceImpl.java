/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.data.mgt.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.data.*;
import com.supermap.gaf.common.storage.client.StorageClient;
import com.supermap.gaf.commontypes.ExceptionFunction;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.common.IServerManager;
import com.supermap.gaf.data.mgt.entity.DataWorkspace;
import com.supermap.gaf.data.mgt.entity.iserver.BaseResponse;
import com.supermap.gaf.data.mgt.entity.iserver.PublishParam;
import com.supermap.gaf.data.mgt.entity.iserver.PublishResult;
import com.supermap.gaf.data.mgt.entity.vo.DataWorkspaceSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.WorkspaceIdServiceType;
import com.supermap.gaf.data.mgt.mapper.DataWorkspaceMapper;
import com.supermap.gaf.data.mgt.model.MapSimpleInfo;
import com.supermap.gaf.data.mgt.model.SceneSimpleInfo;
import com.supermap.gaf.data.mgt.model.SimpleInfo;
import com.supermap.gaf.data.mgt.service.DataWorkspaceService;
import com.supermap.gaf.data.mgt.service.publisher.config.UrlConfig;
import com.supermap.gaf.data.mgt.util.IobjectUtils;
import com.supermap.gaf.data.mgt.util.IserverUtils;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.services.providers.util.CommontypesConversion;
import com.supermap.services.rest.management.ServiceType;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * 工作空间服务实现类
 *
 * @author zrc
 * @date yyyy-mm-dd
 */
@Service
public class DataWorkspaceServiceImpl implements DataWorkspaceService {
    private static final String SMWU = "SMWU";
    private static final Logger log = LoggerFactory.getLogger(DataWorkspaceServiceImpl.class);

    @Autowired
    private DataWorkspaceMapper dataWorkspaceMapper;

    @Autowired
    private IServerManager iServerManager;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("DatamgtStorageClient")
    private StorageClient storageClient;


    @Override
    public DataWorkspace getById(String workspaceId) {
        if (workspaceId == null) {
            throw new IllegalArgumentException("workspaceId不能为空");
        }
        return dataWorkspaceMapper.select(workspaceId);
    }

    @Override
    public List<DataWorkspace> selectByIds(Collection<String> workspaceIds) {
        return dataWorkspaceMapper.selectByIds(workspaceIds);
    }

    @Override
    public List<DataWorkspace> selectList(DataWorkspaceSelectVo dataWorkspaceSelectVo) {
        return dataWorkspaceMapper.selectList(dataWorkspaceSelectVo);
    }

    @Override
    public Page<DataWorkspace> listByPageCondition(DataWorkspaceSelectVo dataWorkspaceSelectVo, int pageNum, int pageSize) {
        PageInfo<DataWorkspace> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dataWorkspaceMapper.selectList(dataWorkspaceSelectVo);
        });
        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
    }

    @Override
    public void createWorkspace(String path) {
        Path serverPath = Paths.get(storageClient.getVolumePath(path, SecurityUtilsExt.getTenantId(), false).getPath());
        Workspace workspace = new Workspace();
        WorkspaceConnectionInfo connectionInfo = new WorkspaceConnectionInfo();
        connectionInfo.setServer(serverPath.toString());
        connectionInfo.setType(WorkspaceType.SMWU);
        workspace.saveAs(connectionInfo);
    }

    @Override
    public DataWorkspace insertDataWorkspace(DataWorkspace dataWorkspace) {
        if (SMWU.equals(dataWorkspace.getTypeCode()) || SMWU.equals(dataWorkspace.getTypeCode())) {
            DataWorkspaceSelectVo selectVo = DataWorkspaceSelectVo.builder().server(dataWorkspace.getServer()).build();
            if (!CollectionUtils.isEmpty(dataWorkspaceMapper.selectList(selectVo))) {
                throw new GafException("conflict");
            }
        }
        dataWorkspace.setWorkspaceId(UUID.randomUUID().toString());

        String userName = SecurityUtilsExt.getUserName();
        dataWorkspace.setCreatedBy(userName);
        dataWorkspace.setUpdatedBy(userName);
        dataWorkspaceMapper.insert(dataWorkspace);
        return dataWorkspace;
    }

    @Override
    public void batchInsert(List<DataWorkspace> dataWorkspaces) {
        if (dataWorkspaces != null && dataWorkspaces.size() > 0) {
            String userName = SecurityUtilsExt.getUserName();
            dataWorkspaces.forEach(dataWorkspace -> {
                dataWorkspace.setWorkspaceId(UUID.randomUUID().toString());
                dataWorkspace.setCreatedBy(userName);
                dataWorkspace.setUpdatedBy(userName);
            });
            dataWorkspaceMapper.batchInsert(dataWorkspaces);
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteDataWorkspace(String workspaceId) {
        DataWorkspace dataWorkspace = getById(workspaceId);
        if (dataWorkspace != null) {
            dataWorkspaceMapper.delete(workspaceId);
            if (isFileType(dataWorkspace.getTypeCode()) && !StringUtils.isEmpty(dataWorkspace.getServer())) {
                storageClient.delete(dataWorkspace.getServer(), SecurityUtilsExt.getTenantId());
            }
        }
    }

    boolean isFileType(String typeCode) {
        return WorkspaceType.SMWU.name().equals(typeCode) || WorkspaceType.SXWU.name().equals(typeCode) || WorkspaceType.SMW.name().equals(typeCode) || WorkspaceType.SXW.name().equals(typeCode);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDelete(List<String> workspaceIds) {
        List<DataWorkspace> workspaces = dataWorkspaceMapper.selectByIds(workspaceIds);
        if (!CollectionUtils.isEmpty(workspaces)) {
            dataWorkspaceMapper.batchDelete(workspaceIds);
            String tenantId = SecurityUtilsExt.getTenantId();
            for (DataWorkspace item : workspaces) {
                if (isFileType(item.getTypeCode()) && !StringUtils.isEmpty(item.getServer())) {
                    storageClient.delete(item.getServer(), tenantId);
                }
            }
        }
    }

    @Override
    public DataWorkspace updateDataWorkspace(DataWorkspace dataWorkspace) {
        dataWorkspace.setUpdatedBy(SecurityUtilsExt.getUserName());
        dataWorkspaceMapper.update(dataWorkspace);
        return dataWorkspace;
    }

    /**
     * 新的服务发布 zrc
     * @param publishParam
     * @return
     */
    @Override
    public List<PublishResult> publish(PublishParam publishParam) {
        String workspaceConnectionInfo = publishParam.getWorkspaceConnectionInfo();
        if (StringUtils.isEmpty(workspaceConnectionInfo)) throw new IllegalArgumentException("工作空间信息不能为空");
        String iServerUrl = iServerManager.getAvailableIServerSetting().getHostServerUrl();
        MessageResult<String> token  = iServerManager.applyToken();
        if(!token.IsSuccessed()){
            throw new GafException(token.getMessage(),token.getStatus());
        }
        // 发布服务
        log.info("开始发布工作空间...");
        BaseResponse<List<PublishResult>> baseResponse = IserverUtils.publishWorkspace(publishParam,iServerUrl,false,"token",token.getData());
        if(!baseResponse.getSucceed()){
            throw baseResponse.toGafException();
        }
        log.info("发布完成");
        List<PublishResult> re = baseResponse.getData();
        return re;
    }

    /**
     * 新的服务发布 zrc
     * @param workspaceId
     * @param publishParam
     * @return
     */
    @Override
    public List<PublishResult> publish(String workspaceId, PublishParam publishParam) {
        DataWorkspace dataWorkspace = getById(workspaceId);
        if(dataWorkspace==null){
            throw new GafException("指定工作空间不存在");
        }
        WorkspaceConnectionInfo workspaceConnectionInfo = getWorkspaceConnectionInfo(dataWorkspace);
        if(isFileType(dataWorkspace.getTypeCode())){
            if(!StringUtils.isEmpty(workspaceConnectionInfo.getPassword())){
                StringBuffer sb = new StringBuffer("server=").append(workspaceConnectionInfo.getServer());
                sb.append(";password=").append(workspaceConnectionInfo.getPassword()).append(";");
                publishParam.setWorkspaceConnectionInfo(sb.toString());
            }else{
                publishParam.setWorkspaceConnectionInfo(workspaceConnectionInfo.getServer());
            }
        }else{
            publishParam.setWorkspaceConnectionInfo(CommontypesConversion.getWorkspaceConnectionInfo(workspaceConnectionInfo).toStandardString());
        }
        String iServerUrl = iServerManager.getAvailableIServerSetting().getHostServerUrl();
        MessageResult<String> token  = iServerManager.applyToken();
        if(!token.IsSuccessed()){
            throw new GafException(token.getMessage(),token.getStatus());
        }
        // 发布服务
        log.info("开始发布工作空间...");
        BaseResponse<List<PublishResult>> baseResponse = IserverUtils.publishWorkspace(publishParam,iServerUrl,false,"token",token.getData());
        if(!baseResponse.getSucceed()){
            throw baseResponse.toGafException();
        }
        log.info("发布完成");
        List<PublishResult> re = baseResponse.getData();
        return re;
    }

    @Override
    public Set<ServiceType> getPublishOptionalType(String workspaceId) {
        DataWorkspace dataWorkspace = getById(workspaceId);
        if(dataWorkspace==null){
            throw new GafException("指定工作空间不存在");
        }
        WorkspaceConnectionInfo workspaceConnectionInfo = getWorkspaceConnectionInfo(dataWorkspace);
        return (Set<ServiceType>) IobjectUtils.workspaceProcessor(workspaceConnectionInfo, workspace->{
            return IserverUtils.getPublishOptionalType(workspace);
        });
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<MessageResult<Object>> publishService(List<WorkspaceIdServiceType> workspaceIdServiceTypes) throws IOException {
        List<MessageResult<Object>> resultList = new LinkedList<>();
        for (WorkspaceIdServiceType workspaceIdServiceType : workspaceIdServiceTypes) {
            String workspaceId = workspaceIdServiceType.getWorkspaceId();
            DataWorkspace workspace = getById(workspaceId);
            List<String> serviceTypes = workspaceIdServiceType.getServiceTypes();
            // 1.校验
            if (Objects.isNull(workspace)) {
                MessageResult<Object> failedResult = MessageResult.failed(Object.class).message("未找到工作空间数据").build();
                failedResult.setResourceId(workspaceId);
                resultList.add(failedResult);
                break;
            }
            if (workspace.getPublished()) {
                MessageResult<Object> failedResult = MessageResult.failed(Object.class).message("工作空间已发布").build();
                failedResult.setResourceId(workspaceId);
                resultList.add(failedResult);
                break;
            }
            // 1.1 校验工作空间类型是否支持
            WorkspaceType workspaceType = (WorkspaceType) WorkspaceType.parse(WorkspaceType.class, workspace.getTypeCode());
            if (workspaceType == null) {
                MessageResult<Object> failedResult = MessageResult.failed(Object.class).message(String.format("不支持的工作空间类型:%s", workspace.getTypeCode())).build();
                failedResult.setResourceId(workspaceId);
                resultList.add(failedResult);
                break;
            }
            // 1.2 校验发布的服务类型 请求iserver 查询工作空间可支持的发布类型 todo:
            // 1.3 校验工作空间连接参数是否可用
            if (!WorkspaceType.SMWU.equals(workspaceType) && !WorkspaceType.SXWU.equals(workspaceType)) {
                Workspace workspace1 = new Workspace();
                WorkspaceConnectionInfo workspaceConnectionInfo = getWorkspaceConnectionInfo(workspace);
                boolean opened = workspace1.open(workspaceConnectionInfo);
                workspace1.close();
                workspace1.dispose();
                workspaceConnectionInfo.dispose();
                if (!opened) {
                    MessageResult<Object> failedResult = MessageResult.failed(Object.class).message("打开工作空间失败").build();
                    failedResult.setResourceId(workspaceId);
                    resultList.add(failedResult);
                    break;
                }
            }
            // 2.发布服务
            MessageResult<Object> publishResult = doPublishWorkspace(workspace, serviceTypes);
            resultList.add(publishResult);
        }
        return resultList;
    }

    @Override
    public <T> T workspaceProcessor(DataWorkspace dataWorkspace, ExceptionFunction<Workspace, T > process){
        WorkspaceConnectionInfo workspaceConnectionInfo = getWorkspaceConnectionInfo(dataWorkspace);
        return IobjectUtils.workspaceProcessor2(workspaceConnectionInfo,process);
    }



    @Override
    public List<SimpleInfo> listDatasourceInfo(String workspaceId) {
        DataWorkspace dataWorkspace = getById(workspaceId);
        if (dataWorkspace == null) {
            throw new GafException("未找到工作空间");
        }
        WorkspaceConnectionInfo workspaceConnectionInfo = getWorkspaceConnectionInfo(dataWorkspace);
        List<SimpleInfo> result = IobjectUtils.workspaceProcessor2(workspaceConnectionInfo, workspace -> {
            Datasources datasources = workspace.getDatasources();
            int count = datasources.getCount();
            List<SimpleInfo> simpleInfos = new LinkedList<>();
            for (int i = 0; i < count; i++) {
                Datasource ds = datasources.get(i);
                SimpleInfo simpleInfo = new SimpleInfo();
                simpleInfo.setName(ds.getAlias());
                simpleInfo.setCoordSysName(ds.getPrjCoordSys().getName());
                simpleInfo.setDateLastUpdated(ds.getDateLastUpdated());
                simpleInfo.setDescription(ds.getDescription());
                simpleInfo.setTypeName(ds.getEngineType().name());
                simpleInfo.setUnit(ds.getCoordUnit().toString());
                Datasets datasets = ds.getDatasets();
                int datasetsCount = datasets.getCount();
                List<SimpleInfo> datasetSimpleInfos = new LinkedList<>();
                for (int j = 0; j < datasetsCount; j++) {
                    Dataset dataset = datasets.get(j);
                    SimpleInfo aDatasetInfo = IobjectUtils.datasetProcessor(dataset, dt -> {
                        SimpleInfo datasetInfo = new SimpleInfo();
                        datasetInfo.setName(dt.getName());
                        datasetInfo.setDescription(dt.getDescription());
                        PrjCoordSys prjCoordSys = dt.getPrjCoordSys();
                        datasetInfo.setCoordSysName(prjCoordSys.getName());
                        datasetInfo.setUnit(prjCoordSys.getCoordUnit().toString());
                        DatasetType type = dt.getType();
                        datasetInfo.setTypeName(IobjectUtils.getDatasetTypeZh(type));
                        return datasetInfo;
                    });
                    datasetSimpleInfos.add(aDatasetInfo);
                }
                simpleInfo.setChildren(datasetSimpleInfos);
                simpleInfos.add(simpleInfo);
            }
            return simpleInfos;
        });
        return result;
    }

    @Override
    public List<MapSimpleInfo> listMapSimpleInfos(String workspaceId) {
        DataWorkspace dataWorkspace = getById(workspaceId);
        if (dataWorkspace == null) {
            throw new GafException("未找到工作空间");
        }
        WorkspaceConnectionInfo workspaceConnectionInfo = getWorkspaceConnectionInfo(dataWorkspace);
        List<MapSimpleInfo> result = IobjectUtils.workspaceProcessor2(workspaceConnectionInfo, workspace -> {
            Maps maps = workspace.getMaps();
            int count = maps.getCount();
            List<MapSimpleInfo> mapSimpleInfos = new LinkedList<>();
            for (int i = 0; i < count; i++) {
                MapSimpleInfo mapSimpleInfo = new MapSimpleInfo();
                mapSimpleInfo.setName(maps.get(i));

                String mapXML = maps.getMapXML(i);
                String removedMapXML= mapXML.replace("sml:","");
                InputStream inputStream = new ByteArrayInputStream(removedMapXML.trim().getBytes());

                try {
                    // 创建saxReader对象
                    SAXReader reader = new SAXReader();
                    reader.setFeature("http://xml.org/sax/features/namespaces", false);
                    Document document =reader.read(inputStream);
                    // 获取根节点元素对象
                    Element root = document.getRootElement();
                    Element scaleEl = root.element("ViewSettings").element("DefaultScale");
                    String text = scaleEl.getTextTrim();
                    double scale = Double.parseDouble(text);
                    mapSimpleInfo.setScale(scale);

                    Element prjCoordSysEl = root.element("CoordinateReferenceSystem");

                    Element nameEl = prjCoordSysEl.element("Name");
                    String coordSysName = nameEl.getTextTrim();
                    mapSimpleInfo.setCoordSysName(coordSysName);

                    Element coordUnitEl = prjCoordSysEl.element("Units");
                    String coordUnit = coordUnitEl.getTextTrim();
                    Unit unit = (Unit) Unit.parse(Unit.class, coordUnit);
                    mapSimpleInfo.setUnit(unit.toString());
                } catch (DocumentException | SAXException e) {
                    e.printStackTrace();
                }
                mapSimpleInfos.add(mapSimpleInfo);
            }
            return mapSimpleInfos;
        });
        return result;
    }

    @Override
    public List<SceneSimpleInfo> listSceneSimpleInfos(String workspaceId) {

        DataWorkspace dataWorkspace = getById(workspaceId);
        if (dataWorkspace == null) {
            throw new GafException("未找到工作空间");
        }
        WorkspaceConnectionInfo workspaceConnectionInfo = getWorkspaceConnectionInfo(dataWorkspace);
        List<SceneSimpleInfo> result = IobjectUtils.workspaceProcessor2(workspaceConnectionInfo, workspace -> {
            Scenes scenes = workspace.getScenes();
            int count = scenes.getCount();
            List<SceneSimpleInfo> sceneSimpleInfos = new LinkedList<>();
            for (int i = 0; i < count; i++) {
                try {
                    SceneSimpleInfo sceneSimpleInfo = new SceneSimpleInfo();
                    sceneSimpleInfo.setName(scenes.get(i));

                    String sceneXML = scenes.getSceneXML(i);
                    String removedMapXML= sceneXML.replace("sml:","");
                    InputStream inputStream = new ByteArrayInputStream(removedMapXML.trim().getBytes());
                    // 创建saxReader对象
                    SAXReader reader = new SAXReader();
                    Document document =reader.read(inputStream);

                    // 获取根节点元素对象
                    Element root = document.getRootElement();
                    Element sceneTypeEl = root.element("SceneType");
                    String sceneType = sceneTypeEl.getTextTrim();
                    if ("GLOBE".equals(sceneType)) {
                        sceneSimpleInfo.setSceneType("球面场景");
                    } else if ("NONEARTH".equals(sceneType)) {
                        sceneSimpleInfo.setSceneType("平面场景");
                    } else {
                        sceneSimpleInfo.setSceneType(sceneType);
                    }

                    sceneSimpleInfos.add(sceneSimpleInfo);
                } catch (DocumentException e) {
                    e.printStackTrace();
                }

            }
            return sceneSimpleInfos;
        });
        return result;
    }


    private MessageResult<Object> doPublishWorkspace(DataWorkspace workspace, List<String> serviceTypes) throws IOException {
        com.supermap.services.rest.management.PublishServiceParameter publishServiceParameter = new com.supermap.services.rest.management.PublishServiceParameter();
        WorkspaceConnectionInfo workspaceConnectionInfo = getWorkspaceConnectionInfo(workspace);
        if (WorkspaceType.SXWU.equals(workspaceConnectionInfo.getType()) || WorkspaceType.SMWU.equals(workspaceConnectionInfo.getType())) {
            if (StringUtils.isNotEmpty(workspaceConnectionInfo.getPassword())) {
                publishServiceParameter.workspaceConnectionInfo = "server=" + workspaceConnectionInfo.getServer() + ";password=" + workspaceConnectionInfo.getPassword();
            } else {
                publishServiceParameter.workspaceConnectionInfo = workspaceConnectionInfo.getServer();
            }
        } else {
            publishServiceParameter.workspaceConnectionInfo = CommontypesConversion.getWorkspaceConnectionInfo(workspaceConnectionInfo).toStandardString();
        }
        // publishServiceParameter.workspaceConnectionInfo = CommontypesConversion.getWorkspaceConnectionInfo(workspaceConnectionInfo).toStandardString();
        workspaceConnectionInfo.dispose();
        if (Objects.nonNull(serviceTypes) && !serviceTypes.isEmpty()) {
            ServiceType[] serviceTypesArray = new ServiceType[serviceTypes.size()];
            for (int i = 0; i < serviceTypes.size(); i++) {
                serviceTypesArray[i] = ServiceType.valueOf(serviceTypes.get(i));
            }
            publishServiceParameter.servicesTypes = serviceTypesArray;
        }
        MessageResult<String> generateTokenResult = iServerManager.applyToken();
        String iServerUrl = iServerManager.getAvailableIServerSetting().getHostServerUrl();
        String publishWorkspaceUrl = UrlConfig.getPublishWorkspaceUrlWithReturn(iServerUrl, generateTokenResult.getData());
        MessageResult<Object> publishResult = executePublish(publishWorkspaceUrl, publishServiceParameter);
        publishResult.setResourceId(workspace.getWorkspaceId());
        if (publishResult.isSuccessed()) {
            workspace.setPublished(true);
            updateDataWorkspace(workspace);
        }
        return publishResult;
    }

    private WorkspaceConnectionInfo getWorkspaceConnectionInfo(DataWorkspace workspace) {
        WorkspaceConnectionInfo workspaceConnectionInfo = new WorkspaceConnectionInfo();
        workspaceConnectionInfo.setName(workspace.getWsName());
        WorkspaceType type = (WorkspaceType) WorkspaceType.parse(WorkspaceType.class, workspace.getTypeCode());
        workspaceConnectionInfo.setType(type);
        workspaceConnectionInfo.setUser(workspace.getUserName());
        workspaceConnectionInfo.setPassword(workspace.getPassword());
        if (WorkspaceType.SXWU.equals(type) || WorkspaceType.SMWU.equals(type)) {
            Path serverPath = Paths.get(storageClient.getVolumePath(workspace.getServer(), SecurityUtilsExt.getTenantId(), false).getPath()); // Paths.get("F:/tmpfortest/" + workspace.getServer());
            workspaceConnectionInfo.setServer(serverPath.toString());
        } else {
            workspaceConnectionInfo.setServer(workspace.getServer());
            workspaceConnectionInfo.setDatabase(workspace.getDatabase());
            workspaceConnectionInfo.setVersion(WorkspaceVersion.UGC70);
            if (WorkspaceType.SQL.equals(type)) {
                workspaceConnectionInfo.setDriver("SQL Server");
            }
        }
        return workspaceConnectionInfo;
    }

    private MessageResult<Object> executePublish(String url, com.supermap.services.rest.management.PublishServiceParameter param) {
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, param, String.class);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            String body = responseEntity.getBody();
            if (StringUtils.isNotBlank(body)) {
                Object parse = JSONObject.parse(body);
                return MessageResult.data(parse).message("发布成功").build();
            }
        }
        return MessageResult.failed(Object.class).message("发布失败").build();
    }

}
