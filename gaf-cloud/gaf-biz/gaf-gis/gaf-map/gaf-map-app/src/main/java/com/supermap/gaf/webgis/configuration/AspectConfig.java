package com.supermap.gaf.webgis.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author wxl
 * @since 2021/11/25
 */
@Configuration
@EnableAspectJAutoProxy
public class AspectConfig {
}
