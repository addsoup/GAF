package com.supermap.gaf.webgis.client;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.webgis.entity.ServiceSource;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * ClassName:WebgisServiceClient
 * Package:com.supermap.gaf.webgis.client
 *
 * @date:2021/7/15 10:06
 * @author:Yw
 */
@FeignClient(name = "GAF-MAP", contextId = "ServiceSourceClient")
public interface ServiceSourceClient {
    @PostMapping(value = "/map/service-sources/handlers/get-by-service-ids",consumes = MediaType.APPLICATION_JSON_VALUE)
    MessageResult<List<ServiceSource>> selectByServiceIdsAndSourceType(@RequestBody Set<String> serviceIds, @RequestParam("sourceType") Integer sourceType);
}
