package com.supermap.gaf.webgis.client;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.webgis.entity.WebgisServiceDo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * ClassName:WebgisServiceClient
 * Package:com.supermap.gaf.webgis.client
 *
 * @date:2021/7/15 10:06
 * @author:Yw
 */
@FeignClient(name = "GAF-MAP", contextId = "WebgisServiceAssociationClient")
public interface WebgisServiceAssociationClient {
    @PostMapping(value = "/map/webgis-service-associations/handlers/get-by-dataservice-ids",consumes = MediaType.APPLICATION_JSON_VALUE)
    MessageResult<List<WebgisServiceDo>> selectServiceDoByDataServiceIds(List<String> serviceIds);
}
