//package com.supermap.gaf.webgis.domain;
//
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.validation.constraints.NotNull;
//
///**
// * @author heykb
// */
//@Data
//@Builder
//@AllArgsConstructor
//@NoArgsConstructor
//@ApiModel("模板应用添加参数")
//public class WebgisTemplateAppParam {
//    @NotNull
//    @ApiModelProperty("来源应用id")
//    private String sourceGisAppId;
//    @ApiModelProperty("模板名称")
//    @NotNull
//    private String name;
//    @ApiModelProperty("模板英文名称")
//    private String appAliasEn;
//}
