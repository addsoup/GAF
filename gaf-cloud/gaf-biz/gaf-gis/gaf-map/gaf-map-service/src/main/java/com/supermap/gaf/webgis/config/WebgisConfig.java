package com.supermap.gaf.webgis.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author heykb
 */
@Configuration
public class WebgisConfig {
    //private static String CONFIG_URL;
    //private static String APP_VIEW_URL;
    //private static String APP_PREVIEW_URL;
    private static String HOST;
    //public static String STATIC_RESOURCE_PATH;
    //public static Integer STATIC_CACHE_TIMEOUT;
    //public static String DEFAULT_TEMPLATE_ID;
    public static boolean SERVICE_PROXY_ENABLE = true;

    @Value("${gaf.host:}")
    public void setHost(String host) {
        WebgisConfig.HOST = host;
        //WebgisConfig.APP_PREVIEW_URL = host+"/api/map/webgis-preview/%s.html";
        //WebgisConfig.CONFIG_URL = host+"/api/map/webgis-apps/%s/config";
        //WebgisConfig.APP_VIEW_URL = host+"/api/map/webgis-view/%s.html";
    }
    @Value("#{new Boolean('${gaf.service.proxy.enable:true}'.trim())}")
    public void setServiceProxyEnable(Boolean enable){
        SERVICE_PROXY_ENABLE = enable;
    }

    //@Value("${webgis.static.resource.path:webgis/}")
    //public void setStaticResourcePath(String componentRootPath){
    //    WebgisConfig.STATIC_RESOURCE_PATH = componentRootPath;
    //}
    //@Value("${webgis.static.cache.timeout:86400}")
    //public void setStaticCacheTimeout(Integer STATIC_CACHE_TIMEOUT) {
    //    WebgisConfig.STATIC_CACHE_TIMEOUT = STATIC_CACHE_TIMEOUT;
    //}
    //@Value("${webgis.template.defaultid:0}")
    //public void setDefaultTemplateId(String defaultTemplateId) {
    //    WebgisConfig.DEFAULT_TEMPLATE_ID = defaultTemplateId;
    //}
    //public static String getPreviewUrl(){
    //    return getHost()+"/api/map/webgis-preview/%s.html";
    //}
    //public static String getViewUrl(){
    //
    //    return getHost()+"/api/map/webgis-view/%s.html";
    //}


    //public static String getConfigUrl(){
    //    return getHost()+"/api/map/webgis-apps/%s/config";
    //}
    public static String getHost(){
        if(!StringUtils.isEmpty(WebgisConfig.HOST)){
            return WebgisConfig.HOST;
        }
        HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        if(request==null){
            return WebgisConfig.HOST;
        }
        String host = request.getHeader("SourceHost")!=null?request.getHeader("SourceHost"):request.getHeader("Host");
        return "http://"+host;
    }
}
