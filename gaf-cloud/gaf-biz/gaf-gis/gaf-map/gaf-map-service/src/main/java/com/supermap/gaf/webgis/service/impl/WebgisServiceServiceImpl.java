/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.webgis.service.impl;

import cn.hutool.core.net.URLDecoder;
import cn.hutool.core.util.URLUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.authority.client.AuthResourceApiClient;
import com.supermap.gaf.authority.client.AuthUserInfoDetailsClient;
import com.supermap.gaf.authority.commontype.AuthResourceApi;
import com.supermap.gaf.authority.enums.ResourceApiMethodEnum;
import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.KgGraph;
import com.supermap.gaf.commontypes.Link;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.Node;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.sys.mgt.client.SysCatalogClient;
import com.supermap.gaf.sys.mgt.commontype.SysCatalog;
import com.supermap.gaf.sys.mgt.enums.CatalogTypeEnum;
import com.supermap.gaf.utils.AppConfigParser;
import com.supermap.gaf.utils.PropertiesUtil;
import com.supermap.gaf.webgis.cache.RegistryResultCacheI;
import com.supermap.gaf.webgis.config.WebgisConfig;
import com.supermap.gaf.webgis.dao.ServiceSourceMapper;
import com.supermap.gaf.webgis.dao.WebgisDataServiceFieldMapper;
import com.supermap.gaf.webgis.dao.WebgisServiceAssociationMapper;
import com.supermap.gaf.webgis.dao.WebgisServiceMapper;
import com.supermap.gaf.webgis.domain.BatchRegistryServiceResult;
import com.supermap.gaf.webgis.entity.*;
import com.supermap.gaf.webgis.enums.ServiceTypeEnum;
import com.supermap.gaf.webgis.proxy.ProxyHelper;
import com.supermap.gaf.webgis.service.ServiceShareSettingService;
import com.supermap.gaf.webgis.service.WebgisServiceService;
import com.supermap.gaf.webgis.util.WebgisCommonUtils;
import com.supermap.gaf.webgis.vo.ServiceSourceSelectVo;
import com.supermap.gaf.webgis.vo.WebgisServiceSelectVo;
import com.supermap.gaf.webgis.vo.WebgisServiceVO;
import com.supermap.services.rest.management.ServiceType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Nullable;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * GIS服务服务实现类
 *
 * @author wangxiaolong
 * @date 2020-12-05
 */
@Service
public class WebgisServiceServiceImpl implements WebgisServiceService {

    public static final String REGISTRY_TYPE_SERVER = "server";
    public static final String REGISTRY_TYPE_BATCH = "batch";
    public static final String REGISTRY_TYPE_SINGLE = "single";


    private static final Logger log = LoggerFactory.getLogger(WebgisServiceServiceImpl.class);

    @Autowired
    @Qualifier("workflowRestTemplate")
    private RestTemplate restTemplate;

    @Autowired
    public RegistryResultCacheI registryResultCacheI;


    @Autowired
    private WebgisServiceMapper webgisServiceMapper;
    //
    //@Autowired
    //private WebgisCatalogLayerService webgisCatalogLayerService;

    @Autowired
    private ServiceShareSettingService serviceShareSettingService;

    @Autowired
    private SysCatalogClient sysCatalogClient;

    @Autowired
    private AuthResourceApiClient authResourceApiClient;

    @Autowired
    private AuthUserInfoDetailsClient authUserInfoDetailsClient;

    @Autowired
    private WebgisServiceAssociationMapper webgisServiceAssociationMapper;

    @Autowired
    private WebgisDataServiceFieldMapper webgisDataServiceFieldMapper;

    //@Autowired
    //private WebgisConfigService webgisConfigService;

    @Autowired
    private ServiceSourceMapper serviceSourceMapper;


    @Override
    public WebgisService getByIdHasRealAddress(String gisServiceId) {
        if (gisServiceId == null) {
            throw new IllegalArgumentException("gisServiceId不能为空");
        }
        return webgisServiceMapper.selectHasRealAddress(gisServiceId);
    }

    @Override
    public WebgisService getById(String gisServiceId) {
        if (gisServiceId == null) {
            throw new IllegalArgumentException("gisServiceId不能为空");
        }
        return webgisServiceMapper.select(gisServiceId);
    }
    @Override
    public List<String> filterByOwn(Collection<String> gisServiceIds) {
        if (CollectionUtils.isEmpty(gisServiceIds)) {
            return new ArrayList<>();
        }
        List<WebgisService> services = webgisServiceMapper.selectByIdsAndCreatedBy(gisServiceIds,SecurityUtilsExt.getUserName(),"gis_service_id");
        if(services==null){
            return new ArrayList<>();
        }
        return services.stream().map(item->item.getGisServiceId()).collect(Collectors.toList());
    }
    @Override
    public String filterByOwn(String gisServiceId) {
        List<String> re=  filterByOwn(Arrays.asList(gisServiceId));
        if(CollectionUtils.isEmpty(re)){
            return null;
        }
        return re.get(0);
    }



    @Override
    public BatchRegistryServiceResult getRegistryServiceResult(String resultCode) {
        BatchRegistryServiceResult registryServiceResult = registryResultCacheI.get(resultCode);
        if (registryServiceResult == null) {
            throw new GafException("指定requestCode不存在");
        }
        return registryServiceResult;
    }

    @Override
    public Page<WebgisServiceVO> listByPageCondition(WebgisServiceSelectVo webgisServiceSelectVo, int pageNum, int pageSize) {
        String userId = SecurityUtilsExt.getUserId();
        String name = SecurityUtilsExt.getUserName();
        PageInfo<WebgisService> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            webgisServiceMapper.selectList(webgisServiceSelectVo);
        });
        List<WebgisService> list = pageInfo.getList();
        if (list.isEmpty()) {
            return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), Collections.emptyList());
        }
        List<String> gisServiceIds = list.stream().map(WebgisService::getGisServiceId).collect(Collectors.toList());
        List<ServiceShareSetting> serviceShareSettings = serviceShareSettingService.listByUserIdAndServiceIds(userId,gisServiceIds);
        Set<String> sharedServices = serviceShareSettings.stream().map(ServiceShareSetting::getGisServiceId).collect(Collectors.toSet());
        List<WebgisServiceVO> webgisServiceVOList = list.stream().map(webgisService -> {
            WebgisServiceVO webgisServiceVO = new WebgisServiceVO();
            BeanUtils.copyProperties(webgisService, webgisServiceVO);
            webgisServiceVO.setOwner(name.equals(webgisService.getCreatedBy()));
            webgisServiceVO.setShareToMe(sharedServices.contains(webgisService.getGisServiceId()));
            return webgisServiceVO;
        }).collect(Collectors.toList());
        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), webgisServiceVOList);
    }
    @Override
    public List<WebgisService> selectNoRealAddressList(List<String> proxyAddress,String typeCode) {
        return webgisServiceMapper.selectNoRealAddressList(proxyAddress,typeCode);
    }

    @Override
    public WebgisService insertWebgisService(WebgisService webgisService, @Nullable String sourceId, @Nullable Integer sourceType) {

        String userName = SecurityUtilsExt.getUserName();
        webgisService.setCreatedBy(userName);
        webgisService.setUpdatedBy(userName);
        if(!WebgisCommonUtils.checkServiceUrlFormat(webgisService.getAddress(),webgisService.getTypeCode())){
            throw new GafException("url 格式校验失败",400);
        }
        registryWebgis(webgisService,sourceId,sourceType);
        return webgisService;
    }

    @Override
    @Transactional
    public WebgisService registryWebgis(WebgisService webgisService, @Nullable String sourceId, @Nullable Integer sourceType) {
        // 自动将不能识别的iserver ServiceType转换为GAF能识别的
        ServiceTypeEnum typeEnum = ServiceTypeEnum.OTHER;
        try{
            typeEnum = ServiceTypeEnum.valueOf(webgisService.getTypeCode());
        }catch (Exception e){
            try{
                typeEnum = WebgisCommonUtils.convert2GafTypeCode(ServiceType.valueOf(webgisService.getTypeCode()));
            }catch (Exception ex){}
        }
        webgisService.setTypeCode(typeEnum.getCode());
        if (!"MAPWORLD".equals(webgisService.getTypeCode())) {
            List<WebgisService> webgisServices = webgisServiceMapper.selectNoRealAddressListByVo(WebgisServiceSelectVo.builder().address(webgisService.getAddress()).build());
            if (!CollectionUtils.isEmpty(webgisServices)) {
                throw new GafException("服务地址重复", GafException.CONFLICT);
            }
        }
        String id = UUID.randomUUID().toString();
        Date now  = new Date();
        webgisService.setCreatedTime(now);
        webgisService.setUpdatedTime(now);
        webgisService.setGisServiceId(id);
        String decodeAddress = URLDecoder.decode(webgisService.getAddress(),StandardCharsets.UTF_8);
//        AuthResourceApi authResourceApi = addWebgisServiceApi(webgisService);
//        webgisService.setResourceApiId(authResourceApi.getResourceApiId());
        // 代理地址存储url 中的path，查询时拼接路由返回完整地址
        webgisService.setProxyAddress(UriComponentsBuilder.fromHttpUrl(decodeAddress).build().getPath());
        // 若为三维服务  且服务地址不以/rest/realspace 结尾的，则增加扩展属性resourceTag: "RESTREALSPACE-DATA"
        modifyMoreProperties(webgisService, decodeAddress);
        webgisServiceMapper.insert(webgisService);
        log.info("服务：{},{}",id,webgisService.getAddress());
        if(sourceId!=null && sourceType!=null){
            List<ServiceSource> list = serviceSourceMapper.selectList(ServiceSourceSelectVo.builder().serviceId(id).sourceId(sourceId).sourceType(sourceType).build());
            if(CollectionUtils.isEmpty(list)){
                log.info("关联：{},{},{}",id,sourceId,sourceType);
                serviceSourceMapper.insert(ServiceSource.builder().serviceSourceId(UUID.randomUUID().toString()).serviceId(id).sourceId(sourceId).sourceType(sourceType).build());
            }
        }
        // 添加路由
        ProxyHelper.getProxyAddress(WebgisConfig.getHost(),webgisService.getAddress(),true);
        return webgisService;
    }

    public static void modifyMoreProperties(WebgisService webgisService, String decodeAddress) {
        String typeCode = webgisService.getTypeCode();
        if (ServiceTypeEnum.RESTREALSPACE.getCode().equals(typeCode) && !decodeAddress.endsWith("/rest/realspace")) {
            String moreProperties = webgisService.getMoreProperties();
            if (!StringUtils.isEmpty(moreProperties)) {
                JSONObject jsonObject = JSONObject.parseObject(moreProperties);
                if (!jsonObject.containsKey("resourceTag")) {
                    jsonObject.put("resourceTag","RESTREALSPACE-DATA");
                    webgisService.setMoreProperties(jsonObject.toJSONString());
                }
            } else {
                webgisService.setMoreProperties("{\"resourceTag\":\"RESTREALSPACE-DATA\"}");
            }
        }
    }

    private AuthResourceApi addWebgisServiceApi(WebgisService webgisService) {
        List<SysCatalog> catalogs = listCatalogs(SysCatalog.builder().type(CatalogTypeEnum.WEBGIS_SERVICE_GROUP_TYPE.getValue()).name(ServiceTypeEnum.getMap().get(webgisService.getTypeCode())).build());
        SysCatalog webgisTypeCatalog = null;
        if (catalogs.size() <= 0) {
            // 插入目录 前查找名为webgis服务的根目录
            List<SysCatalog> webgisRootCatalogs = listCatalogs(SysCatalog.builder().type(CatalogTypeEnum.WEBGIS_SERVICE_GROUP_TYPE.getValue()).name("webgis服务").build());
            SysCatalog webgisRoot = null;
            if (webgisRootCatalogs.size() <= 0) {
                // 插入根目录
                webgisRoot = addSysCatalog(SysCatalog.builder()
                        .type(CatalogTypeEnum.WEBGIS_SERVICE_GROUP_TYPE.getValue())
                        .parentId("0")
                        .name("webgis服务")
                        .description("webgis服务根目录")
                        .sortSn(1)
                        .build());
            } else {
                webgisRoot = webgisRootCatalogs.get(0);
            }
            webgisTypeCatalog = addSysCatalog(SysCatalog.builder()
                    .type(webgisRoot.getType())
                    .parentId(webgisRoot.getCatalogId())
                    .name(ServiceTypeEnum.getMap().get(webgisService.getTypeCode()))
                    .description("webgis服务类型为" + ServiceTypeEnum.getMap().get(webgisService.getTypeCode()) + "对应的分组目录")
                    .sortSn(1)
                    .build());
        } else {
            webgisTypeCatalog = catalogs.get(0);
        }
        String routeUrl = URLUtil.getPath(URLUtil.normalize(webgisService.getAddress()));
        // 查询在该分组下是否有同名的routeUrl
//        List<AuthResourceApi> sameNameApis = listApis(AuthResourceApi.builder().apiCatalogId(webgisTypeCatalog.getCatalogId()).routeUrl(routeUrl).build());
//        if (sameNameApis.size() > 0) {
//            return sameNameApis.get(0);
//        }
        AuthResourceApi authResourceApi = new AuthResourceApi();
        authResourceApi.setMethod(ResourceApiMethodEnum.GET.getValue());
        authResourceApi.setRouteUrl(routeUrl);
        authResourceApi.setName(webgisService.getName());
        authResourceApi.setStatus(true);
        // 类型2表示第三方
        authResourceApi.setType("2");
        authResourceApi.setApiCatalogId(webgisTypeCatalog.getCatalogId());
        return addApi(authResourceApi);
    }

    private List<SysCatalog> listCatalogs(SysCatalog queryCatalog) {
        MessageResult<List<SysCatalog>> messageResult = sysCatalogClient.list(queryCatalog);
        if (!messageResult.isSuccessed()) {
            throw new GafException(messageResult.getMessage());
        }
        return messageResult.getData();
    }

    private SysCatalog addSysCatalog(SysCatalog sysCatalog) {
        MessageResult<SysCatalog> messageResult = sysCatalogClient.insertSysCatalog(sysCatalog);
        if (!messageResult.isSuccessed()) {
            throw new GafException(messageResult.getMessage());
        }
        return messageResult.getData();
    }

    private List<AuthResourceApi> listApis(AuthResourceApi query) {
        MessageResult<List<AuthResourceApi>> messageResult = authResourceApiClient.list(query);
        if (!messageResult.isSuccessed()) {
            throw new GafException(messageResult.getMessage());
        }
        return messageResult.getData();
    }

    private AuthResourceApi addApi(AuthResourceApi authResourceApi) {
        MessageResult<AuthResourceApi> messageResult = authResourceApiClient.insertAuthResourceApi(authResourceApi);
        if (!messageResult.isSuccessed()) {
            throw new GafException(messageResult.getMessage());
        }
        return messageResult.getData();
    }


    @Override
    public void batchInsert(List<WebgisService> webgisServices) {
        if (webgisServices != null && webgisServices.size() > 0) {
            for (WebgisService webgisService : webgisServices) {
                insertWebgisService(webgisService,null,null);
            }
        }

    }

    @Transactional
    @Override
    public void deleteWebgisService(String gisServiceId) {
        String id = this.filterByOwn(gisServiceId);
        if(id == null){
            return;
        }
        webgisServiceMapper.deleteByIdAndCreatedBy(id,SecurityUtilsExt.getUserName());
        serviceSourceMapper.deleteByServiceIds(Arrays.asList(id));
        webgisServiceAssociationMapper.deleteByServiceIds(Arrays.asList(id));
    }

    @Override
    public void batchDelete(List<String> gisServiceIds) {
        List<String> ids = filterByOwn(gisServiceIds);
        if(CollectionUtils.isEmpty(ids)){
            return;
        }
        webgisServiceMapper.batchDeleteByIdAndCreatedBy(ids, SecurityUtilsExt.getUserName());
        serviceSourceMapper.deleteByServiceIds(ids);
        webgisServiceAssociationMapper.deleteByServiceIds(ids);
    }

    @Override
    public WebgisService updateWebgisService(WebgisService webgisService) {
        String id = this.filterByOwn(webgisService.getGisServiceId());
        if(id == null){
            throw new GafException("资源不存在或没有权限");
        }
        webgisService.setUpdatedBy(SecurityUtilsExt.getUserName());
        if (StringUtils.isBlank(webgisService.getMoreProperties())) {
            webgisService.setMoreProperties(null);
        }
        webgisServiceMapper.update(webgisService);
        return webgisService;

    }

    @Override
    public List<TreeNode> getServiceTypes() {
        ServiceTypeEnum[] values = ServiceTypeEnum.values();
        List<TreeNode> res = new LinkedList<>();
        for (int i = 0; i < values.length; i++) {
            TreeNode treeNode = new TreeNode();
            treeNode.setParentId("0");
            treeNode.setKey(values[i].getCode());
            treeNode.setTitle(values[i].getName());
            treeNode.setSortSn(i + 1);
            treeNode.setType(0);
            res.add(treeNode);
        }
        return res;
    }

    @Override
    public List<WebgisService> listByIds(List<String> ids) {
        return webgisServiceMapper.selectByIds(ids);
    }

    //@Override
    //public Page<WebgisService> listByPageCondition(WebgisServiceConditonVo webgisServiceConditonVo, Integer pageNum, Integer pageSize, String layerCatalogId) {
    //    Set<String> serviceIdSet = new HashSet<>();
    //    if (!StringUtils.isEmpty(layerCatalogId)) {
    //        List<String> serviceIds = webgisCatalogLayerService.listGisIdsByCatalogId(layerCatalogId);
    //        serviceIdSet.addAll(serviceIds);
    //    }
    //    PageInfo<WebgisService> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
    //        webgisServiceMapper.selectNotInSet(webgisServiceConditonVo, serviceIdSet);
    //    });
    //    return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
    //}

    // [{url: '',title: '', fields: [], alias: []}]
    @Override
    public List selectAssociationDataServices(String gisServiceId) {
        WebgisService webgisService = getById(gisServiceId);
        if (webgisService == null) {
            return new ArrayList();
        }
        List<WebgisServiceDo> serviceDos = new ArrayList<>();
        // 本身就是数据服务
        if (ServiceTypeEnum.RESTDATA.getCode().equals(webgisService.getTypeCode())) {
            WebgisServiceDo webgisServiceDo = new WebgisServiceDo();
            BeanUtils.copyProperties(webgisService, webgisServiceDo);
            serviceDos.add(webgisServiceDo);
        } else {
            serviceDos = webgisServiceAssociationMapper.selectAssociationServices(gisServiceId, WebgisServiceSelectVo.builder().typeCode(ServiceTypeEnum.RESTDATA.getCode()).build());
        }

        List re = (List) AppConfigParser.parse(serviceDos);
        for (Object item : re) {
            Map<String, Object> itemMap = (Map<String, Object>) item;
            List<WebgisDataServiceField> selectedFields = webgisDataServiceFieldMapper.selectByCombination(WebgisDataServiceField.builder()
                    .gisDataServiceId((String) itemMap.get("resourceId")).build());
            if (!CollectionUtils.isEmpty(selectedFields)) {
                List<String> fields = new ArrayList<>();
                List<String> alias = new ArrayList<>();
                for (WebgisDataServiceField serviceField : selectedFields) {
                    fields.add(serviceField.getFieldName());
                    alias.add(serviceField.getFieldAlias());
                }
                itemMap.put("fields", fields);
                itemMap.put("alias", alias);
            }
        }

        return re;
    }

    @Override
    public Page<WebgisService> listByTypeCodes(String typeCodes, int pageNum, int pageSize) {
        String[] typeCodeArr = typeCodes.split(",");
        List<String> arr = Arrays.asList(typeCodeArr);
        PageInfo<WebgisService> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            webgisServiceMapper.selectByTypeCodes(arr);
        });
        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
    }

    @Override
    public Page<WebgisService> share2mePageList(Integer pageNum, Integer pageSize) {
        PageInfo<WebgisService> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            webgisServiceMapper.share2me(SecurityUtilsExt.getUserId());
        });
        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
    }

    @Override
    public int disable(String gisServiceId) {
        gisServiceId = filterByOwn(gisServiceId);
        if(gisServiceId == null){
            throw new GafException("无权限");
        }
        return webgisServiceMapper.disable(gisServiceId);
    }

    @Override
    public int enable(String gisServiceId) {
        gisServiceId = filterByOwn(gisServiceId);
        if(gisServiceId == null){
            throw new GafException("无权限");
        }
        return webgisServiceMapper.enable(gisServiceId);
    }


    @Override
    public List<WebgisService> selectRestDataByDatasourceNameAndDatasetName(String datasourceName, String datasetName){
        List<WebgisService> dataServices = webgisServiceMapper.selectRestDataByDatasourceNameAndDatasetName(datasourceName,datasetName);
        return dataServices;
    }

    @Override
    public KgGraph selectConsanguinityByDateset(String datasourceName, String datasetName) {
        List<WebgisService> dataServices = webgisServiceMapper.selectRestDataByDatasourceNameAndDatasetName(datasourceName,datasetName);
        List<Node> nodes = new ArrayList<>();
        List<Link> links = new ArrayList<>();
        for(WebgisService item:dataServices){
            Node node = new Node(item.getGisServiceId(),item.getName());
            PropertiesUtil.copyProperties2Map(item,node,"gisServiceId","name");
            nodes.add(node);
        }
        if(!CollectionUtils.isEmpty(dataServices)){
            List<String> dataServiceIds = dataServices.stream().map(WebgisService::getGisServiceId).collect(Collectors.toList());
            List<WebgisServiceDo> associationServices = webgisServiceAssociationMapper.selectServiceDoByDataServiceIds(dataServiceIds);
            if(!CollectionUtils.isEmpty(associationServices)){
                for(WebgisServiceDo item:associationServices){
                    Node node = new Node(item.getGisServiceId(),item.getName());
                    PropertiesUtil.copyProperties2Map(item,node,"gisServiceId","name","gisServiceAssocId","assocServiceId");
                    nodes.add(node);
                    Link link = new Link(item.getAssocServiceId(),item.getGisServiceId());
                    links.add(link);
                }
            }
        }
        return new KgGraph(nodes,links);
    }

}
