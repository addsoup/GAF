/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.webgis.resource.root;

import com.supermap.gaf.rest.jersey.JaxrsStaticViewResource;
import com.supermap.gaf.webgis.resource.*;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;

/**
 * webgis jersey资源根类
 *
 * @author wangxiaolong
 * @Copyright
 * @date 2020-12-05
 */
@Component
@Path("/")
@Api("地图应用组件")
public class WebgisRootResource {


    @Path("/view")
    public Class<JaxrsStaticViewResource> staticViewResoruce() {
        return JaxrsStaticViewResource.class;
    }

    @Path("/webgis-services")
    public Class<WebgisServiceResource> webgisServiceResource() {
        return WebgisServiceResource.class;
    }

    @Path("/iservices")
    public Class<WebgisIserverResource> webgisIserverResource() {
        return WebgisIserverResource.class;
    }


    @Path("/webgis-service-associations")
    public Class<WebgisServiceAssociationResource> webgisServiceAssociationResource() {
        return WebgisServiceAssociationResource.class;
    }

    @Path("/webgis-data-service-fields")
    public Class<WebgisDataServiceFieldResource> webgisDataServiceFieldResource() {
        return WebgisDataServiceFieldResource.class;
    }

    @Path("/webgis-roam-routes")
    public Class<WebgisRoamRouteResource> webgisRoamRouteResource() {
        return WebgisRoamRouteResource.class;
    }

    @Path("/webgis-roam-stops")
    public Class<WebgisRoamStopResource> webgisRoamStopResource() {
        return WebgisRoamStopResource.class;
    }

    @Path("/service-sources")
    public Class<ServiceSourceResource> serviceSourceResource(){
        return  ServiceSourceResource.class;
    }

    @Path("/service-share-setting")
    public Class<ServiceShareSettingResource> serviceShareSettingResource(){
        return  ServiceShareSettingResource.class;
    }

    @Path("/administrativedivisions")
    public Class<AdministrativeDivisionsResource> administrativeDivisionsResource(){
        return  AdministrativeDivisionsResource.class;
    }
    @Path("/iportal")
    public Class<IportalResource> iportalResource(){
        return  IportalResource.class;
    }


    @Path("/docker")
    public Class<DockerBuildResource> dockerBuildResourceClass(){
        return  DockerBuildResource.class;
    }


    @Path("/prop")
    public Class<PropResource> propResourceClass(){
        return  PropResource.class;
    }
}
