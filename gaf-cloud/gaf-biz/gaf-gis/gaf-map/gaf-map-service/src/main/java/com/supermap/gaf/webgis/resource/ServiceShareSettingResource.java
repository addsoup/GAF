package com.supermap.gaf.webgis.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.webgis.client.ServiceShareSettingClient;
import com.supermap.gaf.webgis.entity.ServiceShareSetting;
import com.supermap.gaf.webgis.entity.ServiceShareSettingBatchParam;
import com.supermap.gaf.webgis.entity.ServiceShareSettingBatchUpdateParam;
import com.supermap.gaf.webgis.service.ServiceShareSettingService;
import com.supermap.gaf.webgis.vo.ServiceShareSettingSelectVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 服务共享设置表接口
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "服务共享设置表接口")
public class ServiceShareSettingResource implements ServiceShareSettingClient {
    @Autowired
    private ServiceShareSettingService serviceShareSettingService;

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询服务共享设置表", notes = "通过id查询服务共享设置表")
	@Path("/{serviceShareSettingId}")
    public MessageResult<ServiceShareSetting> getById(@PathParam("serviceShareSettingId")String serviceShareSettingId){
        ServiceShareSetting serviceShareSetting = serviceShareSettingService.getById(serviceShareSettingId);
		return MessageResult.successe(ServiceShareSetting.class).data(serviceShareSetting).status(200).message("查询成功").build();
    }
    @Override
	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询服务共享设置表", notes = "分页条件查询服务共享设置表")
    public MessageResult<Page<ServiceShareSetting>> pageList(@BeanParam ServiceShareSettingSelectVo serviceShareSettingSelectVo,
										@DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
										@DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<ServiceShareSetting> page = serviceShareSettingService.listByPageCondition(serviceShareSettingSelectVo, pageNum, pageSize);
		return MessageResult.data(page).status(200).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增服务共享设置表", notes = "新增服务共享设置表")
    public MessageResult<Void> insertServiceShareSetting(ServiceShareSetting serviceShareSetting){
        serviceShareSettingService.insertServiceShareSetting(serviceShareSetting);
		return MessageResult.successe(Void.class).status(200).message("新增操作成功").build();
    }
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    @ApiOperation(value = "批量新增服务共享设置表", notes = "批量新增服务共享设置表")
    public MessageResult<Void> batchInsert(ServiceShareSettingBatchParam serviceShareSettingBatchParam){
	    List<ServiceShareSetting> serviceShareSettings = serviceShareSettingBatchParam.getServiceShareSettings();
	    if(!CollectionUtils.isEmpty(serviceShareSettings)){
	        for(ServiceShareSetting serviceShareSetting:serviceShareSettings){
	            serviceShareSetting.setSpatialSetting(serviceShareSettingBatchParam.getSpatialSetting());
                serviceShareSetting.setFieldsSetting(serviceShareSettingBatchParam.getFieldsSetting());
            }
            serviceShareSettingService.batchInsert(serviceShareSettings);
        }
		return MessageResult.successe(Void.class).status(200).message("批量新增操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除服务共享设置表", notes = "根据id删除服务共享设置表")
	@Path("/{serviceShareSettingId}")
    public MessageResult<Void> deleteServiceShareSetting(@PathParam("serviceShareSettingId")String serviceShareSettingId){
        serviceShareSettingService.deleteServiceShareSetting(serviceShareSettingId);
		return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除服务共享设置表", notes = "批量删除服务共享设置表")
    public MessageResult<Void> batchDelete(List<String> serviceShareSettingIds){
        serviceShareSettingService.batchDelete(serviceShareSettingIds);
		return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新服务共享设置表", notes = "根据id更新服务共享设置表")
	@Path("/{serviceShareSettingId}")
    public MessageResult<Void> updateServiceShareSetting(ServiceShareSetting serviceShareSetting,@PathParam("serviceShareSettingId")String serviceShareSettingId){
        serviceShareSetting.setServiceShareSettingId(serviceShareSettingId);
        serviceShareSettingService.updateServiceShareSetting(serviceShareSetting);
		return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }


    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据ids批量更新空间范围或字段", notes = "根据ids批量更新空间范围或字段")
    @Path("/batch")
    public MessageResult<Void> batchUpdateServiceShareSetting(ServiceShareSettingBatchUpdateParam serviceShareSettingBatchUpdateParam){
        serviceShareSettingService.batchUpdateByIds(serviceShareSettingBatchUpdateParam);
        return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }
	
	


}
