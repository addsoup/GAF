package com.supermap.gaf.webgis.resource;

import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.utils.FileUtil;
import com.supermap.iportal.web.utils.PrjCoordSysUtil;
import com.supermap.services.components.commontypes.Feature;
import com.supermap.services.components.commontypes.Geometry;
import com.supermap.services.components.commontypes.PrjCoordSys;
import com.supermap.services.components.commontypes.QueryParameter;
import com.supermap.services.providers.GeoPackageDataProvider;
import com.supermap.services.providers.GeoPackageDataProviderSetting;
import com.supermap.services.util.CoordinateConversionTool;
import com.supermap.services.util.PrjCoordSysConversionTool;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.util.List;

@Component
@Api(value = "行政区划范围查询")
public class AdministrativeDivisionsResource {

    private final static String CHN_DB_PATH = "CHN.gpkg";

    private final static Logger log = LoggerFactory.getLogger(AdministrativeDivisionsResource.class);

    @PostConstruct
    void init(){
        File dbFile = FileUtil.getResourceAsFile(CHN_DB_PATH,true,"map");

    }

    @GET
    @Path("/{A3Code:[a-zA-Z]{3}}/{adcode:\\w+}")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "A3Code", value = "国家代码", example = "CHN", paramType = "path", dataType = "string", required = true),
            @ApiImplicitParam(name = "adcode", value = "区划代码", example = "220000", paramType = "path", dataType = "string", required = true)
    })
    @ApiOperation(value = "通过国家代码和区划代码查询空间范围", notes = "通过国家代码和区划代码查询空间范围")
    public Geometry getAdministrativeDivisionGeometry(@PathParam("A3Code") String a3Code, @PathParam("adcode") String adcode, @QueryParam("prjCoordSys") String prjCoordSysJson){
        a3Code = a3Code.toUpperCase();
        File dbFile = FileUtil.getResourceAsFile(CHN_DB_PATH,true,"map");
        PrjCoordSys prjCoordSys = PrjCoordSysUtil.getPrjCoordSysFromJson(prjCoordSysJson);
        QueryParameter queryParameter = new QueryParameter();
        queryParameter.attributeFilter = "ADCODE = " + adcode;
        queryParameter.name = a3Code;
        GeoPackageDataProviderSetting providerSetting =  new GeoPackageDataProviderSetting();
        providerSetting.setFilePath(dbFile.getAbsolutePath());
        GeoPackageDataProvider geoPackageDataProvider = new GeoPackageDataProvider(providerSetting);
        List<Feature> features = geoPackageDataProvider.getFeature(a3Code, queryParameter);
        if(CollectionUtils.isEmpty(features)){
            throw new GafException("找不到指定code的数据");
        }
        Geometry re = features.get(0).geometry;
        geoPackageDataProvider.dispose();
        if (re != null && prjCoordSys != null) {
            re = CoordinateConversionTool.convert(re, PrjCoordSysConversionTool.getWGS1984(), prjCoordSys);
        }
        return re;
    }


}
