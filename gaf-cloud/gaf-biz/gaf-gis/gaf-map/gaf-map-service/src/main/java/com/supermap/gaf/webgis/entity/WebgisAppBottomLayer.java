package com.supermap.gaf.webgis.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.supermap.gaf.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 地图应用底图
 * @author zhurongcheng 
 * @date yyyy-mm-dd
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("地图应用底图")
public class WebgisAppBottomLayer implements Serializable{
    @ApiModelProperty("地图应用底图id")
    @Id
    private String gisAppBottomLayerId;
    @ApiModelProperty("地图名称")
    private String name;
    @ApiModelProperty("地图应用")
    @ParentIdField
    private String gisAppId;
    @NotNull
    @ApiModelProperty("底图")
    private String bottomServiceId;
    @ApiModelProperty("缩略图")
    @ConfigName("thumbnailAddr")
    private String thumbnailAddr;
    @ApiModelProperty("描述")
    private String description;
    @ApiModelProperty("排序序号")
    @SortSnField
    private Integer sortSn;
    /**
    * 默认值1：false
    */
    @ApiModelProperty("默认底图")
    @ConfigName("isDefault")
    @JSONField(name="isDefault")
    private Boolean isDefault;
    @ApiModelProperty("状态")
    @JSONField(name="isStatus")
    @LogicDeleteField
    private Boolean status;
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("修改时间")
    @UpdatedTimeField
    private Date updatedTime;
    @ApiModelProperty("修改人")
    private String updatedBy;

}