//package com.supermap.gaf.webgis.service.impl;
//
//import com.github.pagehelper.PageHelper;
//import com.github.pagehelper.PageInfo;
//import com.supermap.gaf.commontypes.pagination.Page;
//import com.supermap.gaf.exception.GafException;
//import com.supermap.gaf.webgis.dao.WebgisAppToolbarMapper;
//import com.supermap.gaf.webgis.dao.WebgisButtonMapper;
//import com.supermap.gaf.webgis.dao.WebgisToolbarButtonMapper;
//import com.supermap.gaf.webgis.entity.WebgisAppToolbar;
//import com.supermap.gaf.webgis.entity.WebgisButton;
//import com.supermap.gaf.webgis.entity.WebgisToolbarButton;
//import com.supermap.gaf.webgis.service.WebgisAppToolbarService;
//import com.supermap.gaf.webgis.vo.WebgisAppToolbarSelectVo;
//import com.supermap.gaf.webgis.vo.WebgisAppToolbarVo;
//import com.supermap.gaf.webgis.vo.WebgisToolbarButtonSelectVo;
//import com.supermap.gaf.webgis.vo.WebgisToolbarButtonVo;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.CollectionUtils;
//import org.springframework.util.StringUtils;
//
//import java.util.*;
//import java.util.stream.Collectors;
//
///**
// * 地图工具条模板服务实现类
// * @author zrc
// * @date yyyy-mm-dd
// */
//@Service
//public class WebgisAppToolbarServiceImpl implements WebgisAppToolbarService {
//
//	private static final Logger  log = LoggerFactory.getLogger(WebgisAppToolbarServiceImpl.class);
//
//	private static final String BUTTONID_CATALOG_PREFIX = "catalog:";
//
//	@Autowired
//    private WebgisAppToolbarMapper webgisAppToolbarMapper;
//
//	@Autowired
//	private WebgisToolbarButtonMapper webgisAppToolbarButtonMapper;
//
//	@Autowired
//	private WebgisButtonMapper webgisButtonMapper;
//
//    @Override
//    public WebgisAppToolbar getAndCheckById(String appToolbarId, boolean templateOp){
//        WebgisAppToolbar re =  getById(appToolbarId,templateOp);
//        if(re==null){
//            throw new GafException("工具条不存在");
//        }
//        return re;
//    }
//
//	@Override
//    public WebgisAppToolbar getById(String appToolbarId, boolean templateOp){
//        if(appToolbarId == null){
//            throw new IllegalArgumentException("appToolbarId不能为空");
//        }
//        return  webgisAppToolbarMapper.select(appToolbarId,templateOp);
//    }
//
//    @Override
//    public WebgisAppToolbarVo getVoById(String appToolbarId, boolean templateOp){
//        WebgisAppToolbar toolbarTp = getAndCheckById(appToolbarId,templateOp);
//        return toVo(toolbarTp);
//    }
//
//
//
//	@Override
//    public Page<WebgisAppToolbarVo> listByPageCondition(WebgisAppToolbarSelectVo WebgisAppToolbarSelectVo, int pageNum, int pageSize, boolean templateOp) {
//        PageInfo<WebgisAppToolbar> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
//            webgisAppToolbarMapper.selectList(WebgisAppToolbarSelectVo, templateOp);
//        });
//        List<WebgisAppToolbar> list = pageInfo.getList();
//        List<WebgisAppToolbarVo> vos = list.stream().map(item->toVo(item)).collect(Collectors.toList());
//        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),vos);
//    }
//
//	@Override
//	public 	List<WebgisAppToolbarVo> selectList(WebgisAppToolbarSelectVo WebgisAppToolbarSelectVo){
//        List<WebgisAppToolbar> list = webgisAppToolbarMapper.selectList(WebgisAppToolbarSelectVo,false);
//        List<WebgisAppToolbarVo> vos = list.stream().map(item->toVo(item)).collect(Collectors.toList());
//        return vos;
//	}
//
//	@Override
//    @Transactional(rollbackFor = Exception.class)
//    public void insertWebgisAppToolbar(WebgisAppToolbarVo WebgisAppToolbarVo){
//        WebgisAppToolbar appToolbar2 = new WebgisAppToolbar();
//        BeanUtils.copyProperties(WebgisAppToolbarVo,appToolbar2);
//        String appToolbarId = UUID.randomUUID().toString();
//        appToolbar2.setAppToolbarId(appToolbarId);
//        webgisAppToolbarMapper.insert(appToolbar2);
//        insertToolbarTpButtonVos(appToolbarId,WebgisAppToolbarVo.getToolbarButtonVos());
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public void batchInsert(List<WebgisAppToolbarVo> appToolbarVos) {
//        if(!CollectionUtils.isEmpty(appToolbarVos)){
//            List<WebgisToolbarButton> addToolbarButton2s = new ArrayList<>();
//            List<WebgisAppToolbar> appToolbars = new ArrayList<>();
//            for(WebgisAppToolbarVo item:appToolbarVos){
//                String appToolbarId = UUID.randomUUID().toString();
//                item.setAppToolbarId(appToolbarId);
//                List<WebgisToolbarButton> toolbarButtonVos = insertToolbarTpButtonVos(appToolbarId,item.getToolbarButtonVos(),false);
//                if(toolbarButtonVos != null){
//                    addToolbarButton2s.addAll(toolbarButtonVos);
//                }
//                WebgisAppToolbar appToolbar2 = new WebgisAppToolbar();
//                BeanUtils.copyProperties(item,appToolbar2);
//                appToolbars.add(appToolbar2);
//            }
//            webgisAppToolbarMapper.batchInsert(appToolbars);
//            if(!addToolbarButton2s.isEmpty()){
//                webgisAppToolbarButtonMapper.batchInsert(addToolbarButton2s);
//            }
//        }
//    }
//
//
//    @Override
//    public int deleteWebgisAppToolbar(String appToolbarId, boolean templateOp){
//		return batchDelete(Arrays.asList(appToolbarId),templateOp);
//    }
//
//	@Override
//    @Transactional(rollbackFor = Exception.class)
//    public int batchDelete(List<String> appToolbarId, boolean templateOp){
//		if(!CollectionUtils.isEmpty(appToolbarId)){
//		    webgisAppToolbarButtonMapper.deleteByAppToolbarIds(appToolbarId);
//			return webgisAppToolbarMapper.batchDelete(appToolbarId, templateOp);
//		}
//		return 0;
//    }
//
//    @Override
//    public int deleteByGisAppIds(List<String> gisAppIds) {
//        if(!CollectionUtils.isEmpty(gisAppIds)){
//            List<WebgisAppToolbar> appToolbars =webgisAppToolbarMapper.selectByGisAppIds(gisAppIds);
//            if(!CollectionUtils.isEmpty(appToolbars)){
//                List<String> appToolbarId = appToolbars.stream().map(WebgisAppToolbar::getAppToolbarId).collect(Collectors.toList());
//                return batchDelete(appToolbarId,false);
//            }
//        }
//        return 0;
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public int updateWebgisAppToolbar(WebgisAppToolbarVo toolbarTpVo, boolean templateOp){
//        webgisAppToolbarButtonMapper.deleteByAppToolbarIds(Arrays.asList(toolbarTpVo.getAppToolbarId()));
//        insertToolbarTpButtonVos(toolbarTpVo.getAppToolbarId(),toolbarTpVo.getToolbarButtonVos());
//		return webgisAppToolbarMapper.update(toolbarTpVo,templateOp);
//    }
//
//    private WebgisAppToolbarVo toVo(WebgisAppToolbar WebgisAppToolbar){
//        WebgisAppToolbarVo result = new WebgisAppToolbarVo();
//        String appToolbarId = WebgisAppToolbar.getAppToolbarId();
//        BeanUtils.copyProperties(WebgisAppToolbar,result);
//        List<WebgisToolbarButtonVo> parents = new ArrayList<>();
//        List<WebgisToolbarButtonVo> allButtons = webgisAppToolbarButtonMapper.selectVoList(WebgisToolbarButtonSelectVo
//                .builder().appToolbarId(appToolbarId)
//                .orderFieldName("sort_sn").build());
//        // 处理按钮children
//        if (allButtons != null) {
//            List<WebgisToolbarButtonVo> hasParentButtons = new ArrayList<>();
//            // LinkedHashMap保持map遍历有序
//            Map<String, WebgisToolbarButtonVo> parentId2Button = new LinkedHashMap<>();
//            for (WebgisToolbarButtonVo item : allButtons) {
//                if (item.getPid() != null) {
//                    hasParentButtons.add(item);
//                } else {
//                    if(item.getButtonId().startsWith(BUTTONID_CATALOG_PREFIX)){
//                        item.setName(item.getButtonId().substring(BUTTONID_CATALOG_PREFIX.length()));
//                        item.setButtonId(null);
//                    }
//                    parentId2Button.put(item.getToolbarButtonId(), item);
//                }
//            }
//            for (WebgisToolbarButtonVo item : hasParentButtons) {
//                WebgisToolbarButtonVo parentButton = parentId2Button.get(item.getPid());
//                if(parentButton==null) {
//                    parents.add(item);
//                }else {
//                    List<WebgisToolbarButtonVo> childrens = parentButton.getChildren();
//                    parentButton.setIsCatalog(true);
//
//                    if(parentButton.getChildren() == null){
//                        childrens = new ArrayList<>();
//                        parentButton.setChildren(childrens);
//                    }
//                    childrens.add(item);
//                }
//            }
//            parents.addAll(parentId2Button.values());
//        }
//        result.setToolbarButtonVos(parents);
//        return result;
//    }
//
//
//    List<WebgisToolbarButton> insertToolbarTpButtonVos(String appToolbarId, List<WebgisToolbarButtonVo> toolbarTpButtonVos){
//	    return insertToolbarTpButtonVos(appToolbarId,toolbarTpButtonVos,true);
//    }
//    List<WebgisToolbarButton> insertToolbarTpButtonVos(String appToolbarId, List<WebgisToolbarButtonVo> toolbarTpButtonVos, boolean executeInsert){
//        if(!CollectionUtils.isEmpty(toolbarTpButtonVos)){
//            List<WebgisToolbarButton> toolbarButtons = new ArrayList<>();
//            for(WebgisToolbarButtonVo item:toolbarTpButtonVos){
//                String toolbarButtonId = UUID.randomUUID().toString();
//                item.setToolbarButtonId(toolbarButtonId);
//                item.setAppToolbarId(appToolbarId);
//                // 严格按照children组织上下级，pid无效
//                item.setPid(null);
//                if(!CollectionUtils.isEmpty(item.getChildren())){
//                    item.setIsCatalog(true);
//                }
//                if(item.getIsCatalog() == null || !item.getIsCatalog()){
//                    checkButtonAndCopyProp(item);
//                }
//                if(item.getIsCatalog() == true){
//                    if(item.getButtonId()==null){
//                        if(item.getName()==null){
//                            throw new GafException("name目录名称不能为null");
//                        }
//                        // 当是目录并未关联按钮，用buttonId存储目录名称
//                        item.setButtonId(BUTTONID_CATALOG_PREFIX+item.getName());
//                    }
//                    if(item.getChildren() != null){
//                        for(WebgisToolbarButtonVo children:item.getChildren()){
//                            checkButtonAndCopyProp(children);
//                            children.setToolbarButtonId(UUID.randomUUID().toString());
//                            children.setPid(toolbarButtonId);
//                            children.setAppToolbarId(appToolbarId);
//                            toolbarButtons.add(children);
//                        }
//                        resetSortSn(item.getChildren());
//                    }
//                }
//                toolbarButtons.add(item);
//            }
//            resetSortSn(toolbarTpButtonVos);
//            if(executeInsert){
//                webgisAppToolbarButtonMapper.batchInsert(toolbarButtons);
//            }
//            return toolbarButtons;
//        }
//        return null;
//    }
//
//    void resetSortSn(List<WebgisToolbarButtonVo> toolbarButtonVos){
//        Collections.sort(toolbarButtonVos,(s1,s2)->{
//            Integer x = s1.getSortSn()==null?Integer.MAX_VALUE:s1.getSortSn();
//            Integer y = s2.getSortSn()==null?Integer.MAX_VALUE:s2.getSortSn();
//            return (x < y) ? -1 : 1;
//        });
//        for (int i = 0; i < toolbarButtonVos.size(); ++i) {
//            toolbarButtonVos.get(i).setSortSn(i+1);
//        }
//    }
//
//    void checkButtonAndCopyProp(WebgisToolbarButtonVo toolbarButtonVo){
//        if(toolbarButtonVo.getButtonId() == null){
//            throw new GafException("buttonId不能为null");
//        }
//        WebgisButton webgisButton = webgisButtonMapper.select(toolbarButtonVo.getButtonId());
//        if(webgisButton == null){
//            throw new GafException("按钮不存在："+toolbarButtonVo.getButtonId());
//        }
//        if(StringUtils.isEmpty(toolbarButtonVo.getIcon())){
//            toolbarButtonVo.setIcon(webgisButton.getIcon());
//        }
//    }
//
//}
