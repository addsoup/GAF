//package com.supermap.gaf.webgis.domain;
//
//import com.supermap.gaf.webgis.entity.WebgisAppBottomLayer;
//import com.supermap.gaf.webgis.entity.WebgisAppLocaction;
//import com.supermap.gaf.webgis.vo.WebgisAppToolbarVo;
//import com.supermap.gaf.webgis.vo.WebgisCatalogTreeVo;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import java.util.List;
//
///**
// * @author heykb
// */
//@Data
//@Builder
//@AllArgsConstructor
//@NoArgsConstructor
//@ApiModel("预览app接口参数")
//public class WebgisAppPreviewParam {
//    @ApiModelProperty("应用工具条预览配置")
//    private List<WebgisAppToolbarVo> toolbars;
//    @ApiModelProperty("资源目录树")
//    private WebgisCatalogTreeVo webgisCatalogTreeVo;
//    @ApiModelProperty("应用底图预览配置")
//    private List<WebgisAppBottomLayer> webgisAppBottomLayers;
//    @ApiModelProperty("应用底图预览配置类型。0：天地图，1：自定义")
//    private String bottomLayersType = "1";
//    @ApiModelProperty("应用初始定位预览配置")
//    private WebgisAppLocaction location;
//    @ApiModelProperty("专题分析服务配置")
//    private List<String> themeServiceCodes;
//    private long timestamp;
//}
