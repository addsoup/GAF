/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.webgis.vo;

import com.supermap.gaf.annotation.ConfigName;
import com.supermap.gaf.webgis.entity.WebgisCatalogLayer;
import com.supermap.gaf.webgis.entity.WebgisServiceDo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * 图层
 *
 * @author wangxiaolong
 * @date 2020-12-05
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("图层")
public class WebgisCatalogLayerVo extends WebgisCatalogLayer {

    @ApiModelProperty("服务名称")
    private String serviceName;
    @ApiModelProperty("服务英文名称")
    private String serviceNameEn;
    @ApiModelProperty("服务访问地址")
    @ConfigName("resourceLocation")
    private String address;
    @ApiModelProperty("服务类型")
    @ConfigName({"resourceTag", "sourceType"})
    private String typeCode;
    @ApiModelProperty(value = "扩展属性", notes = "自定义属性,json,数据只加一层而不加多层，接口读出转为json时平铺放到服务属性中去")
    @ConfigName(value = "moreProperties", expand = true)
    private String moreProperties;


    @ApiModelProperty("关联服务")
    private List<WebgisServiceDo> associationServices;
}
