//package com.supermap.gaf.webgis.dao;
//
//import com.supermap.gaf.webgis.entity.WebgisToolbarButton;
//import com.supermap.gaf.webgis.vo.WebgisToolbarButtonSelectVo;
//import com.supermap.gaf.webgis.vo.WebgisToolbarButtonVo;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Param;
//import org.springframework.stereotype.Component;
//
//import java.util.Collection;
//import java.util.List;
//
///**
// * 地图工具条模板按钮数据访问类
// * @author zrc
// * @date yyyy-mm-dd
// */
//@Mapper
//@Component
//public interface WebgisToolbarButtonMapper {
//	/**
//     * 根据主键 toolbarButtonId 查询
//     *
//	 */
//    WebgisToolbarButton select(@Param("toolbarButtonId") String toolbarButtonId);
//
//	List<String> getRelatedToolbarNames(@Param("list") Collection<String> buttonIds);
//
//
//	/**
//     * 多条件查询
//     * @param WebgisAppToolbarButtonSelectVo 查询条件
//     * @return 若未查询到则返回空集合
//     */
//	List<WebgisToolbarButton> selectList(WebgisToolbarButtonSelectVo WebgisAppToolbarButtonSelectVo);
//
//
//	List<WebgisToolbarButtonVo> selectVoList(WebgisToolbarButtonSelectVo selectVo);
//    /**
//     * 新增
//     *
//	 */
//    void insert(WebgisToolbarButton WebgisAppToolbarButton);
//
//	/**
//     * 批量插入
//     *
//	 */
//	void batchInsert(@Param("list") Collection<WebgisToolbarButton> WebgisAppToolbarButtons);
//
//	/**
//     * 批量删除
//     *
//	 */
//    int batchDelete(@Param("list") Collection<String> toolbarButtonIds);
//
//	/**
//     * 刪除
//     *
//	 */
//    int delete(@Param("toolbarButtonId") String toolbarButtonId);
//
//    /**
//    * 更新
//    *
//    **/
//    int update(WebgisToolbarButton WebgisAppToolbarButton);
//	/**
//	 * 选择性更新
//	 *
//	 **/
//	int updateSelective(WebgisToolbarButton WebgisAppToolbarButton);
//
//	int deleteByAppToolbarIds(@Param("list") Collection<String> appToolbarIds);
//
//}
