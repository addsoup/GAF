package com.supermap.gaf.webgis.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;
import javax.validation.constraints.*;
import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import java.io.Serializable;

/**
 * 地图应用专题分析服务配置
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("地图应用专题分析服务配置")
public class WebgisAppThemeService implements Serializable{
    private static final long serialVersionUID = -1L;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("地图应用。")
    private String gisAppId;
    @ApiModelProperty("创建时间。生成时间不可变更")
    private Date createdTime;
    @ApiModelProperty("创建人。创建人user_id")
    private String createdBy;
    @ApiModelProperty("修改时间。修改时更新")
    private Date updatedTime;
    @ApiModelProperty("修改人。修改人user_id")
    private String updatedBy;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("专题分析服务编码。服务编码")
    private String themeServiceCode;
    @ApiModelProperty("地图应用专题分析配置id。")
    private String appThemeServiceId;
}