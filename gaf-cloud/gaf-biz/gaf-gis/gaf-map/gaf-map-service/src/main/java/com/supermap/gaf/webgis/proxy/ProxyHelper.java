package com.supermap.gaf.webgis.proxy;

import com.alibaba.fastjson.JSON;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.pagehelper.Page;
import com.github.pagehelper.page.PageMethod;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.gateway.GatewayFilterDefinition;
import com.supermap.gaf.gateway.GatewayPredicateDefinition;
import com.supermap.gaf.gateway.GatewayRouteDefinition;
import com.supermap.gaf.gateway.client.GatewayRouteClient;
import com.supermap.gaf.gateway.commontypes.RouteSearchParam;
import com.supermap.gaf.webgis.config.WebgisConfig;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;


/**
 * The type Proxy helper.
 * @author zrc
 */
@Component
public class ProxyHelper extends PageMethod {

    private static final Cache<String, String> SERVICE_PROXY_CACHE
            = Caffeine.newBuilder().maximumSize(100).build();

    private static final Logger log = LoggerFactory.getLogger(ProxyHelper.class);
    private static GatewayRouteClient routeClient;


    /**
     * Sets gateway client.
     *
     * @param routeClients the route clients
     */
    @Autowired
    public void setGatewayClient(@Autowired GatewayRouteClient routeClients) {
        ProxyHelper.routeClient = routeClients;
    }


    /**
     * Add proxy gateway route definition.
     *
     * @param schemeHostport  the scheme hostport
     * @param routeId         the route id
     * @param routePathPrefix the route path prefix
     * @return the gateway route definition
     */
    public static GatewayRouteDefinition addProxy(String schemeHostport, String routeId, String routePathPrefix){
        GatewayRouteDefinition routeDefinition = new GatewayRouteDefinition();
        routeDefinition.setUri(schemeHostport);
        routeDefinition.setOrder(0);
        routeDefinition.setRouteId(routeId);
        GatewayPredicateDefinition predicateDefinition = new GatewayPredicateDefinition();
        predicateDefinition.setArgs(String.format("/service-proxy/%s/**",UUID.randomUUID().toString().replace("-","")));
        predicateDefinition.setName("Path");
        routeDefinition.setPredicates(Arrays.asList(predicateDefinition));
        GatewayFilterDefinition filterDefinition = new GatewayFilterDefinition();
        filterDefinition.setArgs("2");
        filterDefinition.setName("StripPrefix");
        routeDefinition.setFilters(Arrays.asList(filterDefinition));
        log.info("添加iserver代理：{}->{}",predicateDefinition.getArgs(),schemeHostport);
        routeClient.addRoute(routeDefinition);
        return routeDefinition;
    }

    /**
     * 获取代理后的地址
     * @param proxyHost
     * @param originAddress
     * @param autoCreateAble 当路由不存在时是否自动添加路由
     * @return
     */
    public static String getProxyAddress(String proxyHost, String originAddress,boolean autoCreateAble){
        /**处理pageHelper,防止当运行在已存在的Page线程中时出现bug。gaf-boot情况下会出现该问题 ***********/
        Page page = getLocalPage();
        clearPage();
        /*************/
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(originAddress).build();
        String re =  proxyHost + getProxyRoutePathPrefixReal(originAddress,autoCreateAble)+uriComponents.getPath();
        /** 查询结束还原pageHelper ***********/
        setLocalPage(page);
        /*************/
        if(WebgisConfig.SERVICE_PROXY_ENABLE){
            return re;
        }
        return originAddress;
    }

    /**
     * 查询代理path prefix
     * @param serviceAddress
     * @return /service-proxy/xx
     */
    private static String getProxyRoutePathPrefixReal(String serviceAddress,boolean autoCreateAble){
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(serviceAddress).build();
        String schemeHostport = uriComponents.getScheme()+"://"+uriComponents.getHost()+(uriComponents.getPort()!=-1?":"+uriComponents.getPort():"");
        String key = "iserver-"+schemeHostport;
        return SERVICE_PROXY_CACHE.get(key,routeId->{
            RouteSearchParam param = new RouteSearchParam();
            param.setRouteId(routeId);
            MessageResult<List<GatewayRouteDefinition>> re = routeClient.queryGatewayRoutes(JSON.toJSONString(param));
            if(!re.IsSuccessed()){
                throw new GafException("代理失败(queryRoutes)："+re.getMessage());
            }
            GatewayRouteDefinition routeDefinition = null;
            if(CollectionUtils.isEmpty(re.getData())){
                if(!autoCreateAble){
                    return null;
                }
                // 添加路由
                routeDefinition = addProxy(schemeHostport,routeId, UUID.randomUUID().toString().replace("-",""));
            }else{
                routeDefinition = re.getData().get(0);
            }
            String routePrefix = null;
            for(GatewayPredicateDefinition item:routeDefinition.getPredicates()){
                if("Path".equals(item.getName())){
                    routePrefix = item.getArgs().replace("/**","");
                    break;
                }
            }
            if(routePrefix==null){
                throw new GafException("代理失败，请检查路由配置");
            }
            return routePrefix;
        });
    }

}
