/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.webgis.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.webgis.dao.WebgisDataServiceFieldMapper;
import com.supermap.gaf.webgis.dao.WebgisServiceAssociationMapper;
import com.supermap.gaf.webgis.domain.WebgisServiceWithFieldsDo;
import com.supermap.gaf.webgis.entity.WebgisDataServiceField;
import com.supermap.gaf.webgis.entity.WebgisServiceAssociation;
import com.supermap.gaf.webgis.entity.WebgisServiceDo;
import com.supermap.gaf.webgis.service.WebgisServiceAssociationService;
import com.supermap.gaf.webgis.vo.WebgisServiceAssociationSelectVo;
import com.supermap.gaf.webgis.vo.WebgisServiceSelectVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * GIS服务关联服务实现类
 *
 * @author wangxiaolong
 * @date yyyy-mm-dd
 */
@Service
public class WebgisServiceAssociationServiceImpl implements WebgisServiceAssociationService {
    @Autowired
    private WebgisServiceAssociationMapper webgisServiceAssociationMapper;

    @Autowired
    private WebgisDataServiceFieldMapper webgisDataServiceFieldMapper;


    @Override
    public WebgisServiceAssociation getById(String gisServiceAssocId) {
        if (gisServiceAssocId == null) {
            throw new IllegalArgumentException("gisServiceAssocId不能为空");
        }
        return webgisServiceAssociationMapper.select(gisServiceAssocId);
    }

    @Override
    public Page<WebgisServiceAssociation> listByPageCondition(WebgisServiceAssociationSelectVo webgisServiceAssociationSelectVo, int pageNum, int pageSize) {
        PageInfo<WebgisServiceAssociation> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            webgisServiceAssociationMapper.selectByOneField(webgisServiceAssociationSelectVo);
        });
        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
    }

    @Override
    public WebgisServiceAssociation insertWebgisServiceAssociation(WebgisServiceAssociation webgisServiceAssociation) {
        //TODO: 主键非GeneratedKey，此处添加自定义主键生成策略
        webgisServiceAssociation.setGisServiceAssocId(UUID.randomUUID().toString());

        String userName = SecurityUtilsExt.getUserName();
        webgisServiceAssociation.setCreatedBy(userName);
        webgisServiceAssociation.setUpdatedBy(userName);
        webgisServiceAssociationMapper.insert(webgisServiceAssociation);
        return webgisServiceAssociation;
    }

    @Override
    public void batchInsert(List<WebgisServiceAssociation> webgisServiceAssociations) {
        if (webgisServiceAssociations != null && webgisServiceAssociations.size() > 0) {
            String userName = SecurityUtilsExt.getUserName();
            webgisServiceAssociations.forEach(webgisServiceAssociation -> {
                webgisServiceAssociation.setGisServiceAssocId(UUID.randomUUID().toString());
                webgisServiceAssociation.setCreatedBy(userName);
                webgisServiceAssociation.setUpdatedBy(userName);
            });
            webgisServiceAssociationMapper.batchInsert(webgisServiceAssociations);
        }

    }

    @Override
    public void deleteWebgisServiceAssociation(String gisServiceAssocId) {
        webgisServiceAssociationMapper.delete(gisServiceAssocId);
    }

    @Override
    public void batchDelete(List<String> gisServiceAssocIds) {
        webgisServiceAssociationMapper.batchDelete(gisServiceAssocIds);
    }

    @Override
    public WebgisServiceAssociation updateWebgisServiceAssociation(WebgisServiceAssociation webgisServiceAssociation) {
        webgisServiceAssociation.setUpdatedBy(SecurityUtilsExt.getUserName());
        webgisServiceAssociationMapper.update(webgisServiceAssociation);
        return webgisServiceAssociation;
    }

    @Override
    public Page<WebgisServiceDo> listAssociationServices(String webgisServiceId, WebgisServiceSelectVo webgisServiceSelectVo, Integer pageNum, Integer pageSize) {
        PageInfo<WebgisServiceDo> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            webgisServiceAssociationMapper.selectAssociationServices(webgisServiceId, webgisServiceSelectVo);
        });
        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
    }

    @Override
    public List<WebgisServiceWithFieldsDo> listAssociationServicesWithFields(String webgisServiceId, WebgisServiceSelectVo webgisServiceSelectVo) {
        List<WebgisServiceDo> webgisServiceDos = webgisServiceAssociationMapper.selectAssociationServices(webgisServiceId, webgisServiceSelectVo);
        return webgisServiceDos.stream().map(webgisServiceDo -> {
            WebgisDataServiceField query = WebgisDataServiceField.builder().status(true).gisDataServiceId(webgisServiceDo.getGisServiceId()).build();
            List<WebgisDataServiceField> selectedFields = this.webgisDataServiceFieldMapper.selectByCombination(query);
            WebgisServiceWithFieldsDo webgisServiceWithFieldsDo = new WebgisServiceWithFieldsDo();
            BeanUtils.copyProperties(webgisServiceDo, webgisServiceWithFieldsDo);
            webgisServiceWithFieldsDo.setFields(selectedFields);
            return webgisServiceWithFieldsDo;
        }).collect(Collectors.toList());
    }

    @Override
    public Page<WebgisServiceDo> listUnrelatedServices(String webgisServiceId, WebgisServiceSelectVo webgisServiceSelectVo, Integer pageNum, Integer pageSize) {
        PageInfo<WebgisServiceDo> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            webgisServiceAssociationMapper.selectUnrelatedServices(webgisServiceId, webgisServiceSelectVo);
        });
        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
    }

    @Override
    public List<WebgisServiceDo> selectServiceDoByDataServiceIds(List<String> dataServiceId) {
        return webgisServiceAssociationMapper.selectServiceDoByDataServiceIds(dataServiceId);
    }

}
