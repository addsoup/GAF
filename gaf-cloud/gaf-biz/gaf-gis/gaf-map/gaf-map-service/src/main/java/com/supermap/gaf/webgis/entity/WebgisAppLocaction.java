package com.supermap.gaf.webgis.entity;
import com.supermap.gaf.annotation.ConfigName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;
import javax.validation.constraints.*;
import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 初始定位
 * @author zhurongcheng
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("初始定位")
public class WebgisAppLocaction implements Serializable{
    @ApiModelProperty("地图应用显示id")
    private String gisAppLocationId;
    @NotNull
    @ApiModelProperty("地图应用")
    private String gisAppId;
    @ApiModelProperty("飞入方式")
    private String moveMode;
    @ApiModelProperty("高亮特效")
    private String highlightEffect;
    @ApiModelProperty("定位方式")
    private String locationMode;
    @ApiModelProperty("中心点经度")
    @ConfigName("longitude")
    private Double centreLongtitude;
    @ApiModelProperty("中心点纬度")
    @ConfigName("latitude")
    private Double centreLatitude;
    @ApiModelProperty("中心点高度")
    @ConfigName("altitude")
    private Double centreAltitude;
    @ApiModelProperty("朝向")
    @ConfigName("heading")
    private Double heading;
    @ApiModelProperty("倾斜度")
    @ConfigName("tilt")
    private Double tilt;
    @ApiModelProperty("边界")
    private String bounds;
    @ApiModelProperty("描述")
    private String description;
    @ApiModelProperty("状态")
    @JSONField(name="isStatus")
    private Boolean status;
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("修改时间")
    private Date updatedTime;
    @ApiModelProperty("修改人")
    private String updatedBy;
}