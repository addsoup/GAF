package com.supermap.desktop.develop.rewrite;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.supermap.desktop.core.Application;
import com.supermap.desktop.core.iserver.ServerReleaseMapCache;
import com.supermap.desktop.core.properties.CoreProperties;
import com.supermap.desktop.core.tools.iserverInfo.IServerLoginInfo;
import com.supermap.desktop.core.utilties.PathUtilities;
import com.supermap.desktop.core.utilties.StringUtilities;
import com.supermap.desktop.netservices.NetServicesProperties;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 解决idesktop发布MongoDB瓦片的BUG
 */
public class ServerReleaseGAFMongoDBMapCache  extends ServerReleaseMapCache {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private ArrayList<String> g;
    private String h;

    public ServerReleaseGAFMongoDBMapCache(IServerLoginInfo iServerLoginInfo) {
        super(iServerLoginInfo);
    }

    public Boolean release() {
        boolean result = false;

        try {
            if (!this.isCancel()) {
                String token = this.getToken();
                if (StringUtilities.isNullOrEmptyString(token)) {
                    return false;
                }
                boolean resultMap = false;
                if(this.getServicesType() != 0){
                    this.fireFunctionProgress(0, 0, CoreProperties.getString("String_ReleaseStart"), "", 2, 1);
                    resultMap = this.releaseMapCacheWithOutUploading();
                    this.fireFunctionProgress(50, 50, CoreProperties.getString("String_Releasing"), "", 2, 1);

                }
                boolean result3DMap = false;
                if (this.getTilesetNames().size() > 0) {
                    this.fireFunctionProgress(75, 80, CoreProperties.getString("String_Releasing"), NetServicesProperties.getString("String_Release3DServer"), 2, 2);
                    result3DMap = this.a();
                }

                this.fireFunctionProgress(100, 100, CoreProperties.getString("String_ReleaseCompleted"), "", 2, 2);
                result = result3DMap || resultMap;
            }
        } catch (Exception var5) {
            Application.getActiveApplication().getOutput().output(var5);
        }

        return result;
    }
    private void deleteProviders(List<String> names, String host, String token){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String url = MessageFormat.format("{0}/manager/providers.rjson?token={1}", host, token);
        HttpPut put = new HttpPut(url);
        StringEntity stringEntity = new StringEntity(JSON.toJSONString(names), ContentType.TEXT_PLAIN);
        put.setEntity(stringEntity);
        try(CloseableHttpResponse response = httpClient.execute(put);) {
        } catch (Exception exception) {
            Application.getActiveApplication().getOutput().output(exception);
        }
    }
    private void deleteComponents(List<String> names, String host, String token){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String url = MessageFormat.format("{0}/manager/components.rjson?token={1}", host, token);
        HttpPut put = new HttpPut(url);
        StringEntity stringEntity = new StringEntity(JSON.toJSONString(names), ContentType.TEXT_PLAIN);
        put.setEntity(stringEntity);
        try(CloseableHttpResponse response = httpClient.execute(put);) {
        } catch (Exception exception) {
            Application.getActiveApplication().getOutput().output(exception);
        }
    }

    private boolean a() throws IOException {
        boolean result = false;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse mapCacheProviderResponse = null;
        String host = getHost();
        String token = getToken();
        try {
            String releaseMapCacheProviderURL = MessageFormat.format("{0}/manager/providers.rjson?token={1}", host, token);
            HttpPost mapCacheProviderPost = new HttpPost(releaseMapCacheProviderURL);
            String mapCacheProvider = this.b();
            StringEntity stringEntity = new StringEntity(mapCacheProvider, ContentType.APPLICATION_JSON);
            mapCacheProviderPost.setEntity(stringEntity);
            mapCacheProviderResponse = httpClient.execute(mapCacheProviderPost);
            StatusLine responseStatus = mapCacheProviderResponse.getStatusLine();
            String providersResponseText = EntityUtils.toString(mapCacheProviderResponse.getEntity(), "utf-8");
            if(responseStatus.getStatusCode() == 400){
                if(!StringUtilities.isNullOrEmpty(providersResponseText)){
                    JSONObject re = JSON.parseObject(providersResponseText);
                    String errorMsg = re.getJSONObject("error").getString("errorMsg");
                    String name = JSON.parseObject(mapCacheProvider).getString("name");
                    if(errorMsg!= null && (errorMsg.contains("已经存在") || errorMsg.contains("同名"))){
                        try{
                            deleteProviders(Arrays.asList(name),host,token);
                            deleteComponents(Arrays.asList(name),host,token);
                            return a();
                        }catch (Exception e){}
                    }
                }
            }
            if (responseStatus.getStatusCode() != 200 && responseStatus.getStatusCode() != 201 && responseStatus.getStatusCode() != 202) {
                this.releaseFailed(providersResponseText);
            }else if (!StringUtilities.isNullOrEmpty(providersResponseText)) {
                String releaseMapCacheComponentsURL = MessageFormat.format("{0}/manager/components.rjson?token={1}", this.getHost(), this.getToken());
                HttpPost releaseMapCacheComponentsPost = new HttpPost(releaseMapCacheComponentsURL);
                String mapCacheComponents = this.c();
                StringEntity componentsStringEntity = new StringEntity(mapCacheComponents, ContentType.APPLICATION_JSON);
                releaseMapCacheComponentsPost.setEntity(componentsStringEntity);
                CloseableHttpResponse mapCacheComponentsResponse = httpClient.execute(releaseMapCacheComponentsPost);
                StatusLine componentsResponseStatus = mapCacheComponentsResponse.getStatusLine();
                String componentsResponseText = EntityUtils.toString(mapCacheComponentsResponse.getEntity(), "utf-8");
                if (componentsResponseStatus.getStatusCode() != 200 && componentsResponseStatus.getStatusCode() != 201 && componentsResponseStatus.getStatusCode() != 202) {
                    this.releaseFailed(componentsResponseText);
                } else if (!StringUtilities.isNullOrEmpty(componentsResponseText)) {
                    String url = this.getResultRootServer(componentsResponseText) + "/rest" + "/realspace";
                    this.resultURLs.add(PathUtilities.convertToIServerSeparator(url));
                    mapCacheComponentsResponse.close();
                    result = true;
                }
            }
        } catch (Exception var21) {
            Application.getActiveApplication().getOutput().output(var21);
        } finally {
            httpClient.close();
            if (mapCacheProviderResponse != null) {
                mapCacheProviderResponse.close();
            }

        }

        return result;
    }

    protected String getType() {
        return this.isVector ? "com.supermap.services.providers.MongoDBMVTTileProvider" : "com.supermap.services.providers.MongoDBTileProvider";
    }

    protected String getProviderName() {
        if (StringUtilities.isNullOrEmpty(this.getMapName())) {
            this.providerName = "mongodb" + (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime());
        } else {
            this.providerName = "mongodb-" + this.getMapName().replaceAll("@", "") + (new Date()).hashCode();
            this.providerName = StringUtilities.convertToPinyin(this.providerName);
        }

        return this.providerName;
    }

    public String getConfigInfo() {
        JSONObject configInfoEntityBody = new JSONObject();
        ArrayList serverAdresses = new ArrayList();
        serverAdresses.add(this.getMongoDBHost());
        configInfoEntityBody.put("serverAdresses", serverAdresses);
        configInfoEntityBody.put("username", this.getUsername());
        configInfoEntityBody.put("password", this.getPassword());
        configInfoEntityBody.put("database", this.getDatabase());
        configInfoEntityBody.put("mapName", this.getMapName());
        configInfoEntityBody.put("tilesetName", this.getTilesetName());
        configInfoEntityBody.put("cacheEnabled", false);
        return configInfoEntityBody.toString();
    }

    private String b() {
        JSONObject cacheProviderEntityBody = new JSONObject();
        cacheProviderEntityBody.put("config", this.d());
        cacheProviderEntityBody.put("type", this.e());
        cacheProviderEntityBody.put("name", this.f());
        cacheProviderEntityBody.put("enable", true);
        return cacheProviderEntityBody.toString();
    }

    private String c() {
        JSONObject cacheComponentsEntityBody = new JSONObject();
        cacheComponentsEntityBody.put("interfaceNames", "rest");
        cacheComponentsEntityBody.put("name", this.h);
        cacheComponentsEntityBody.put("providers", this.h);
        cacheComponentsEntityBody.put("type", "com.supermap.services.components.impl.RealspaceImpl");
        return cacheComponentsEntityBody.toString();
    }

    private String d() {
        JSONObject configInfoEntityBody = new JSONObject();
        ArrayList serverAdresses = new ArrayList();
        serverAdresses.add(this.getMongoDBHost());
        configInfoEntityBody.put("serverAdresses", serverAdresses);
        configInfoEntityBody.put("username", this.getUsername());
        configInfoEntityBody.put("password", this.getPassword());
        configInfoEntityBody.put("database", this.getDatabase());
        configInfoEntityBody.put("tilesetNames", this.getTilesetNames());
        return configInfoEntityBody.toString();
    }

    private String e() {
        return "com.supermap.services.providers.MongoDBRealspaceProvider";
    }

    private String f() {
        if (this.getTilesetNames().size() == 1) {
            this.h = "3D-mongodb-" + ((String)this.getTilesetNames().get(0)).replace("@", "");
        } else {
            this.h = "3D-mongodb";
        }

        this.h = this.getAvailableProviderName(StringUtilities.convertToPinyin(this.h));
        return this.h;
    }

    public void setMongoDBHost(String mongoDBHost) {
        this.a = mongoDBHost;
    }

    public void setDatabase(String database) {
        this.b = database;
    }

    public void setUsername(String username) {
        this.c = username;
    }

    public void setPassword(String password) {
        this.d = password;
    }

    public void setMapName(String mapName) {
        this.e = mapName;
    }

    public void setTilesetName(String tilesetName) {
        this.f = tilesetName;
    }

    public ArrayList<String> getTilesetNames() {
        return this.g;
    }

    public void setTilesetNames(ArrayList<String> tilesetNames) {
        this.g = tilesetNames;
    }

    public String getMongoDBHost() {
        return this.a;
    }

    public String getDatabase() {
        return this.b;
    }

    public String getUsername() {
        return this.c;
    }

    public String getPassword() {
        return this.d;
    }

    public String getMapName() {
        return this.e;
    }

    public String getTilesetName() {
        return this.f;
    }
}
