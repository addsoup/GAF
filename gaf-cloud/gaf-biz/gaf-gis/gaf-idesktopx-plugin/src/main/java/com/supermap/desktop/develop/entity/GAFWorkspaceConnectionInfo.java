package com.supermap.desktop.develop.entity;

import com.supermap.data.WorkspaceConnectionInfo;

public class GAFWorkspaceConnectionInfo extends WorkspaceConnectionInfo implements GAFSource {
    private String id;

    private String localPath;

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
