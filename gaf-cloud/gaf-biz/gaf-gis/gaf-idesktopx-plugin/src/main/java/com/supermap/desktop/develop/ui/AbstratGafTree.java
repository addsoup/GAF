/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.ui;

import com.alibaba.fastjson.JSONArray;
import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.desktop.controls.ui.controls.SmFileChoose;
import com.supermap.desktop.controls.utilities.WorkspaceTreeManagerUIUtilities;
import com.supermap.desktop.core.Application;
import com.supermap.desktop.core.FileChooseMode;
import com.supermap.desktop.core.Interface.IBaseItem;
import com.supermap.desktop.core.implement.SmPopupMenu;
import com.supermap.desktop.core.tools.historyManager.HistoryManager;
import com.supermap.desktop.develop.ctrlaction.GAFTreeNodeCtrlAction;
import com.supermap.desktop.develop.entity.Catalog;
import com.supermap.desktop.develop.entity.GAFDatasourceConnectionInfo;
import com.supermap.desktop.develop.entity.GAFSource;
import com.supermap.desktop.develop.utils.CommonUtils;
import org.apache.arrow.vector.util.CallBack;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultTreeCellRenderer;

import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author heykb
 * @date:2021/3/25
 */
public abstract class AbstratGafTree extends JTree {
    private SmPopupMenu leafPopupMenu;
    private SmPopupMenu catalogPopupMenu;

    protected SmFileChoose smFileChoose;

    protected DefaultTreeModel treeModel;

    protected HistoryManager historyManager;

    public AbstratGafTree() {
        this(null, null);
    }

    public DefaultTreeModel getTreeModel() {
        return treeModel;
    }

    protected void initCellRenderer(List opened) {
        Map<String, String> source2Path = new HashMap<>();
        if (historyManager != null) {
            source2Path = CommonUtils.getSourceId2PathMap(historyManager);
        }
        setCellRenderer(new TreeCellRender(this, source2Path, opened));
    }

    public AbstratGafTree(HistoryManager historyManager, JSONArray list) {
        this.historyManager = historyManager;
        initFileChoose();
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("root");
        buildTree(root, list);
        treeModel = new DefaultTreeModel(root);
        setModel(treeModel);
        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        setShowsRootHandles(true);
        initCellRenderer(getCurrentOpened());
        setRootVisible(false);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evt) {
                treeMousePressed(evt);
            }
        });

        expandRow(3);
        expandRow(2);
        expandRow(1);
        expandRow(0);
        expandRow(1);
        expandRow(2);
    }

    public void reDrawTree(List opened) {
        if (treeModel.getRoot() != null) {
            initCellRenderer(opened);
            this.treeModel.nodeChanged((TreeNode) treeModel.getRoot());
        }
    }

    abstract public List getCurrentOpened();

    public void updateNode(TreeNode node) {
        initCellRenderer(getCurrentOpened());
        this.treeModel.nodeChanged(node);
    }

    private String getFileChooseTitle() {
        return "下载到";
    }

    private void initFileChoose() {
        this.smFileChoose = new SmFileChoose("", FileChooseMode.SAVE_ONE, null);
        this.smFileChoose.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        this.smFileChoose.setTitle(getFileChooseTitle());
    }

    abstract protected void buildTree(DefaultMutableTreeNode root, JSONArray list);

    abstract void doubleClickHandler(DefaultMutableTreeNode node, CallBack cb);

    private void treeMousePressed(MouseEvent evt) {
        try {
            int clickCount = evt.getClickCount();
            int selRow = getRowForLocation(evt.getX(), evt.getY());
            TreePath selPath = getPathForLocation(evt.getX(), evt.getY());
            if (selPath == null) {
                return;
            }
            setSelectionPath(selPath);
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) getLastSelectedPathComponent();
            if (selRow != -1) {
                if (SwingUtilities.isLeftMouseButton(evt) && clickCount == 2) {
                    //双击事件
                    doubleClickHandler(node, null);
                }
                if (SwingUtilities.isLeftMouseButton(evt) && clickCount == 1) {
                    // 点击
                    if (!node.isLeaf()) {
                        if (isExpanded(selPath)) {
                            collapsePath(selPath);
                        } else {
                            expandPath(selPath);
                        }
                    }
                }
                if (SwingUtilities.isRightMouseButton(evt) && clickCount == 1) {
                    //右键单击
                    JPopupMenu popupMenu = getPopMenu(node);
                    if (popupMenu != null) {
                        popupMenu.show(this, evt.getX(), evt.getY());
                    }
                }
            }
        } catch (Exception e) {
            Application.getActiveApplication().getOutput().output(e);
        }

    }

    protected boolean faceLeafAndCheckOpened(TreeCellRender render, List opened, JTree tree,  Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        render.setText(value.toString());
        return false;
    }

    abstract SmPopupMenu buildLeafPopMenu(DefaultMutableTreeNode treeNode);

    abstract SmPopupMenu buildCatalogPopMenu(DefaultMutableTreeNode treeNode);

    protected SmPopupMenu getPopMenu(DefaultMutableTreeNode treeNode) {
        if (treeNode.getUserObject() instanceof Catalog) {
            this.catalogPopupMenu = this.catalogPopupMenu != null ? this.catalogPopupMenu : buildCatalogPopMenu(treeNode);
            return this.catalogPopupMenu;
        } else{
            this.leafPopupMenu = this.leafPopupMenu != null ? this.leafPopupMenu : buildLeafPopMenu(treeNode);
            for (IBaseItem iBaseItem : this.leafPopupMenu.items()) {
                if (iBaseItem.getCtrlAction().getCurrentCtrlAction() instanceof GAFTreeNodeCtrlAction) {
                    ((GAFTreeNodeCtrlAction) iBaseItem.getCtrlAction().getCurrentCtrlAction()).setNode(treeNode);
                }
            }
            return this.leafPopupMenu;
        }
    }

    protected static class TreeCellRender extends SubstanceDefaultTreeCellRenderer {
        private AbstratGafTree abstratGafTree;
        protected Map<String, String> source2pathMap;
        protected List opened;

        public TreeCellRender(AbstratGafTree abstratGafTree, Map<String, String> source2pathMap, List opened) {
            this.abstratGafTree = abstratGafTree;
            this.source2pathMap = source2pathMap;
            this.opened = opened;
        }

        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
            DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) value;
            Object userObject = defaultMutableTreeNode.getUserObject();
            String suffix = "";
            if (userObject instanceof String) {
                setIcon(WorkspaceTreeManagerUIUtilities.getIconByPath(WorkspaceTreeManagerUIUtilities.SYMBOL_ICON));
                setText((String) userObject);
                return this;
            }
            if (userObject instanceof Catalog) {
                Catalog catalog = (Catalog) userObject;
                setIcon(WorkspaceTreeManagerUIUtilities.getIconByPath(WorkspaceTreeManagerUIUtilities.SYMBOL_ICON));
                setText(catalog.getCatalogName());
                return this;
            }
            // 已下载状态
            if (userObject instanceof GAFSource) {
                GAFSource gafSource = (GAFSource) userObject;
                if (source2pathMap.containsKey(gafSource.getId())) {
                    gafSource.setLocalPath(source2pathMap.get(gafSource.getId()));
                    suffix = "（已下载）";
//                    setText(getText()+String.format("（已下载）"));
                }
            }
            boolean isOpened = abstratGafTree.faceLeafAndCheckOpened(this,opened, tree, value, sel, expanded, leaf, row, hasFocus);
            if (isOpened) {
                suffix = "（已打开）";
            }
            setText(getText() + suffix);
            return this;
        }
    }

}
