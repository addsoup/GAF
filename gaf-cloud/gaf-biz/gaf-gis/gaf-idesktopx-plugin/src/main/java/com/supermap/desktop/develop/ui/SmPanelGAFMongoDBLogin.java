package com.supermap.desktop.develop.ui;

import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.controls.ui.UICommonToolkit;
import com.supermap.desktop.controls.ui.controls.panels.SmPanelMongoDBLogin;
import com.supermap.desktop.core.utilties.JOptionPaneUtilities;
import com.supermap.desktop.core.utilties.MongoDBUtilities;
import com.supermap.tilestorage.*;

import java.util.ArrayList;

public class SmPanelGAFMongoDBLogin {

    private SmPanelMongoDBLogin smPanelMongoDBLogin;
    private TileStorageConnection connectionInfo;
    private ArrayList<TileStorageInfo> infos;
    public SmPanelGAFMongoDBLogin(TileStorageConnection connectionInfo) {
//        this.server = server;
//        this.databaseName = databaseName;
//        this.userName = userName;
//        this.password = password;
//        if(!MongoDBUtilities.isServerValidate(getServer())){
//            UICommonToolkit.showMessageDialog(ControlsProperties.getString("String_ServerUnAvailable"));
//            throw new RuntimeException();
//        }
        if(!MongoDBUtilities.isServerValidate(connectionInfo.getServer())){
            UICommonToolkit.showMessageDialog(ControlsProperties.getString("String_ServerUnAvailable"));
            throw new RuntimeException();
        }
        this.connectionInfo = connectionInfo;
    }

    public ArrayList<TileStorageInfo> getTileStorageInfos() {
        if(infos==null){
            infos = new ArrayList<>();
            TileStorageMiniInfo[] mongoTileStorageMiniInfo = TileStorageManager.getMongoTileStorageMiniInfo(connectionInfo.getServer(), connectionInfo.getDatabase(), connectionInfo.getUser(), connectionInfo.getPassword());
            boolean found = false;
            for(TileStorageMiniInfo item:mongoTileStorageMiniInfo){
                if(item.getName().equals(this.connectionInfo.getName())){
                    this.connectionInfo.setStorageType(item.getStorageType());
                    found = true;
                }
            }
            if(!found){
                JOptionPaneUtilities.showErrorMessageDialog("瓦片不存在!!!");
                throw new RuntimeException();
            }
            TileStorageManager tileStorageManager = new TileStorageManager();
            boolean status = tileStorageManager.open(this.connectionInfo);
            if(!status){
                JOptionPaneUtilities.showErrorMessageDialog("打开瓦片失败!!!");
                throw new RuntimeException();
            }
            TileStorageInfo info = tileStorageManager.getInfo();
            infos.add(info);
        }
        return infos;
    }
//        if (null == this.connectionInfo) {
//            return null;
//        } else {
//            TileStorageManager tileStorageManager = new TileStorageManager();
//            ArrayList infos = new ArrayList();
//            TileStorageMiniInfo[] mongoTileStorageMiniInfo = TileStorageManager.getMongoTileStorageMiniInfo(connectionInfo.getServer(), connectionInfo.getDatabase(), connectionInfo.getUser(), connectionInfo.getPassword());
//            if (mongoTileStorageMiniInfo.length > 0) {
//                int count = mongoTileStorageMiniInfo.length;
//
//                for(int i = 0; i < count; ++i) {
//                    TileStorageMiniInfo tileStorageMiniInfo = mongoTileStorageMiniInfo[i];
//                    if(tileStorageMiniInfo.getName())
//                    this.connectionInfo.setName(tileStorageMiniInfo.getName());
//
//
//                }
//                tileStorageManager.open(this.connectionInfo);
//                TileStorageInfo info = tileStorageManager.getInfo();
//                return Arrays.asList(info);
//    }


    public TileStorageConnection getConnectionInfo() {
        return connectionInfo;
    }
}
