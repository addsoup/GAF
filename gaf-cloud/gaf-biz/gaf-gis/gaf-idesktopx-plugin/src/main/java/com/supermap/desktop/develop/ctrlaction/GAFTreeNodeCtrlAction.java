package com.supermap.desktop.develop.ctrlaction;

import javax.swing.tree.DefaultMutableTreeNode;

public interface GAFTreeNodeCtrlAction {

    void setNode(DefaultMutableTreeNode node);
}
