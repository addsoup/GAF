package com.supermap.desktop.mapview.map.propertycontrols;

import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.controls.ControlsResources;
import com.supermap.desktop.controls.ui.controls.DialogResult;
import com.supermap.desktop.controls.ui.controls.SmDialogMongoDBLoader;
import com.supermap.desktop.controls.ui.controls.SmFileChoose;
import com.supermap.desktop.controls.ui.controls.progress.SmDialogProgressTotal;
import com.supermap.desktop.controls.utilities.ComponentFactory;
import com.supermap.desktop.controls.utilities.MapCacheUtilities;
import com.supermap.desktop.core.Application;
import com.supermap.desktop.core.FileChooseMode;
import com.supermap.desktop.core.Interface.IFormMap;
import com.supermap.desktop.core.enums.WindowType;
import com.supermap.desktop.core.properties.CoreProperties;
import com.supermap.desktop.core.utilties.*;
import com.supermap.desktop.develop.ui.GafTileManager;
import com.supermap.desktop.develop.utils.CommonUtils;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.mapview.MapViewResources;
import com.supermap.desktop.mapview.dialog.DialogDeleteMapCache;
import com.supermap.desktop.mapview.dialog.DialogMergeMapCache;
import com.supermap.desktop.mapview.layersPlayer.cache.CachePlayerBar;
import com.supermap.desktop.mapview.mapCache.DeleteMapCacheInfo;
import com.supermap.desktop.mapview.mapCache.LayerCacheData;
import com.supermap.desktop.mapview.mapCache.LayerCacheDataMongo;
import com.supermap.desktop.mapview.mapCache.MapCacheAllocation.DialogMapCacheAllocation;
import com.supermap.desktop.mapview.mapCache.MergeCacheProgressCallable;
import com.supermap.desktop.mapview.utilties.MergeCacheUtilities;
import com.supermap.mapping.LayerCache;
import com.supermap.mapping.Map;
import com.supermap.tilestorage.TileStorageConnection;
import com.supermap.tilestorage.TileStorageManager;
import com.supermap.tilestorage.TileVersion;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GAFControlMapMongoDBCacheManage extends ControlMapCacheManager {
    public static List<TileStorageConnection> CURRENT_LIST_CACHE = new ArrayList<>();
    private JButton buttonAddLocalCache;
    private JButton buttonMerge;
    private JButton buttonAllocate;
    private JButton buttonDelete;
    private JLabel labelCacheType;
    private JTextField textFieldCacheType;
    private JLabel labelVersion;
    private JComboBox<TileVersion> version;
    private JTextArea textAreaInfo;
    private boolean addItem = false;
    private ItemListener versionItemListener = (e) -> {
        if (!this.addItem) {
            TileVersion selectedItem = (TileVersion) this.version.getSelectedItem();
            if (selectedItem != null) {
                CachePlayerBar.currentLayerCache = selectedItem.getName();
                ((LayerCacheData) this.localTableModel.getSelectedDatas().get(0)).getLayerCache().setCurrentVersion(selectedItem.getName());
                this.mapControl.getMap().refresh();
            }
        }

    };
    private ActionListener actionListener = (e) -> {
        if (e.getSource() == this.buttonAddLocalCache) {
            this.addLocalData();
        } else if (e.getSource() == this.buttonMerge) {
            try {
                this.merge();
            } catch (Exception var3) {
                var3.printStackTrace();
            }
        } else if (e.getSource() == this.buttonAllocate) {
            this.allocate();
        } else if (e.getSource() == this.buttonDelete) {
            this.delete();
        }

    };

    private TableModelListener tableModelListener = e->{
        List<LayerCacheData> list = this.localTableModel.getLayerList();
        List<TileStorageConnection> conns = new ArrayList<>();
        for(LayerCacheData item:list){
            if(item!=null){
                conns.add(CommonUtils.getTileStorageConnection(item.getLayerCache()));
            }
        }
        CURRENT_LIST_CACHE = conns;
        GafTileManager.gafTilesManagerTree.reDrawTree(conns);
    };

    protected void showInfo(List<LayerCacheData> selectedDatas) {
        if (selectedDatas.size() != 1) {
            this.version.removeAllItems();
            this.textAreaInfo.setText("");
        } else {
            LayerCacheData layerCacheData = (LayerCacheData) selectedDatas.get(0);
            this.version.removeAllItems();
            if (layerCacheData instanceof LayerCacheDataMongo) {
                Iterator var3 = ((LayerCacheDataMongo) layerCacheData).getVersions().iterator();

                while (var3.hasNext()) {
                    TileVersion tileVersion = (TileVersion) var3.next();
                    this.addItem = true;
                    this.version.addItem(tileVersion);
                }

                this.addItem = false;
                this.setSelectedItem(layerCacheData);
            }

            String description = layerCacheData.getDescription();
            this.textAreaInfo.setText(description);
            this.textFieldCacheType.setText(MapCacheUtilities.getTileTypeDescription(layerCacheData.getTileFormat().value()) + "_" + MapCacheUtilities.getStorageTypeDescription(layerCacheData.getStorageType()));
        }

        this.version.setEnabled(this.version.getItemCount() > 0);
    }

    public GAFControlMapMongoDBCacheManage() {
        this.unRegisterEvents();
        this.registerEvents();
        this.localTableModel.removeTableModelListener(tableModelListener);
        this.localTableModel.addTableModelListener(tableModelListener);
    }


    protected void checkoutButtonEnable() {
    }

    protected void initLayout() {
        GroupLayout groupLayout = new GroupLayout(this);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setPreferredSize(new Dimension(200, 600));
        scrollPane.setViewportView(this.textAreaInfo);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup().addComponent(this.toolBar1).addComponent(this.toolBar2).addComponent(this.jScrollPane).addGroup(groupLayout.createSequentialGroup().addComponent(this.labelCacheType).addComponent(this.textFieldCacheType)).addGroup(groupLayout.createSequentialGroup().addComponent(this.labelVersion).addComponent(this.version)).addComponent(scrollPane));
        groupLayout.setVerticalGroup(groupLayout.createSequentialGroup().addComponent(this.toolBar1, -2, -1, -2).addComponent(this.toolBar2, -2, -1, -2).addComponent(this.jScrollPane).addGroup(groupLayout.createParallelGroup().addComponent(this.labelCacheType).addComponent(this.textFieldCacheType, -2, -1, -2)).addGroup(groupLayout.createParallelGroup().addComponent(this.labelVersion).addComponent(this.version, -2, -1, -2)).addComponent(scrollPane));
        this.setLayout(groupLayout);
    }

    protected void initializeResources() {
        super.initializeResources();
        this.buttonRemove.setToolTipText(ControlsProperties.getString("String_Button_Close"));
        this.buttonRemove.setIcon(ControlsResources.getIcon("/controlsresources/ToolBar/Image_delete.png"));
        this.labelPreViewType.setText(MapViewProperties.getString("String_Label_PreViewType"));
        this.labelVersion.setText(MapViewProperties.getString("String_Label_VersionName"));
        this.labelCacheType.setText(ControlsProperties.getString("String_MapCache_TileType"));
    }

    private void setSelectedItem(LayerCacheData layerCacheData) {
        LayerCache layerCache = layerCacheData.getLayerCache();
        if (!layerCache.isDisposed()) {
            String currentVersion = layerCache.getCurrentVersion();
            List versions = ((LayerCacheDataMongo) layerCacheData).getVersions();

            for (int i = 0; i < versions.size(); ++i) {
                if (((TileVersion) versions.get(i)).getName().equals(currentVersion)) {
                    this.version.setSelectedIndex(i);
                }
            }

        }
    }

    protected void initComponents() {
        super.initComponents();
        this.currentPreviewType = PREVIEW_TYPE_ALL;
        this.comboBoxPreViewType.setSelectedItem(PREVIEW_TYPE_ALL);
        this.buttonAddLocalCache = new JButton();
        this.buttonAddLocalCache.setIcon(MapViewResources.getIcon("/mapviewresources/Image_AddLocalCache.svg"));
        this.buttonAddLocalCache.setToolTipText(MapViewProperties.getString("String_AddLocalCache"));
        this.buttonMerge = new JButton();
        this.buttonMerge.setIcon(MapViewResources.getIcon("/mapviewresources/Image_Merge.svg"));
        this.buttonMerge.setToolTipText(CoreProperties.getString("String_OverlayAnalystMethod_Union"));
        this.buttonAllocate = new JButton();
        this.buttonAllocate.setIcon(MapViewResources.getIcon("/mapviewresources/Image_Allocate.svg"));
        this.buttonAllocate.setToolTipText(MapViewProperties.getString("String_Allocation"));
        this.buttonDelete = ComponentFactory.createToolBarButtonDelete();
        this.toolBar1.add(this.buttonAddLocalCache, 1);
        this.toolBar1.add(this.buttonMerge, 3);
        this.toolBar1.add(this.buttonAllocate, 4);
        this.toolBar1.add(new JToolBar.Separator(), 5);
        this.toolBar1.add(this.buttonDelete, 6);
        this.labelCacheType = new JLabel();
        this.textFieldCacheType = new JTextField();
        this.labelVersion = new JLabel();
        this.version = new JComboBox();
        this.version.setRenderer((list, value, index, isSelected, cellHasFocus) -> {
            JLabel label = new JLabel();
            if (value != null) {
                label.setText(value.getDesc());
            } else {
                label.setText("");
            }

            return label;
        });
        this.textAreaInfo = new JTextArea();
        this.textAreaInfo.setEditable(false);
        this.textAreaInfo.setWrapStyleWord(true);
        this.textAreaInfo.setWrapStyleWord(true);

    }

    protected void unRegisterEvents() {
        super.unRegisterEvents();
        this.buttonAddLocalCache.removeActionListener(this.actionListener);
        this.buttonMerge.removeActionListener(this.actionListener);
        this.buttonAllocate.removeActionListener(this.actionListener);
        this.buttonDelete.removeActionListener(this.actionListener);
        this.version.removeItemListener(this.versionItemListener);
    }

    private List<Double> getList(double[] array) {
        ArrayList arrayList = new ArrayList();
        double[] var3 = array;
        int var4 = array.length;

        for (int var5 = 0; var5 < var4; ++var5) {
            double item = var3[var5];
            arrayList.add(item);
        }

        return arrayList;
    }

    private double[] getDoubleArray(List<Double> list) {
        double[] array = new double[list.size()];

        for (int i = 0; i < list.size(); ++i) {
            array[i] = (Double) list.get(i);
        }

        return array;
    }

    private int[] getIntArray(List<Integer> list) {
        int[] array = new int[list.size()];

        for (int i = 0; i < list.size(); ++i) {
            array[i] = (Integer) list.get(i);
        }

        return array;
    }

    private boolean contains(int[] array, int item) {
        boolean isContains = false;
        int[] var4 = array;
        int var5 = array.length;

        for (int var6 = 0; var6 < var5; ++var6) {
            int temp = var4[var6];
            if (temp == item) {
                isContains = true;
                break;
            }
        }

        return isContains;
    }

    protected void registerEvents() {
        super.registerEvents();
        this.buttonAddLocalCache.addActionListener(this.actionListener);
        this.buttonMerge.addActionListener(this.actionListener);
        this.buttonAllocate.addActionListener(this.actionListener);
        this.buttonDelete.addActionListener(this.actionListener);
        this.version.addItemListener(this.versionItemListener);
    }

    private void delete() {
        List deleteCacheList = new ArrayList();
        this.table.getSelectionModel().removeListSelectionListener(this.listSelectionListener);
        this.table.getSelectionModel().addListSelectionListener(this.listSelectionListener);
        List layerList = this.localTableModel.getSelectedDatas();
        if (layerList.size() == 1 && ((LayerCacheDataMongo) layerList.get(0)).getVersions().size() == 1) {
            String message = MessageFormat.format(MapViewProperties.getString("String_Message_DeleteMapCache"), ((LayerCacheData) layerList.get(0)).getLayerCache().getName());
            if (JOptionPaneUtilities.showConfirmDialog(message) == 0) {
                LayerCacheDataMongo layerCacheDataMongo = (LayerCacheDataMongo) layerList.get(0);
                DeleteMapCacheInfo myCache = new DeleteMapCacheInfo(layerCacheDataMongo.getLayerCache(), layerCacheDataMongo.getTileStorageConnection(), (TileVersion) layerCacheDataMongo.getVersions().get(0));
                ((List) deleteCacheList).add(myCache);
            }
        } else {
            DialogDeleteMapCache dialogDeleteMapCache = new DialogDeleteMapCache(layerList);
            dialogDeleteMapCache.showDialog();
            deleteCacheList = dialogDeleteMapCache.getDeleteMyCacheList();
        }

        Iterator var9 = ((List) deleteCacheList).iterator();

        while (var9.hasNext()) {
            DeleteMapCacheInfo myCache = (DeleteMapCacheInfo) var9.next();
            TileStorageConnection tileStorageConnection = myCache.getTileStorageConnection();
            TileVersion tileVersion = myCache.getTileVersion();
            boolean result = MergeCacheUtilities.deleteMongodbCache(tileStorageConnection, tileVersion);
            if (result) {
                this.removeDatas(myCache.getLayerCache(), tileVersion);
                if (this.table.getRowCount() > 0) {
                    this.table.setRowSelectionInterval(0, 0);
                }

                Application.getActiveApplication().getOutput().output(MessageFormat.format(MapViewProperties.getString("String_DeleteMongoDBCacheSucceed"), tileStorageConnection.getName(), tileVersion.getDesc()));
            } else {
                Application.getActiveApplication().getOutput().output(MessageFormat.format(MapViewProperties.getString("String_DeleteMongoDBCacheFailed"), tileStorageConnection.getName(), tileVersion.getDesc()));
            }
        }

        this.checkoutButtonEnable();
    }

    public void removeDatas(LayerCache layerCache, TileVersion tileVersion) {
        for (int i = 0; i < this.version.getItemCount(); ++i) {
            TileVersion itemAt = (TileVersion) this.version.getItemAt(i);
            if (itemAt.getName().equals(tileVersion.getName())) {
                this.version.removeItem(itemAt);
            }
        }

        this.deleteVersion(layerCache, tileVersion);
        this.deleteLayer(layerCache);
        this.localTableModel.refreshLayerList();
    }

    private void deleteLayer(LayerCache layerCache) {
        if (this.version.getItemCount() == 0) {
            for (int i = this.localTableModel.getLayerList().size() - 1; i >= 0; --i) {
                if (((LayerCacheData) this.localTableModel.getLayerList().get(i)).getLayerCache() == layerCache) {
                    this.localTableModel.getLayerList().remove(i);
                    MapUtilities.removeLayer(this.mapControl.getMap(), layerCache.getName());
                }
            }
        }

    }

    private void deleteVersion(LayerCache layerCache, TileVersion tileVersion) {
        Iterator var3 = this.localTableModel.getLayerList().iterator();

        while (var3.hasNext()) {
            LayerCacheData layerCacheData = (LayerCacheData) var3.next();
            if (layerCacheData.getLayerCache() == layerCache) {
                List versions = ((LayerCacheDataMongo) layerCacheData).getVersions();
                versions.remove(tileVersion);
            }
        }

    }

    protected void addData() {
        this.table.getSelectionModel().removeListSelectionListener(this.listSelectionListener);
        this.table.getSelectionModel().addListSelectionListener(this.listSelectionListener);
        SmDialogMongoDBLoader dialogMongoDBLoader = new SmDialogMongoDBLoader();
        dialogMongoDBLoader.setSVersionComponentVisible();
        this.addCache = true;
        dialogMongoDBLoader.setScalesEnabled(false);
        if (dialogMongoDBLoader.showDialog() == DialogResult.OK) {
            this.addLayerCacheData(dialogMongoDBLoader.getLayerCache(), dialogMongoDBLoader.getTileStorageConnection());
            this.updatePreViewResult();
        }
        this.addCache = false;
        this.checkoutButtonEnable();
    }

    public void addData(String path){
        this.addMapCacheToForMap(path);
        this.updatePreViewResult();
    }


    public void addData(TileStorageConnection tileStorageConnection){
        if(CommonUtils.isFileTypeSource(tileStorageConnection)){
            addData(tileStorageConnection.getServer());
        }else{
            IFormMap formMap = (IFormMap)Application.getActiveApplication().getActiveForm();
            Map map = formMap.getMapControl().getMap();
            String server =tileStorageConnection.getServer();
            String databaseName = tileStorageConnection.getDatabase();
            String userName = tileStorageConnection.getUser();
            String password = tileStorageConnection.getPassword();
            LayerCache layerCache = null;
            if (StringUtilities.isNullOrEmpty(userName)) {
                layerCache = map.getLayers().AddCache(server, databaseName, tileStorageConnection.getName(), true);
            } else {
                layerCache = map.getLayers().AddCache(server, databaseName, tileStorageConnection.getName(), true, userName, password);
            }

            this.addLayerCacheData(layerCache, tileStorageConnection);
            this.updatePreViewResult();
        }
    }



    private void addLayerCacheData(LayerCache layerCache, TileStorageConnection tileStorageConnection) {
        layerCache.setVisible(!this.comboBoxPreViewType.getSelectedItem().equals(PREVIEW_TYPE_BOUNDS));
        TileStorageManager tileStorageManager = new TileStorageManager();
        tileStorageManager.open(tileStorageConnection);
        LayerCacheData layerCacheData = CommonUtils.createLayerCacheData(layerCache);

//        IFormMap formMap = (IFormMap)Application.getActiveApplication().getActiveForm();
//        Map map = formMap.getMapControl().getMap();
//        MapUtilities.setDynamic(layerCacheData.getPrjCoordSys(), map);

        this.localTableModel.addData(layerCacheData);
    }

    private void addLocalData() {
        this.table.getSelectionModel().removeListSelectionListener(this.listSelectionListener);
        this.table.getSelectionModel().addListSelectionListener(this.listSelectionListener);

        try {
            SmFileChoose fileChoose = (new SmFileChoose("AddMapLocalCache", FileChooseMode.OPEN_MANY)).setTitle(ControlsProperties.getString("String_OpenFile")).addFileFilter(MapViewProperties.getString("String_MapCache_CacheConfigFile"), new String[]{"sci"});
            if (fileChoose.showDefaultDialog() == 0) {
                if (!this.existActiveFormMap()) {
                    FormUtilities.fireNewWindowEvent(WindowType.MAP);
                }

                CursorUtilities.setWaitCursor(this);
                File[] selectFiles = fileChoose.getSelectFiles();
                File[] var3 = selectFiles;
                int var4 = selectFiles.length;

                for (int var5 = 0; var5 < var4; ++var5) {
                    File file = var3[var5];
                    super.addMapCacheToForMap(file.getPath());
                }

                this.updatePreViewResult();
            }

            this.checkoutButtonEnable();
        } catch (Exception var10) {
            Application.getActiveApplication().getOutput().output(var10);
        } finally {
            CursorUtilities.setDefaultCursor(this);
        }

    }

    private void merge() {
        this.table.getSelectionModel().removeListSelectionListener(this.listSelectionListener);
        this.table.getSelectionModel().addListSelectionListener(this.listSelectionListener);
        List layerList = this.localTableModel.getLayerList();
        DialogMergeMapCache dialogMergeMapCache = new DialogMergeMapCache(layerList);
        if (dialogMergeMapCache.showDialog() == DialogResult.OK) {
            List selectedData = dialogMergeMapCache.getSelectedData();
            TileStorageConnection tileStorageConnection = dialogMergeMapCache.getCurrentConnection();
            SmDialogProgressTotal smDialogProgressTotal = new SmDialogProgressTotal();
            smDialogProgressTotal.setTitle(MapViewProperties.getString("String_MergeTiles"));
            smDialogProgressTotal.doWork(new MergeCacheProgressCallable(selectedData, tileStorageConnection, dialogMergeMapCache.isMultiProcess(), dialogMergeMapCache.getProcessCount()));
            TileStorageConnection currentConnection = dialogMergeMapCache.getCurrentConnection();
            if (currentConnection != null) {
                LayerCache layerCache = this.mapControl.getMap().getLayers().AddCache(currentConnection.getServer(), currentConnection.getDatabase(), currentConnection.getName(), true);
                this.addLayerCacheData(layerCache, currentConnection);
            }
        }

        this.checkoutButtonEnable();
    }

    private void allocate() {
        this.table.getSelectionModel().removeListSelectionListener(this.listSelectionListener);
        this.table.getSelectionModel().addListSelectionListener(this.listSelectionListener);
        List layerList = this.localTableModel.getLayerList();
        Iterator var2 = layerList.iterator();
        if (var2.hasNext()) {
            LayerCacheData myLayer = (LayerCacheData) var2.next();
            DialogMapCacheAllocation dialogMapCacheAllocation = new DialogMapCacheAllocation(layerList, myLayer);
            dialogMapCacheAllocation.showDialog();
        }

    }



    protected LayerCacheData getLayerCacheData(LayerCache layerCache) {
        LayerCacheData layerCacheData = CommonUtils.createLayerCacheData(layerCache);
        return layerCacheData instanceof LayerCacheDataMongo ? layerCacheData : null;
    }
}