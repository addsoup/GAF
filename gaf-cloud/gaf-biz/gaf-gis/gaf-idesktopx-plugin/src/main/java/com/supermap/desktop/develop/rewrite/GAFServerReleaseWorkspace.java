package com.supermap.desktop.develop.rewrite;

import com.supermap.data.WorkspaceType;
import com.supermap.desktop.core.iserver.ServerReleaseWorkspace;
import com.supermap.desktop.core.tools.iserverInfo.IServerLoginInfo;
import org.nd4j.common.io.StringUtils;

public class GAFServerReleaseWorkspace extends ServerReleaseWorkspace {
    public GAFServerReleaseWorkspace(IServerLoginInfo loginInfo) {
        super(loginInfo);
    }

    @Override
    protected String getWorkspaceConnectionString() {
        String re =  super.getWorkspaceConnectionString();
        if(this.connectionInfo.getType() == WorkspaceType.PGSQL && StringUtils.isEmpty(re)){
            StringBuilder workspaceConnection = new StringBuilder();
            workspaceConnection.append("server=" + this.connectionInfo.getServer());
            workspaceConnection.append(";");
            workspaceConnection.append("username=" + this.connectionInfo.getUser());
            workspaceConnection.append(";");
            workspaceConnection.append("password=" + this.connectionInfo.getPassword());
            workspaceConnection.append(";");
            workspaceConnection.append("type=" + WorkspaceType.PGSQL);
            workspaceConnection.append(";");
            workspaceConnection.append("database=" + this.connectionInfo.getDatabase());
            workspaceConnection.append(";");
            workspaceConnection.append("name=" + this.connectionInfo.getName());
            workspaceConnection.append(";");
            workspaceConnection.append("driver=pgSQL Server");
            re = workspaceConnection.toString();
        }
        return re;
    }
}
