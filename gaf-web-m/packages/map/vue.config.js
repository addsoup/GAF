const path = require('path')
const rimraf = require('rimraf')
const isDev = process.env.NODE_ENV !== 'production'
const { proxy } = require('../../project.config.json')

module.exports = {
  lintOnSave: false,
  publicPath: isDev ? '/' : '/apps-map/',
  assetsDir: 'static',
  devServer: {
    port: 10203,
    disableHostCheck: true,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    proxy
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true
        }
      }
    }
  },
  productionSourceMap: process.env.NODE_ENV === 'development',
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src')
      }
    },
    output: {
      // 微应用的包名，这里与主应用中注册的微应用名称一致
      library: 'apps-map',
      // 将你的 library 暴露为所有的模块定义下都可运行的方式
      libraryTarget: 'umd',
      // 按需加载相关，设置为 webpackJsonp_pkg-name 即可
      jsonpFunction: 'webpackJsonp_map'
    },
    plugins: [
      {
        apply: (compiler) => {
          compiler.hooks.done.tap('BuildStatsPlugin', (stats) => {
            rimraf('dist/Build', ()=> {})
            // resolveReplace()
          })
        }
      }
    ]
  }
}

function resolveReplace() {
  try {
    const file = fs.readFileSync('dist/index.html', 'utf-8')
    const content = file
      .replace(
        '<link ignore rel="stylesheet" href="/Build/Cesium/Widgets/widgets.css">',
        ''
      )  .replace(
        '<link ignore rel="stylesheet" href="/css/font_577982_beimm7aom3g.css">',
        ''
      ).replace('<link rel="stylesheet" href="/Build/iframe/css/geoFont/iconfont.css" />')
      .replace('<script ignore src="/Build/Cesium/Cesium.js"></script>', '')
      .replace('<script ignore src="/Build/webgl/js/Convert.js"></script>', '')
      .replace('<script ignore src="/Build/webgl/js/tooltip.js"></script>', '')
      .replace(
        '<script ignore src="/Build/Supermap/classic/SuperMap.Include.js"></script>',
        ''
      )
    fs.writeFileSync('dist/index.html', content, 'utf-8')
  } catch {}
}
