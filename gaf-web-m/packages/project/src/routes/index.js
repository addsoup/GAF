import Home from '../pages/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Login.vue')
  },
  {
    path: '/table',
    name: 'Table',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Table.vue')
  },
  // //工程组管理
  // {
  //   path: '/projGroup',
  //   name: 'ProjGroup',
  //   component: () => import(/* webpackChunkName: "about" */ '../pages/projGroup/index.vue')
  // },
  // //工程管理
  // {
  //   path: '/projects',
  //   name: 'Projects',
  //   component: () => import(/* webpackChunkName: "about" */ '../pages/projects/index.vue')
  // },
  //代码生成
  {
    path: '/CodeGenerate',
    name: 'CodeGenerate',
    component: () => import(/* webpackChunkName: "about" */ '../pages/CodeGenerate/index.vue')
  },
  //模板管理
  {
    path: '/codeTemplate',
    name: 'CodeTemplate',
    component: () => import(/* webpackChunkName: "about" */ '../pages/codeTemplate/index.vue')
  }
]

export default routes
