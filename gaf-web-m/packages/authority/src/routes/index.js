import Home from '../pages/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Login.vue')
  },
  {
    path: '/table',
    name: 'Table',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Table.vue')
  },
  {
    path: '/AuthRole',
    name: 'AuthRole',
    component: () => import(/* webpackChunkName: "about" */ '../pages/AuthRole')
  },
  {
    path: '/AuthUserInfo',
    name: 'AuthUserInfo',
    component: () => import(/* webpackChunkName: "about" */ '../pages/AuthUserInfo')
  },
  {
    path: '/ComponentApi',
    name: 'ComponentApi',
    component: () => import(/* webpackChunkName: "about" */ '../pages/ComponentApi')
  },
  {
    path: '/DepartmentPostManage',
    name: 'DepartmentPostManage',
    component: () => import(/* webpackChunkName: "about" */ '../pages/DepartmentPostManage')
  },
  {
    path: '/dics',
    name: 'dics',
    component: () => import(/* webpackChunkName: "about" */ '../pages/dics')
  },
  {
    path: '/Menu',
    name: 'Menu',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Menu')
  },
  {
    path: '/RoleApi',
    name: 'RoleApi',
    component: () => import(/* webpackChunkName: "about" */ '../pages/RoleApi')
  },
  {
    path: '/RoleMenu',
    name: 'RoleMenu',
    component: () => import(/* webpackChunkName: "about" */ '../pages/RoleMenu')
  },
  {
    path: '/SysCatalog',
    name: 'SysCatalog',
    component: () => import(/* webpackChunkName: "about" */ '../pages/SysCatalog')
  },
  {
    path: '/user',
    name: 'user',
    component: () => import(/* webpackChunkName: "about" */ '../pages/User')
  }

]

export default routes
