const fs = require('fs')
const { proxy } = require('../../project.config.json')
module.exports = {
  lintOnSave: false,
  publicPath: '/',
  assetsDir: 'static',
  devServer: {
    port: 10200,
    proxy
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true
        }
      }
    }
  },
  productionSourceMap: process.env.NODE_ENV === 'development',
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      config.plugins.push({
        apply: (compiler) => {
          compiler.hooks.done.tap('BuildStatsPlugin', stats => {
            resolveReplace()
          })
        }
      })
    }
  }
}


function resolveReplace () {
  const file = fs.readFileSync('dist/index.html', 'utf-8')
  const scripts = '</title><link rel="stylesheet" href="/Build/Cesium/Widgets/widgets.css"><link rel="stylesheet" href="/Build/iframe/css/geoFont/iconfont.css" /><link rel="stylesheet" href="/css/font_577982_beimm7aom3g.css"><script src="/Build/Cesium/Cesium.js"></script><script src="/Build/webgl/js/Convert.js"></script><script src="/Build/webgl/js/tooltip.js"></script><script src="/Build/Supermap/classic/SuperMap.Include.js"></script>'
  const content = file.replace('</title>', scripts)
  fs.writeFileSync('dist/index.html', content, 'utf-8')
}
