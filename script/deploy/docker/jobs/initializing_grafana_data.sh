[ ! $Root_Current_Dir ] && exit 1


#使用api导入grafana的基础数据
docker run --rm --net=gaf-net -v $Root_Current_Dir/data/grafana:/opt/grafana -v $Root_Current_Dir/jobs/grafana-import.sh:/opt/grafana/grafana-import.sh registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 bash -c "cd /opt/grafana && chmod +x grafana-import.sh && bash /opt/grafana/grafana-import.sh"