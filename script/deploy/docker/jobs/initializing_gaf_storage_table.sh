docker run --rm --net=gaf-net -v $GAF_BASE_DATA_PATH:/opt/liquibase-data registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 \
    liquibase \
      --driver=$GAF_ENV_DATASOURCE_DRIVER \
      --classpath=/usr/local/liquibase/liquibase-classpath/postgresql-42.2.23.jar \
      --url=$GAF_ENV_STORAGE_DATASOURCE_URL \
      --username=$GAF_ENV_DATASOURCE_USERNAME \
      --password=$GAF_ENV_DATASOURCE_PASSWORD \
      --changeLogFile=liquibase-data/new/others/storage/all.xml \
      update